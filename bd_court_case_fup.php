<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 18th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_court_case_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query string data
	if(isset($_GET["case"]))
	{
		$case_id = $_GET["case"];
	}
	else
	{
		$case_id = "";
	}	
	
	// Capture the form data
	if(isset($_POST["add_court_case_fup_submit"]))
	{
		$case_id  = $_POST["hd_case_id"];
		$fup_date = $_POST["dt_fup_date"];
		$status   = $_POST["case_status"];
		$remarks  = $_POST["txt_remarks"];		
		
		// Check for mandatory fields
		if(($case_id !="") && ($fup_date !="") && ($remarks !=""))
		{
			$case_fup_iresult = i_add_legal_court_case_fup($case_id,$fup_date,$status,$remarks,$user);
			
			if($case_fup_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert	    = 'Follow Up Successfully Added';
				
				header('location:bd_court_case_list.php');
			}
			else
			{
				$alert_type = 0;
				$alert      = $case_fup_iresult['data'];
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}		
	
	// Follow Up Details for this court case
	$court_case_fup_data = array('case_id'=>$case_id);
	$case_fup_list = i_get_court_case_fup_details($court_case_fup_data);
	if($case_fup_list["status"] == SUCCESS)
	{
		$case_fup_list_data = $case_fup_list["data"];
	}
	
	// Get the court case details as captured already
	$case_details = array('legal_court_case_id'=>$case_id);
	$court_case_details = i_get_legal_court_case($case_details);
	if($court_case_details["status"] == SUCCESS)
	{
		$court_case_details_data = $court_case_details["data"];
		if($case_fup_list["status"] == SUCCESS)
		{
			$current_status = $case_fup_list_data[0]['legal_court_case_follow_up_status'];
			$next_fup_date = $case_fup_list_data[0]['legal_court_case_follow_up_date'];
		}
		else
		{
			$current_status = $court_case_details_data[0]['legal_court_case_status'];
			$next_fup_date = '';
		}	
	}
	else
	{
		$alert = $court_case_details["data"];
		$alert_type = 0;
		$current_status = '';
		$next_fup_date = '';
	}
	
	// Get list of Status
	$status_list = i_get_court_status_list('','');
	if($status_list["status"] == SUCCESS)
	{
		$status_list_data = $status_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$status_list["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Follow Up for Court Case</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header" style="height:80px;">
	      				<i class="icon-user"></i>
	      				<h3>Case Type: <?php echo $court_case_details_data[0]["bd_court_case_type_master_type"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Case No: <?php echo $court_case_details_data[0]["legal_court_case_number"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Survey No: <?php echo $court_case_details_data[0]["legal_court_case_survey_no"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Village: <?php echo $court_case_details_data[0]["village_name"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Filing Date: <?php echo date('d-M-Y',strtotime($court_case_details_data[0]["legal_court_case_date"])); ?>&nbsp;&nbsp;&nbsp;&nbsp;Jurisdiction: <?php echo $court_case_details_data[0]["bd_court_establishment_name"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Case Status: <?php echo $court_case_details_data[0]["bd_court_status_name"]; ?></h3>					
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    Add Follow Up for Court Case
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>								
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_court_case_fup" class="form-horizontal" method="post" action="bd_court_case_fup.php">
								<input type="hidden" name="hd_case_id" value="<?php echo $case_id; ?>" />								
									<fieldset>			
										<div class="control-group">											
											<label class="control-label" for="ddl_reason">Follow Up Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_fup_date" required />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="case_status">Status *</label>
												<div class="controls">
											    <select name="case_status" required>
												<option value="">- - Select Current Case Status - -</option>
												<?php
												for($count = 0; $count < count($status_list_data); $count++)
												{
												?>
												<option value="<?php echo $status_list_data[$count]["bd_court_status_master_id"]; ?>" <?php if($current_status == $status_list_data[$count]["bd_court_status_master_id"]){ ?> selected <?php } ?>><?php echo $status_list_data[$count]["bd_court_status_name"]; ?></option>					
												<?php
												}
												?>
												</select>												
											</div> <!-- /controls -->									
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks*</label>
											<div class="controls">
												<textarea name="txt_remarks" class="span6" required></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_court_case_fup_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								<table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>
									<th>Follow Up Date</th>					
									<th>Remarks</th>	
									<th>Status</th>	
									<th>Last Updated By</th>
									<th>Last Updated On</th>
									<th>&nbsp;</th>
								</tr>
								</thead>
								<tbody>							
								<?php
								if($case_fup_list["status"] == SUCCESS)
								{									
									for($count = 0; $count < count($case_fup_list_data); $count++)
									{																	
									?>
									<tr>
									<td style="word-wrap:break-word;"><?php echo date('d-M-Y',strtotime($case_fup_list_data[$count]["legal_court_case_follow_up_date"])); ?></td>
									<td style="word-wrap:break-word;"><?php echo $case_fup_list_data[$count]["legal_court_case_follow_up_remarks"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $case_fup_list_data[$count]["bd_court_status_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $case_fup_list_data[$count]["user_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i:s",strtotime($case_fup_list_data[$count]["legal_court_case_follow_up_updated_on"])); ?></td>
									<td style="word-wrap:break-word;"><a href="bd_court_case_fup_update.php?fup=<?php echo $case_fup_list_data[$count]['legal_court_case_follow_up_id']; ?>">Update</a></td>
									</tr>
									<?php									
									}
								}
								else
								{
								?>
								<td colspan="5">No follow ups planned yet!</td>
								<?php
								}	
								?>	
								</tbody>
							  </table>
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
