<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 5th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query String
	if(isset($_GET["profile"]))
	{
		$profile_id = $_GET["profile"];
	}
	else
	{
		$profile_id = "";
	}
	
	$prospective_client_list = i_get_prospective_clients($profile_id,'','','','');
	if($prospective_client_list["status"] == SUCCESS)
	{
		$prospective_client_list_data = $prospective_client_list["data"];
	}
	else
	{
		$alert_type = 0;
		$alert = "Invalid Profile ID!";
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Prospective Client Profile</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Prospective Client Profile&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enquiry ID: <?php echo $prospective_client_list_data[0]["enquiry_number"]; ?></a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_enquiry" class="form-horizontal" method="post" action="crm_add_prospective_profile.php">
								<input type="hidden" name="hd_enquiry" value="<?php echo $enquiry_id; ?>" />
									<fieldset>										
												
										<div class="control-group">											
											<label class="control-label" for="rd_occupation">Occupation*</label>
											<div class="controls">
											<?php 
											switch($prospective_client_list_data[0]["crm_prospective_profile_occupation"])
											{
											case "1":
											echo "Self-Employed";
											break;
											
											case "2":
											echo "Salaried";
											break;
											
											default:
											echo "Salaried";
											break;
											}
											?>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_num_dependents">No. of Dependants</label>
											<div class="controls">
												<?php echo $prospective_client_list_data[0]["crm_prospective_profile_num_dependents"]; ?>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->																				
                                                                                                                                                           										    <div class="control-group">											
											<label class="control-label" for="stxt_annual_income">Annual Income*</label>
											<div class="controls">
												<?php echo $prospective_client_list_data[0]["crm_prospective_profile_annual_income"]; ?>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="stxt_other_income">Other sources of Income</label>
											<div class="controls">
												<?php echo $prospective_client_list_data[0]["crm_prospective_profile_other_income"]; ?>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="cb_array_loan_type">Existing Loan</label>
											<div class="controls">
											<?php
											$loans = explode(',',$prospective_client_list_data[0]["crm_prospective_profile_loan_type"]);
											$loan_string = "";
											for($count = 0; $count < count($loans); $count++)
											{
												switch($loans[$count])
												{
												case "1";
												$loan_type = "Home Loan";
												break;
												
												case "2";
												$loan_type = "Car Loan";
												break;
												
												case "3";
												$loan_type = "Personal Loan";
												break;
												
												case "4";
												$loan_type = "Other Loan";
												break;
												
												default;
												$loan_type = "";
												break;
												}
												$loan_string = $loan_string.$loan_type." , ";
											}
											echo trim($loan_string,', ');
											?>												
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_emi">EMI</label>
											<div class="controls">
												<?php echo $prospective_client_list_data[0]["crm_prospective_profile_total_emi"]; ?>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="cb_loan_reject">Loan Rejected</label>
											<div class="controls">
												<?php
												if($prospective_client_list_data[0]["crm_prospective_profile_loan_reject"] == 1)
												{
													echo "Yes";
												}
												else
												{
													echo "No";
												}?>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="rd_current_living">Currently Staying</label>
											<div class="controls">
											<?php 
											switch($prospective_client_list_data[0]["crm_prospective_profile_occupation"])
											{
											case "1":
											echo "Rent";
											break;
											
											case "2":
											echo "Own House";
											break;
											
											case "3":
											echo "On Lease";
											break;
											
											default:
											echo "Rent";
											break;
											}
											?>											
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_rent">Rent*</label>
											<div class="controls">
												<?php echo $prospective_client_list_data[0]["crm_prospective_profile_total_emi"]; ?>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="cb_buy_interest">Buying Interest</label>
											<div class="controls">
											<?php
											$buy_interest = explode(',',$prospective_client_list_data[0]["crm_prospective_profile_buying_interest"]);
											$buy_interest_string = "";
											for($count = 0; $count < count($buy_interest); $count++)
											{
												switch($buy_interest[$count])
												{
												case "1";
												$buy_interest_type = "20x30";
												break;
												
												case "2";
												$buy_interest_type = "30x40";
												break;
												
												case "3";
												$buy_interest_type = "40x60";
												break;
												
												case "4";
												$buy_interest_type = "Other";
												break;
												
												case "5";
												$buy_interest_type = "30x50";
												break;
												
												default;
												$buy_interest_type = "";
												break;
												}
												$buy_interest_string = $buy_interest_string.$buy_interest_type." , ";
											}
											echo trim($buy_interest_string,', ');
											?>												
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->										
										
										<div class="control-group">											
											<label class="control-label" for="rd_fund_source">Funding Source*</label>
											<div class="controls">
												<?php 
												switch($prospective_client_list_data[0]["crm_prospective_profile_funding_source"])
												{
												case "1":
												echo "Loan";
												break;
												
												case "2":
												echo "Self Funding";
												break;
												
												default:
												echo "Loan";
												break;
												}
												?>													
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_loan_eligibility">Loan Eligibility</label>
											<div class="controls">
												<?php echo $prospective_client_list_data[0]["crm_prospective_profile_loan_eligibility"]; ?>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_loan_tenure">Loan Tenure</label>
											<div class="controls">
												<?php echo $prospective_client_list_data[0]["crm_prospective_profile_loan_tenure"]; ?>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="rd_buy_duration">Buying Duration</label>
											<div class="controls">
												<?php 
												switch($prospective_client_list_data[0]["crm_prospective_profile_buy_duration"])
												{
												case "1":
												echo "Immediately";
												break;
												
												case "2":
												echo "Next Month";
												break;
												
												case "3":
												echo "Later";
												break;
												
												default:
												echo "Immediately";
												break;
												}
												?>												
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<?php echo $prospective_client_list_data[0]["crm_prospective_profile_remarks"]; ?>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->			
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
