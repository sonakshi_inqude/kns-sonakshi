<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: stock_storage_list.php
CREATED ON	: 28-Sep-2016
CREATED BY	: Lakshmi
PURPOSE     : List of indent for customer withdrawals
*/

/*
TBD: 
*/
$_SESSION['module'] = 'Stock Masters';

/* DEFINES - START */
define('STORAGE_MASTER_FUNC_ID','157');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	$alert_type = -1;
	$alert      = "";

	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',STORAGE_MASTER_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',STORAGE_MASTER_FUNC_ID,'3','1');
	$delete_perms_list = i_get_user_perms($user,'',STORAGE_MASTER_FUNC_ID,'4','1');
	$add_perms_list    = i_get_user_perms($user,'',STORAGE_MASTER_FUNC_ID,'1','1');
	
	// Query String Data
	// Nothing

	// Temp data
	$storage_search_data = array("active"=>'1');
	$storage_list = i_get_storage_condition_list($storage_search_data);
	if($storage_list["status"] == SUCCESS)
	{
		$storage_list_data = $storage_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$storage_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Storage List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Storage List</h3><?php if($add_perms_list['status'] == SUCCESS){ ?><span class="pull-right" style="padding-right:10px;"><a href="stock_master_add_storage.php">Add Storage Master</a></span><?php } ?>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>SL No</th>
					<th>Storage Name</th>
					<th>Remarks</th>
					<th>Last Updated By</th>					
					<th>Last Updated On</th>									
					<th>&nbsp;</th>										
					<th>&nbsp;</th>																								
				</tr>
				</thead>
				<tbody>							
				<?php
				if($storage_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($storage_list_data); $count++)
					{
						$sl_no++;
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $storage_list_data[$count]["stock_storage_name"]; ?></td>
					<td><?php echo $storage_list_data[$count]["stock_storage_remarks"]; ?></td>
					<td><?php echo $storage_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($storage_list_data[$count][
					"stock_storage_added_on"])); ?></td>
					<td><?php if($edit_perms_list['status'] == SUCCESS){ ?><a href="stock_master_edit_storage.php?storage_id=<?php echo $storage_list_data[$count]["stock_storage_id"]; ?>">Edit</a><?php } ?></td>	
					<td><?php if($delete_perms_list['status'] == SUCCESS){ ?><?php if(($storage_list_data[$count]["stock_storage_active"] == "1")){?><a href="#" onclick="return delete_storage(<?php echo $storage_list_data[$count]["stock_storage_id"]; ?>);">Delete</a><?php } ?><?php } ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="7">No Storage condition added yet!</td>
				<?php
				}
				?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function delete_storage(storage_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{				
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "stock_master_storage_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/stock_master_delete_storage.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("storage_id=" + storage_id + "&action=0");
		}
	}	
}

</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>


  </body>

</html>