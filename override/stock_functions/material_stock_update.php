<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: crm_indent_list.php

CREATED ON	: 28-Sep-2016

CREATED BY	: Lakshmi

PURPOSE     : List of indent for customer withdrawals

*/



/*

TBD: 

*/
$_SESSION['module'] = 'Stock Transactions';


/* DEFINES - START */

define('ISSUE_FUNC_ID','172');

/* DEFINES - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

/*include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_masters_functions.php');*/

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_indent_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');

$project = '31';

//Get Material List

//Get Material List
$stock_material_search_data = array();
$material_master_list = i_get_stock_material_master_list($stock_material_search_data);
if($material_master_list["status"] == SUCCESS)
{
	$material_master_list_data  = $material_master_list["data"];
	for($count = 0; $count < count($material_master_list_data); $count++)
	{					
		// Get Stock History for Opening
		$stock_history_search_data = array("transaction_type"=>'Opening',"material_id"=>$material_master_list_data[$count]["stock_material_id"],"project"=>$project);
		$stock_history_list = i_get_sum_stock_history($stock_history_search_data);

		if($stock_history_list['status'] == SUCCESS)
		{
			$stock_history_list_data = $stock_history_list['data'];
			$opening_qty = $stock_history_list_data[0]["total_stock_history_quantity"];
		}	
		else
		{
			$opening_qty  = "0";
		}

		// Get Stock history for Issue
		$stock_history_search_data = array("transaction_type"=>'Issue',"material_id"=>$material_master_list_data[$count]["stock_material_id"],"project"=>$project);
		$stock_history_list = i_get_sum_stock_history($stock_history_search_data);
		if($stock_history_list['status'] == SUCCESS)
		{
			$stock_history_list_data = $stock_history_list['data'];

			$issue_qty = $stock_history_list_data[0]["total_stock_history_quantity"];
		}	
		else
		{
			$issue_qty  = "0";
		}

		$stock_history_search_data = array("transaction_type"=>'Transfer Out',"material_id"=>$material_master_list_data[$count]["stock_material_id"],"project"=>$project);
		$stock_history_list = i_get_sum_stock_history($stock_history_search_data);

		if($stock_history_list['status'] == SUCCESS)
		{
			$stock_history_list_data = $stock_history_list['data'];
			$issue_qty = $issue_qty + $stock_history_list_data[0]["total_stock_history_quantity"];
		}	
		else
		{
			$issue_qty  = $issue_qty + 0;;
		}

		// Get Stock history for Purchase
		$stock_history_search_data = array("transaction_type"=>'Purchase',"material_id"=>$material_master_list_data[$count]["stock_material_id"],"project"=>$project);
		$stock_history_list = i_get_sum_stock_history($stock_history_search_data);

		if($stock_history_list['status'] == SUCCESS)
		{
			$stock_history_list_data = $stock_history_list['data'];
			$purchase_qty = $stock_history_list_data[0]["total_stock_history_quantity"];
		}	
		else
		{
			$purchase_qty  = "0";
		}

		$stock_history_search_data = array("transaction_type"=>'Transfer In',"material_id"=>$material_master_list_data[$count]["stock_material_id"],"project"=>$project);
		$stock_history_list = i_get_sum_stock_history($stock_history_search_data);
		if($stock_history_list['status'] == SUCCESS)
		{
			$stock_history_list_data = $stock_history_list['data'];
			$purchase_qty = $purchase_qty + $stock_history_list_data[0]["total_stock_history_quantity"];
		}	
		else
		{
			$purchase_qty  = $purchase_qty + 0;
		}
		
		$closing_stock = ($opening_qty + $purchase_qty - $issue_qty);
		
		$material_stock_iresults = i_add_material_stock($material_master_list_data[$count]["stock_material_id"],$closing_stock,$material_master_list_data[$count]["stock_material_unit_of_measure"],'',$project,'0','0','0','0','','143620071466608200');
		if($material_stock_iresults["status"] == SUCCESS)
		{
			echo "Success";
		}
	}
}
































?>
