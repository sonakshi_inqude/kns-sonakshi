<?php
/**
 * @author Nitin kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_crm_marketing_sales_planning.php');

/*
PURPOSE : To add new sales target
INPUT 	: Month, Year, Target Count, Target Area, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_sales_target($month,$year,$target_sales_count,$target_sales_area,$added_by)
{	
	$sales_target_iresult = db_add_sales_target($month,$year,$target_sales_count,$target_sales_area,$added_by);
	
	if($sales_target_iresult["status"] == SUCCESS)
	{
		$return["status"] = SUCCESS;
		$return["data"]   = "Sales Target Successfully Added";
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"]   = "Internal Error. Please try again later!";
	}
		
	return $return;
}

/*
PURPOSE : To get sales target
INPUT 	: Sales Target Data Array
OUTPUT 	: Sales Target List; Success or Failure Message
BY 		: Nitin Kashyap
*/
function i_get_sales_target($sales_target_data)
{
	$sales_target_sresult = db_get_sales_targets($sales_target_data);
	
	if($sales_target_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $sales_target_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No sales target added yet!"; 
    }
	
	return $return;
}
?>