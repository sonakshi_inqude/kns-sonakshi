<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 18th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_court_case_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query string data
	if(isset($_GET["fup"]))
	{
		$fup_id = $_GET["fup"];
	}
	else
	{
		$fup_id = "-1";
	}	
	
	// Capture the form data
	if(isset($_POST["edit_court_case_fup_submit"]))
	{
		$fup_id   = $_POST["hd_fup_id"];
		$case_id  = $_POST["hd_case_id"];
		$fup_date = date('Y-m-d');
		$remarks  = $_POST["txt_remarks"];		
		
		// Check for mandatory fields
		if(($fup_id != "") && ($fup_date !="") && ($remarks !=""))
		{
			$case_fup_data = array('date'=>$fup_date,'remarks'=>$remarks,'updated_by'=>$user);
			$case_fup_uresult = i_update_case_fup($fup_id,$case_fup_data);
			
			if($case_fup_uresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert	    = 'Follow Up Successfully Updated';
				
				header("location:bd_court_case_fup.php?case=".$case_id);
			}
			else
			{
				$alert_type = 0;
				$alert      = $case_fup_uresult['data'];
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}		
	
	// Follow Up Details for this court case
	$court_case_fup_data = array('fup_id'=>$fup_id);
	$case_fup_list = i_get_court_case_fup_details($court_case_fup_data);
	if($case_fup_list["status"] == SUCCESS)
	{
		$case_fup_list_data = $case_fup_list["data"];
	}
	
	// Get the court case details as captured already
	$case_details = array('legal_court_case_id'=>$case_fup_list_data[0]['legal_court_case_id']);
	$court_case_details = i_get_legal_court_case($case_details);
	if($court_case_details["status"] == SUCCESS)
	{
		$court_case_details_data = $court_case_details["data"];
	}
	else
	{
		$alert = $court_case_details["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Follow Up for Court Case</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header" style="height:70px;">
	      				<i class="icon-user"></i>
	      				<h3>Case File ID: <?php echo $court_case_details_data[0]["legal_court_case_file_id"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Case Type: <?php echo $court_case_details_data[0]["bd_court_case_type_master_type"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Village: <?php echo $court_case_details_data[0]["village_name"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Extent: <?php echo $court_case_details_data[0]["bd_file_extent"].' guntas'; ?></h3>					
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    Edit Follow Up for Court Case
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>								
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="edit_court_case_fup" class="form-horizontal" method="post" action="bd_court_case_fup_update.php">
								<input type="hidden" name="hd_fup_id" value="<?php echo $fup_id; ?>" />	
								<input type="hidden" name="hd_case_id" value="<?php echo $case_fup_list_data[0]['legal_court_case_id']; ?>" />	
									<fieldset>													
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks*</label>
											<div class="controls">
												<textarea name="txt_remarks" class="span6" required><?php echo $case_fup_list_data[0]['legal_court_case_follow_up_remarks']; ?></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_court_case_fup_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
