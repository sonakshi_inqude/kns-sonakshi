<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: user_list.php
CREATED ON	: 05-July-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Users
*/

/*
TBD: 
*/$_SESSION['module'] = 'HR';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_employee_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_attendance_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Data initialization
	$alert_type = -1;
	$alert      = "";
	
	$self_employee_filter_data = array("employee_user"=>$user,"user_status"=>'1');
	$self_employee_details     = i_get_employee_list($self_employee_filter_data);
	$search_employee           = $self_employee_details["data"][0]["hr_employee_id"];
	if(isset($_POST["out_pass_search_submit"]))
	{
		$date            = $_POST["dt_out_pass_date"];		
		$status          = $_POST["ddl_status"];
		$type            = $_POST["ddl_type"];
	}
	else
	{
		$date            = "";		
		$status          = "0";
		$type            = "";
	}

	// Get list of leaves applied by self
	$out_pass_filter_data["employee_id"] = $search_employee;
	if($date != "")
	{
		$out_pass_filter_data["date"] = $date;
	}
	if($status != "")
	{
		$out_pass_filter_data["status"] = $status;
	}
	if($type != "")
	{
		$out_pass_filter_data["type"] = $type;
	}
	$out_pass_list = i_get_out_pass_list($out_pass_filter_data);
	
	if($out_pass_list["status"] == SUCCESS)
	{
		$out_pass_list_data = $out_pass_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$out_pass_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Out Pass Requests By Self</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Out Pass Requests By Self</h3><span style="flost:right;" id="span_msg"></span>
            </div>
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="out_pass_search_form" action="hr_out_pass_self.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="date" name="dt_out_pass_date" value="<?php echo $date; ?>" />
			  </span>			  			  			  			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_status">			  
			  <option value="0" <?php if($status == "0"){?> selected <?php } ?>>Pending</option>
			  <option value="1" <?php if($status == "1"){?> selected <?php } ?>>Approved</option>
			  <option value="2" <?php if($status == "2"){?> selected <?php } ?>>Rejected</option>
			  <option value="3" <?php if($status == "3"){?> selected <?php } ?>>Cancelled</option>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_type">			  
			  <option value="">- - Select Out Pass Type - -</option>
			  <option value="0" <?php if($type == "0"){?> selected <?php } ?>>Personal</option>
			  <option value="1" <?php if($type == "1"){?> selected <?php } ?>>On Duty</option>			  
			  </select>
			  </span>
			  <input type="submit" name="out_pass_search_submit" />
			  </form>			  
            </div>            
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>				    				
					<th>Date</th>
					<th>Out Time</th>
					<th>Finish Time</th>
					<th>Total Time</th>
					<th>Emp Remarks</th>
					<th>Out Pass Type</th>
				    <th>Out Pass Status</th>
					<th>Out Pass Applied On</th>	
					<th>Out Pass Approver</th>
					<th>Manager Action On</th>	
					<th>&nbsp;</th>					
				</tr>
				</thead>
				<tbody>
				<?php
				if($out_pass_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($out_pass_list_data); $count++)
					{						
					?>
					<tr>											
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($out_pass_list_data[$count]["hr_out_pass_date"],"d-M-Y"); ?></td>						
						<td style="word-wrap:break-word;"><?php echo $out_pass_list_data[$count]["hr_out_pass_out_time"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $out_pass_list_data[$count]["hr_out_pass_finish_time"]; ?></td>
						<td style="word-wrap:break-word;"><?php $out_time_array = explode(':',$out_pass_list_data[$count]["hr_out_pass_out_time"]); 
						$finish_time_array = explode(':',$out_pass_list_data[$count]["hr_out_pass_finish_time"]); 
						$total_time = (($finish_time_array[0]*60) + $finish_time_array[1]) - (($out_time_array[0]*60) + $out_time_array[1]); 
						echo (str_pad(intval($total_time/60),2,"0",STR_PAD_LEFT)).':'.(str_pad(intval($total_time%60),2,"0",STR_PAD_LEFT));						 
						?></td>
						<td style="word-wrap:break-word;"><?php echo $out_pass_list_data[$count]["hr_out_pass_request_remarks"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo i_get_out_pass_type_name($out_pass_list_data[$count]["hr_out_pass_type"]); ?></td>
						<td style="word-wrap:break-word;"><?php echo i_get_out_pass_status_name($out_pass_list_data[$count]["hr_out_pass_approval_status"]); ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($out_pass_list_data[$count]["hr_out_pass_request_added_on"],"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;"><?php echo $out_pass_list_data[$count]["manager"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($out_pass_list_data[$count]["hr_out_pass_approved_on"],"d-M-Y"); ?></td>
						<td style="word-wrap:break-word;">
						<?php if($out_pass_list_data[$count]["hr_out_pass_approval_status"] == OUT_PASS_STATUS_PENDING)
						{?>
						<a href="#" onclick="return cancel_out_pass(<?php echo $out_pass_list_data[$count]["hr_out_pass_id"]; ?>);"><span style="color:black; text-decoration: underline;">Cancel</span></a>
						<?php
						}
						?></td>						
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<tr>
				<td colspan="11">No Out Pass applications!</td>
				</tr>
				<?php
				}
				?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function cancel_out_pass(out_pass_id)
{
	var ok = confirm("Are you sure you want to cancel?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{				
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{					
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "hr_out_pass_self.php";
					}
				}
			}

			xmlhttp.open("POST", "hr_update_out_pass.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("outpass=" + out_pass_id + "&action=3");
		}
	}	
}
</script><script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>
