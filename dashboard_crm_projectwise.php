<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 13th Jan 2017
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert      = -1;
	$alert_type = '';
	$today      = 0;
	$this_week  = 0;
	$this_month = 0;	
	/* DATA INITIALIZATION - END */
	
	// Get project list
	$project_sresult = i_get_project_list('','1');
	if($project_sresult['status'] == SUCCESS)
	{
		$project_list_data = $project_sresult['data'];
	}
	else
	{
		$alert_type = 0;
	}
	
	// Today start date and end date
	$today_start_date = date('Y-m-d');
	$today_end_date   = date('Y-m-d');
	
	// Week start date and end date
	$week_start_date = $today_start_date;
	$week_end_date   = $today_start_date;
	
	while(date("D",strtotime($week_start_date)) != "Mon")
	{
		$week_start_date = date("Y-m-d",strtotime($week_start_date." -1 days"));
	}
	while(date("D",strtotime($week_end_date)) != "Sun")
	{
		$week_end_date = date("Y-m-d",strtotime($week_end_date." +1 days"));
	}
	
	// Month start and end date
	$month_start_date = get_month_start_date($today_start_date,0);
	$no_of_days_in_month = cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($month_start_date)),date('Y',strtotime($month_start_date)));
	$month_end_date = date('Y',strtotime($month_start_date)).'-'.date('m',strtotime($month_start_date)).'-'.$no_of_days_in_month;	
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>CRM Projectwise - Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
	
    <div class="main">
        <div class="main-inner">
            <div class="container">
                <div class="row">
                    <div class="span6">                        
                    </div>
                    <!-- /span6 -->
                    <div class="span9">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-bar-chart"></i>
                                <h3>Collection Analysis</h3>
                            </div>
                            <!-- /widget-header -->							
                            <div class="widget-content">
                                <!-- Tables to go here -->
								<table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>
									<th rowspan="2">Project</th>
									<th colspan="3">Collection - Plan</th>
									<th colspan="3">Collection - Actual</th>
									<th colspan="3">Variance</th>									
								  </tr>
								  <tr>
								  <th style="word-wrap:break-word;">Daily</th>
								  <th style="word-wrap:break-word;">Weekly</th>
								  <th style="word-wrap:break-word;">Monthly</th>
								  <th style="word-wrap:break-word;">Daily</th>
								  <th style="word-wrap:break-word;">Weekly</th>
								  <th style="word-wrap:break-word;">Monthly</th>
								  <th style="word-wrap:break-word;">Daily</th>
								  <th style="word-wrap:break-word;">Weekly</th>
								  <th style="word-wrap:break-word;">Monthly</th>
								  </tr>
								</thead>
								<tbody>
								<?php 								
								if($project_sresult['status'] == SUCCESS)
								{									
									for($count = 0; $count < count($project_list_data); $count++)
									{																
										// Scheduled payment collection - daily
										$scheduled_collection_result = i_get_pay_schedule('',$today_start_date,$today_end_date,'','','',$project_list_data[$count]['project_id']);										
										if($scheduled_collection_result['status'] == SUCCESS)
										{
											$today_scheduled_collection = 0;
											for($sch_count = 0; $sch_count < count($scheduled_collection_result['data']); $sch_count++)
											{
												$today_scheduled_collection = $today_scheduled_collection + $scheduled_collection_result['data'][$sch_count]['crm_payment_schedule_amount'];
											}
										}
										else
										{
											$today_scheduled_collection = 0;
										}
										
										// Actual payment collection - daily
										$payment_collection_result = i_get_payment('','','',$today_start_date,$today_end_date,$project_list_data[$count]['project_id']);
										if($payment_collection_result['status'] == SUCCESS)
										{
											$today_payment_collection = 0;
											for($pay_count = 0; $pay_count < count($payment_collection_result['data']); $pay_count++)
											{
												$today_payment_collection = $today_payment_collection + $payment_collection_result['data'][$pay_count]['crm_payment_amount'];
											}
										}
										else
										{
											$today_payment_collection = 0;
										}
										
										// Scheduled payment collection - weekly
										$scheduled_collection_result = i_get_pay_schedule('',$week_start_date,$week_end_date,'','','',$project_list_data[$count]['project_id']);
										if($scheduled_collection_result['status'] == SUCCESS)
										{
											$week_scheduled_collection = 0;
											for($sch_count = 0; $sch_count < count($scheduled_collection_result['data']); $sch_count++)
											{
												$week_scheduled_collection = $week_scheduled_collection + $scheduled_collection_result['data'][$sch_count]['crm_payment_schedule_amount'];
											}
										}
										else
										{
											$week_scheduled_collection = 0;
										}
										
										// Actual payment collection - weekly
										$payment_collection_result = i_get_payment('','','',$week_start_date,$week_end_date,$project_list_data[$count]['project_id']);
										if($payment_collection_result['status'] == SUCCESS)
										{
											$week_payment_collection = 0;
											for($pay_count = 0; $pay_count < count($payment_collection_result['data']); $pay_count++)
											{
												$week_payment_collection = $week_payment_collection + $payment_collection_result['data'][$pay_count]['crm_payment_amount'];
											}
										}
										else
										{
											$week_payment_collection = 0;
										}
										
										// Scheduled payment collection - monthly
										$scheduled_collection_result = i_get_pay_schedule('',$month_start_date,$month_end_date,'','','',$project_list_data[$count]['project_id']);
										if($scheduled_collection_result['status'] == SUCCESS)
										{
											$month_scheduled_collection = 0;
											for($sch_count = 0; $sch_count < count($scheduled_collection_result['data']); $sch_count++)
											{
												$month_scheduled_collection = $month_scheduled_collection + $scheduled_collection_result['data'][$sch_count]['crm_payment_schedule_amount'];
											}
										}
										else
										{
											$month_scheduled_collection = 0;
										}
										
										// Actual payment collection - monthly
										$payment_collection_result = i_get_payment('','','',$month_start_date,$month_end_date,$project_list_data[$count]['project_id']);
										if($payment_collection_result['status'] == SUCCESS)
										{
											$month_payment_collection = 0;
											for($pay_count = 0; $pay_count < count($payment_collection_result['data']); $pay_count++)
											{
												$month_payment_collection = $month_payment_collection + $payment_collection_result['data'][$pay_count]['crm_payment_amount'];
											}
										}
										else
										{
											$month_payment_collection = 0;
										}
																				
										?>
										<tr>
										<td style="word-wrap:break-word;"><?php echo $project_list_data[$count]['project_name']; ?></td>
										<td style="word-wrap:break-word;"><?php echo $today_scheduled_collection; ?></td>									
										<td style="word-wrap:break-word;"><?php echo $week_scheduled_collection; ?></td>									
										<td style="word-wrap:break-word;"><?php echo $month_scheduled_collection; ?></td>									
										<td style="word-wrap:break-word;"><a href="crm_collected_details.php?project=<?php echo $project_list_data[$count]['project_id']; ?>&start=<?php echo $today_start_date; ?>&end=<?php echo $today_end_date; ?>&projectname=<?php echo $project_list_data[$count]['project_name']; ?>"><?php echo $today_payment_collection; ?></a></td>
										<td style="word-wrap:break-word;"><a href="crm_collected_details.php?project=<?php echo $project_list_data[$count]['project_id']; ?>&start=<?php echo $week_start_date; ?>&end=<?php echo $week_end_date; ?>&projectname=<?php echo $project_list_data[$count]['project_name']; ?>"><?php echo $week_payment_collection; ?></a></td>
										<td style="word-wrap:break-word;"><a href="crm_collected_details.php?project=<?php echo $project_list_data[$count]['project_id']; ?>&start=<?php echo $month_start_date; ?>&end=<?php echo $month_end_date; ?>&projectname=<?php echo $project_list_data[$count]['project_name']; ?>"><?php echo $month_payment_collection; ?></a></td>
										<td style="word-wrap:break-word;"><?php echo ($today_payment_collection - $today_scheduled_collection); ?></td>
										<td style="word-wrap:break-word;"><?php echo ($week_payment_collection - $week_scheduled_collection); ?></td>
										<td style="word-wrap:break-word;"><?php echo ($month_payment_collection - $month_scheduled_collection); ?></td>
										</tr>
										<?php
									}
								}
								?>
								</tbody>
								</table>
                            </div>
                            <!-- /widget-content -->
                        </div>
                        <!-- /widget -->
                    </div>
                    <!-- /span6 -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /main-inner -->
    </div>
    <!-- /main -->
    <div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2016
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    <!-- Le javascript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.7.2.min.js"></script>
    <script src="js/excanvas.min.js"></script>
    <script src="js/chart.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/base.js"></script>
<script>
function go_to_enquiry_list(start_date,end_date)
{	
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "crm_enquiry_list.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","dt_start_date_entry_form");
	hiddenField1.setAttribute("value",start_date);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","dt_end_date_entry_form");
	hiddenField2.setAttribute("value",end_date);
	
	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","enquiry_search_submit");
	hiddenField3.setAttribute("value",'submit');
	
    form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_booking_list(start_date,end_date)
{	
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "crm_overall_report.php");

	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","dt_start_date");
	hiddenField1.setAttribute("value",start_date);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","dt_end_date");
	hiddenField2.setAttribute("value",end_date);
	
	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","search_report_submit");
	hiddenField3.setAttribute("value",'submit');
	
    form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);
	
	document.body.appendChild(form);
    form.submit();
}
</script>
</body>
</html>