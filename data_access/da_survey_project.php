<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. Wherever there is name search, there should be a wildcard
*/

/*
PURPOSE : To add new Survey Process
INPUT 	: Survey ID, Process ID, Start Date, End Date, Remarks, Added By
OUTPUT 	: Survey Process ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_process($survey_id,$process_id,$process_start_date,$process_end_date,$remarks,$added_by)
{
	// Query
   $survey_process_iquery = "insert into survey_process
   (survey_process_survey_id,process_id,survey_process_start_date,survey_process_end_date,survey_process_active,survey_process_remarks,
   survey_process_added_by,survey_process_added_on)values(:survey_id,:process_id,:process_start_date,:process_end_date,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $survey_process_istatement = $dbConnection->prepare($survey_process_iquery);
        
        // Data
        $survey_process_idata = array(':survey_id'=>$survey_id,':process_id'=>$process_id,':process_start_date'=>$process_start_date,':process_end_date'=>$process_end_date,
		':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $survey_process_istatement->execute($survey_process_idata);
		$survey_process_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_process_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Survey Process List
INPUT 	: Survey Process ID, Survey ID, Process ID, Start Date, End Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Process 
BY 		: Lakshmi
*/
function db_get_survey_process($survey_process_search_data)
{  
	if(array_key_exists("survey_process_id",$survey_process_search_data))
	{
		$survey_process_id = $survey_process_search_data["survey_process_id"];
	}
	else
	{
		$survey_process_id= "";
	}
	
	if(array_key_exists("survey_id",$survey_process_search_data))
	{
		$survey_id = $survey_process_search_data["survey_id"];
	}
	else
	{
		$survey_id = "";
	}
	
	if(array_key_exists("process_id",$survey_process_search_data))
	{
		$process_id = $survey_process_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("process_start_date",$survey_process_search_data))
	{
		$process_start_date = $survey_process_search_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}
	
	if(array_key_exists("process_end_date",$survey_process_search_data))
	{
		$process_end_date = $survey_process_search_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}
	
	if(array_key_exists("active",$survey_process_search_data))
	{
		$active = $survey_process_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$survey_process_search_data))
	{
		$added_by = $survey_process_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$survey_process_search_data))
	{
		$start_date= $survey_process_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$survey_process_search_data))
	{
		$end_date= $survey_process_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	if(array_key_exists("project",$survey_process_search_data))
	{
		$project= $survey_process_search_data["project"];
	}
	else
	{
		$project= "";
	}
	
	if(array_key_exists("process_name",$survey_process_search_data))
	{
		$process_name= $survey_process_search_data["process_name"];
	}
	else
	{
		$process_name= "";
	}
	
	if(array_key_exists("village",$survey_process_search_data))
	{
		$village= $survey_process_search_data["village"];
	}
	else
	{
		$village= "";
	}
	
	$get_survey_process_list_squery_base = "select * from survey_process SP inner join users U on U.user_id = SP.survey_process_added_by inner join survey_process_master SPM on SPM.survey_process_master_id = SP.process_id inner join survey_details SD on SD.survey_details_id = SP.survey_process_survey_id inner join village_master VM on VM.village_id = SD.survey_details_village inner join survey_project_master SPPM on SPPM.survey_project_master_id = SD.survey_details_project";
	
	$get_survey_process_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_survey_process_list_sdata = array();
	
	if($survey_process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." where survey_process_id = :survey_process_id";								
		}
		else
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." and survey_process_id = :survey_process_id";				
		}
		
		// Data
		$get_survey_process_list_sdata[':survey_process_id'] = $survey_process_id;
		
		$filter_count++;
	}
	
	if($survey_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." where survey_process_survey_id = :survey_id";								
		}
		else
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." and survey_process_survey_id = :survey_id";				
		}
		
		// Data
		$get_survey_process_list_sdata[':survey_id'] = $survey_id;
		
		$filter_count++;
	}
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." where process_id = :process_id";								
		}
		else
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." and process_id = :process_id";				
		}
		
		// Data
		$get_survey_process_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($process_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." where SPM.survey_process_master_id = :process_name";								
		}
		else
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." and SPM.survey_process_master_id = :process_name";				
		}
		
		// Data
		$get_survey_process_list_sdata[':process_name'] = $process_name;
		
		$filter_count++;
	}
	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." where SPPM.survey_project_master_id = :project";								
		}
		else
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." and SPPM.survey_project_master_id = :project";				
		}
		
		// Data
		$get_survey_process_list_sdata[':project'] = $project;
		
		$filter_count++;
	}
	
	if($village != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." where VM.village_id = :village";								
		}
		else
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." and VM.village_id = :village";				
		}
		
		// Data
		$get_survey_process_list_sdata[':village'] = $village;
		
		$filter_count++;
	}
	
	if($process_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." where survey_process_start_date = :process_start_date";
		}
		else
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." and survey_process_start_date = :process_start_date";
		}
		
		// Data
		$get_survey_process_list_sdata[':process_start_date'] = $process_start_date;
		
		$filter_count++;
	}
	
	if($process_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." where survey_process_end_date = :process_end_date";								
		}
		else
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." and survey_process_end_date = :process_end_date";				
		}
		
		// Data
		$get_survey_process_list_sdata[':process_end_date'] = $process_end_date;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." where survey_process_active = :active";								
		}
		else
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." and survey_process_active = :active";				
		}
		
		// Data
		$get_survey_process_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." where survey_process_added_by = :added_by";
		}
		else
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." and survey_process_added_by = :added_by";
		}
		
		//Data
		$get_survey_process_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." where survey_process_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." and survey_process_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_process_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." where survey_process_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_survey_process_list_squery_where = $get_survey_process_list_squery_where." and survey_process_added_on <= :end_date";
		}
		
		//Data
		$get_survey_process_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_survey_process_list_squery_order = " order by survey_process_master_order ASC";
	$get_survey_process_list_squery = $get_survey_process_list_squery_base.$get_survey_process_list_squery_where.$get_survey_process_list_squery_order;	

	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_process_list_sstatement = $dbConnection->prepare($get_survey_process_list_squery);
		
		$get_survey_process_list_sstatement -> execute($get_survey_process_list_sdata);
		
		$get_survey_process_list_sdetails = $get_survey_process_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_process_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_process_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_process_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
  /*
PURPOSE : To update Survey Process 
INPUT 	: Survey Process ID,Survey Process Update Array
OUTPUT 	: Survey Process ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_process($survey_process_id,$survey_process_update_data)
{
	if(array_key_exists("survey_id",$survey_process_update_data))
	{	
		$survey_id = $survey_process_update_data["survey_id"];
	}
	else
	{
		$survey_id = "";
	}
	
	if(array_key_exists("process_id",$survey_process_update_data))
	{	
		$process_id = $survey_process_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("process_start_date",$survey_process_update_data))
	{	
		$process_start_date = $survey_process_update_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}
	
	if(array_key_exists("process_end_date",$survey_process_update_data))
	{	
		$process_end_date = $survey_process_update_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}
	
	if(array_key_exists("active",$survey_process_update_data))
	{	
		$active = $survey_process_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$survey_process_update_data))
	{	
		$remarks = $survey_process_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$survey_process_update_data))
	{	
		$added_by = $survey_process_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_process_update_data))
	{	
		$added_on = $survey_process_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $survey_process_update_uquery_base = "update survey_process set ";  
	
	$survey_process_update_uquery_set = "";
	
	$survey_process_update_uquery_where = " where survey_process_id = :survey_process_id";
	
	$survey_process_update_udata = array(":survey_process_id"=>$survey_process_id);
	
	$filter_count = 0;
	
	if($survey_id != "")
	{
		$survey_process_update_uquery_set = $survey_process_update_uquery_set." survey_process_survey_id = :survey_id,";
		$survey_process_update_udata[":survey_id"] = $survey_id;
		$filter_count++;
	}
	
	if($process_id != "")
	{
		$survey_process_update_uquery_set = $survey_process_update_uquery_set." process_id = :process_id,";
		$survey_process_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}
	
	if($process_start_date != "")
	{
		$survey_process_update_uquery_set = $survey_process_update_uquery_set." survey_process_start_date = :process_start_date,";
		$survey_process_update_udata[":process_start_date"] = $process_start_date;
		$filter_count++;
	}
	
	if($process_end_date != "")
	{
		$survey_process_update_uquery_set = $survey_process_update_uquery_set." survey_process_end_date = :process_end_date,";
		$survey_process_update_udata[":process_end_date"] = $process_end_date;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_process_update_uquery_set = $survey_process_update_uquery_set." survey_process_active = :active,";
		$survey_process_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$survey_process_update_uquery_set = $survey_process_update_uquery_set." survey_process_remarks = :remarks,";
		$survey_process_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_process_update_uquery_set = $survey_process_update_uquery_set." survey_process_added_by = :added_by,";
		$survey_process_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_process_update_uquery_set = $survey_process_update_uquery_set." survey_process_added_on = :added_on,";
		$survey_process_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_process_update_uquery_set = trim($survey_process_update_uquery_set,',');
	}
	
	$survey_process_update_uquery = $survey_process_update_uquery_base.$survey_process_update_uquery_set.$survey_process_update_uquery_where;
 
    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_process_update_ustatement = $dbConnection->prepare($survey_process_update_uquery);		
        
        $survey_process_update_ustatement -> execute($survey_process_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_process_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Survey Process Delay
INPUT 	: Reason ID, Process ID, Remarks, Added By
OUTPUT 	: Delay ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_process_delay($reason_id,$process_id,$remarks,$added_by)
{
	// Query
   $survey_process_delay_iquery = "insert into survey_process_delay
   (survey_process_delay_reason_id,survey_process_delay_process_id,survey_process_delay_active,survey_process_delay_remarks,survey_process_delay_added_by,
   survey_process_delay_added_on)values(:reason_id,:process_id,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $survey_process_delay_istatement = $dbConnection->prepare($survey_process_delay_iquery);
        
        // Data
        $survey_process_delay_idata = array(':reason_id'=>$reason_id,':process_id'=>$process_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $survey_process_delay_istatement->execute($survey_process_delay_idata);
		$survey_process_delay_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_process_delay_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Survey Process Delay List
INPUT 	: Delay ID, Reason ID, Process ID, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Process Delay
BY 		: Lakshmi
*/
function db_get_survey_process_delay($survey_process_delay_search_data)
{  
	if(array_key_exists("delay_id",$survey_process_delay_search_data))
	{
		$delay_id = $survey_process_delay_search_data["delay_id"];
	}
	else
	{
		$delay_id= "";
	}
	
	if(array_key_exists("reason_id",$survey_process_delay_search_data))
	{
		$reason_id = $survey_process_delay_search_data["reason_id"];
	}
	else
	{
		$reason_id = "";
	}
	
	if(array_key_exists("process_id",$survey_process_delay_search_data))
	{
		$process_id = $survey_process_delay_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("active",$survey_process_delay_search_data))
	{
		$active = $survey_process_delay_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$survey_process_delay_search_data))
	{
		$added_by = $survey_process_delay_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$survey_process_delay_search_data))
	{
		$start_date= $survey_process_delay_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$survey_process_delay_search_data))
	{
		$end_date= $survey_process_delay_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_survey_process_delay_list_squery_base = "select * from survey_process_delay SPD inner join users U on U.user_id = SPD.survey_process_delay_added_by inner join survey_delay_reason_master SDRM on SDRM.survey_delay_reason_master_id=SPD.survey_process_delay_reason_id inner join survey_process_master SPM on SPM.survey_process_master_id = SPD.survey_process_delay_process_id";
	
	$get_survey_process_delay_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_survey_process_delay_list_sdata = array();
	
	if($delay_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_delay_list_squery_where = $get_survey_process_delay_list_squery_where." where survey_process_delay_id = :delay_id";								
		}
		else
		{
			// Query
			$get_survey_process_delay_list_squery_where = $get_survey_process_delay_list_squery_where." and survey_process_delay_id = :delay_id";				
		}
		
		// Data
		$get_survey_process_delay_list_sdata[':delay_id'] = $delay_id;
		
		$filter_count++;
	}
	
	if($reason_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_delay_list_squery_where = $get_survey_process_delay_list_squery_where." where survey_process_delay_reason_id = :reason_id";								
		}
		else
		{
			// Query
			$get_survey_process_delay_list_squery_where = $get_survey_process_delay_list_squery_where." and survey_process_delay_reason_id = :reason_id";				
		}
		
		// Data
		$get_survey_process_delay_list_sdata[':reason_id'] = $reason_id;
		
		$filter_count++;
	}
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_delay_list_squery_where = $get_survey_process_delay_list_squery_where." where survey_process_delay_process_id = :process_id";								
		}
		else
		{
			// Query
			$get_survey_process_delay_list_squery_where = $get_survey_process_delay_list_squery_where." and survey_process_delay_process_id = :process_id";				
		}
		
		// Data
		$get_survey_process_delay_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_delay_list_squery_where = $get_survey_process_delay_list_squery_where." where survey_process_delay_active = :active";
		}
		else
		{
			// Query
			$get_survey_process_delay_list_squery_where = $get_survey_process_delay_list_squery_where." and survey_process_delay_active = :active";
		}
		
		// Data
		$get_survey_process_delay_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_delay_list_squery_where = $get_survey_process_delay_list_squery_where." where survey_process_delay_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_survey_process_delay_list_squery_where = $get_survey_process_delay_list_squery_where." and survey_process_delay_added_by = :added_by";
		}
		
		//Data
		$get_survey_process_delay_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_delay_list_squery_where = $get_survey_process_delay_list_squery_where." where survey_process_delay_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_survey_process_delay_list_squery_where = $get_survey_process_delay_list_squery_where." and survey_process_delay_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_process_delay_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_delay_list_squery_where = $get_survey_process_delay_list_squery_where." where survey_process_delay_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_survey_process_delay_list_squery_where = $get_survey_process_delay_list_squery_where." and survey_process_delay_added_on <= :end_date";
		}
		
		//Data
		$get_survey_process_delay_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_survey_process_delay_list_squery_order = " order by survey_process_delay_added_on DESC";
	$get_survey_process_delay_list_squery = $get_survey_process_delay_list_squery_base.$get_survey_process_delay_list_squery_where.$get_survey_process_delay_list_squery_order;

	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_process_delay_list_sstatement = $dbConnection->prepare($get_survey_process_delay_list_squery);
		
		$get_survey_process_delay_list_sstatement -> execute($get_survey_process_delay_list_sdata);
		
		$get_survey_process_delay_list_sdetails = $get_survey_process_delay_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_process_delay_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_process_delay_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_process_delay_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
  /*
PURPOSE : To update Survey Process Delay 
INPUT 	: Survey Process ID,Survey Process Update Array
OUTPUT 	: Survey Process ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_process_delay($delay_id,$survey_process_delay_update_data)
{
	if(array_key_exists("reason_id",$survey_process_delay_update_data))
	{	
		$reason_id = $survey_process_delay_update_data["reason_id"];
	}
	else
	{
		$reason_id = "";
	}
	
	if(array_key_exists("process_id",$survey_process_delay_update_data))
	{	
		$process_id = $survey_process_delay_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("active",$survey_process_delay_update_data))
	{	
		$active = $survey_process_delay_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$survey_process_delay_update_data))
	{	
		$remarks = $survey_process_delay_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$survey_process_delay_update_data))
	{	
		$added_by = $survey_process_delay_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_process_delay_update_data))
	{	
		$added_on = $survey_process_delay_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $survey_process_delay_update_uquery_base = "update survey_process_delay set";  
	
	$survey_process_delay_update_uquery_set = "";
	
	$survey_process_delay_update_uquery_where = " where survey_process_delay_id = :delay_id";
	
	$survey_process_delay_update_udata = array(":delay_id"=>$delay_id);
	
	$filter_count = 0;
	
	if($reason_id != "")
	{
		$survey_process_delay_update_uquery_set = $survey_process_delay_update_uquery_set." survey_process_delay_reason_id = :reason_id,";
		$survey_process_delay_update_udata[":reason_id"] = $reason_id;
		$filter_count++;
	}
	
	if($process_id != "")
	{
		$survey_process_delay_update_uquery_set = $survey_process_delay_update_uquery_set." survey_process_delay_process_id = :process_id,";
		$survey_process_delay_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_process_delay_update_uquery_set = $survey_process_delay_update_uquery_set." survey_process_delay_active = :active,";
		$survey_process_delay_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$survey_process_delay_update_uquery_set = $survey_process_delay_update_uquery_set." survey_process_delay_remarks = :remarks,";
		$survey_process_delay_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_process_delay_update_uquery_set = $survey_process_delay_update_uquery_set." survey_process_delay_added_by = :added_by,";
		$survey_process_delay_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_process_delay_update_uquery_set = $survey_process_delay_update_uquery_set." survey_process_delay_added_on = :added_on,";
		$survey_process_delay_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_process_delay_update_uquery_set = trim($survey_process_delay_update_uquery_set,',');
	}
	
	$survey_process_delay_update_uquery = $survey_process_delay_update_uquery_base.$survey_process_delay_update_uquery_set.$survey_process_delay_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_process_delay_update_ustatement = $dbConnection->prepare($survey_process_delay_update_uquery);		
        
        $survey_process_delay_update_ustatement -> execute($survey_process_delay_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $delay_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Survey Details
INPUT 	: Project, Village, Planned Start Date, Planned End Date, Status, Remarks, Added By
OUTPUT 	: Survey ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_details($project,$village,$planned_start_date,$planned_end_date,$remarks,$added_by)
{
	// Query
   $survey_details_iquery = "insert into survey_details (survey_details_project,survey_details_village,survey_details_planned_start_date,survey_details_planned_end_date,survey_details_status,survey_details_active,
   survey_details_remarks,survey_details_added_by,survey_details_added_on) values (:project,:village,:planned_start_date,:planned_end_date,:status,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $survey_details_istatement = $dbConnection->prepare($survey_details_iquery);
        
        // Data
        $survey_details_idata = array(':project'=>$project,':village'=>$village,':planned_start_date'=>$planned_start_date,':planned_end_date'=>$planned_end_date,
		':status'=>'Pending',':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $survey_details_istatement->execute($survey_details_idata);
		$survey_details_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_details_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Survey Details List
INPUT 	: Survey ID, Project, Village, Planned Start Date, Planned End Date, Status, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Details 
BY 		: Lakshmi
*/
function db_get_survey_details($survey_details_search_data)
{ 
	if(array_key_exists("survey_id",$survey_details_search_data))
	{
		$survey_id = $survey_details_search_data["survey_id"];
	}
	else
	{
		$survey_id = "";
	}
	
	if(array_key_exists("project",$survey_details_search_data))
	{
		$project = $survey_details_search_data["project"];
	}
	else
	{
		$project = "";
	}
	
	if(array_key_exists("village",$survey_details_search_data))
	{
		$village = $survey_details_search_data["village"];
	}
	else
	{
		$village = "";
	}
	
	if(array_key_exists("planned_start_date",$survey_details_search_data))
	{
		$planned_start_date = $survey_details_search_data["planned_start_date"];
	}
	else
	{
		$planned_start_date = "";
	}
	
	if(array_key_exists("planned_end_date",$survey_details_search_data))
	{
		$planned_end_date = $survey_details_search_data["planned_end_date"];
	}
	else
	{
		$planned_end_date = "";
	}
	
	if(array_key_exists("status",$survey_details_search_data))
	{
		$status = $survey_details_search_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("active",$survey_details_search_data))
	{
		$active = $survey_details_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$survey_details_search_data))
	{
		$added_by = $survey_details_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$survey_details_search_data))
	{
		$start_date = $survey_details_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$survey_details_search_data))
	{
		$end_date = $survey_details_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}
	
	$get_survey_details_list_squery_base = "select * from survey_details SD inner join users U on U.user_id = SD.survey_details_added_by inner join village_master VM on VM.village_id = SD.survey_details_village inner join survey_project_master SPMM on SPMM.survey_project_master_id = SD.survey_details_project";
	
	$get_survey_details_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_survey_details_list_sdata = array();
	
	if($survey_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." where survey_details_id = :survey_id";
		}
		else
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." and survey_details_id = :survey_id";				
		}
		
		// Data
		$get_survey_details_list_sdata[':survey_id'] = $survey_id;
		
		$filter_count++;
	}
	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." where survey_details_project = :project";
		}
		else
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." and survey_details_project = :project";				
		}
		
		// Data
		$get_survey_details_list_sdata[':project'] = $project;
		
		$filter_count++;
	}
	
	if($village != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." where survey_details_village = :village";
		}
		else
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." and survey_details_village = :village";				
		}
		
		// Data
		$get_survey_details_list_sdata[':village'] = $village;
		
		$filter_count++;
	}
	
	if($planned_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." where survey_details_planned_start_date = :planned_start_date";
		}
		else
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." and survey_details_planned_start_date = :planned_start_date";
		}
		
		// Data
		$get_survey_details_list_sdata[':planned_start_date'] = $planned_start_date;
		
		$filter_count++;
	}
	
	if($planned_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." where survey_details_planned_end_date = :planned_end_date";
		}
		else
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." and survey_details_planned_end_date = :planned_end_date";
		}
		
		// Data
		$get_survey_details_list_sdata[':planned_end_date'] = $planned_end_date;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." where survey_details_status = :status";
		}
		else
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." and survey_details_status = :status";
		}
		
		// Data
		$get_survey_details_list_sdata[':status'] = $status;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." where survey_details_active = :active";
		}
		else
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." and survey_details_active = :active";				
		}
		
		// Data
		$get_survey_details_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." where survey_details_added_by = :added_by";
		}
		else
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." and survey_details_added_by = :added_by";
		}
		
		//Data
		$get_survey_details_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." where survey_details_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." and survey_details_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_details_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." where survey_details_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_survey_details_list_squery_where = $get_survey_details_list_squery_where." and survey_details_added_on <= :end_date";
		}
		
		//Data
		$get_survey_details_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_survey_details_list_squery = $get_survey_details_list_squery_base.$get_survey_details_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_details_list_sstatement = $dbConnection->prepare($get_survey_details_list_squery);
		
		$get_survey_details_list_sstatement -> execute($get_survey_details_list_sdata);
		
		$get_survey_details_list_sdetails = $get_survey_details_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_details_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_details_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_details_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}
 
/*
PURPOSE : To update Survey Details 
INPUT 	: Survey ID, Survey Details Update Array
OUTPUT 	: Survey ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_details($survey_id,$survey_details_update_data)
{	
	if(array_key_exists("project",$survey_details_update_data))
	{	
		$project = $survey_details_update_data["project"];
	}
	else
	{
		$project = "";
	}
	
	if(array_key_exists("village",$survey_details_update_data))
	{	
		$village = $survey_details_update_data["village"];
	}
	else
	{
		$village = "";
	}
	
	if(array_key_exists("planned_start_date",$survey_details_update_data))
	{	
		$planned_start_date = $survey_details_update_data["planned_start_date"];
	}
	else
	{
		$planned_start_date = "";
	}
	
	if(array_key_exists("planned_end_date",$survey_details_update_data))
	{	
		$planned_end_date = $survey_details_update_data["planned_end_date"];
	}
	else
	{
		$planned_end_date = "";
	}
	
	if(array_key_exists("status",$survey_details_update_data))
	{	
		$status = $survey_details_update_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("active",$survey_details_update_data))
	{	
		$active = $survey_details_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$survey_details_update_data))
	{	
		$remarks = $survey_details_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$survey_details_update_data))
	{	
		$added_by = $survey_details_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_details_update_data))
	{	
		$added_on = $survey_details_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $survey_details_update_uquery_base = "update survey_details set";  
	
	$survey_details_update_uquery_set = "";
	
	$survey_details_update_uquery_where = " where survey_details_id = :survey_id";
	
	$survey_details_update_udata = array(":survey_id"=>$survey_id);
	
	$filter_count = 0;
	
	if($project != "")
	{
		$survey_details_update_uquery_set = $survey_details_update_uquery_set." survey_details_project = :project,";
		$survey_details_update_udata[":project"] = $project;
		$filter_count++;
	}
	
	if($village != "")
	{
		$survey_details_update_uquery_set = $survey_details_update_uquery_set." survey_details_village = :village,";
		$survey_details_update_udata[":village"] = $village;
		$filter_count++;
	}
	
	if($planned_start_date != "")
	{
		$survey_details_update_uquery_set = $survey_details_update_uquery_set." survey_details_planned_start_date = :planned_start_date,";
		$survey_details_update_udata[":planned_start_date"] = $planned_start_date;
		$filter_count++;
	}
	
	if($planned_end_date != "")
	{
		$survey_details_update_uquery_set = $survey_details_update_uquery_set." survey_details_planned_end_date = :planned_end_date,";
		$survey_details_update_udata[":planned_end_date"] = $planned_end_date;
		$filter_count++;
	}
	
	if($status != "")
	{
		$survey_details_update_uquery_set = $survey_details_update_uquery_set." survey_details_status = :status,";
		$survey_details_update_udata[":status"] = $status;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_details_update_uquery_set = $survey_details_update_uquery_set." survey_details_active = :active,";
		$survey_details_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$survey_details_update_uquery_set = $survey_details_update_uquery_set." survey_details_remarks = :remarks,";
		$survey_details_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_details_update_uquery_set = $survey_details_update_uquery_set." survey_details_added_by = :added_by,";
		$survey_details_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_details_update_uquery_set = $survey_details_update_uquery_set." survey_details_added_on = :added_on,";
		$survey_details_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_details_update_uquery_set = trim($survey_details_update_uquery_set,',');
	}
	
	$survey_details_update_uquery = $survey_details_update_uquery_base.$survey_details_update_uquery_set.$survey_details_update_uquery_where;
 
    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_details_update_ustatement = $dbConnection->prepare($survey_details_update_uquery);		
        
        $survey_details_update_ustatement -> execute($survey_details_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Survey File
INPUT 	: BD File ID, Survey ID, Remarks, Added By
OUTPUT 	: File ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_file($bd_file_id,$survey_id,$remarks,$added_by)
{
	// Query
    $survey_file_iquery = "insert into survey_file (survey_file_bd_file_id,survey_file_details_id,survey_file_active,survey_file_remarks,survey_file_added_by,survey_file_added_on) values(:bd_file_id,:survey_id,:active,:remarks,:added_by,:added_on)";  
  
    try
    {
        $dbConnection = get_conn_handle();
        $survey_file_istatement = $dbConnection->prepare($survey_file_iquery);
        
        // Data
        $survey_file_idata = array(':bd_file_id'=>$bd_file_id,':survey_id'=>$survey_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $survey_file_istatement->execute($survey_file_idata);
		$survey_file_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_file_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Survey file list
INPUT 	: BD File ID, File ID, Survey ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey File
BY 		: Lakshmi
*/
function db_get_survey_file($survey_file_search_data)
{  
	if(array_key_exists("file_id",$survey_file_search_data))
	{
		$file_id = $survey_file_search_data["file_id"];
	}
	else
	{
		$file_id = "";
	}
	
	if(array_key_exists("bd_file_id",$survey_file_search_data))
	{
		$bd_file_id = $survey_file_search_data["bd_file_id"];
	}
	else
	{
		$bd_file_id = "";
	}
	
	if(array_key_exists("survey_id",$survey_file_search_data))
	{
		$survey_id = $survey_file_search_data["survey_id"];
	}
	else
	{
		$survey_id = "";
	}
	
	if(array_key_exists("active",$survey_file_search_data))
	{
		$active = $survey_file_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$survey_file_search_data))
	{
		$added_by = $survey_file_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	if(array_key_exists("start_date",$survey_file_search_data))
	{
		$start_date = $survey_file_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$survey_file_search_data))
	{
		$end_date = $survey_file_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}
	
	
	if(array_key_exists("project",$survey_file_search_data))
	{
		$project = $survey_file_search_data["project"];
	}
	else
	{
		$project = "";
	}
	
	
	if(array_key_exists("village",$survey_file_search_data))
	{
		$village = $survey_file_search_data["village"];
	}
	else
	{
		$village = "";
	}
	
	if(array_key_exists("status",$survey_file_search_data))
	{
		$status = $survey_file_search_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("parent_survey_id",$survey_file_search_data))
	{
		$parent_survey_id = $survey_file_search_data["parent_survey_id"];
	}
	else
	{
		$parent_survey_id = "";
	}

	$get_survey_file_list_squery_base = "select * from survey_file SF inner join users U on U.user_id = SF.survey_file_added_by inner join survey_details SD on SD.survey_details_id = SF.survey_file_details_id inner join survey_master SM on SM.survey_master_id = SF.survey_file_bd_file_id inner join village_master VM on VM.village_id = SD.survey_details_village inner join survey_project_master SPPM on SPPM.survey_project_master_id = SD.survey_details_project";
	
	$get_survey_file_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_survey_file_list_sdata = array();
	
	if($file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." where survey_file_id = :file_id";								
		}
		else
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." and survey_file_id = :file_id";				
		}
		
		// Data
		$get_survey_file_list_sdata[':file_id'] = $file_id;
		
		$filter_count++;
	}
	
	if($bd_file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." where survey_file_bd_file_id = :bd_file_id";
		}
		else
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." and survey_file_bd_file_id = :bd_file_id";				
		}
		
		// Data
		$get_survey_file_list_sdata[':bd_file_id'] = $bd_file_id;
		
		$filter_count++;
	}
	
	if($survey_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." where survey_file_details_id = :survey_id";
		}
		else
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." and survey_file_details_id = :survey_id";				
		}
		
		// Data
		$get_survey_file_list_sdata[':survey_id'] = $survey_id;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." where survey_file_active = :active";								
		}
		else
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." and survey_file_active = :active";				
		}
		
		// Data
		$get_survey_file_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." where survey_file_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." and survey_file_added_by = :added_by";
		}
		
		//Data
		$get_survey_file_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." where survey_file_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." and survey_file_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_file_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." where survey_file_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." and survey_file_added_on <= :end_date";
		}
		
		//Data
		$get_survey_file_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." where SPPM.survey_project_master_id = :project";
		}
		else
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." and SPPM.survey_project_master_id = :project";
		}
		
		//Data
		$get_survey_file_list_sdata['project']  = $project;
		
		$filter_count++;
	}
	
	if($village != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." where VM.village_id = :village";
		}
		else
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." and VM.village_id = :village";
		}
		
		//Data
		$get_survey_file_list_sdata['village']  = $village;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." where SD.survey_details_status = :status";								
		}
		else
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." and SD.survey_details_status = :status";				
		}
		
		// Data
		$get_survey_file_list_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($parent_survey_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." where SM.survey_master_parent_survey_id = :parent_survey_id";								
		}
		else
		{
			// Query
			$get_survey_file_list_squery_where = $get_survey_file_list_squery_where." and SM.survey_master_parent_survey_id = :parent_survey_id";				
		}
		
		// Data
		$get_survey_file_list_sdata[':parent_survey_id']  = $parent_survey_id;
		
		$filter_count++;
	}

		
	$get_survey_file_list_squery = $get_survey_file_list_squery_base.$get_survey_file_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_file_list_sstatement = $dbConnection->prepare($get_survey_file_list_squery);
		
		$get_survey_file_list_sstatement -> execute($get_survey_file_list_sdata);
		
		$get_survey_file_list_sdetails = $get_survey_file_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_file_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_file_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_file_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Survey File 
INPUT 	: File ID,Survey File Update Array
OUTPUT 	: File ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_file($file_id,$survey_file_update_data)
{
	if(array_key_exists("bd_file_id",$survey_file_update_data))
	{	
		$bd_file_id = $survey_file_update_data["bd_file_id"];
	}
	else
	{
		$bd_file_id = "";
	}
	
	if(array_key_exists("survey_id",$survey_file_update_data))
	{	
		$survey_id = $survey_file_update_data["survey_id"];
	}
	else
	{
		$survey_id = "";
	}
	
	if(array_key_exists("active",$survey_file_update_data))
	{	
		$active = $survey_file_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$survey_file_update_data))
	{	
		$remarks = $survey_file_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$survey_file_update_data))
	{	
		$added_by = $survey_file_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_file_update_data))
	{	
		$added_on = $survey_file_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $survey_file_update_uquery_base = "update survey_file set";  
	
	$survey_file_update_uquery_set = "";
	
	$survey_file_update_uquery_where = " where survey_file_id = :file_id";
	
	$survey_file_update_udata = array(":file_id"=>$file_id);
	
	$filter_count = 0;
	
	if($bd_file_id != "")
	{
		$survey_file_update_uquery_set = $survey_file_update_uquery_set." survey_file_bd_file_id = :bd_file_id,";
		$survey_file_update_udata[":bd_file_id"] = $bd_file_id;
		$filter_count++;
	}
	
	if($survey_id != "")
	{
		$survey_file_update_uquery_set = $survey_file_update_uquery_set." survey_file_details_id = :survey_id,";
		$survey_file_update_udata[":survey_id"] = $survey_id;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_file_update_uquery_set = $survey_file_update_uquery_set." survey_file_active = :active,";
		$survey_file_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$survey_file_update_uquery_set = $survey_file_update_uquery_set." survey_file_remarks = :remarks,";
		$survey_file_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_file_update_uquery_set = $survey_file_update_uquery_set." survey_file_added_by = :added_by,";
		$survey_file_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_file_update_uquery_set = $survey_file_update_uquery_set." survey_file_added_on = :added_on,";
		$survey_file_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_file_update_uquery_set = trim($survey_file_update_uquery_set,',');
	}
	
	$survey_file_update_uquery = $survey_file_update_uquery_base.$survey_file_update_uquery_set.$survey_file_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_file_update_ustatement = $dbConnection->prepare($survey_file_update_uquery);		
        
        $survey_file_update_ustatement -> execute($survey_file_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $file_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Survey Document
INPUT 	: Process ID, File Path ID, Document Type ID, Remarks, Added By
OUTPUT 	: Document ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_document($process_id,$file_path_id,$document_type_id,$remarks,$added_by)
{
	// Query
	$survey_document_iquery = "insert into survey_document (survey_document_process_id,survey_document_file_path,survey_document_type_id,survey_document_active,survey_document_remarks,survey_document_added_by,survey_document_added_on)  values (:process_id,:file_path_id,:document_type_id,:active,:remarks,:added_by,:added_on)";  
  
    try
    {
        $dbConnection = get_conn_handle();
        $survey_document_istatement = $dbConnection->prepare($survey_document_iquery);
        
        // Data
        $survey_document_idata = array(':process_id'=>$process_id,':file_path_id'=>$file_path_id,':document_type_id'=>$document_type_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $survey_document_istatement->execute($survey_document_idata);
		$survey_document_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_document_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Survey Document list
INPUT 	: Document ID, Process ID, File Path ID, Document Type ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Document
BY 		: Lakshmi
*/
function db_get_survey_document($survey_document_search_data)
{  
	if(array_key_exists("document_id",$survey_document_search_data))
	{
		$document_id = $survey_document_search_data["document_id"];
	}
	else
	{
		$document_id = "";
	}
	
	if(array_key_exists("process_id",$survey_document_search_data))
	{
		$process_id = $survey_document_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("file_path_id",$survey_document_search_data))
	{
		$file_path_id = $survey_document_search_data["file_path_id"];
	}
	else
	{
		$file_path_id = "";
	}
	
	if(array_key_exists("document_type_id",$survey_document_search_data))
	{
		$document_type_id = $survey_document_search_data["document_type_id"];
	}
	else
	{
		$document_type_id = "";
	}
	
	if(array_key_exists("active",$survey_document_search_data))
	{
		$active = $survey_document_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$survey_document_search_data))
	{
		$added_by = $survey_document_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$survey_document_search_data))
	{
		$start_date = $survey_document_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$survey_document_search_data))
	{
		$end_date = $survey_document_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}
	
	if(array_key_exists("project_name",$survey_document_search_data))
	{
		$project_name = $survey_document_search_data["project_name"];
	}
	else
	{
		$project_name = "";
	}
	
	if(array_key_exists("process_name",$survey_document_search_data))
	{
		$process_name = $survey_document_search_data["process_name"];
	}
	else
	{
		$process_name = "";
	}

	$get_survey_document_list_squery_base = "select * from survey_document SD inner join users U on U.user_id = SD.survey_document_added_by inner join survey_process SP on SP.survey_process_id = SD.survey_document_process_id inner join survey_process_master SPM on SPM.survey_process_master_id = SP.process_id inner join survey_details AD on AD.survey_details_id = SP.survey_process_survey_id left outer join survey_project_master SPPM on SPPM.survey_project_master_id = AD.survey_details_project inner join survey_document_type_master SDTM on SDTM.survey_document_type_master_id = SD.survey_document_type_id";
	
	$get_survey_document_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_survey_document_list_sdata = array();
	
	if($document_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." where survey_document_id = :document_id";
		}
		else
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." and survey_document_id = :document_id";				
		}
		
		// Data
		$get_survey_document_list_sdata[':document_id'] = $document_id;
		
		$filter_count++;
	}
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." where survey_document_process_id = :process_id";
		}
		else
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." and survey_document_process_id = :process_id";				
		}
		
		// Data
		$get_survey_document_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($file_path_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." where survey_document_file_path = :file_path_id";
		}
		else
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." and survey_document_file_path = :file_path_id";				
		}
		
		// Data
		$get_survey_document_list_sdata[':file_path_id'] = $file_path_id;
		
		$filter_count++;
	}
	
	if($document_type_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." where survey_document_type_id = :document_type_id";
		}
		else
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." and survey_document_type_id = :document_type_id";				
		}
		
		// Data
		$get_survey_document_list_sdata[':document_type_id'] = $document_type_id;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." where survey_document_active = :active";
		}
		else
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." and survey_document_active = :active";				
		}
		
		// Data
		$get_survey_document_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." where survey_document_added_by = :added_by";
		}
		else
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." and survey_document_added_by = :added_by";
		}
		
		//Data
		$get_survey_document_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." where survey_document_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." and survey_document_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_document_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." where survey_document_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." and survey_document_added_on <= :end_date";
		}
		
		//Data
		$get_survey_document_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($process_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." where SPM.survey_process_master_id = :process_name";								
		}
		else
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." and SPM.survey_process_master_id = :process_name";				
		}
		
		// Data
		$get_survey_document_list_sdata[':process_name'] = $process_name;
		
		$filter_count++;
	}
	
	if($project_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." where SPPM.survey_project_master_id = :project_name";								
		}
		else
		{
			// Query
			$get_survey_document_list_squery_where = $get_survey_document_list_squery_where." and SPPM.survey_project_master_id = :project_name";				
		}
		
		// Data
		$get_survey_document_list_sdata[':project_name'] = $project_name;
		
		$filter_count++;
	}
	
	$get_survey_document_list_sorder = " order by survey_document_added_on DESC";
	$get_survey_document_list_squery = $get_survey_document_list_squery_base.$get_survey_document_list_squery_where.$get_survey_document_list_sorder;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_document_list_sstatement = $dbConnection->prepare($get_survey_document_list_squery);
		
		$get_survey_document_list_sstatement -> execute($get_survey_document_list_sdata);
		
		$get_survey_document_list_sdetails = $get_survey_document_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_document_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_document_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_document_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Survey Document 
INPUT 	: Document ID, Survey Document Update Array
OUTPUT 	: Document ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_document($document_id,$survey_document_update_data)
{
	if(array_key_exists("process_id",$survey_document_update_data))
	{	
		$process_id = $survey_document_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("file_path_id",$survey_document_update_data))
	{	
		$file_path_id = $survey_document_update_data["file_path_id"];
	}
	else
	{
		$file_path_id = "";
	}
	
	if(array_key_exists("document_type_id",$survey_document_update_data))
	{	
		$document_type_id = $survey_document_update_data["document_type_id"];
	}
	else
	{
		$document_type_id = "";
	}
	
	if(array_key_exists("active",$survey_document_update_data))
	{	
		$active = $survey_document_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$survey_document_update_data))
	{	
		$remarks = $survey_document_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$survey_document_update_data))
	{	
		$added_by = $survey_document_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_document_update_data))
	{	
		$added_on = $survey_document_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $survey_document_update_uquery_base = "update survey_document set";  
	
	$survey_document_update_uquery_set = "";
	
	$survey_document_update_uquery_where = " where survey_document_id = :document_id";
	
	$survey_document_update_udata = array(":document_id"=>$document_id);
	
	$filter_count = 0;
	
	if($process_id != "")
	{
		$survey_document_update_uquery_set = $survey_document_update_uquery_set." survey_document_process_id = :process_id,";
		$survey_document_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}
	
	if($file_path_id != "")
	{
		$survey_document_update_uquery_set = $survey_document_update_uquery_set." survey_document_file_path = :file_path_id,";
		$survey_document_update_udata[":file_path_id"] = $file_path_id;
		$filter_count++;
	}
	
	if($document_type_id != "")
	{
		$survey_document_update_uquery_set = $survey_document_update_uquery_set." survey_document_type_id = :document_type_id,";
		$survey_document_update_udata[":document_type_id"] = $document_type_id;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_document_update_uquery_set = $survey_document_update_uquery_set." survey_document_active = :active,";
		$survey_document_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$survey_document_update_uquery_set = $survey_document_update_uquery_set." survey_document_remarks = :remarks,";
		$survey_document_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_document_update_uquery_set = $survey_document_update_uquery_set." survey_document_added_by = :added_by,";
		$survey_document_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_document_update_uquery_set = $survey_document_update_uquery_set." survey_document_added_on = :added_on,";
		$survey_document_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_document_update_uquery_set = trim($survey_document_update_uquery_set,',');
	}
	
	$survey_document_update_uquery = $survey_document_update_uquery_base.$survey_document_update_uquery_set.$survey_document_update_uquery_where;
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_document_update_ustatement = $dbConnection->prepare($survey_document_update_uquery);		
        
        $survey_document_update_ustatement -> execute($survey_document_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $document_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Survey Process Task
INPUT 	: Process ID, Request Remarks, Updated By, Updated On, Added By
OUTPUT 	: Task ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_process_task($process_id,$request_remarks,$updated_by,$updated_on,$added_by)
{
	// Query
   $survey_process_task_iquery = "insert into survey_process_task(survey_process_task_process_id,survey_process_task_status,survey_process_task_active,survey_process_task_requested_remarks,survey_process_task_response_remarks,survey_process_task_updated_by,survey_process_task_updated_on,survey_process_task_added_by,survey_process_task_added_on) values (:process_id,:status,:active,:request_remarks,:response_remarks,:updated_by,:updated_on,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $survey_process_task_istatement = $dbConnection->prepare($survey_process_task_iquery);
        
        // Data
        $survey_process_task_idata = array(':process_id'=>$process_id,':status'=>'0',':request_remarks'=>$request_remarks,':response_remarks'=>'',
		':active'=>'1',':updated_by'=>$updated_by,':updated_on'=>$updated_on,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $survey_process_task_istatement->execute($survey_process_task_idata);
		$survey_process_task_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_process_task_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Survey Process Task List
INPUT 	: Task ID, Process ID, Status, Active, Requested Remarks, Response Remarks, Updated By, Updated On, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Process Task 
BY 		: Lakshmi
*/
function db_get_survey_process_task($survey_process_task_search_data)
{  
	if(array_key_exists("task_id",$survey_process_task_search_data))
	{
		$task_id = $survey_process_task_search_data["task_id"];
	}
	else
	{
		$task_id= "";
	}
	
	if(array_key_exists("process_id",$survey_process_task_search_data))
	{
		$process_id = $survey_process_task_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("status",$survey_process_task_search_data))
	{
		$status = $survey_process_task_search_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("active",$survey_process_task_search_data))
	{
		$active = $survey_process_task_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("request_remarks",$survey_process_task_search_data))
	{
		$request_remarks = $survey_process_task_search_data["request_remarks"];
	}
	else
	{
		$request_remarks = "";
	}
	
	if(array_key_exists("response_remarks",$survey_process_task_search_data))
	{
		$response_remarks = $survey_process_task_search_data["response_remarks"];
	}
	else
	{
		$response_remarks = "";
	}
	
	if(array_key_exists("updated_by",$survey_process_task_search_data))
	{
		$updated_by = $survey_process_task_search_data["updated_by"];
	}
	else
	{
		$updated_by = "";
	}
	
	if(array_key_exists("updated_on",$survey_process_task_search_data))
	{
		$updated_on = $survey_process_task_search_data["updated_on"];
	}
	else
	{
		$updated_on = "";
	}
	
	if(array_key_exists("added_by",$survey_process_task_search_data))
	{
		$added_by = $survey_process_task_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$survey_process_task_search_data))
	{
		$start_date= $survey_process_task_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$survey_process_task_search_data))
	{
		$end_date= $survey_process_task_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_survey_process_task_list_squery_base = "select *,U.user_name as added_by, AU.user_name as assigned_user from survey_process_task SPT inner join users U on U.user_id = SPT.survey_process_task_added_by left outer join users AU on AU.user_id = SPT.survey_process_task_updated_by inner join survey_process_master SPM on SPM.survey_process_master_id = SPT.survey_process_task_process_id";
	
	$get_survey_process_task_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_survey_process_task_list_sdata = array();
	
	if($task_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." where survey_process_task_id = :task_id";								
		}
		else
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." and survey_process_task_id = :task_id";				
		}
		
		// Data
		$get_survey_process_task_list_sdata[':task_id'] = $task_id;
		
		$filter_count++;
	}

	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." where survey_process_task_process_id = :process_id";
		}
		else
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." and survey_process_task_process_id = :process_id";
		}
		
		// Data
		$get_survey_process_task_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." where survey_process_task_status = :status";
		}
		else
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." and survey_process_task_status = :status";
		}
		
		// Data
		$get_survey_process_task_list_sdata[':status'] = $status;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." where survey_process_task_active = :active";
		}
		else
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." and survey_process_task_active = :active";
		}
		
		// Data
		$get_survey_process_task_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($request_remarks != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." where survey_process_task_requested_remarks = :request_remarks";								
		}
		else
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." and survey_process_task_requested_remarks = :request_remarks";				
		}
		
		// Data
		$get_survey_process_task_list_sdata[':request_remarks'] = $request_remarks;
		
		$filter_count++;
	}
	
	if($response_remarks != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." where survey_process_task_response_remarks = :response_remarks";								
		}
		else
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." and survey_process_task_response_remarks = :response_remarks";				
		}
		
		// Data
		$get_survey_process_task_list_sdata[':response_remarks'] = $response_remarks;
		
		$filter_count++;
	}
	
	if($updated_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." where survey_process_task_updated_by = :updated_by";
		}
		else
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." and survey_process_task_updated_by = :updated_by";
		}
		
		// Data
		$get_survey_process_task_list_sdata[':updated_by'] = $updated_by;
		
		$filter_count++;
	}
	
	if($updated_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." where survey_process_task_updated_on = :updated_on";
		}
		else
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." and survey_process_task_updated_on = :updated_on";
		}
		
		// Data
		$get_survey_process_task_list_sdata[':updated_on'] = $updated_on;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." where survey_process_task_added_by = :added_by";
		}
		else
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." and survey_process_task_added_by = :added_by";
		}
		
		//Data
		$get_survey_process_task_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." where survey_process_task_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." and survey_process_task_added_on >= :start_date";
		}
		
		//Data
		$get_survey_process_task_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." where survey_process_task_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_survey_process_task_list_squery_where = $get_survey_process_task_list_squery_where." and survey_process_task_added_on <= :end_date";
		}
		
		//Data
		$get_survey_process_task_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_survey_process_task_list_squery = $get_survey_process_task_list_squery_base.$get_survey_process_task_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_process_task_list_sstatement = $dbConnection->prepare($get_survey_process_task_list_squery);
		
		$get_survey_process_task_list_sstatement -> execute($get_survey_process_task_list_sdata);
		
		$get_survey_process_task_list_sdetails = $get_survey_process_task_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_process_task_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_process_task_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_process_task_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}
 
/*
PURPOSE : To update Survey Process Task 
INPUT 	: Task ID,Survey Process Task Update Array
OUTPUT 	: Task ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_process_task($task_id,$survey_process_task_update_data)
{
	if(array_key_exists("process_id",$survey_process_task_update_data))
	{	
		$process_id = $survey_process_task_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("status",$survey_process_task_update_data))
	{	
		$status = $survey_process_task_update_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("active",$survey_process_task_update_data))
	{	
		$active = $survey_process_task_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("request_remarks",$survey_process_task_update_data))
	{	
		$request_remarks = $survey_process_task_update_data["request_remarks"];
	}
	else
	{
		$request_remarks = "";
	}
	
	if(array_key_exists("response_remarks",$survey_process_task_update_data))
	{	
		$response_remarks = $survey_process_task_update_data["response_remarks"];
	}
	else
	{
		$response_remarks = "";
	}

	if(array_key_exists("updated_by",$survey_process_task_update_data))
	{	
		$updated_by = $survey_process_task_update_data["updated_by"];
	}
	else
	{
		$updated_by = "";
	}
	
	if(array_key_exists("updated_on",$survey_process_task_update_data))
	{	
		$updated_on = $survey_process_task_update_data["updated_on"];
	}
	else
	{
		$updated_on = "";
	}

	if(array_key_exists("added_by",$survey_process_task_update_data))
	{	
		$added_by = $survey_process_task_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_process_task_update_data))
	{	
		$added_on = $survey_process_task_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $survey_process_task_update_uquery_base = "update survey_process_task set ";  
	
	$survey_process_task_update_uquery_set = "";
	
	$survey_process_task_update_uquery_where = " where survey_process_task_id = :task_id";
	
	$survey_process_task_update_udata = array(":task_id"=>$task_id);
	
	$filter_count = 0;
	
	if($process_id != "")
	{
		$survey_process_task_update_uquery_set = $survey_process_task_update_uquery_set." survey_process_task_process_id = :process_id,";
		$survey_process_task_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}
	
	if($status != "")
	{
		$survey_process_task_update_uquery_set = $survey_process_task_update_uquery_set." survey_process_task_status = :status,";
		$survey_process_task_update_udata[":status"] = $status;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_process_task_update_uquery_set = $survey_process_task_update_uquery_set." survey_process_task_active = :active,";
		$survey_process_task_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($request_remarks != "")
	{
		$survey_process_task_update_uquery_set = $survey_process_task_update_uquery_set." survey_process_task_requested_remarks = :request_remarks,";
		$survey_process_task_update_udata[":request_remarks"] = $request_remarks;
		$filter_count++;
	}
	
	if($response_remarks != "")
	{
		$survey_process_task_update_uquery_set = $survey_process_task_update_uquery_set." survey_process_task_response_remarks = :response_remarks,";
		$survey_process_task_update_udata[":response_remarks"] = $response_remarks;
		$filter_count++;
	}
	
	if($updated_by != "")
	{
		$survey_process_task_update_uquery_set = $survey_process_task_update_uquery_set." survey_process_task_updated_by = :updated_by,";
		$survey_process_task_update_udata[":updated_by"] = $updated_by;
		$filter_count++;
	}
	
	if($updated_on != "")
	{
		$survey_process_task_update_uquery_set = $survey_process_task_update_uquery_set." survey_process_task_updated_on = :updated_on,";
		$survey_process_task_update_udata[":updated_on"] = $updated_on;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_process_task_update_uquery_set = $survey_process_task_update_uquery_set." survey_process_task_added_by = :added_by,";
		$survey_process_task_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_process_task_update_uquery_set = $survey_process_task_update_uquery_set." survey_process_task_added_on = :added_on,";
		$survey_process_task_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_process_task_update_uquery_set = trim($survey_process_task_update_uquery_set,',');
	}
	
	$survey_process_task_update_uquery = $survey_process_task_update_uquery_base.$survey_process_task_update_uquery_set.$survey_process_task_update_uquery_where;
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_process_task_update_ustatement = $dbConnection->prepare($survey_process_task_update_uquery);		
        
        $survey_process_task_update_ustatement -> execute($survey_process_task_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $task_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Survey File Process
INPUT 	: File ID, Process ID, File Start Date, File End Date, Remarks, Added By
OUTPUT 	: File Process ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_file_process($file_id,$process_id,$file_start_date,$file_end_date,$remarks,$added_by)
{
	// Query
    $survey_file_process_iquery = "insert into survey_file_process (survey_file_process_file_id,survey_file_survey_process,survey_file_process_start_date,survey_file_process_end_date,survey_file_process_active,survey_file_process_remarks,
	survey_file_process_updated_by,survey_file_process_updated_on,survey_file_process_added_by,survey_file_process_added_on) values(:file_id,:process_id,:file_start_date,:file_end_date,:active,:remarks,:updated_by,:updated_on,:added_by,:added_on)";  
   
    try
    {
        $dbConnection = get_conn_handle();
        $survey_file_process_istatement = $dbConnection->prepare($survey_file_process_iquery);
        
        // Data
        $survey_file_process_idata = array(':file_id'=>$file_id,':process_id'=>$process_id,':file_start_date'=>$file_start_date,':file_end_date'=>$file_end_date,':active'=>'1',':remarks'=>$remarks,':updated_by'=>'',':added_by'=>$added_by,':updated_on'=>'',':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $survey_file_process_istatement->execute($survey_file_process_idata);
		$survey_file_process_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_file_process_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Survey file Process list
INPUT 	: File Process ID, File ID, Process ID, File Start Date,  File End Date, Active, Updated By, Updated On, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey File Process
BY 		: Lakshmi
*/
function db_get_survey_file_process($survey_file_process_search_data)
{  
	if(array_key_exists("file_process_id",$survey_file_process_search_data))
	{
		$file_process_id = $survey_file_process_search_data["file_process_id"];
	}
	else
	{
		$file_process_id = "";
	}
	
	if(array_key_exists("file_id",$survey_file_process_search_data))
	{
		$file_id = $survey_file_process_search_data["file_id"];
	}
	else
	{
		$file_id = "";
	}
	
	if(array_key_exists("process_id",$survey_file_process_search_data))
	{
		$process_id = $survey_file_process_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("file_start_date",$survey_file_process_search_data))
	{
		$file_start_date = $survey_file_process_search_data["file_start_date"];
	}
	else
	{
		$file_start_date = "";
	}
	
	if(array_key_exists("file_end_date",$survey_file_process_search_data))
	{
		$file_end_date = $survey_file_process_search_data["file_end_date"];
	}
	else
	{
		$file_end_date = "";
	}
	
	if(array_key_exists("active",$survey_file_process_search_data))
	{
		$active = $survey_file_process_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("updated_by",$survey_file_process_search_data))
	{
		$updated_by = $survey_file_process_search_data["updated_by"];
	}
	else
	{
		$updated_by = "";
	}
	
	if(array_key_exists("updated_on",$survey_file_process_search_data))
	{
		$updated_on = $survey_file_process_search_data["updated_on"];
	}
	else
	{
		$updated_on = "";
	}
	
	if(array_key_exists("added_by",$survey_file_process_search_data))
	{
		$added_by = $survey_file_process_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$survey_file_process_search_data))
	{
		$start_date = $survey_file_process_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$survey_file_process_search_data))
	{
		$end_date = $survey_file_process_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}

	$get_survey_file_process_list_squery_base = "select * from survey_file_process SFP inner join users U on U.user_id = SFP.survey_file_process_added_by inner join survey_process_master SPM on SPM.survey_process_master_id=SFP.survey_file_survey_process";
	
	$get_survey_file_process_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_survey_file_process_list_sdata = array();
	
	if($file_process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." where survey_file_process_id = :file_process_id";								
		}
		else
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." and survey_file_process_id = :file_process_id";				
		}
		
		// Data
		$get_survey_file_process_list_sdata[':file_process_id'] = $file_process_id;
		
		$filter_count++;
	}
	
	if($file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." where survey_file_process_file_id = :file_id";								
		}
		else
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." and survey_file_process_file_id = :file_id";				
		}
		
		// Data
		$get_survey_file_process_list_sdata[':file_id'] = $file_id;
		
		$filter_count++;
	}
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." where survey_file_survey_process = :process_id";
		}
		else
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." and survey_file_survey_process = :process_id";				
		}
		
		// Data
		$get_survey_file_process_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($file_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." where survey_file_process_start_date = :file_start_date";
		}
		else
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." and survey_file_process_start_date = :file_start_date";				
		}
		
		// Data
		$get_survey_file_process_list_sdata[':file_start_date'] = $file_start_date;
		
		$filter_count++;
	}
	
	if($file_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." where survey_file_process_end_date = :file_end_date";
		}
		else
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." and survey_file_process_end_date = :file_end_date";				
		}
		
		// Data
		$get_survey_file_process_list_sdata[':file_end_date'] = $file_end_date;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." where survey_file_process_active = :active";								
		}
		else
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." and survey_file_process_active = :active";				
		}
		
		// Data
		$get_survey_file_process_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($updated_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." where survey_file_process_updated_by = :updated_by";								
		}
		else
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." and survey_file_process_updated_by = :updated_by";
		}
		
		//Data
		$get_survey_file_process_list_sdata[':updated_by']  = $updated_by;
		
		$filter_count++;
	}
	
	if($updated_on != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." where survey_file_process_updated_on = :updated_on";								
		}
		else
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." and survey_file_process_updated_on = :updated_on";
		}
		
		//Data
		$get_survey_file_process_list_sdata[':updated_on']  = $updated_on;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." where survey_file_process_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." and survey_file_process_added_by = :added_by";
		}
		
		//Data
		$get_survey_file_process_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." where survey_file_process_added_on >= :start_date";
		}
		else
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." and survey_file_process_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_file_process_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." where survey_file_process_added_on <= :end_date";
		}
		else
		{
			// Query
			$get_survey_file_process_list_squery_where = $get_survey_file_process_list_squery_where." and survey_file_process_added_on <= :end_date";
		}
		
		//Data
		$get_survey_file_process_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
		
	$get_survey_file_process_list_squery = $get_survey_file_process_list_squery_base.$get_survey_file_process_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_file_process_list_sstatement = $dbConnection->prepare($get_survey_file_process_list_squery);
		
		$get_survey_file_process_list_sstatement -> execute($get_survey_file_process_list_sdata);
		
		$get_survey_file_process_list_sdetails = $get_survey_file_process_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_file_process_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_file_process_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_file_process_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
/*
PURPOSE : To update Survey File Process 
INPUT 	: File Process ID,Survey File Process Update Array
OUTPUT 	: File Process ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_file_process($file_process_id,$survey_file_process_update_data)
{
	if(array_key_exists("file_id",$survey_file_process_update_data))
	{	
		$file_id = $survey_file_process_update_data["file_id"];
	}
	else
	{
		$file_id = "";
	}
	
	if(array_key_exists("process_id",$survey_file_process_update_data))
	{	
		$process_id = $survey_file_process_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("file_start_date",$survey_file_process_update_data))
	{	
		$file_start_date = $survey_file_process_update_data["file_start_date"];
	}
	else
	{
		$file_start_date = "";
	}
	
	if(array_key_exists("file_end_date",$survey_file_process_update_data))
	{	
		$file_end_date = $survey_file_process_update_data["file_end_date"];
	}
	else
	{
		$file_end_date = "";
	}
	
	if(array_key_exists("active",$survey_file_process_update_data))
	{	
		$active = $survey_file_process_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$survey_file_process_update_data))
	{	
		$remarks = $survey_file_process_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("updated_by",$survey_file_process_update_data))
	{	
		$updated_by = $survey_file_process_update_data["updated_by"];
	}
	else
	{
		$updated_by = "";
	}
	
	if(array_key_exists("updated_on",$survey_file_process_update_data))
	{	
		$updated_on = $survey_file_process_update_data["updated_on"];
	}
	else
	{
		$updated_on = "";
	}
	
	if(array_key_exists("added_by",$survey_file_process_update_data))
	{	
		$added_by = $survey_file_process_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_file_process_update_data))
	{	
		$added_on = $survey_file_process_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $survey_file_process_update_uquery_base = "update survey_file_process set ";  
	
	$survey_file_process_update_uquery_set = "";
	
	$survey_file_process_update_uquery_where = " where survey_file_process_id = :file_process_id";
	
	$survey_file_process_update_udata = array(":file_process_id"=>$file_process_id);
	
	$filter_count = 0;
	
	if($file_id != "")
	{
		$survey_file_process_update_uquery_set = $survey_file_process_update_uquery_set." survey_file_process_file_id = :file_id,";
		$survey_file_process_update_udata[":file_id"] = $file_id;
		$filter_count++;
	}
	
	if($process_id != "")
	{
		$survey_file_process_update_uquery_set = $survey_file_process_update_uquery_set." survey_file_survey_process = :process_id,";
		$survey_file_process_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}
	
	if($file_start_date != "")
	{
		$survey_file_process_update_uquery_set = $survey_file_process_update_uquery_set." survey_file_process_start_date = :file_start_date,";
		$survey_file_process_update_udata[":file_start_date"] = $file_start_date;
		$filter_count++;
	}
	
	if($file_end_date != "")
	{
		$survey_file_process_update_uquery_set = $survey_file_process_update_uquery_set." survey_file_process_end_date = :file_end_date,";
		$survey_file_process_update_udata[":file_end_date"] = $file_end_date;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_file_process_update_uquery_set = $survey_file_process_update_uquery_set." survey_file_process_active = :active,";
		$survey_file_process_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$survey_file_process_update_uquery_set = $survey_file_process_update_uquery_set." survey_file_process_remarks = :remarks,";
		$survey_file_process_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($updated_by != "")
	{
		$survey_file_process_update_uquery_set = $survey_file_process_update_uquery_set." survey_file_process_updated_by = :updated_by,";
		$survey_file_process_update_udata[":updated_by"] = $updated_by;
		$filter_count++;
	}
	
	if($updated_on != "")
	{
		$survey_file_process_update_uquery_set = $survey_file_process_update_uquery_set." survey_file_process_updated_on = :updated_on,";
		$survey_file_process_update_udata[":updated_on"] = $updated_on;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_file_process_update_uquery_set = $survey_file_process_update_uquery_set." survey_file_process_added_by = :added_by,";
		$survey_file_process_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_file_process_update_uquery_set = $survey_file_process_update_uquery_set." survey_file_process_added_on = :added_on,";
		$survey_file_process_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_file_process_update_uquery_set = trim($survey_file_process_update_uquery_set,',');
	}
	
	$survey_file_process_update_uquery = $survey_file_process_update_uquery_base.$survey_file_process_update_uquery_set.$survey_file_process_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_file_process_update_ustatement = $dbConnection->prepare($survey_file_process_update_uquery);		
        
        $survey_file_process_update_ustatement -> execute($survey_file_process_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $file_process_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Survey File Process User
INPUT 	: File Process ID, User ID, Remarks, Added By
OUTPUT 	: File User ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_file_process_user($file_process_id,$user_id,$remarks,$added_by)
{
	// Query
   $survey_file_process_user_iquery = "insert into survey_file_process_user
   (survey_file_user_file_process_id,survey_file_user_id,survey_file_process_user_active,survey_file_process_user_remarks,survey_file_process_user_added_by,
   survey_file_process_user_added_on)values(:file_process_id,:user_id,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $survey_file_process_user_istatement = $dbConnection->prepare($survey_file_process_user_iquery);
        
        // Data
        $survey_file_process_user_idata = array(':file_process_id'=>$file_process_id,':user_id'=>$user_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,
		':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $survey_file_process_user_istatement->execute($survey_file_process_user_idata);
		$survey_file_process_user_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_file_process_user_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Survey File Process User List
INPUT 	: File User ID, File Process ID, User ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey File Process User
BY 		: Lakshmi
*/
function db_get_survey_file_process_user($survey_file_process_user_search_data)
{  
	if(array_key_exists("file_user_id",$survey_file_process_user_search_data))
	{
		$file_user_id = $survey_file_process_user_search_data["file_user_id"];
	}
	else
	{
		$file_user_id= "";
	}
	
	if(array_key_exists("file_process_id",$survey_file_process_user_search_data))
	{
		$file_process_id = $survey_file_process_user_search_data["file_process_id"];
	}
	else
	{
		$file_process_id = "";
	}
	
	if(array_key_exists("user_id",$survey_file_process_user_search_data))
	{
		$user_id = $survey_file_process_user_search_data["user_id"];
	}
	else
	{
		$user_id = "";
	}
	
	if(array_key_exists("active",$survey_file_process_user_search_data))
	{
		$active = $survey_file_process_user_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$survey_file_process_user_search_data))
	{
		$added_by = $survey_file_process_user_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$survey_file_process_user_search_data))
	{
		$start_date= $survey_file_process_user_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$survey_file_process_user_search_data))
	{
		$end_date= $survey_file_process_user_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_survey_file_process_user_list_squery_base = "select *,U.user_name as added_by,AU.user_name as user_name from survey_file_process_user SFPU inner join users U on U.user_id = SFPU.survey_file_process_user_added_by inner join users AU on AU.user_id = SFPU.survey_file_user_id inner join survey_process SP on SP.survey_process_id = SFPU.survey_file_process_user_id inner join survey_process_master SPM on SPM.survey_process_master_id = SP.process_id";
	
	$get_survey_file_process_user_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_survey_file_process_user_list_sdata = array();
	
	if($file_user_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_user_list_squery_where = $get_survey_file_process_user_list_squery_where." where survey_file_process_user_id = :file_user_id";								
		}
		else
		{
			// Query
			$get_survey_file_process_user_list_squery_where = $get_survey_file_process_user_list_squery_where." and survey_file_process_user_id = :file_user_id";				
		}
		
		// Data
		$get_survey_file_process_user_list_sdata[':file_user_id'] = $file_user_id;
		
		$filter_count++;
	}
	
	if($file_process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_user_list_squery_where = $get_survey_file_process_user_list_squery_where." where survey_file_user_file_process_id = :file_process_id";								
		}
		else
		{
			// Query
			$get_survey_file_process_user_list_squery_where = $get_survey_file_process_user_list_squery_where." and survey_file_user_file_process_id = :file_process_id";				
		}
		
		// Data
		$get_survey_file_process_user_list_sdata[':file_process_id'] = $file_process_id;
		
		$filter_count++;
	}
	
	if($user_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_user_list_squery_where = $get_survey_file_process_user_list_squery_where." where survey_file_user_id = :user_id";								
		}
		else
		{
			// Query
			$get_survey_file_process_user_list_squery_where = $get_survey_file_process_user_list_squery_where." and survey_file_user_id = :user_id";				
		}
		
		// Data
		$get_survey_file_process_user_list_sdata[':user_id'] = $user_id;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_user_list_squery_where = $get_survey_file_process_user_list_squery_where." where survey_file_process_user_active = :active";
		}
		else
		{
			// Query
			$get_survey_file_process_user_list_squery_where = $get_survey_file_process_user_list_squery_where." and survey_file_process_user_active = :active";
		}
		
		// Data
		$get_survey_file_process_user_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_user_list_squery_where = $get_survey_file_process_user_list_squery_where." where survey_file_process_user_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_survey_file_process_user_list_squery_where = $get_survey_file_process_user_list_squery_where." and survey_file_process_user_added_by = :added_by";
		}
		
		//Data
		$get_survey_file_process_user_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_user_list_squery_where = $get_survey_file_process_user_list_squery_where." where survey_file_process_user_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_survey_file_process_user_list_squery_where = $get_survey_file_process_user_list_squery_where." and survey_file_process_user_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_file_process_user_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_file_process_user_list_squery_where = $get_survey_file_process_user_list_squery_where." where survey_file_process_user_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_survey_file_process_user_list_squery_where = $get_survey_file_process_user_list_squery_where." and survey_file_process_user_added_on <= :end_date";
		}
		
		//Data
		$get_survey_file_process_user_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	$get_survey_file_process_user_list_squery = $get_survey_file_process_user_list_squery_base.$get_survey_file_process_user_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_file_process_user_list_sstatement = $dbConnection->prepare($get_survey_file_process_user_list_squery);
		
		$get_survey_file_process_user_list_sstatement -> execute($get_survey_file_process_user_list_sdata);
		
		$get_survey_file_process_user_list_sdetails = $get_survey_file_process_user_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_file_process_user_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_file_process_user_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_file_process_user_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
  /*
PURPOSE : To update Survey File Process User
INPUT 	: File User ID,Survey File Process User Update Array
OUTPUT 	: File User ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_file_process_user($file_user_id,$survey_file_process_user_update_data)
{
	if(array_key_exists("file_process_id",$survey_file_process_user_update_data))
	{	
		$file_process_id = $survey_file_process_user_update_data["file_process_id"];
	}
	else
	{
		$file_process_id = "";
	}
	
	if(array_key_exists("user_id",$survey_file_process_user_update_data))
	{	
		$user_id = $survey_file_process_user_update_data["user_id"];
	}
	else
	{
		$user_id = "";
	}
	
	if(array_key_exists("active",$survey_file_process_user_update_data))
	{	
		$active = $survey_file_process_user_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$survey_file_process_user_update_data))
	{	
		$remarks = $survey_file_process_user_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$survey_file_process_user_update_data))
	{	
		$added_by = $survey_file_process_user_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_file_process_user_update_data))
	{	
		$added_on = $survey_file_process_user_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $survey_file_process_user_update_uquery_base = "update survey_file_process_user set ";  
	
	$survey_file_process_user_update_uquery_set = "";
	
	$survey_file_process_user_update_uquery_where = " where survey_file_process_user_id = :file_user_id";
	
	$survey_file_process_user_update_udata = array(":file_user_id"=>$file_user_id);
	
	$filter_count = 0;
	
	if($file_process_id != "")
	{
		$survey_file_process_user_update_uquery_set = $survey_file_process_user_update_uquery_set." survey_file_user_file_process_id = :file_process_id,";
		$survey_file_process_user_update_udata[":file_process_id"] = $file_process_id;
		$filter_count++;
	}
	
	if($user_id != "")
	{
		$survey_file_process_user_update_uquery_set = $survey_file_process_user_update_uquery_set." survey_file_user_id = :user_id,";
		$survey_file_process_user_update_udata[":user_id"] = $user_id;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_file_process_user_update_uquery_set = $survey_file_process_user_update_uquery_set." survey_file_process_user_active = :active,";
		$survey_file_process_user_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$survey_file_process_user_update_uquery_set = $survey_file_process_user_update_uquery_set." survey_file_process_user_remarks = :remarks,";
		$survey_file_process_user_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_file_process_user_update_uquery_set = $survey_file_process_user_update_uquery_set." survey_file_process_user_added_by = :added_by,";
		$survey_file_process_user_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_file_process_user_update_uquery_set = $survey_file_process_user_update_uquery_set." survey_file_process_user_added_on = :added_on,";
		$survey_file_process_user_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_file_process_user_update_uquery_set = trim($survey_file_process_user_update_uquery_set,',');
	}
	
	$survey_file_process_user_update_uquery = $survey_file_process_user_update_uquery_base.$survey_file_process_user_update_uquery_set.$survey_file_process_user_update_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_file_process_user_update_ustatement = $dbConnection->prepare($survey_file_process_user_update_uquery);		
        
        $survey_file_process_user_update_ustatement -> execute($survey_file_process_user_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $file_user_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Survey Query
INPUT 	: Process ID, Query, Query By, Query Date, Remarks, Added By
OUTPUT 	: Query ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_query($process_id,$query,$query_by,$remarks,$added_by)
{
	// Query
   $survey_query_iquery = "insert into survey_query
   (survey_query_process_id,survey_query,survey_query_by,survey_query_date,survey_query_active,survey_query_remarks,survey_query_added_by,survey_query_added_on) values(:process_id,:query,:query_by,:query_date,:active,:remarks,:added_by,:added_on)";  
  
    try
    {
        $dbConnection = get_conn_handle();
        $survey_query_istatement = $dbConnection->prepare($survey_query_iquery);
        
        // Data
        $survey_query_idata = array(':process_id'=>$process_id,':query'=>$query,':query_by'=>$query_by,':query_date'=>date("Y-m-d"),':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $survey_query_istatement->execute($survey_query_idata);
		$survey_query_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_query_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get survey Query list
INPUT 	: Query ID, Process ID, Query, Query By, Query Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of survey Query
BY 		: Lakshmi
*/
function db_get_survey_query($survey_query_search_data)
{  
	if(array_key_exists("query_id",$survey_query_search_data))
	{
		$query_id = $survey_query_search_data["query_id"];
	}
	else
	{
		$query_id = "";
	}
	
	if(array_key_exists("process_id",$survey_query_search_data))
	{
		$process_id = $survey_query_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("query",$survey_query_search_data))
	{
		$query = $survey_query_search_data["query"];
	}
	else
	{
		$query = "";
	}
	
	if(array_key_exists("query_by",$survey_query_search_data))
	{
		$query_by = $survey_query_search_data["query_by"];
	}
	else
	{
		$query_by = "";
	}
	
	if(array_key_exists("query_date",$survey_query_search_data))
	{
		$query_date = $survey_query_search_data["query_date"];
	}
	else
	{
		$query_date = "";
	}
	
	if(array_key_exists("active",$survey_query_search_data))
	{
		$active = $survey_query_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$survey_query_search_data))
	{
		$added_by = $survey_query_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	if(array_key_exists("start_date",$survey_query_search_data))
	{
		$start_date = $survey_query_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$survey_query_search_data))
	{
		$end_date = $survey_query_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}
	
	if(array_key_exists("project_name",$survey_query_search_data))
	{
		$project_name = $survey_query_search_data["project_name"];
	}
	else
	{
		$project_name = "";
	}
	
	if(array_key_exists("process_name",$survey_query_search_data))
	{
		$process_name = $survey_query_search_data["process_name"];
	}
	else
	{
		$process_name = "";
	}
	
	if(array_key_exists("village_name",$survey_query_search_data))
	{
		$village_name = $survey_query_search_data["village_name"];
	}
	else
	{
		$village_name = "";
	}

	$get_survey_query_list_squery_base = "select *,U.user_name as added_by from survey_query AQR inner join users U on U.user_id = AQR.survey_query_added_by inner join survey_process AP on AP.survey_process_id = AQR.survey_query_process_id inner join survey_details AD on AD.survey_details_id = AP.survey_process_survey_id left outer join survey_project_master SPPM on SPPM.survey_project_master_id = AD.survey_details_project inner join survey_process_master APMM on APMM.survey_process_master_id = AP.process_id inner join village_master VM on VM.village_id = AD.survey_details_village";
	
	$get_survey_query_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_survey_query_list_sdata = array();
	
	if($query_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." where survey_query_id = :query_id";								
		}
		else
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." and survey_query_id = :query_id";				
		}
		
		// Data
		$get_survey_query_list_sdata[':query_id'] = $query_id;
		
		$filter_count++;
	}
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." where survey_query_process_id = :process_id";								
		}
		else
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." and survey_query_process_id = :process_id";				
		}
		
		// Data
		$get_survey_query_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($query != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." where survey_query = :query";								
		}
		else
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." and survey_query = :query";				
		}
		
		// Data
		$get_survey_query_list_sdata[':query'] = $query;
		
		$filter_count++;
	}
	
	if($query_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." where survey_query_by = :query_by";								
		}
		else
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." and survey_query_by = :query_by";				
		}
		
		// Data
		$get_survey_query_list_sdata[':query_by'] = $query_by;
		
		$filter_count++;
	}
	
	if($query_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." where survey_query_query_date = :query_date";								
		}
		else
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." and survey_query_query_date = :query_date";				
		}
		
		// Data
		$get_survey_query_list_sdata[':query_date'] = $query_date;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." where survey_query_active = :active";								
		}
		else
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." and survey_query_active = :active";				
		}
		
		// Data
		$get_survey_query_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." where survey_query_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." and survey_query_added_by = :added_by";
		}
		
		//Data
		$get_survey_query_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." where survey_query_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." and survey_query_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_query_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." where survey_query_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." and survey_query_added_on <= :end_date";
		}
		
		//Data
		$get_survey_query_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($project_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." where SPPM.survey_project_master_id = :project_name";								
		}
		else
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." and SPPM.survey_project_master_id = :project_name";				
		}
		
		// Data
		$get_survey_query_list_sdata[':project_name'] = $project_name;
		
		$filter_count++;
	}
	
	if($process_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." where APMM.survey_process_master_id = :process_name";								
		}
		else
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." and APMM.survey_process_master_id = :process_name";				
		}
		
		// Data
		$get_survey_query_list_sdata[':process_name'] = $process_name;
		
		$filter_count++;
	}
	
	if($village_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." where VM.village_id = :village_name";								
		}
		else
		{
			// Query
			$get_survey_query_list_squery_where = $get_survey_query_list_squery_where." and VM.village_id = :village_name";				
		}
		
		// Data
		$get_survey_query_list_sdata[':village_name'] = $village_name;
		
		$filter_count++;
	}
	
	$get_survey_query_list_squery = $get_survey_query_list_squery_base.$get_survey_query_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_query_list_sstatement = $dbConnection->prepare($get_survey_query_list_squery);
		
		$get_survey_query_list_sstatement -> execute($get_survey_query_list_sdata);
		
		$get_survey_query_list_sdetails = $get_survey_query_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_query_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_query_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_query_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
  /*
PURPOSE : To update survey Query 
INPUT 	: Query ID, survey Query Update Array
OUTPUT 	: Query ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_query($query_id,$survey_query_update_data)
{
	
	if(array_key_exists("process_id",$survey_query_update_data))
	{	
		$process_id = $survey_query_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("query",$survey_query_update_data))
	{	
		$query = $survey_query_update_data["query"];
	}
	else
	{
		$query = "";
	}
	
	if(array_key_exists("query_by",$survey_query_update_data))
	{	
		$query_by = $survey_query_update_data["query_by"];
	}
	else
	{
		$query_by = "";
	}
	
	if(array_key_exists("query_date",$survey_query_update_data))
	{	
		$query_date = $survey_query_update_data["query_date"];
	}
	else
	{
		$query_date = "";
	}
	
	if(array_key_exists("active",$survey_query_update_data))
	{	
		$active = $survey_query_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$survey_query_update_data))
	{	
		$remarks = $survey_query_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$survey_query_update_data))
	{	
		$added_by = $survey_query_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_query_update_data))
	{	
		$added_on = $survey_query_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $survey_query_update_uquery_base = "update survey_query set";  
	
	$survey_query_update_uquery_set = "";
	
	$survey_query_update_uquery_where = " where survey_query_id = :query_id";
	
	$survey_query_update_udata = array(":query_id"=>$query_id);
	
	$filter_count = 0;
	
	if($process_id != "")
	{
		$survey_query_update_uquery_set = $survey_query_update_uquery_set." survey_query_process_id = :process_id,";
		$survey_query_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}
	
	if($query != "")
	{
		$survey_query_update_uquery_set = $survey_query_update_uquery_set." survey_query = :query,";
		$survey_query_update_udata[":query"] = $query;
		$filter_count++;
	}
	
	if($query_by != "")
	{
		$survey_query_update_uquery_set = $survey_query_update_uquery_set." survey_query_by = :query_by,";
		$survey_query_update_udata[":query_by"] = $query_by;
		$filter_count++;
	}
	
	if($query_date != "")
	{
		$survey_query_update_uquery_set = $survey_query_update_uquery_set." survey_query_query_date = :query_date,";
		$survey_query_update_udata[":query_date"] = $query_date;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_query_update_uquery_set = $survey_query_update_uquery_set." survey_query_active = :active,";
		$survey_query_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$survey_query_update_uquery_set = $survey_query_update_uquery_set." survey_query_remarks = :remarks,";
		$survey_query_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_query_update_uquery_set = $survey_query_update_uquery_set." survey_query_added_by = :added_by,";
		$survey_query_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_query_update_uquery_set = $survey_query_update_uquery_set." survey_query_added_on = :added_on,";
		$survey_query_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_query_update_uquery_set = trim($survey_query_update_uquery_set,',');
	}
	
	$survey_query_update_uquery = $survey_query_update_uquery_base.$survey_query_update_uquery_set.$survey_query_update_uquery_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_query_update_ustatement = $dbConnection->prepare($survey_query_update_uquery);		
        
        $survey_query_update_ustatement -> execute($survey_query_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $query_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
} 

 /*
PURPOSE : To add new Survey Response
INPUT 	: Query ID, response, Follow up Date, Status, Remarks, Added By
OUTPUT 	: Response ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_response($query_id,$response,$follow_up_date,$status,$remarks,$added_by)
{
	// response
   $survey_response_iresponse = "insert into survey_response
   (survey_response_query_id,survey_response,survey_response_follow_up_date,survey_response_status,survey_response_active,survey_response_remarks,survey_response_added_by,survey_response_added_on) values(:query_id,:response,:follow_up_date,:status,:active,:remarks,:added_by,:added_on)";  
  
    try
    {
        $dbConnection = get_conn_handle();
        $survey_response_istatement = $dbConnection->prepare($survey_response_iresponse);
        
        // Data
        $survey_response_idata = array(':query_id'=>$query_id,':response'=>$response,':follow_up_date'=>$follow_up_date,':status'=>$status,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));
		$dbConnection->beginTransaction();
        $survey_response_istatement->execute($survey_response_idata);
		$survey_response_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_response_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Survey Response list
INPUT 	: Response ID, Query ID, response, Follow Up Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey response
BY 		: Lakshmi
*/
function db_get_survey_response($survey_response_search_data)
{  
	if(array_key_exists("response_id",$survey_response_search_data))
	{
		$response_id = $survey_response_search_data["response_id"];
	}
	else
	{
		$response_id = "";
	}
	
	if(array_key_exists("query_id",$survey_response_search_data))
	{
		$query_id = $survey_response_search_data["query_id"];
	}
	else
	{
		$query_id = "";
	}
	
	if(array_key_exists("response",$survey_response_search_data))
	{
		$response = $survey_response_search_data["response"];
	}
	else
	{
		$response = "";
	}
	
	if(array_key_exists("follow_up_date",$survey_response_search_data))
	{
		$follow_up_date = $survey_response_search_data["follow_up_date"];
	}
	else
	{
		$follow_up_date = "";
	}
	
	if(array_key_exists("status",$survey_response_search_data))
	{
		$status = $survey_response_search_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("active",$survey_response_search_data))
	{
		$active = $survey_response_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$survey_response_search_data))
	{
		$added_by = $survey_response_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	if(array_key_exists("start_date",$survey_response_search_data))
	{
		$start_date = $survey_response_search_data["start_date"];
	}
	else
	{
		$start_date = "";
	}
	
	if(array_key_exists("end_date",$survey_response_search_data))
	{
		$end_date = $survey_response_search_data["end_date"];
	}
	else
	{
		$end_date = "";
	}

	$get_survey_response_list_sresponse_base = "select *,U.user_name as added_by from survey_response AQR inner join users U on U.user_id = AQR.survey_response_added_by";
	
	$get_survey_response_list_sresponse_where = "";
	$filter_count = 0;
	
	// Data
	$get_survey_response_list_sdata = array();
	
	if($response_id != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." where survey_response_id = :response_id";								
		}
		else
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." and survey_response_id = :response_id";				
		}
		
		// Data
		$get_survey_response_list_sdata[':response_id'] = $response_id;
		
		$filter_count++;
	}
	
	if($query_id != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." where survey_response_query_id = :query_id";								
		}
		else
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." and survey_response_query_id = :query_id";				
		}
		
		// Data
		$get_survey_response_list_sdata[':query_id'] = $query_id;
		
		$filter_count++;
	}
	
	if($response != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." where survey_response = :response";								
		}
		else
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." and survey_response = :response";				
		}
		
		// Data
		$get_survey_response_list_sdata[':response'] = $response;
		
		$filter_count++;
	}
	
	if($follow_up_date != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." where survey_response_follow_up_date = :follow_up_date";								
		}
		else
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." and survey_response_follow_up_date = :follow_up_date";				
		}
		
		// Data
		$get_survey_response_list_sdata[':follow_up_date'] = $follow_up_date;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." where survey_response_status = :status";								
		}
		else
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." and survey_response_status = :status";				
		}
		
		// Data
		$get_survey_response_list_sdata[':status'] = $status;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." where survey_response_active = :active";								
		}
		else
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." and survey_response_active = :active";				
		}
		
		// Data
		$get_survey_response_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." where survey_response_added_by = :added_by";								
		}
		else
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." and survey_response_added_by = :added_by";
		}
		
		//Data
		$get_survey_response_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." where survey_response_added_on >= :start_date";								
		}
		else
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." and survey_response_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_response_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." where survey_response_added_on <= :end_date";								
		}
		else
		{
			// response
			$get_survey_response_list_sresponse_where = $get_survey_response_list_sresponse_where." and survey_response_added_on <= :end_date";
		}
		
		//Data
		$get_survey_response_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_survey_response_list_sresponse_order = " order by survey_response_added_on desc";
	
	$get_survey_response_list_sresponse = $get_survey_response_list_sresponse_base.$get_survey_response_list_sresponse_where.$get_survey_response_list_sresponse_order;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_response_list_sstatement = $dbConnection->prepare($get_survey_response_list_sresponse);
		
		$get_survey_response_list_sstatement -> execute($get_survey_response_list_sdata);
		
		$get_survey_response_list_sdetails = $get_survey_response_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_response_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_response_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_response_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
  /*
PURPOSE : To update survey Response 
INPUT 	: Response ID, survey Response Update Array
OUTPUT 	: Response ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_response($response_id,$survey_response_update_data)
{
	
	if(array_key_exists("query_id",$survey_response_update_data))
	{	
		$query_id = $survey_response_update_data["query_id"];
	}
	else
	{
		$query_id = "";
	}
	
	if(array_key_exists("response",$survey_response_update_data))
	{	
		$response = $survey_response_update_data["response"];
	}
	else
	{
		$response = "";
	}
	
	if(array_key_exists("follow_up_date",$survey_response_update_data))
	{	
		$follow_up_date = $survey_response_update_data["follow_up_date"];
	}
	else
	{
		$follow_up_date = "";
	}
	
	if(array_key_exists("status",$survey_response_update_data))
	{	
		$status = $survey_response_update_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("active",$survey_response_update_data))
	{	
		$active = $survey_response_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$survey_response_update_data))
	{	
		$remarks = $survey_response_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$survey_response_update_data))
	{	
		$added_by = $survey_response_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_response_update_data))
	{	
		$added_on = $survey_response_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// response
    $survey_response_update_uresponse_base = "update survey_response set";  
	
	$survey_response_update_uresponse_set = "";
	
	$survey_response_update_uresponse_where = " where survey_response_id = :response_id";
	
	$survey_response_update_udata = array(":response_id"=>$response_id);
	
	$filter_count = 0;
	
	if($query_id != "")
	{
		$survey_response_update_uresponse_set = $survey_response_update_uresponse_set." survey_response_query_id = :query_id,";
		$survey_response_update_udata[":query_id"] = $query_id;
		$filter_count++;
	}
	
	if($response != "")
	{
		$survey_response_update_uresponse_set = $survey_response_update_uresponse_set." survey_response = :response,";
		$survey_response_update_udata[":response"] = $response;
		$filter_count++;
	}
	
	if($follow_up_date != "")
	{
		$survey_response_update_uresponse_set = $survey_response_update_uresponse_set." survey_response_follow_up_date = :follow_up_date,";
		$survey_response_update_udata[":follow_up_date"] = $follow_up_date;
		$filter_count++;
	}
	
	if($status != "")
	{
		$survey_response_update_uresponse_set = $survey_response_update_uresponse_set." survey_response_status = :status,";
		$survey_response_update_udata[":status"] = $status;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_response_update_uresponse_set = $survey_response_update_uresponse_set." survey_response_active = :active,";
		$survey_response_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$survey_response_update_uresponse_set = $survey_response_update_uresponse_set." survey_response_remarks = :remarks,";
		$survey_response_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_response_update_uresponse_set = $survey_response_update_uresponse_set." survey_response_added_by = :added_by,";
		$survey_response_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_response_update_uresponse_set = $survey_response_update_uresponse_set." survey_response_added_on = :added_on,";
		$survey_response_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_response_update_uresponse_set = trim($survey_response_update_uresponse_set,',');
	}
	
	$survey_response_update_uresponse = $survey_response_update_uresponse_base.$survey_response_update_uresponse_set.$survey_response_update_uresponse_where;
    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_response_update_ustatement = $dbConnection->prepare($survey_response_update_uresponse);		
        
        $survey_response_update_ustatement -> execute($survey_response_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $response_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To Delete Survey Process 
INPUT 	: Survey ID, Survey Process Update Array
OUTPUT 	: Survey ID; Message of success or failure
BY 		: Lakshmi
*/
function db_delete_all_process($survey_process_update_data)
{
	if(array_key_exists("survey_id",$survey_process_update_data))
	{	
		$survey_id = $survey_process_update_data["survey_id"];
	}
	else
	{
		$survey_id = "";
	}

	// Query
    $survey_process_update_uquery_base = "update survey_process";  
	
	$survey_process_update_uquery_set = " set survey_process_active = 0";
	
	$survey_process_update_uquery_where = " where survey_process_survey_id = :survey_id";
	
	$survey_process_update_udata = array(":survey_id"=>$survey_id);
	
	$filter_count = 0;
	
	if($filter_count > 0)
	{
		$survey_process_update_uquery_set = trim($survey_process_update_uquery_set,',');
	}
	
	$survey_process_update_uquery = $survey_process_update_uquery_base.$survey_process_update_uquery_set.$survey_process_update_uquery_where;
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_process_update_ustatement = $dbConnection->prepare($survey_process_update_uquery);		
        
        $survey_process_update_ustatement -> execute($survey_process_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To Delete survey File 
INPUT 	: File ID, survey File Update Array
OUTPUT 	: File ID; Message of success or failure
BY 		: Lakshmi
*/
function db_delete_all_survey_file($survey_file_update_data)
{
	if(array_key_exists("survey_id",$survey_file_update_data))
	{	
		$survey_id = $survey_file_update_data["survey_id"];
	}
	else
	{
		$survey_id = "";
	}

	// Query
    $survey_file_update_uquery_base = "update survey_file";   
	
	$survey_file_update_uquery_set = " set survey_file_active = 0";
	
	$survey_file_update_uquery_where = " where survey_file_details_id = :survey_id";
	
	$survey_file_update_udata = array(":survey_id"=>$survey_id);
	
	$filter_count = 0;
	
	if($filter_count > 0)
	{
		$survey_file_update_uquery_set = trim($survey_file_update_uquery_set,',');
	}
	
	$survey_file_update_uquery = $survey_file_update_uquery_base.$survey_file_update_uquery_set.$survey_file_update_uquery_where;
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_file_update_ustatement = $dbConnection->prepare($survey_file_update_uquery);		
        
        $survey_file_update_ustatement -> execute($survey_file_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
} 

/*
PURPOSE : To add new Survey Single Process
INPUT 	: Survey ID, Process ID, Start Date, End Date, Remarks, Added By
OUTPUT 	: Survey Process ID, success or failure message
BY 		: Lakshmi
*/
function db_add_survey_single_process($survey_id,$process_id,$process_start_date,$process_end_date,$remarks,$added_by)
{
	// Query
   $survey_single_process_iquery = "insert into survey_single_process
   (survey_single_process_survey_id,single_process_id,survey_single_process_start_date,survey_single_process_end_date,survey_single_process_active,survey_single_process_remarks,
   survey_single_process_added_by,survey_single_process_added_on) values (:survey_id,:process_id,:process_start_date,:process_end_date,:active,:remarks,:added_by,:added_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        $survey_single_process_istatement = $dbConnection->prepare($survey_single_process_iquery);
        
        // Data
        $survey_single_process_idata = array(':survey_id'=>$survey_id,':process_id'=>$process_id,':process_start_date'=>$process_start_date,':process_end_date'=>$process_end_date,
		':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	
	
		$dbConnection->beginTransaction();
        $survey_single_process_istatement->execute($survey_single_process_idata);
		$survey_single_process_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
       
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_single_process_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Survey Single Process List
INPUT 	: Survey Process ID, Survey ID, Process ID, Start Date, End Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Process 
BY 		: Lakshmi
*/
function db_get_survey_single_process($survey_single_process_search_data)
{  
	if(array_key_exists("survey_process_id",$survey_single_process_search_data))
	{
		$survey_process_id = $survey_single_process_search_data["survey_process_id"];
	}
	else
	{
		$survey_process_id= "";
	}
	
	if(array_key_exists("survey_id",$survey_single_process_search_data))
	{
		$survey_id = $survey_single_process_search_data["survey_id"];
	}
	else
	{
		$survey_id = "";
	}
	
	if(array_key_exists("process_id",$survey_single_process_search_data))
	{
		$process_id = $survey_single_process_search_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("process_start_date",$survey_single_process_search_data))
	{
		$process_start_date = $survey_single_process_search_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}
	
	if(array_key_exists("process_end_date",$survey_single_process_search_data))
	{
		$process_end_date = $survey_single_process_search_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}
	
	if(array_key_exists("active",$survey_single_process_search_data))
	{
		$active = $survey_single_process_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("added_by",$survey_single_process_search_data))
	{
		$added_by = $survey_single_process_search_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("start_date",$survey_single_process_search_data))
	{
		$start_date= $survey_single_process_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$survey_single_process_search_data))
	{
		$end_date= $survey_single_process_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_survey_single_process_list_squery_base = "select * from survey_single_process SSP inner join users U on U.user_id = SSP.survey_single_process_added_by inner join survey_process_master SPM on SPM.survey_process_master_id = SSP.single_process_id";
	
	$get_survey_single_process_list_squery_where = "";
	$filter_count = 0;
	
	// Data
	$get_survey_single_process_list_sdata = array();
	
	if($survey_process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." where survey_single_process_id = :survey_process_id";								
		}
		else
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." and survey_single_process_id = :survey_process_id";				
		}
		
		// Data
		$get_survey_single_process_list_sdata[':survey_process_id'] = $survey_process_id;
		
		$filter_count++;
	}
	
	if($survey_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." where survey_single_process_survey_id = :survey_id";								
		}
		else
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." and survey_single_process_survey_id = :survey_id";				
		}
		
		// Data
		$get_survey_single_process_list_sdata[':survey_id'] = $survey_id;
		
		$filter_count++;
	}
	
	if($process_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." where single_process_id = :process_id";								
		}
		else
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." and single_process_id = :process_id";				
		}
		
		// Data
		$get_survey_single_process_list_sdata[':process_id'] = $process_id;
		
		$filter_count++;
	}
	
	if($process_start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." where survey_single_process_start_date = :process_start_date";
		}
		else
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." and survey_single_process_start_date = :process_start_date";
		}
		
		// Data
		$get_survey_single_process_list_sdata[':process_start_date'] = $process_start_date;
		
		$filter_count++;
	}
	
	if($process_end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." where survey_single_process_end_date = :process_end_date";								
		}
		else
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." and survey_single_process_end_date = :process_end_date";				
		}
		
		// Data
		$get_survey_single_process_list_sdata[':process_end_date'] = $process_end_date;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." where survey_single_process_active = :active";								
		}
		else
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." and survey_single_process_active = :active";				
		}
		
		// Data
		$get_survey_single_process_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." where survey_single_process_added_by = :added_by";
		}
		else
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." and survey_single_process_added_by = :added_by";
		}
		
		//Data
		$get_survey_single_process_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." where survey_single_process_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." and survey_single_process_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_single_process_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." where survey_single_process_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_survey_single_process_list_squery_where = $get_survey_single_process_list_squery_where." and survey_single_process_added_on <= :end_date";
		}
		
		//Data
		$get_survey_single_process_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_survey_single_process_list_squery_order = " order by survey_process_master_order ASC";
	$get_survey_single_process_list_squery = $get_survey_single_process_list_squery_base.$get_survey_single_process_list_squery_where.$get_survey_single_process_list_squery_order;	

	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_single_process_list_sstatement = $dbConnection->prepare($get_survey_single_process_list_squery);
		
		$get_survey_single_process_list_sstatement -> execute($get_survey_single_process_list_sdata);
		
		$get_survey_single_process_list_sdetails = $get_survey_single_process_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_single_process_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_single_process_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_single_process_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 
  /*
PURPOSE : To update Survey Single Process 
INPUT 	: Survey Process ID,Survey Process Update Array
OUTPUT 	: Survey Process ID; Message of success or failure
BY 		: Lakshmi
*/
function db_update_survey_single_process($survey_process_id,$survey_single_process_update_data)
{
	if(array_key_exists("survey_id",$survey_single_process_update_data))
	{	
		$survey_id = $survey_single_process_update_data["survey_id"];
	}
	else
	{
		$survey_id = "";
	}
	
	if(array_key_exists("process_id",$survey_single_process_update_data))
	{	
		$process_id = $survey_single_process_update_data["process_id"];
	}
	else
	{
		$process_id = "";
	}
	
	if(array_key_exists("process_start_date",$survey_single_process_update_data))
	{	
		$process_start_date = $survey_single_process_update_data["process_start_date"];
	}
	else
	{
		$process_start_date = "";
	}
	
	if(array_key_exists("process_end_date",$survey_single_process_update_data))
	{	
		$process_end_date = $survey_single_process_update_data["process_end_date"];
	}
	else
	{
		$process_end_date = "";
	}
	
	if(array_key_exists("active",$survey_single_process_update_data))
	{	
		$active = $survey_single_process_update_data["active"];
	}
	else
	{
		$active = "";
	}

	if(array_key_exists("remarks",$survey_single_process_update_data))
	{	
		$remarks = $survey_single_process_update_data["remarks"];
	}
	else
	{
		$remarks = "";
	}
	
	if(array_key_exists("added_by",$survey_single_process_update_data))
	{	
		$added_by = $survey_single_process_update_data["added_by"];
	}
	else
	{
		$added_by = "";
	}
	
	if(array_key_exists("added_on",$survey_single_process_update_data))
	{	
		$added_on = $survey_single_process_update_data["added_on"];
	}
	else
	{
		$added_on = "";
	}
	
	// Query
    $survey_single_process_update_uquery_base = "update survey_single_process set ";  
	
	$survey_single_process_update_uquery_set = "";
	
	$survey_single_process_update_uquery_where = " where survey_single_process_id = :survey_process_id";
	
	$survey_single_process_update_udata = array(":survey_process_id"=>$survey_process_id);
	
	$filter_count = 0;
	
	if($survey_id != "")
	{
		$survey_single_process_update_uquery_set = $survey_single_process_update_uquery_set." survey_single_process_survey_id = :survey_id,";
		$survey_single_process_update_udata[":survey_id"] = $survey_id;
		$filter_count++;
	}
	
	if($process_id != "")
	{
		$survey_single_process_update_uquery_set = $survey_single_process_update_uquery_set." single_process_id = :process_id,";
		$survey_single_process_update_udata[":process_id"] = $process_id;
		$filter_count++;
	}
	
	if($process_start_date != "")
	{
		$survey_single_process_update_uquery_set = $survey_single_process_update_uquery_set." survey_single_process_start_date = :process_start_date,";
		$survey_single_process_update_udata[":process_start_date"] = $process_start_date;
		$filter_count++;
	}
	
	if($process_end_date != "")
	{
		$survey_single_process_update_uquery_set = $survey_single_process_update_uquery_set." survey_single_process_end_date = :process_end_date,";
		$survey_single_process_update_udata[":process_end_date"] = $process_end_date;
		$filter_count++;
	}
	
	if($active != "")
	{
		$survey_single_process_update_uquery_set = $survey_single_process_update_uquery_set." survey_single_process_active = :active,";
		$survey_single_process_update_udata[":active"] = $active;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$survey_single_process_update_uquery_set = $survey_single_process_update_uquery_set." survey_single_process_remarks = :remarks,";
		$survey_single_process_update_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$survey_single_process_update_uquery_set = $survey_single_process_update_uquery_set." survey_single_process_added_by = :added_by,";
		$survey_single_process_update_udata[":added_by"] = $added_by;
		$filter_count++;
	}
	
	if($added_on != "")
	{
		$survey_single_process_update_uquery_set = $survey_single_process_update_uquery_set." survey_single_process_added_on = :added_on,";
		$survey_single_process_update_udata[":added_on"] = $added_on;		
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$survey_single_process_update_uquery_set = trim($survey_single_process_update_uquery_set,',');
	}
	
	$survey_single_process_update_uquery = $survey_single_process_update_uquery_base.$survey_single_process_update_uquery_set.$survey_single_process_update_uquery_where;
 
    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_single_process_update_ustatement = $dbConnection->prepare($survey_single_process_update_uquery);		
        
        $survey_single_process_update_ustatement -> execute($survey_single_process_update_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_process_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

?>