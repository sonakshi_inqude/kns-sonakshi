<?php

$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');



/*

LIST OF TBDs

1. Wherever there is name search, there should be a wildcard

*/

/*

PURPOSE : To add new Material Stock

INPUT 	: Material Id, Quantity, Uom, Location, Min Qty, Max Qty, Re-order Qty, Re-order Level,Remarks, Last Updated By

OUTPUT 	: Stock Id, success or failure message

BY 		: Lakshmi

*/
function db_add_material_stock($material_id,$quantity,$uom,$location,$project,$min_qty,$max_qty,$re_order_qty,$re_order_level,$remarks,$last_updated_by)
{

	// Query
   $material_stock_iquery = "insert into material_stock (material_id,material_stock_quantity,material_stock_uom,material_stock_location,material_stock_project,material_stock_min_qty,material_stock_max_qty,material_stock_re_order_qty,material_stock_re_order_level,material_stock_active,material_stock_remarks,material_stock_last_updated_by,material_stock_last_updated_on) values (:material_id,:quantity,:uom,:location,:project,:min_qty,:max_qty,:re_order_qty,:re_order_level,:active,:remarks,:last_updated_by,:last_updated_on)";  
	

    try

    {

        $dbConnection = get_conn_handle();

        $material_stock_istatement = $dbConnection->prepare($material_stock_iquery);

        

        // Data
        $material_stock_idata = array(':material_id'=>$material_id,':quantity'=>$quantity,':uom'=>$uom,':location'=>$location,"project"=>$project,':min_qty'=>$min_qty,':max_qty'=>$max_qty,':re_order_qty'=>$re_order_qty,':re_order_level'=>$re_order_level,':active'=>'1',':remarks'=>$remarks,':last_updated_by'=>$last_updated_by,':last_updated_on'=>date("Y-m-d H:i:s"));	
	

		$dbConnection->beginTransaction();

        $material_stock_istatement->execute($material_stock_idata);

		$material_stock_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

       

        $return["status"] = SUCCESS;

		$return["data"]   = $material_stock_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get Material Stock list

INPUT 	: Material Id, Quantity, Uom, Location, Active, Last Updated By,Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Material Stock

BY 		: Lakshmi

*/

function db_get_material_stock($material_stock_search_data)

{  

 

	if(array_key_exists("stock_id",$material_stock_search_data))

	{

		$stock_id = $material_stock_search_data["stock_id"];

	}

	else

	{

		$stock_id= "";

	}

	

	if(array_key_exists("material_id",$material_stock_search_data))

	{

		$material_id = $material_stock_search_data["material_id"];

	}

	else

	{

		$material_id = "";

	}

	

	if(array_key_exists("quantity",$material_stock_search_data))

	{

		$quantity = $material_stock_search_data["quantity"];

	}

	else

	{

		$quantity = "";

	}

	

	if(array_key_exists("uom",$material_stock_search_data))

	{

		$uom = $material_stock_search_data["uom"];

	}

	else

	{

		$uom = "";

	}

	

	if(array_key_exists("location",$material_stock_search_data))

	{

		$location = $material_stock_search_data["location"];

	}

	else

	{

		$location = "";

	}

	
	if(array_key_exists("project",$material_stock_search_data))
	{
		$project = $material_stock_search_data["project"];
	}
	else
	{
		$project = "";
	}
	
	if(array_key_exists("active",$material_stock_search_data))

	{

		$active = $material_stock_search_data["active"];

	}

	else

	{

		$active = "";

	}

	

	if(array_key_exists("last_updated_by",$material_stock_search_data))

	{

		$last_updated_by = $material_stock_search_data["last_updated_by"];

	}

	else

	{

		$last_updated_by = "";

	}

	

	if(array_key_exists("start_date",$material_stock_search_data))

	{

		$start_date= $material_stock_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}

	

	if(array_key_exists("end_date",$material_stock_search_data))

	{

		$end_date= $material_stock_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}

	
	$get_material_stock_list_squery_base = "select * from material_stock  MS inner join stock_material_master SMM on SMM.stock_material_id= MS.material_id inner join stock_unit_measure_master SUMM on SUMM.stock_unit_id= MS.material_stock_uom inner join users U on U.user_id=MS.material_stock_last_updated_by inner join stock_project SP on SP.stock_project_id= MS.material_stock_project left outer join stock_location_master SLM on SLM.stock_location_id = MS.material_stock_location";
	$get_material_stock_list_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_material_stock_list_sdata = array();

	

	if($stock_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_id = :stock_id";								

		}

		else

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_id = :stock_id";				

		}

		

		// Data

		$get_material_stock_list_sdata[':stock_id'] = $stock_id;

		

		$filter_count++;

	}

	

	if($material_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_id = :material_id";								

		}

		else

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_id = :material_id";				

		}

		

		// Data

		$get_material_stock_list_sdata[':material_id'] = $material_id;

		

		$filter_count++;

	}

	

	if($quantity != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_quantity = :quantity";								

		}

		else

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_quantity = :quantity";				

		}

		

		// Data

		$get_material_stock_list_sdata[':quantity'] = $quantity;

		

		$filter_count++;

	}

	

	if($uom != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_uom = :uom";								

		}

		else

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_uom = :uom";				

		}

		

		// Data

		$get_material_stock_list_sdata[':uom'] = $uom;

		

		$filter_count++;

	}

	

	if($location != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_location = :location";								

		}

		else

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_location = :location";				

		}

		

		// Data

		$get_material_stock_list_sdata[':location'] = $location;

		

		$filter_count++;

	}
	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_project = :project";								
		}
		else
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_project = :project";				
		}
		
		// Data
		$get_material_stock_list_sdata[':project'] = $project;
		
		$filter_count++;
	}

	

	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_active = :active";								

		}

		else

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_active = :active";				

		}

		

		// Data

		$get_material_stock_list_sdata[':active']  = $active;

		

		$filter_count++;

	}

	

	if($last_updated_by!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_last_updated_by >= :last_updated_by";								

		}

		else

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_last_updated_by >= :last_updated_by";				

		}

		

		//Data

		$get_material_stock_list_sdata[':last_updated_by']  = $last_updated_by;

		

		$filter_count++;

	}

	

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_last_updated_on >= :start_date";								

		}

		else

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_last_updated_on >= :start_date";				

		}

		

		//Data

		$get_material_stock_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_last_updated_on <= :end_date";								

		}

		else

		{

			// Query

			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_last_updated_on <= :end_date";				

		}

		

		//Data

		$get_material_stock_list_sdata['end_date']  = $end_date;

		

		$filter_count++;

	}

	$get_material_stock_list_order = " order by material_stock_last_updated_on DESC";

	$get_material_stock_list_squery = $get_material_stock_list_squery_base.$get_material_stock_list_squery_where.$get_material_stock_list_order;

	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_material_stock_list_sstatement = $dbConnection->prepare($get_material_stock_list_squery);

		

		$get_material_stock_list_sstatement -> execute($get_material_stock_list_sdata);

		

		$get_material_stock_list_sdetails = $get_material_stock_list_sstatement -> fetchAll();

		

		if(FALSE === $get_material_stock_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_material_stock_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_material_stock_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

 }

 /*
PURPOSE : To get sum of material Stock
INPUT 	: Material Id, Quantity, Uom, Location, Active, Last Updated By,Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Material Stock
BY 		: Sonakshi D
*/
function db_get_sum_material_stock($material_stock_search_data)
{  
 
	if(array_key_exists("stock_id",$material_stock_search_data))
	{
		$stock_id = $material_stock_search_data["stock_id"];
	}
	else
	{
		$stock_id= "";
	}
	
	if(array_key_exists("material_id",$material_stock_search_data))
	{
		$material_id = $material_stock_search_data["material_id"];
	}
	else
	{
		$material_id = "";
	}
	
	if(array_key_exists("quantity",$material_stock_search_data))
	{
		$quantity = $material_stock_search_data["quantity"];
	}
	else
	{
		$quantity = "";
	}
	
	if(array_key_exists("uom",$material_stock_search_data))
	{
		$uom = $material_stock_search_data["uom"];
	}
	else
	{
		$uom = "";
	}
	
	if(array_key_exists("location",$material_stock_search_data))
	{
		$location = $material_stock_search_data["location"];
	}
	else
	{
		$location = "";
	}
	
	if(array_key_exists("project",$material_stock_search_data))
	{
		$project = $material_stock_search_data["project"];
	}
	else
	{
		$project = "";
	}
	
	if(array_key_exists("active",$material_stock_search_data))
	{
		$active = $material_stock_search_data["active"];
	}
	else
	{
		$active = "";
	}
	
	if(array_key_exists("last_updated_by",$material_stock_search_data))
	{
		$last_updated_by = $material_stock_search_data["last_updated_by"];
	}
	else
	{
		$last_updated_by = "";
	}
	
	if(array_key_exists("start_date",$material_stock_search_data))
	{
		$start_date= $material_stock_search_data["start_date"];
	}
	else
	{
		$start_date= "";
	}
	
	if(array_key_exists("end_date",$material_stock_search_data))
	{
		$end_date= $material_stock_search_data["end_date"];
	}
	else
	{
		$end_date= "";
	}
	
	$get_material_stock_list_squery_base = "select *,sum(MS.material_stock_quantity*SMM.stock_material_price) as total_material_value from material_stock  MS inner join stock_material_master SMM on SMM.stock_material_id= MS.material_id inner join stock_unit_measure_master SUMM on SUMM.stock_unit_id= MS.material_stock_uom inner join users U on U.user_id=MS.material_stock_last_updated_by inner join stock_location_master SLM on SLM.stock_location_id = MS.material_stock_location";
	
	$get_material_stock_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_material_stock_list_sdata = array();
	
	if($stock_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_id = :stock_id";								
		}
		else
		{
			// Query
			$get_material_stock_list_squery_where = $w." and material_stock_id = :stock_id";				
		}
		
		// Data
		$get_material_stock_list_sdata[':stock_id'] = $stock_id;
		
		$filter_count++;
	}
	
	if($material_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_id = :material_id";								
		}
		else
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_id = :material_id";				
		}
		
		// Data
		$get_material_stock_list_sdata[':material_id'] = $material_id;
		
		$filter_count++;
	}
	
	if($quantity != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_quantity = :quantity";								
		}
		else
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_quantity = :quantity";				
		}
		
		// Data
		$get_material_stock_list_sdata[':quantity'] = $quantity;
		
		$filter_count++;
	}
	
	if($uom != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_uom = :uom";								
		}
		else
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_uom = :uom";				
		}
		
		// Data
		$get_material_stock_list_sdata[':uom'] = $uom;
		
		$filter_count++;
	}
	
	if($location != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_location = :location";								
		}
		else
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_location = :location";				
		}
		
		// Data
		$get_material_stock_list_sdata[':location'] = $location;
		
		$filter_count++;
	}
	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_project = :project";								
		}
		else
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_project = :project";				
		}
		
		// Data
		$get_material_stock_list_sdata[':project'] = $project;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_active = :active";								
		}
		else
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_active = :active";				
		}
		
		// Data
		$get_material_stock_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($last_updated_by!= "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_last_updated_by >= :last_updated_by";								
		}
		else
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_last_updated_by >= :last_updated_by";				
		}
		
		//Data
		$get_material_stock_list_sdata[':last_updated_by']  = $last_updated_by;
		
		$filter_count++;
	}
	
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_last_updated_on >= :start_date";								
		}
		else
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_last_updated_on >= :start_date";				
		}
		
		//Data
		$get_material_stock_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." where material_stock_last_updated_on <= :end_date";								
		}
		else
		{
			// Query
			$get_material_stock_list_squery_where = $get_material_stock_list_squery_where." and material_stock_last_updated_on <= :end_date";				
		}
		
		//Data
		$get_material_stock_list_sdata['end_date']  = $end_date;
		
		$filter_count++;
	}
	$get_material_stock_list_order = " order by material_stock_last_updated_on DESC";
	$get_material_stock_list_squery = $get_material_stock_list_squery_base.$get_material_stock_list_squery_where.$get_material_stock_list_order;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_material_stock_list_sstatement = $dbConnection->prepare($get_material_stock_list_squery);
		
		$get_material_stock_list_sstatement -> execute($get_material_stock_list_sdata);
		
		$get_material_stock_list_sdetails = $get_material_stock_list_sstatement -> fetchAll();
		
		if(FALSE === $get_material_stock_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_material_stock_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_material_stock_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
 }
 

 /*

PURPOSE : To update Material Stock

INPUT 	: Stock ID, Stock Update Array

OUTPUT 	: Stock ID; Message of success or failure

BY 		: Lakshmi

*/
function db_update_material_stock($material_id,$project,$material_stock_update_data)
{

	if(array_key_exists("quantity",$material_stock_update_data))

	{	

		$quantity = $material_stock_update_data["quantity"];

	}

	else

	{

		$quantity = "";

	}

	

	if(array_key_exists("uom",$material_stock_update_data))

	{	

		$uom = $material_stock_update_data["uom"];

	}

	else

	{

		$uom = "";

	}

	

	if(array_key_exists("active",$material_stock_update_data))

	{	

		$active = $material_stock_update_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("remarks",$material_stock_update_data))

	{	

		$remarks = $material_stock_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}

	if(array_key_exists("min_qty",$material_stock_update_data))

	{	

		$min_qty = $material_stock_update_data["min_qty"];

	}

	else

	{

		$min_qty = "";

	}

	if(array_key_exists("max_qty",$material_stock_update_data))

	{	

		$max_qty = $material_stock_update_data["max_qty"];

	}

	else

	{

		$max_qty = "";

	}

	if(array_key_exists("re_order_qty",$material_stock_update_data))

	{	

		$re_order_qty = $material_stock_update_data["re_order_qty"];

	}

	else

	{

		$re_order_qty = "";

	}

	if(array_key_exists("re_order_level",$material_stock_update_data))

	{	

		$re_order_level = $material_stock_update_data["re_order_level"];

	}

	else

	{

		$re_order_level = "";

	}

	

	if(array_key_exists("last_updated_by",$material_stock_update_data))

	{	

		$last_updated_by = $material_stock_update_data["last_updated_by"];

	}

	else

	{

		$last_updated_by = "";

	}

	

	if(array_key_exists("last_updated_on",$material_stock_update_data))

	{	

		$last_updated_on = $material_stock_update_data["last_updated_on"];

	}

	else

	{

		$last_updated_on = "";

	}

	

	// Query

    $material_stock_update_uquery_base = "update material_stock set";  

	

	$material_stock_update_uquery_set = "";

	
	$material_stock_update_uquery_where = " where material_id = :material_id and material_stock_project = :project";
	
	$material_stock_update_udata = array(":material_id"=>$material_id,":project"=>$project);
	

	$filter_count = 0;

	

	if($quantity != "")

	{

		$material_stock_update_uquery_set = $material_stock_update_uquery_set." material_stock_quantity = :quantity,";

		$material_stock_update_udata[":quantity"] = $quantity;

		$filter_count++;

	}

	

	if($uom != "")

	{

		$material_stock_update_uquery_set = $material_stock_update_uquery_set." material_stock_uom = :uom,";

		$material_stock_update_udata[":uom"] = $uom;

		$filter_count++;

	}

	

	if($active != "")

	{

		$material_stock_update_uquery_set = $material_stock_update_uquery_set." material_stock_active = :active,";

		$material_stock_update_udata[":active"] = $active;

		$filter_count++;

	}

	

	if($remarks != "")

	{

		$material_stock_update_uquery_set = $material_stock_update_uquery_set." material_stock_remarks = :remarks,";

		$material_stock_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}

	if($min_qty != "")

	{

		$material_stock_update_uquery_set = $material_stock_update_uquery_set." material_stock_min_qty = :min_qty,";

		$material_stock_update_udata[":min_qty"] = $min_qty;

		$filter_count++;

	}

	if($max_qty != "")

	{

		$material_stock_update_uquery_set = $material_stock_update_uquery_set." material_stock_max_qty = :max_qty,";

		$material_stock_update_udata[":max_qty"] = $max_qty;

		$filter_count++;

	}

	if($re_order_qty != "")

	{

		$material_stock_update_uquery_set = $material_stock_update_uquery_set." material_stock_re_order_qty = :re_order_qty,";

		$material_stock_update_udata[":re_order_qty"] = $re_order_qty;

		$filter_count++;

	}

	if($re_order_level != "")

	{

		$material_stock_update_uquery_set = $material_stock_update_uquery_set." material_stock_re_order_level = :re_order_level,";

		$material_stock_update_udata[":re_order_level"] = $re_order_level;

		$filter_count++;

	}

	

	if($last_updated_by != "")

	{

		$material_stock_update_uquery_set = $material_stock_update_uquery_set." material_stock_last_updated_by = :last_updated_by,";

		$material_stock_update_udata[":last_updated_by"] = $last_updated_by;

		$filter_count++;

	}

	

	if($last_updated_on != "")

	{

		$material_stock_update_uquery_set = $material_stock_update_uquery_set." material_stock_last_updated_on = :last_updated_on,";

		$material_stock_update_udata[":last_updated_on"] = $last_updated_on;		

		$filter_count++;

	}

	

	if($filter_count > 0)

	{

		$material_stock_update_uquery_set = trim($material_stock_update_uquery_set,',');

	}

	

	$material_stock_update_uquery = $material_stock_update_uquery_base.$material_stock_update_uquery_set.$material_stock_update_uquery_where;

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $material_stock_update_ustatement = $dbConnection->prepare($material_stock_update_uquery);	

        

        $material_stock_update_ustatement -> execute($material_stock_update_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = "";

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}



/*

PURPOSE : To add new Stock Issue

INPUT 	: Indent Item ID, Indent ID, Returnable On, Remarks, Added By

OUTPUT 	: Issue ID, success or failure message

BY 		: Lakshmi

*/

function db_add_stock_issue($issue_no,$indent_item_id,$indent_id,$returnable_on,$qty,$remarks,$issued_by,$added_by)

{

	// Query

	$stock_issue_iquery = "insert into stock_issue (stock_issue_no,stock_issue_indent_item_id,stock_issue_indent_id,stock_issue_returnable_on,stock_issue_qty,stock_issue_active,stock_issue_status,stock_issue_remarks,stock_issue_issued_by,stock_issue_issued_on,stock_issue_added_by,stock_issue_added_on) values  (:issue_no,:indent_item_id,:indent_id,:returnable_on,:qty,:active,:status,:remarks,:issued_by,:issued_on,:added_by,:added_on)";  

	

    try

    {

        $dbConnection = get_conn_handle();

        $stock_issue_istatement = $dbConnection->prepare($stock_issue_iquery);

        

        // Data

        $stock_issue_idata = array(':issue_no'=>$issue_no,':indent_item_id'=>$indent_item_id,':indent_id'=>$indent_id,':returnable_on'=>$returnable_on,':qty'=>$qty,':active'=>'1',':status'=>'0',

		':remarks'=>$remarks,':issued_by'=>$issued_by,':issued_on'=>date("Y-m-d H:i:s"),':added_by'=>$added_by,':added_on'=>date("Y-m-d H:i:s"));	

		

		$dbConnection->beginTransaction();

        $stock_issue_istatement->execute($stock_issue_idata);

		$stock_issue_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

       

        $return["status"] = SUCCESS;

		$return["data"]   = $stock_issue_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get Stock Issue list

INPUT 	: Issue Id, Indent Item ID, Indent ID, Returnable On, Active, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Stock Issue

BY 		: Lakshmi

*/

function db_get_stock_issue($stock_issue_search_data)

{  

 

	if(array_key_exists("issue_id",$stock_issue_search_data))

	{

		$issue_id = $stock_issue_search_data["issue_id"];

	}

	else

	{

		$issue_id= "";

	}

	

	if(array_key_exists("indent_item_id",$stock_issue_search_data))

	{

		$indent_item_id = $stock_issue_search_data["indent_item_id"];

	}

	else

	{

		$indent_item_id = "";

	}

	

	if(array_key_exists("indent_id",$stock_issue_search_data))

	{

		$indent_id = $stock_issue_search_data["indent_id"];

	}

	else

	{

		$indent_id = "";

	}

	

	if(array_key_exists("returnable_on",$stock_issue_search_data))

	{

		$returnable_on = $stock_issue_search_data["returnable_on"];

	}

	else

	{

		$returnable_on = "";

	}

	

	if(array_key_exists("active",$stock_issue_search_data))

	{

		$active = $stock_issue_search_data["active"];

	}

	else

	{

		$active = "";

	}

	

	if(array_key_exists("added_by",$stock_issue_search_data))

	{

		$added_by = $stock_issue_search_data["added_by"];

	}

	else

	{

		$added_by = "";

	}

	

	if(array_key_exists("start_date",$stock_issue_search_data))

	{

		$start_date= $stock_issue_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}

	

	if(array_key_exists("end_date",$stock_issue_search_data))

	{

		$end_date= $stock_issue_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}
	
	if(array_key_exists("start",$stock_issue_search_data))
	{
		$start= $stock_issue_search_data["start"];
	}
	else
	{
		$start= "-1";
	}
	
	if(array_key_exists("limit",$stock_issue_search_data))
	{
		$limit= $stock_issue_search_data["limit"];
	}
	else
	{
		$limit= "";
	}

	

	$get_stock_issue_list_squery_base = " select * from stock_issue SI inner join stock_indent S on S.stock_indent_id= SI.stock_issue_indent_id inner join users U on U.user_id=SI.stock_issue_issued_by";

	

	$get_stock_issue_list_squery_where = "";

	

	$get_issue_list_squery_order = "";

	

	$filter_count = 0;

	

	// Data

	$get_stock_issue_list_sdata = array();

	

	if($issue_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." where stock_issue_id = :issue_id";								

		}

		else

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." and stock_issue_id = :issue_id";				

		}

		

		// Data

		$get_stock_issue_list_sdata[':issue_id'] = $issue_id;

		

		$filter_count++;

	}

	

	if($indent_item_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." where stock_issue_indent_item_id = :indent_item_id";								

		}

		else

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." and stock_issue_indent_item_id = :indent_item_id";				

		}

		

		// Data

		$get_stock_issue_list_sdata[':indent_item_id'] = $indent_item_id;

		

		$filter_count++;

	}

	

	if($indent_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." where stock_issue_indent_id = :indent_id";								

		}

		else

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." and stock_issue_indent_id = :indent_id";				

		}

		

		// Data

		$get_stock_issue_list_sdata[':indent_id'] = $indent_id;

		

		$filter_count++;

	}

	

	if($returnable_on != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." where stock_issue_returnable_on = :returnable_on";								

		}

		else

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." and stock_issue_returnable_on = :returnable_on";				

		}

		

		// Data

		$get_stock_issue_list_sdata[':returnable_on'] = $returnable_on;

		

		$filter_count++;

	}

	

	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." where stock_issue_active = :active";								

		}

		else

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." and stock_issue_active = :active";				

		}

		

		// Data

		$get_stock_issue_list_sdata[':active']  = $active;

		

		$filter_count++;

	}

	

	if($added_by!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." where stock_issue_added_by >= :added_by";								

		}

		else

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." and stock_issue_added_by >= :added_by";				

		}

		

		//Data

		$get_stock_issue_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." where stock_issue_added_on >= :start_date";								

		}

		else

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." and stock_issue_added_on >= :start_date";				

		}

		

		//Data

		$get_stock_issue_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." where stock_issue_added_on <= :end_date";								

		}

		else

		{

			// Query

			$get_stock_issue_list_squery_where = $get_stock_issue_list_squery_where." and stock_issue_added_on <= :end_date";				

		}

		

		//Data

		$get_stock_issue_list_sdata['end_date']  = $end_date;

		

		$filter_count++;

	}
	
	if($start >= 0)
	{
			// Query
			$get_stock_issue_list_squery_limit = $get_stock_issue_list_squery_limit." limit $start,$limit";	
	}			
	else
	{
		// Query
		$get_stock_issue_list_squery_limit = "";				
	}

	if(array_key_exists("sort",$stock_issue_search_data))

	{

		if($stock_issue_search_data['sort'] == '1')

		{

			$get_issue_list_squery_order = " order by cast(substring(stock_issue_no,locate('.',stock_issue_no,8)+1) as signed) desc limit 0,1";

		}

		else

		{

			$get_issue_list_squery_order = " order by stock_issue_added_on DESC";

		}

	}
	else
	{
		
		$get_issue_list_squery_order = " order by stock_issue_added_on  DESC";
	
	}
	
	$get_stock_issue_list_squery = $get_stock_issue_list_squery_base.$get_stock_issue_list_squery_where.$get_issue_list_squery_order.$get_stock_issue_list_squery_limit;

	



	try

	{

		$dbConnection = get_conn_handle();

		

		$get_stock_issue_list_sstatement = $dbConnection->prepare($get_stock_issue_list_squery);

		

		$get_stock_issue_list_sstatement -> execute($get_stock_issue_list_sdata);

		

		$get_stock_issue_list_sdetails = $get_stock_issue_list_sstatement -> fetchAll();

		

		if(FALSE === $get_stock_issue_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_issue_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_issue_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

 }

 

 /*

PURPOSE : To update Stock Issue

INPUT 	: Issue ID, Stock Issue Update Array

OUTPUT 	: Issue ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_stock_issue($issue_id,$stock_issue_update_data)

{

	if(array_key_exists("indent_item_id",$stock_issue_update_data))

	{	

		$indent_item_id = $stock_issue_update_data["indent_item_id"];

	}

	else

	{

		$indent_item_id = "";

	}

	

	if(array_key_exists("indent_id",$stock_issue_update_data))

	{	

		$indent_id = $stock_issue_update_data["indent_id"];

	}

	else

	{

		$indent_id = "";

	}

	

	if(array_key_exists("returnable_on",$stock_issue_update_data))

	{	

		$returnable_on = $stock_issue_update_data["returnable_on"];

	}

	else

	{

		$returnable_on = "";

	}

	

	if(array_key_exists("active",$stock_issue_update_data))

	{	

		$active = $stock_issue_update_data["active"];

	}

	else

	{

		$active = "";

	}

	

	if(array_key_exists("status",$stock_issue_update_data))

	{	

		$status = $stock_issue_update_data["status"];

	}

	else

	{

		$status = "";

	}



	if(array_key_exists("remarks",$stock_issue_update_data))

	{	

		$remarks = $stock_issue_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}

	

	if(array_key_exists("added_by",$stock_issue_update_data))

	{	

		$added_by = $stock_issue_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}

	

	if(array_key_exists("added_on",$stock_issue_update_data))

	{	

		$added_on = $stock_issue_update_data["added_on"];

	}

	else

	{

		$added_on = "";

	}

	

	// Query

    $stock_issue_update_uquery_base = "update stock_issue set";  

	

	$stock_issue_update_uquery_set = "";

	

	$stock_issue_update_uquery_where = " where stock_issue_id = :issue_id";

	

	$stock_issue_update_udata = array(":issue_id"=>$issue_id);

	

	$filter_count = 0;

	

	if($indent_item_id != "")

	{

		$stock_issue_update_uquery_set = $stock_issue_update_uquery_set." stock_issue_indent_item_id = :indent_item_id,";

		$stock_issue_update_udata[":indent_item_id"] = $indent_item_id;

		$filter_count++;

	}

	

	if($indent_id != "")

	{

		$stock_issue_update_uquery_set = $stock_issue_update_uquery_set." stock_issue_indent_id = :indent_id,";

		$stock_issue_update_udata[":indent_id"] = $indent_id;

		$filter_count++;

	}

	

	if($returnable_on != "")

	{

		$stock_issue_update_uquery_set = $stock_issue_update_uquery_set." stock_issue_returnable_on = :returnable_on,";

		$stock_issue_update_udata[":returnable_on"] = $returnable_on;

		$filter_count++;

	}

	

	if($active != "")

	{

		$stock_issue_update_uquery_set = $stock_issue_update_uquery_set." stock_issue_active = :active,";

		$stock_issue_update_udata[":active"] = $active;

		$filter_count++;

	}

	

	if($status != "")

	{

		$stock_issue_update_uquery_set = $stock_issue_update_uquery_set." stock_issue_status = :status,";

		$stock_issue_update_udata[":status"] = $status;

		$filter_count++;

	}

	

	if($remarks != "")

	{

		$stock_issue_update_uquery_set = $stock_issue_update_uquery_set." stock_issue_remarks = :remarks,";

		$stock_issue_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}

	

	if($added_by != "")

	{

		$stock_issue_update_uquery_set = $stock_issue_update_uquery_set." stock_issue_added_by = :added_by,";

		$stock_issue_update_udata[":added_by"] = $added_by;

		$filter_count++;

	}

	

	if($added_on != "")

	{

		$stock_issue_update_uquery_set = $stock_issue_update_uquery_set." stock_issue_added_on = :added_on,";

		$stock_issue_update_udata[":added_on"] = $added_on;		

		$filter_count++;

	}

	

	if($filter_count > 0)

	{

		$stock_issue_update_uquery_set = trim($stock_issue_update_uquery_set,',');

	}

	

	$stock_issue_update_uquery = $stock_issue_update_uquery_base.$stock_issue_update_uquery_set.$stock_issue_update_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();

        

        $stock_issue_update_ustatement = $dbConnection->prepare($stock_issue_update_uquery);		

        

        $stock_issue_update_ustatement -> execute($stock_issue_update_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $issue_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}

/*

PURPOSE : To add new Stock Issue Item

INPUT 	: Issue ID, Material ID, Qty, Remarks, Issued By

OUTPUT 	: Issue Item ID, success or failure message

BY 		: Lakshmi

*/
function db_add_stock_issue_item($issue_id,$material_id,$qty,$machine_details,$project,$remarks,$issued_by)
{

	// Query
   $stock_issue_item_iquery = "insert into stock_issue_item (stock_issue_item_issue_id,stock_issue_item_material_id,stock_issue_item_qty,stock_issue_item_machine_details,stock_issue_item_project,stock_issue_item_active,stock_issue_item_remarks,stock_issue_item_issued_by,stock_issue_item_issued_on) values (:issue_id,:material_id,:qty,:machine_details,:project,:active,:remarks,:issued_by,:issued_on)"; 
	
    try

    {

        $dbConnection = get_conn_handle();

        $stock_issue_item_istatement = $dbConnection->prepare($stock_issue_item_iquery);

        

        // Data
        $stock_issue_item_idata = array(':issue_id'=>$issue_id,':material_id'=>$material_id,':qty'=>$qty,':machine_details'=>$machine_details,':project'=>$project,':active'=>'1',':remarks'=>$remarks,':issued_by'=>$issued_by,':issued_on'=>date("Y-m-d H:i:s"));	
		

		$dbConnection->beginTransaction();

        $stock_issue_item_istatement->execute($stock_issue_item_idata);

		$stock_issue_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

       

        $return["status"] = SUCCESS;

		$return["data"]   = $stock_issue_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}




/*

PURPOSE : To get Stock Issue Item list

INPUT 	: Issue Item ID, Issue ID, Material ID, Qty, Active, Issued By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Stock Issue Item

BY 		: Lakshmi

*/

function db_get_stock_issue_item($stock_issue_item_search_data)

{  

	if(array_key_exists("issue_item_id",$stock_issue_item_search_data))

	{

		$issue_item_id = $stock_issue_item_search_data["issue_item_id"];

	}

	else

	{

		$issue_item_id= "";

	}

	

	if(array_key_exists("issue_id",$stock_issue_item_search_data))

	{

		$issue_id = $stock_issue_item_search_data["issue_id"];

	}

	else

	{

		$issue_id = "";

	}

	

	if(array_key_exists("indent_id",$stock_issue_item_search_data))

	{

		$indent_id = $stock_issue_item_search_data["indent_id"];

	}

	else

	{

		$indent_id = "";

	}

	

	if(array_key_exists("material_id",$stock_issue_item_search_data))

	{

		$material_id = $stock_issue_item_search_data["material_id"];

	}

	else

	{

		$material_id = "";

	}
	
	if(array_key_exists("machine_id",$stock_issue_item_search_data))

	{

		$machine_id = $stock_issue_item_search_data["machine_id"];

	}

	else

	{

		$machine_id = "";

	}

	

	if(array_key_exists("qty",$stock_issue_item_search_data))

	{

		$qty = $stock_issue_item_search_data["qty"];

	}

	else

	{

		$qty = "";

	}

	
	if(array_key_exists("project",$stock_issue_item_search_data))
	{
		$project = $stock_issue_item_search_data["project"];
	}

	else

	{
		$project = "";
	}

	

	if(array_key_exists("active",$stock_issue_item_search_data))

	{

		$active = $stock_issue_item_search_data["active"];

	}

	else

	{

		$active = "";

	}

	

	if(array_key_exists("issued_by",$stock_issue_item_search_data))

	{

		$issued_by = $stock_issue_item_search_data["issued_by"];

	}

	else

	{

		$issued_by = "";

	}

	

	if(array_key_exists("indent_by",$stock_issue_item_search_data))

	{

		$indent_by = $stock_issue_item_search_data["indent_by"];

	}

	else

	{

		$indent_by = "";

	}

	

	if(array_key_exists("returnable_type",$stock_issue_item_search_data))

	{

		$returnable_type = $stock_issue_item_search_data["returnable_type"];

	}

	else

	{

		$returnable_type = "";

	}

	

	if(array_key_exists("start_date",$stock_issue_item_search_data))

	{

		$start_date= $stock_issue_item_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}

	

	if(array_key_exists("end_date",$stock_issue_item_search_data))

	{

		$end_date= $stock_issue_item_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}

	

	$get_stock_issue_item_list_squery_base = "select *, sum(stock_issue_item_qty) as total_issued_item_quantity ,U.user_name as added_by,AU.user_name as issued_to from stock_issue_item SII inner join stock_issue SI on SI.stock_issue_id= SII.stock_issue_item_issue_id inner join users U on U.user_id=SII.stock_issue_item_issued_by inner join stock_indent SIO on SIO.stock_indent_id=SI.stock_issue_indent_id inner join stock_material_master SMM on SMM.stock_material_id=SII.stock_issue_item_material_id inner join users AU on AU.user_id = SIO.stock_indent_added_by inner join stock_project SP on SP.stock_project_id=SII.stock_issue_item_project left outer join stock_machine_master SMMM on SMMM.stock_machine_master_id= SII.stock_issue_item_machine_details";

	

	$get_stock_issue_item_list_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_stock_issue_item_list_sdata = array();

	

	if($issue_item_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_id = :issue_item_id";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_id = :issue_item_id";				

		}

		

		// Data

		$get_stock_issue_item_list_sdata[':issue_item_id'] = $issue_item_id;

		

		$filter_count++;

	}

	

	if($issue_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_issue_id = :issue_id";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_issue_id = :issue_id";				

		}

		

		// Data

		$get_stock_issue_item_list_sdata[':issue_id'] = $issue_id;

		

		$filter_count++;

	}

	

	if($indent_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where SI.stock_issue_indent_id = :indent_id";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and SI.stock_issue_indent_id = :indent_id";				

		}

		

		// Data

		$get_stock_issue_item_list_sdata[':indent_id'] = $indent_id;

		

		$filter_count++;

	}

	

	if($material_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_material_id = :material_id";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_material_id = :material_id";				

		}

		

		// Data

		$get_stock_issue_item_list_sdata[':material_id'] = $material_id;

		

		$filter_count++;

	}
	
	if($machine_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_machine_details = :machine_id";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_machine_details = :machine_id";				

		}

		

		// Data

		$get_stock_issue_item_list_sdata[':machine_id'] = $machine_id;

		

		$filter_count++;

	}

	

	if($qty != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_qty = :qty";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_qty = :qty";				

		}

		

		// Data

		$get_stock_issue_item_list_sdata[':qty'] = $qty;

		

		$filter_count++;

	}

	

	if($project != "")
	
	{
		
		if($filter_count == 0)
			
		{
			
			// Query
			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_project = :project";	
			
		}
		
		else
			
		{
			// Query
			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_project = :project";	
			
		}
		
		// Data
		$get_stock_issue_item_list_sdata[':project'] = $project;
		
		$filter_count++;
	}

	

	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_active = :active";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_active = :active";				

		}

		

		// Data

		$get_stock_issue_item_list_sdata[':active']  = $active;

		

		$filter_count++;

	}

	

	if($issued_by!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_issued_by = :issued_by";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_issued_by = :issued_by";				

		}

		

		//Data

		$get_stock_issue_item_list_sdata[':issued_by']  = $issued_by;

		

		$filter_count++;

	}

	

	if($indent_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where SIO.stock_indent_added_by = :indent_by";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and SIO.stock_indent_added_by = :indent_by";				

		}

		

		//Data

		$get_stock_issue_item_list_sdata[':indent_by']  = $indent_by;

		

		$filter_count++;

	}

	

	if($returnable_type != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where SMM.stock_material_type = :returnable_type";

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and SMM.stock_material_type = :returnable_type";

		}

		

		//Data

		$get_stock_issue_item_list_sdata[':returnable_type']  = $returnable_type;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_issued_on >= :start_date";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_issued_on >= :start_date";				

		}

		

		//Data

		$get_stock_issue_item_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{
		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_issued_on <= :end_date";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_issued_on <= :end_date";				

		}

		

		//Data

		$get_stock_issue_item_list_sdata['end_date']  = $end_date;

		

		$filter_count++;

	}

	
	$get_stock_issue_item_list_group_by = " group by stock_issue_item_material_id,stock_issue_indent_id";
	$get_stock_issue_item_list_order = " order by stock_issue_item_issued_on  DESC";
	$get_stock_issue_item_list_squery = $get_stock_issue_item_list_squery_base.$get_stock_issue_item_list_squery_where.$get_stock_issue_item_list_group_by.$get_stock_issue_item_list_order;
	try

	{

		$dbConnection = get_conn_handle();

		

		$get_stock_issue_item_list_sstatement = $dbConnection->prepare($get_stock_issue_item_list_squery);

		

		$get_stock_issue_item_list_sstatement -> execute($get_stock_issue_item_list_sdata);

		

		$get_stock_issue_item_list_sdetails = $get_stock_issue_item_list_sstatement -> fetchAll();

		

		if(FALSE === $get_stock_issue_item_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_issue_item_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_issue_item_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

 }

 /*

PURPOSE : To get Stock Issue Item list

INPUT 	: Issue Item ID, Issue ID, Material ID, Qty, Active, Issued By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Stock Issue Item

BY 		: Lakshmi

*/

function db_get_stock_issue_items_without_sum($stock_issue_item_search_data)

{  

	if(array_key_exists("issue_item_id",$stock_issue_item_search_data))

	{

		$issue_item_id = $stock_issue_item_search_data["issue_item_id"];

	}

	else

	{

		$issue_item_id= "";

	}

	

	if(array_key_exists("issue_id",$stock_issue_item_search_data))

	{

		$issue_id = $stock_issue_item_search_data["issue_id"];

	}

	else

	{

		$issue_id = "";

	}

	

	if(array_key_exists("indent_id",$stock_issue_item_search_data))

	{

		$indent_id = $stock_issue_item_search_data["indent_id"];

	}

	else

	{

		$indent_id = "";

	}

	

	if(array_key_exists("material_id",$stock_issue_item_search_data))

	{

		$material_id = $stock_issue_item_search_data["material_id"];

	}

	else

	{

		$material_id = "";

	}

	

	if(array_key_exists("qty",$stock_issue_item_search_data))

	{

		$qty = $stock_issue_item_search_data["qty"];

	}

	else

	{

		$qty = "";

	}

	
	if(array_key_exists("project",$stock_issue_item_search_data))
	{
		$project = $stock_issue_item_search_data["project"];
	}

	else

	{
		$project = "";
	}

	

	if(array_key_exists("active",$stock_issue_item_search_data))

	{

		$active = $stock_issue_item_search_data["active"];

	}

	else

	{

		$active = "";

	}

	

	if(array_key_exists("issued_by",$stock_issue_item_search_data))

	{

		$issued_by = $stock_issue_item_search_data["issued_by"];

	}

	else

	{

		$issued_by = "";

	}

	

	if(array_key_exists("indent_by",$stock_issue_item_search_data))

	{

		$indent_by = $stock_issue_item_search_data["indent_by"];

	}

	else

	{

		$indent_by = "";

	}

	

	if(array_key_exists("returnable_type",$stock_issue_item_search_data))

	{

		$returnable_type = $stock_issue_item_search_data["returnable_type"];

	}

	else

	{

		$returnable_type = "";

	}

	

	if(array_key_exists("start_date",$stock_issue_item_search_data))

	{

		$start_date= $stock_issue_item_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}

	

	if(array_key_exists("end_date",$stock_issue_item_search_data))

	{

		$end_date= $stock_issue_item_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}

	

	$get_stock_issue_item_list_squery_base = "select *,U.user_name as added_by,AU.user_name as issued_to from stock_issue_item SII inner join stock_issue SI on SI.stock_issue_id= SII.stock_issue_item_issue_id inner join users U on U.user_id=SII.stock_issue_item_issued_by inner join stock_indent SIO on SIO.stock_indent_id=SI.stock_issue_indent_id inner join stock_material_master SMM on SMM.stock_material_id=SII.stock_issue_item_material_id inner join users AU on AU.user_id = SIO.stock_indent_added_by left outer join stock_machine_master SMMM on SMMM.stock_machine_master_id= SII.stock_issue_item_machine_details";

	

	$get_stock_issue_item_list_squery_where = "";

	

	$filter_count = 0;

	

	// Data

	$get_stock_issue_item_list_sdata = array();

	

	if($issue_item_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_id = :issue_item_id";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_id = :issue_item_id";				

		}

		

		// Data

		$get_stock_issue_item_list_sdata[':issue_item_id'] = $issue_item_id;

		

		$filter_count++;

	}

	

	if($issue_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_issue_id = :issue_id";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_issue_id = :issue_id";				

		}

		

		// Data

		$get_stock_issue_item_list_sdata[':issue_id'] = $issue_id;

		

		$filter_count++;

	}

	

	if($indent_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where SI.stock_issue_indent_id = :indent_id";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and SI.stock_issue_indent_id = :indent_id";				

		}

		

		// Data

		$get_stock_issue_item_list_sdata[':indent_id'] = $indent_id;

		

		$filter_count++;

	}

	

	if($material_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_material_id = :material_id";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_material_id = :material_id";				

		}

		

		// Data

		$get_stock_issue_item_list_sdata[':material_id'] = $material_id;

		

		$filter_count++;

	}

	

	if($qty != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_qty = :qty";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_qty = :qty";				

		}

		

		// Data

		$get_stock_issue_item_list_sdata[':qty'] = $qty;

		

		$filter_count++;

	}

	

	if($project != "")
	
	{
		
		if($filter_count == 0)
			
		{
			
			// Query
			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_project = :project";	
			
		}
		
		else
			
		{
			// Query
			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_project = :project";	
			
		}
		
		// Data
		$get_stock_issue_item_list_sdata[':project'] = $project;
		
		$filter_count++;
	}

	

	if($active != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_active = :active";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_active = :active";				

		}

		

		// Data

		$get_stock_issue_item_list_sdata[':active']  = $active;

		

		$filter_count++;

	}

	

	if($issued_by!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_issued_by = :issued_by";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_issued_by = :issued_by";				

		}

		

		//Data

		$get_stock_issue_item_list_sdata[':issued_by']  = $issued_by;

		

		$filter_count++;

	}

	

	if($indent_by != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where SIO.stock_indent_added_by = :indent_by";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and SIO.stock_indent_added_by = :indent_by";				

		}

		

		//Data

		$get_stock_issue_item_list_sdata[':indent_by']  = $indent_by;

		

		$filter_count++;

	}

	

	if($returnable_type != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where SMM.stock_material_type = :returnable_type";

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and SMM.stock_material_type = :returnable_type";

		}

		

		//Data

		$get_stock_issue_item_list_sdata[':returnable_type']  = $returnable_type;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_issued_on >= :start_date";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_issued_on >= :start_date";				

		}

		

		//Data

		$get_stock_issue_item_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{
		if($filter_count == 0)

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." where stock_issue_item_issued_on <= :end_date";								

		}

		else

		{

			// Query

			$get_stock_issue_item_list_squery_where = $get_stock_issue_item_list_squery_where." and stock_issue_item_issued_on <= :end_date";				

		}

		

		//Data

		$get_stock_issue_item_list_sdata['end_date']  = $end_date;

		

		$filter_count++;

	}

	
	//$get_stock_issue_item_list_group_by = " group by stock_issue_item_material_id,stock_issue_indent_id";
	$get_stock_issue_item_list_order = " order by stock_issue_item_issued_on  DESC";
	$get_stock_issue_item_list_squery = $get_stock_issue_item_list_squery_base.$get_stock_issue_item_list_squery_where.$get_stock_issue_item_list_order;
	try

	{

		$dbConnection = get_conn_handle();

		

		$get_stock_issue_item_list_sstatement = $dbConnection->prepare($get_stock_issue_item_list_squery);

		

		$get_stock_issue_item_list_sstatement -> execute($get_stock_issue_item_list_sdata);

		

		$get_stock_issue_item_list_sdetails = $get_stock_issue_item_list_sstatement -> fetchAll();

		

		if(FALSE === $get_stock_issue_item_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_issue_item_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_issue_item_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

 }

  /*

PURPOSE : To update Stock Issue Item

INPUT 	: Issue Item ID, Stock Issue Item Update Array

OUTPUT 	: Issue Item ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_stock_issue_item($issue_item_id,$stock_issue_item_update_data)

{

	if(array_key_exists("issue_id",$stock_issue_item_update_data))

	{	

		$issue_id = $stock_issue_item_update_data["issue_id"];

	}

	else

	{

		$issue_id = "";

	}

	

	if(array_key_exists("material_id",$stock_issue_item_update_data))

	{	

		$material_id = $stock_issue_item_update_data["material_id"];

	}

	else

	{

		$material_id = "";

	}

	

	if(array_key_exists("qty",$stock_issue_item_update_data))

	{	

		$qty = $stock_issue_item_update_data["qty"];

	}

	else

	{

		$qty = "";

	}

	

	if(array_key_exists("active",$stock_issue_item_update_data))

	{	

		$active = $stock_issue_item_update_data["active"];

	}

	else

	{

		$active = "";

	}



	if(array_key_exists("remarks",$stock_issue_item_update_data))

	{	

		$remarks = $stock_issue_item_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}

	

	if(array_key_exists("issued_by",$stock_issue_item_update_data))

	{	

		$issued_by = $stock_issue_item_update_data["issued_by"];

	}

	else

	{

		$issued_by = "";

	}

	

	if(array_key_exists("issued_on",$stock_issue_item_update_data))

	{	

		$issued_on = $stock_issue_item_update_data["issued_on"];

	}

	else

	{

		$issued_on = "";

	}

	

	// Query

    $stock_issue_item_update_uquery_base = "update stock_issue_item set";  

	

	$stock_issue_item_update_uquery_set = "";

	

	$stock_issue_item_update_uquery_where = " where stock_issue_item_id = :issue_item_id";

	

	$stock_issue_item_update_udata = array(":issue_item_id"=>$issue_item_id);

	

	$filter_count = 0;

	

	if($issue_id != "")

	{

		$stock_issue_item_update_uquery_set = $stock_issue_item_update_uquery_set." stock_issue_item_issue_id = :issue_id,";

		$stock_issue_item_update_udata[":issue_id"] = $issue_id;

		$filter_count++;

	}

	

	if($material_id != "")

	{

		$stock_issue_item_update_uquery_set = $stock_issue_item_update_uquery_set." stock_issue_item_material_id = :material_id,";

		$stock_issue_item_update_udata[":material_id"] = $material_id;

		$filter_count++;

	}

	

	if($qty != "")

	{

		$stock_issue_item_update_uquery_set = $stock_issue_item_update_uquery_set." stock_issue_item_qty = :qty,";

		$stock_issue_item_update_udata[":qty"] = $qty;

		$filter_count++;

	}

	

	if($active != "")

	{

		$stock_issue_item_update_uquery_set = $stock_issue_item_update_uquery_set." stock_issue_item_active = :active,";

		$stock_issue_item_update_udata[":active"] = $active;

		$filter_count++;

	}



	if($remarks != "")

	{

		$stock_issue_item_update_uquery_set = $stock_issue_item_update_uquery_set." stock_issue_item_remarks = :remarks,";

		$stock_issue_item_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}

	

	if($issued_by != "")

	{

		$stock_issue_item_update_uquery_set = $stock_issue_item_update_uquery_set." stock_issue_item_issued_by = :issued_by,";

		$stock_issue_item_update_udata[":issued_by"] = $issued_by;

		$filter_count++;

	}

	

	if($issued_on != "")

	{

		$stock_issue_item_update_uquery_set = $stock_issue_item_update_uquery_set." stock_issue_item_issued_on = :issued_on,";

		$stock_issue_item_update_udata[":issued_on"] = $issued_on;		

		$filter_count++;

	}

	

	if($filter_count > 0)

	{

		$stock_issue_item_update_uquery_set = trim($stock_issue_item_update_uquery_set,',');

	}

	

	$stock_issue_item_update_uquery = $stock_issue_item_update_uquery_base.$stock_issue_item_update_uquery_set.$stock_issue_item_update_uquery_where;



    try

    {

        $dbConnection = get_conn_handle();

        

        $stock_issue_item_update_ustatement = $dbConnection->prepare($stock_issue_item_update_uquery);		

        

        $stock_issue_item_update_ustatement -> execute($stock_issue_item_update_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $issue_item_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}



/*

PURPOSE : To add new Stock Transfer

INPUT 	: Material ID, Quantity, Source Location, Destination Location, Remarks, Added By

OUTPUT 	: Transfer ID, success or failure message

BY 		: Lakshmi

*/
function db_add_stock_transfer($material_id,$quantity,$source_project,$destination_project,$remarks,$added_by)
{

	// Query
    $stock_transfer_iquery = "insert into stock_transfer(stock_transfer_material_id,stock_transfer_quantity,stock_transfer_source_project,stock_transfer_destination_project,
	stock_transfer_active,stock_transfer_remarks,stock_transfer_added_by,stock_transfer_added_on)
	values(:material_id,:quantity,:source_project,:destination_project,:active,:remarks,:added_by,:now)";  
	

    try

    {

        $dbConnection = get_conn_handle();

        

        $stock_transfer_istatement = $dbConnection->prepare($stock_transfer_iquery);

        

        // Data
        $stock_transfer_idata = array(':material_id'=>$material_id,':quantity'=>$quantity,':source_project'=>$source_project,':destination_project'=>$destination_project,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();

        $stock_transfer_istatement->execute($stock_transfer_idata);

		$stock_transfer_id = $dbConnection->lastInsertId();

		$dbConnection->commit(); 

        

        $return["status"] = SUCCESS;

		$return["data"]   = $stock_transfer_id;		

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

    

    return $return;

}



/*

PURPOSE : To get Stock Transfer list

INPUT 	: Transfer ID, Material ID, Quantity, Source Location, Destination Location, Active, Added By, Start Date(for added on), End Date(for added on)

OUTPUT 	: List of Stock Transfer

BY 		: Lakshmi

*/

function db_get_stock_transfer($stock_transfer_search_data)

{ 

	if(array_key_exists("transfer_id",$stock_transfer_search_data))

	{

		$transfer_id = $stock_transfer_search_data["transfer_id"];

	}

	else

	{

		$transfer_id= "";

	}

	

	if(array_key_exists("material_id",$stock_transfer_search_data))

	{

		$material_id = $stock_transfer_search_data["material_id"];

	}

	else

	{

		$material_id = "";

	}

	

	if(array_key_exists("quantity",$stock_transfer_search_data))

	{

		$quantity = $stock_transfer_search_data["quantity"];

	}

	else

	{

		$quantity = "";

	}

	
	if(array_key_exists("source_project",$stock_transfer_search_data))
	{
		$source_project = $stock_transfer_search_data["source_project"];
	}

	else

	{
		$source_project = "";
	}

	
	if(array_key_exists("destination_project",$stock_transfer_search_data))
	{
		$destination_project = $stock_transfer_search_data["destination_project"];
	}

	else

	{
		$destination_project = "";
	}

	

	if(array_key_exists("active",$stock_transfer_search_data))

	{

		$active= $stock_transfer_search_data["active"];

	}

	else

	{

		$active= "";

	}
	
	if(array_key_exists("status",$stock_transfer_search_data))

	{

		$status= $stock_transfer_search_data["status"];

	}

	else

	{

		$status= "";

	}
	
	if(array_key_exists("secondary_status",$stock_transfer_search_data))

	{

		$secondary_status= $stock_transfer_search_data["secondary_status"];

	}

	else

	{

		$secondary_status= "";

	}

	

	if(array_key_exists("added_by",$stock_transfer_search_data))

	{

		$added_by= $stock_transfer_search_data["added_by"];

	}

	else

	{

		$added_by= "";

	}

	

	if(array_key_exists("start_date",$stock_transfer_search_data))

	{

		$start_date= $stock_transfer_search_data["start_date"];

	}

	else

	{

		$start_date= "";

	}
	
	if(array_key_exists("sort",$stock_transfer_search_data))

	{

		$sort= $stock_transfer_search_data["sort"];

	}

	else

	{

		$sort= "";

	}

	

	if(array_key_exists("end_date",$stock_transfer_search_data))

	{

		$end_date= $stock_transfer_search_data["end_date"];

	}

	else

	{

		$end_date= "";

	}

	
	$get_stock_transfer_list_squery_base = "select *,U.user_name as added_by,AU.user_name as approved_by,OU.user_name as accepted_by,U.user_id as added_by_id,SLM.stock_project_name as source,SLMD.stock_project_name as dest from stock_transfer ST inner join users U on U.user_id = ST.stock_transfer_added_by inner join stock_material_master SMM on SMM.stock_material_id = ST.stock_transfer_material_id inner join stock_project SLM on SLM.stock_project_id = ST.stock_transfer_source_project inner join stock_project SLMD on SLMD.stock_project_id = ST.stock_transfer_destination_project left outer join users AU on AU.user_id= ST.stock_transfer_approved_by left outer join users OU on OU.user_id=ST.stock_transfer_accepted_by";
	

	$get_stock_transfer_list_squery_where = "";
	$get_stock_transfer_list_squery_order_by = "";

	

	$filter_count = 0;

	

	// Data

	$get_stock_transfer_list_sdata = array();

	

	if($transfer_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." where stock_transfer_id = :transfer_id";								

		}

		else

		{

			// Query

			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." and stock_transfer_id = :transfer_id";				

		}

		

		// Data

		$get_stock_transfer_list_sdata[':transfer_id'] = $transfer_id;

		

		$filter_count++;

	}

	

	if($material_id != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." where stock_transfer_material_id = :material_id";								

		}

		else

		{

			// Query

			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." and stock_transfer_material_id = :material_id";				

		}

		

		// Data

		$get_stock_transfer_list_sdata[':material_id'] = $material_id;

		

		$filter_count++;

	}

	

	if($quantity != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." where stock_transfer_quantity = :quantity";								

		}

		else

		{

			// Query

			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." and stock_transfer_quantity = :quantity";				

		}

		

		// Data

		$get_stock_transfer_list_sdata[':quantity'] = $quantity;

		

		$filter_count++;

	}

	
	if($source_project != "")
	{

		if($filter_count == 0)

		{

			// Query
			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." where stock_transfer_source_project = :source_project";								
		}

		else

		{

			// Query
			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." and stock_transfer_source_project = :source_project";				
		}

		

		// Data
		$get_stock_transfer_list_sdata[':source_project'] = $source_project;
		

		$filter_count++;

	}

	
	if($destination_project != "")
	{

		if($filter_count == 0)

		{

			// Query
			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." where stock_transfer_destination_project = :destination_project";								
		}

		else

		{

			// Query
			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." and stock_transfer_destination_project = :destination_project";				
		}

		

		// Data
		$get_stock_transfer_list_sdata[':destination_project'] = $destination_project;
		

		$filter_count++;

	}

	

	if($active!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." where stock_transfer_active = :active";								

		}

		else

		{

			// Query

			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." and stock_transfer_active = :active";				

		}

		

		//Data

		$get_stock_transfer_list_sdata[':active']  = $active;

		

		$filter_count++;

	}
	
	if($status != "")
	{
		if($secondary_status != "")
		{
			if($filter_count == 0)
			{
				// Query
				$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." where (stock_transfer_status = :status or stock_transfer_status = :secondary_status)";								
			}
			else
			{
				// Query
				$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." and (stock_transfer_status = :status or stock_transfer_status = :secondary_status)";				
			}
			
			$get_stock_transfer_list_sdata[':status'] = $status;
			$get_stock_transfer_list_sdata[':secondary_status'] = $secondary_status;
		
			$filter_count++;
			
		}
		else
		{
			if($filter_count == 0)
			{
				// Query
				$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." where stock_transfer_status = :status";								
			}
			else
			{
				// Query
				$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." and stock_transfer_status = :status";				
			}
			// Data
			$get_stock_transfer_list_sdata[':status'] = $status;
			
			$filter_count++;
		}
		// Data
	}

	

	if($added_by!= "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." where stock_transfer_added_by = :added_by";								

		}

		else

		{

			// Query

			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." and stock_transfer_added_by = :added_by";				

		}

		

		//Data

		$get_stock_transfer_list_sdata[':added_by']  = $added_by;

		

		$filter_count++;

	}

	

	if($start_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." where stock_transfer_added_on >= :start_date";								

		}

		else

		{

			// Query

			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." and stock_transfer_added_on >= :start_date";				

		}

		

		//Data

		$get_stock_transfer_list_sdata[':start_date']  = $start_date;

		

		$filter_count++;

	}



	if($end_date != "")

	{

		if($filter_count == 0)

		{

			// Query

			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." where stock_transfer_added_on <= :end_date";								

		}

		else

		{

			// Query

			$get_stock_transfer_list_squery_where = $get_stock_transfer_list_squery_where." and stock_transfer_added_on <= :end_date";				

		}

		

		//Data

		$get_stock_transfer_list_sdata['end_date']  = $end_date;

		

		$filter_count++;

	}
	
	if($sort != "")
	{	
		$get_stock_transfer_list_squery_order_by = " order by stock_transfer_accepted_on DESC";

	}
	else
	{
		$get_stock_transfer_list_squery_order_by = " order by stock_transfer_added_on DESC";
	}

	$get_stock_transfer_list_squery = $get_stock_transfer_list_squery_base.$get_stock_transfer_list_squery_where.$get_stock_transfer_list_squery_order_by;
	

	try

	{

		$dbConnection = get_conn_handle();

		

		$get_stock_transfer_list_sstatement = $dbConnection->prepare($get_stock_transfer_list_squery);

		

		$get_stock_transfer_list_sstatement -> execute($get_stock_transfer_list_sdata);

		

		$get_stock_transfer_list_sdetails = $get_stock_transfer_list_sstatement -> fetchAll();

		

		if(FALSE === $get_stock_transfer_list_sdetails)

		{

			$return["status"] = FAILURE;

			$return["data"]   = "";

		}

		else if(count($get_stock_transfer_list_sdetails) <= 0)

		{

			$return["status"] = DB_NO_RECORD;

			$return["data"]   = "";

		}

		else

		{

			$return["status"] = DB_RECORD_ALREADY_EXISTS;

			$return["data"]   = $get_stock_transfer_list_sdetails;

		}

	}

	catch(PDOException $e)

	{

		// Log the error

		$return["status"] = FAILURE;

		$return["data"] = "";

	}

	

	return $return;

 }

 

 /*

PURPOSE : To update Stock Transfer

INPUT 	: Transfer ID, Stock Transfer Update Array

OUTPUT 	: Transfer ID; Message of success or failure

BY 		: Lakshmi

*/

function db_update_stock_transfer($transfer_id,$stock_transfer_update_data)

{

	if(array_key_exists("material_id",$stock_transfer_update_data))

	{	

		$material_id = $stock_transfer_update_data["material_id"];

	}

	else

	{

		$material_id = "";

	}

	if(array_key_exists("quantity",$stock_transfer_update_data))

	{	

		$quantity = $stock_transfer_update_data["quantity"];

	}

	else

	{

		$quantity = "";

	}
	
	if(array_key_exists("accepted_remarks",$stock_transfer_update_data))

	{	

		$accepted_remarks = $stock_transfer_update_data["accepted_remarks"];

	}

	else

	{

		$accepted_remarks = "";

	}
	
	if(array_key_exists("accepted_qty",$stock_transfer_update_data))

	{	

		$accepted_qty = $stock_transfer_update_data["accepted_qty"];

	}

	else

	{

		$accepted_qty = "";

	}

	
	if(array_key_exists("source_project",$stock_transfer_update_data))
	{	
		$source_project = $stock_transfer_update_data["source_project"];
	}

	else

	{
		$source_project = "";
	}

	
	if(array_key_exists("destination_project",$stock_transfer_update_data))
	{	
		$destination_project = $stock_transfer_update_data["destination_project"];
	}

	else

	{
		$destination_project = "";
	}
	
	if(array_key_exists("status",$stock_transfer_update_data))
	{	
		$status = $stock_transfer_update_data["status"];
	}

	else

	{
		$status = "";
	}

	

	if(array_key_exists("active",$stock_transfer_update_data))

	{	

		$active = $stock_transfer_update_data["active"];

	}

	else

	{

		$active = "";

	}

	

	if(array_key_exists("remarks",$stock_transfer_update_data))

	{	

		$remarks = $stock_transfer_update_data["remarks"];

	}

	else

	{

		$remarks = "";

	}

	

	if(array_key_exists("added_by",$stock_transfer_update_data))

	{	

		$added_by = $stock_transfer_update_data["added_by"];

	}

	else

	{

		$added_by = "";

	}

	if(array_key_exists("added_on",$stock_transfer_update_data))

	{	

		$added_on = $stock_transfer_update_data["added_on"];

	}

	else

	{

		$added_on = "";

	}
	
	if(array_key_exists("approved_by",$stock_transfer_update_data))

	{	

		$approved_by = $stock_transfer_update_data["approved_by"];

	}

	else

	{

		$approved_by = "";

	}

	if(array_key_exists("approved_on",$stock_transfer_update_data))

	{	

		$approved_on = $stock_transfer_update_data["approved_on"];

	}

	else

	{

		$approved_on = "";

	}
	
	if(array_key_exists("accepted_by",$stock_transfer_update_data))

	{	

		$accepted_by = $stock_transfer_update_data["accepted_by"];

	}

	else

	{

		$accepted_by = "";

	}

	if(array_key_exists("accepted_on",$stock_transfer_update_data))

	{	

		$accepted_on = $stock_transfer_update_data["accepted_on"];

	}

	else

	{

		$accepted_on = "";

	}

	

	// Query

    $stock_transfer_update_uquery_base = "update stock_transfer set";  

	

	$stock_transfer_update_uquery_set = "";

	

	$stock_transfer_update_uquery_where = " where stock_transfer_id=:transfer_id";

	

	$stock_transfer_update_udata = array(":transfer_id"=>$transfer_id);

	

	$filter_count = 0;

	

	if($material_id != "")

	{

		$stock_transfer_update_uquery_set = $stock_transfer_update_uquery_set." stock_transfer_material_id = :material_id,";

		$stock_transfer_update_udata[":material_id"] = $material_id;

		$filter_count++;

	}

	

	if($quantity != "")

	{

		$stock_transfer_update_uquery_set = $stock_transfer_update_uquery_set." stock_transfer_quantity = :quantity,";

		$stock_transfer_update_udata[":quantity"] = $quantity;

		$filter_count++;

	}
	
	if($accepted_qty != "")

	{

		$stock_transfer_update_uquery_set = $stock_transfer_update_uquery_set." stock_accepted_quantity = :accepted_qty,";

		$stock_transfer_update_udata[":accepted_qty"] = $accepted_qty;

		$filter_count++;

	}
	
	
	
	if($accepted_remarks != "")

	{

		$stock_transfer_update_uquery_set = $stock_transfer_update_uquery_set." stock_transfer_accepted_remarks = :accepted_remarks,";

		$stock_transfer_update_udata[":accepted_remarks"] = $accepted_remarks;

		$filter_count++;

	}

	
	if($source_project != "")
	{
		$stock_transfer_update_uquery_set = $stock_transfer_update_uquery_set." stock_transfer_source_project = :source_project,";
		$stock_transfer_update_udata[":source_project"] = $source_project;
		$filter_count++;

	}

	
	if($destination_project != "")
	{
		$stock_transfer_update_uquery_set = $stock_transfer_update_uquery_set." stock_transfer_destination_project = :destination_project,";
		$stock_transfer_update_udata[":destination_project"] = $destination_project;
		$filter_count++;

	}
	
	if($status != "")
	{
		$stock_transfer_update_uquery_set = $stock_transfer_update_uquery_set." stock_transfer_status = :status,";
		$stock_transfer_update_udata[":status"] = $status;
		$filter_count++;

	}

	

	if($active != "")

	{

		$stock_transfer_update_uquery_set = $stock_transfer_update_uquery_set." stock_transfer_active = :active,";

		$stock_transfer_update_udata[":active"] = $active;

		$filter_count++;

	}

	

	if($remarks != "")

	{

		$stock_transfer_update_uquery_set = $stock_transfer_update_uquery_set." stock_transfer_remarks = :remarks,";

		$stock_transfer_update_udata[":remarks"] = $remarks;

		$filter_count++;

	}

	

	if($added_by != "")

	{

		$stock_transfer_update_uquery_set = $stock_transfer_update_uquery_set." stock_transfer_added_by = :added_by,";

		$stock_transfer_update_udata[":added_by"] = $added_by;		

		$filter_count++;

	}

	if($added_on != "")

	{

		$stock_transfer_update_uquery_set = $stock_transfer_update_uquery_set." stock_transfer_added_on = :added_on,";

		$stock_transfer_update_udata[":added_on"] = $added_on;		

		$filter_count++;

	}
	
	if($approved_by != "")

	{

		$stock_transfer_update_uquery_set = $stock_transfer_update_uquery_set." stock_transfer_approved_by = :approved_by,";

		$stock_transfer_update_udata[":approved_by"] = $approved_by;		

		$filter_count++;

	}

	if($approved_on != "")

	{

		$stock_transfer_update_uquery_set = $stock_transfer_update_uquery_set." stock_transfer_approved_on = :approved_on,";

		$stock_transfer_update_udata[":approved_on"] = $approved_on;		

		$filter_count++;

	}
	
	if($accepted_by != "")

	{

		$stock_transfer_update_uquery_set = $stock_transfer_update_uquery_set." stock_transfer_accepted_by = :accepted_by,";

		$stock_transfer_update_udata[":accepted_by"] = $accepted_by;		

		$filter_count++;

	}

	if($accepted_on != "")

	{

		$stock_transfer_update_uquery_set = $stock_transfer_update_uquery_set." stock_transfer_accepted_on = :accepted_on,";

		$stock_transfer_update_udata[":accepted_on"] = $accepted_on;		

		$filter_count++;

	}

	

	if($filter_count > 0)

	{

		$stock_transfer_update_uquery_set = trim($stock_transfer_update_uquery_set,',');

	}

	

	$stock_transfer_update_uquery = $stock_transfer_update_uquery_base.$stock_transfer_update_uquery_set.$stock_transfer_update_uquery_where;

	

    try

    {

        $dbConnection = get_conn_handle();

        

        $stock_transfer_update_ustatement = $dbConnection->prepare($stock_transfer_update_uquery);		

        

        $stock_transfer_update_ustatement -> execute($stock_transfer_update_udata);

        

        $return["status"] = SUCCESS;

		$return["data"]   = $transfer_id;

    }

    catch(PDOException $e)

    {

        // Log the error

        $return["status"] = FAILURE;

		$return["data"]   = "";

    }

	

	return $return;

}

?>