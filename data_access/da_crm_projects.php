<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. Wherever there is name search, there should be a wildcard
*/

/*
PURPOSE : To add new project
INPUT 	: Project Name, Address, Client, Location, Authority, USP Added By
OUTPUT 	: Bank id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_project($project_name,$address,$client,$location,$authority,$usp,$added_by)
{
	// Query
    $project_iquery = "insert into crm_project_master (project_name,project_address,project_client_id,project_location,project_authority,project_usp,project_active,project_added_by,project_added_on) values (:project_name,:address,:client,:location,:authority,:usp,:active,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $project_istatement = $dbConnection->prepare($project_iquery);
        
        // Data
        $project_idata = array(':project_name'=>$project_name,':address'=>$address,':client'=>$client,':location'=>$location,':authority'=>$authority,':usp'=>$usp,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $project_istatement->execute($project_idata);
		$project_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $project_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get project list
INPUT 	: Project Name, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of projects
BY 		: Nitin Kashyap
*/
function db_get_project_list($project_name,$active,$added_by,$start_date,$end_date)
{
	$get_project_list_squery_base = "select * from crm_project_master";
	
	$get_project_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_project_list_sdata = array();
	
	if($project_name != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." where project_name=:project_name";								
		}
		else
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." and project_name=:project_name";				
		}
		
		// Data
		$get_project_list_sdata[':project_name']  = $project_name;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." where project_active=:active";								
		}
		else
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." and project_active=:active";				
		}
		
		// Data
		$get_project_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." where project_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." and project_added_by=:added_by";				
		}
		
		// Data
		$get_project_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." where project_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." and project_added_on >= :start_date";				
		}
		
		//Data
		$get_project_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." where project_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_project_list_squery_where = $get_project_list_squery_where." and project_added_on <= :end_date";				
		}
		
		//Data
		$get_project_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_project_list_squery = $get_project_list_squery_base.$get_project_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_project_list_sstatement = $dbConnection->prepare($get_project_list_squery);
		
		$get_project_list_sstatement -> execute($get_project_list_sdata);
		
		$get_project_list_sdetails = $get_project_list_sstatement -> fetchAll();
		
		if(FALSE === $get_project_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_project_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_project_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add sites to a project
INPUT 	: Site No, Project, Block, Wing, Dimension Master, Area, Cost, Site Type, Remarks, Status, Release Status, Bank Name, Added By
OUTPUT 	: Project Site Mapping id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_site_to_project($site_number,$project,$block,$wing,$dimension_master,$area,$cost,$site_type,$remarks,$status,$rel_status,$bank_name,$added_by)
{
	// Query
    $site_iquery = "insert into crm_site_master (crm_site_no,crm_project_id,crm_site_block,crm_site_wing,crm_site_dimension,crm_site_area,crm_site_featured,crm_site_cost,crm_site_type,crm_site_remarks,crm_site_status,crm_site_release_status,crm_site_bank_name,crm_site_mortgaged_on,crm_site_released_on,crm_delete_status,crm_site_added_by,crm_site_added_on,crm_site_last_updated_by,crm_site_last_updated_on) values (:site_no,:project,:block,:wing,:dimension,:area,:featured,:cost,:site_type,:remarks,:status,:rel_status,:bank_name,:mortgaged_on,:released_on,:del_status,:added_by,:now,:updated_by,:now)";  	
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $site_istatement = $dbConnection->prepare($site_iquery);
        
        // Data
        $site_idata = array(':site_no'=>$site_number,':project'=>$project,':block'=>$block,':wing'=>$wing,':dimension'=>$dimension_master,':area'=>$area,':featured'=>'1',':cost'=>$cost,':site_type'=>$site_type,':remarks'=>$remarks,':status'=>$status,':rel_status'=>$rel_status,':bank_name'=>$bank_name,':mortgaged_on'=>'',':released_on'=>'',':del_status'=>'0',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"),':updated_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));			
		$dbConnection->beginTransaction();
        $site_istatement->execute($site_idata);
		$site_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $site_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get site list
INPUT 	: Site Number, Project, Featured, Site Type, Dimension, Site Status, Release Status, Added By, Start Date, End Date
OUTPUT 	: List of sites
BY 		: Nitin Kashyap
*/
function db_get_site_list($site_number,$project,$featured,$type,$dimension,$status,$release_status,$added_by,$start_date,$end_date,$site_id="",$delete_status="0",$project_user="")
{
	$get_site_list_squery_base = "select * from crm_site_master SM inner join crm_dimension_master DM on DM.crm_dimension_id = SM.crm_site_dimension inner join crm_status_master STM on STM.status_id = SM.crm_site_status inner join crm_site_type ST on ST.crm_site_type_id = SM.crm_site_type inner join crm_release_status_master RSM on RSM.release_status_id = SM.crm_site_release_status inner join crm_bank_master CBM on CBM.crm_bank_master_id = SM.crm_site_bank_name inner join crm_project_master PM on PM.project_id = SM.crm_project_id";
	
	$get_site_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_site_list_sdata = array();
	
	if($site_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." where crm_site_id=:site_id";								
		}
		else
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." and crm_site_id=:site_id";				
		}
		
		// Data
		$get_site_list_sdata[':site_id']  = $site_id;
		
		$filter_count++;
	}		if($project_user != "")	{		if($filter_count == 0)		{			// Query			$get_site_list_squery_where = $get_site_list_squery_where." where PM.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";		}		else		{			// Query			$get_site_list_squery_where = $get_site_list_squery_where." and PM.project_id IN (select crm_user_project_mapping_project_id from crm_user_project_mapping where crm_user_project_mapping_user_id = :project_user_id and crm_user_project_mapping_active = 1)";						}				// Data		$get_site_list_sdata[':project_user_id']  = $project_user;		$filter_count++;	}
	
	if($site_number != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." where crm_site_no=:site_no";								
		}
		else
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." and crm_site_no=:site_no";				
		}
		
		// Data
		$get_site_list_sdata[':site_no']  = $site_number;
		
		$filter_count++;
	}
	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." where crm_project_id=:project";								
		}
		else
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." and crm_project_id=:project";				
		}
		
		// Data
		$get_site_list_sdata[':project']  = $project;
		
		$filter_count++;
	}
	
	if($featured != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." where crm_site_featured=:featured";								
		}
		else
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." and crm_site_featured=:featured";				
		}
		
		// Data
		$get_site_list_sdata[':featured']  = $featured;
		
		$filter_count++;
	}
	
	if($type != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." where crm_site_type=:type";								
		}
		else
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." and crm_site_type=:type";				
		}
		
		// Data
		$get_site_list_sdata[':type']  = $type;
		
		$filter_count++;
	}
	
	if($dimension != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." where crm_site_dimension=:dimension";								
		}
		else
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." and crm_site_dimension=:dimension";				
		}
		
		// Data
		$get_site_list_sdata[':dimension']  = $dimension;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." where crm_site_status=:status";								
		}
		else
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." and crm_site_status=:status";				
		}
		
		// Data
		$get_site_list_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($release_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." where crm_site_release_status=:rel_status";								
		}
		else
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." and crm_site_release_status=:rel_status";				
		}
		
		// Data
		$get_site_list_sdata[':rel_status']  = $release_status;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." where crm_site_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." and crm_site_added_by=:added_by";				
		}
		
		// Data
		$get_site_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." where crm_site_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." and crm_site_added_on >= :start_date";				
		}
		
		//Data
		$get_site_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." where crm_site_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." and crm_site_added_on <= :end_date";				
		}
		
		//Data
		$get_site_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($delete_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." where crm_delete_status = :delete_status";								
		}
		else
		{
			// Query
			$get_site_list_squery_where = $get_site_list_squery_where." and crm_delete_status = :delete_status";				
		}
		
		// Data
		$get_site_list_sdata[':delete_status']  = $delete_status;
		
		$filter_count++;
	}
	
	$get_site_list_squery_order = " order by ABS(crm_site_no) asc";
	
	$get_site_list_squery = $get_site_list_squery_base.$get_site_list_squery_where.$get_site_list_squery_order;	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_site_list_sstatement = $dbConnection->prepare($get_site_list_squery);
		
		$get_site_list_sstatement -> execute($get_site_list_sdata);
		
		$get_site_list_sdetails = $get_site_list_sstatement -> fetchAll();
		
		if(FALSE === $get_site_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_site_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_site_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To change status
INPUT 	: Site ID, Status, Updated By
OUTPUT 	: Site id, success or failure message
BY 		: Nitin Kashyap
*/
function db_change_status($site_id,$status,$updated_by)
{
	// Query
    $site_status_uquery = "update crm_site_master set crm_site_status=:status,crm_site_last_updated_by=:updated_by,crm_site_last_updated_on=:now where crm_site_id=:site_id";  	
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $site_status_ustatement = $dbConnection->prepare($site_status_uquery);
        
        // Data
        $site_status_udata = array(':status'=>$status,':updated_by'=>$updated_by,':now'=>date("Y-m-d H:i:s"),':site_id'=>$site_id);				
        
        $site_status_ustatement -> execute($site_status_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $site_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new survey to site
INPUT 	: Survey ID, Site ID, Added By
OUTPUT 	: Site Survey Mapping id, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_site_to_survey($survey_id,$site_id,$added_by)
{
	// Query
    $site_survey_iquery = "insert into crm_site_survey_mapping (crm_mapping_survey_id,crm_mapping_site_id,crm_mapping_added_by,crm_mapping_added_on) values (:survey_id,:site_id,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $site_survey_istatement = $dbConnection->prepare($site_survey_iquery);
        
        // Data
        $site_survey_idata = array(':survey_id'=>$survey_id,':site_id'=>$site_id,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $site_survey_istatement->execute($site_survey_idata);
		$site_survey_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $site_survey_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get site survey mapping
INPUT 	: Site ID, Survey ID, Added By, Start Date, End Date
OUTPUT 	: List of site survey mapping
BY 		: Nitin Kashyap
*/
function db_get_site_survey_list($site_id,$survey_id,$added_by,$start_date,$end_date)
{
	$get_site_survey_list_squery_base = "select * from crm_site_survey_mapping SSM inner join crm_site_master CSM on CSM.crm_site_id = SSM.crm_mapping_site_id inner join crm_survey_master CSUM on CSUM.crm_survey_master_id = SSM.crm_mapping_survey_id inner join users U on U.user_id = SSM.crm_mapping_added_by";
	
	$get_site_survey_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_site_survey_list_sdata = array();
	
	if($site_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_survey_list_squery_where = $get_site_survey_list_squery_where." where crm_mapping_site_id=:site_id";								
		}
		else
		{
			// Query
			$get_site_survey_list_squery_where = $get_site_survey_list_squery_where." and crm_mapping_site_id=:site_id";				
		}
		
		// Data
		$get_site_survey_list_sdata[':site_id']  = $site_id;
		
		$filter_count++;
	}
	
	if($survey_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_survey_list_squery_where = $get_site_survey_list_squery_where." where crm_mapping_survey_id=:survey_id";								
		}
		else
		{
			// Query
			$get_site_survey_list_squery_where = $get_site_survey_list_squery_where." and crm_mapping_survey_id=:survey_id";				
		}
		
		// Data
		$get_site_survey_list_sdata[':survey_id']  = $survey_id;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_survey_list_squery_where = $get_site_survey_list_squery_where." where crm_mapping_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_site_survey_list_squery_where = $get_site_survey_list_squery_where." and crm_mapping_added_by=:added_by";				
		}
		
		// Data
		$get_site_survey_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_survey_list_squery_where = $get_site_survey_list_squery_where." where crm_mapping_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_site_survey_list_squery_where = $get_site_survey_list_squery_where." and crm_mapping_added_on >= :start_date";				
		}
		
		//Data
		$get_site_survey_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_survey_list_squery_where = $get_site_survey_list_squery_where." where crm_mapping_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_site_survey_list_squery_where = $get_site_survey_list_squery_where." and crm_mapping_added_on <= :end_date";				
		}
		
		//Data
		$get_site_survey_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_site_survey_list_squery_order = "";
	
	$get_site_survey_list_squery = $get_site_survey_list_squery_base.$get_site_survey_list_squery_where.$get_site_survey_list_squery_order;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_site_survey_list_sstatement = $dbConnection->prepare($get_site_survey_list_squery);
		
		$get_site_survey_list_sstatement -> execute($get_site_survey_list_sdata);
		
		$get_site_survey_list_sdetails = $get_site_survey_list_sstatement -> fetchAll();
		
		if(FALSE === $get_site_survey_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_site_survey_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_site_survey_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add new survey number
INPUT 	: Survey No, Project, Added By
OUTPUT 	: Survey No ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_add_survey_no($survey_no,$project,$added_by)
{
	// Query
    $survey_iquery = "insert into crm_survey_master (crm_survey_master_survey_no,crm_survey_master_project,crm_survey_master_active,crm_survey_master_added_by,crm_survey_master_added_on) values (:survey_no,:project,:active,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $survey_istatement = $dbConnection->prepare($survey_iquery);
        
        // Data
        $survey_idata = array(':survey_no'=>$survey_no,':project'=>$project,':active'=>'1',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $survey_istatement->execute($survey_idata);
		$survey_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $survey_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get survey number list
INPUT 	: Survey No, Project, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of banks
BY 		: Nitin Kashyap
*/
function db_get_survey_no_list($survey_no,$project,$active,$added_by,$start_date,$end_date)
{
	$get_survey_list_squery_base = "select * from crm_survey_master SM inner join users U on U.user_id = SM.crm_survey_master_added_by inner join crm_project_master PM on PM.project_id = SM.crm_survey_master_project";
	
	$get_survey_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_survey_list_sdata = array();
	
	if($survey_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_list_squery_where = $get_survey_list_squery_where." where crm_survey_master_survey_no=:survey_no";								
		}
		else
		{
			// Query
			$get_survey_list_squery_where = $get_survey_list_squery_where." and crm_survey_master_survey_no=:survey_no";				
		}
		
		// Data
		$get_survey_list_sdata[':survey_no']  = $survey_no;
		
		$filter_count++;
	}
	
	if($project != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_list_squery_where = $get_survey_list_squery_where." where crm_survey_master_project=:project";								
		}
		else
		{
			// Query
			$get_survey_list_squery_where = $get_survey_list_squery_where." and crm_survey_master_project=:project";				
		}
		
		// Data
		$get_survey_list_sdata[':project']  = $project;
		
		$filter_count++;
	}
	
	if($active != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_list_squery_where = $get_survey_list_squery_where." where crm_survey_master_active=:active";								
		}
		else
		{
			// Query
			$get_survey_list_squery_where = $get_survey_list_squery_where." and crm_survey_master_active=:active";				
		}
		
		// Data
		$get_survey_list_sdata[':active']  = $active;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_list_squery_where = $get_survey_list_squery_where." where crm_survey_master_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_survey_list_squery_where = $get_survey_list_squery_where." and crm_survey_master_added_by=:added_by";				
		}
		
		// Data
		$get_survey_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_list_squery_where = $get_survey_list_squery_where." where crm_survey_master_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_survey_list_squery_where = $get_survey_list_squery_where." and crm_survey_master_added_on >= :start_date";				
		}
		
		//Data
		$get_survey_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_survey_list_squery_where = $get_survey_list_squery_where." where crm_survey_master_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_survey_list_squery_where = $get_survey_list_squery_where." and crm_survey_master_added_on <= :end_date";				
		}
		
		//Data
		$get_survey_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_survey_list_squery = $get_survey_list_squery_base.$get_survey_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_survey_list_sstatement = $dbConnection->prepare($get_survey_list_squery);
		
		$get_survey_list_sstatement -> execute($get_survey_list_sdata);
		
		$get_survey_list_sdetails = $get_survey_list_sstatement -> fetchAll();
		
		if(FALSE === $get_survey_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_survey_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_survey_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update site details
INPUT 	: Site ID, Site Data Array, Updated By
OUTPUT 	: Site ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_site_details($site_details_data,$updated_by)
{
	//Extract Data
	if(array_key_exists("site_id",$site_details_data))
	{
		$site_id = $site_details_data["site_id"];
	}
	else
	{
		$site_id = "";
	}
	
	if(array_key_exists("bank",$site_details_data))
	{
		$bank = $site_details_data["bank"];
	}
	else
	{
		$bank = "";
	}
	
	if(array_key_exists("mortgage_date",$site_details_data))
	{
		$mortgage_date = $site_details_data["mortgage_date"];
	}
	else
	{
		$mortgage_date = "";
	}
	
	if(array_key_exists("release_date",$site_details_data))
	{
		$release_date = $site_details_data["release_date"];
	}
	else
	{
		$release_date = "";
	}
	
	if(array_key_exists("site_no",$site_details_data))
	{
		$site_no = $site_details_data["site_no"];
	}
	else
	{
		$site_no = "";
	}
	
	if(array_key_exists("block",$site_details_data))
	{
		$block = $site_details_data["block"];
	}
	else
	{
		$block = "";
	}
	
	if(array_key_exists("wing",$site_details_data))
	{
		$wing = $site_details_data["wing"];
	}
	else
	{
		$wing = "";
	}
	
	if(array_key_exists("dimension",$site_details_data))
	{
		$dimension = $site_details_data["dimension"];
	}
	else
	{
		$dimension = "";
	}
	
	if(array_key_exists("area",$site_details_data))
	{
		$area = $site_details_data["area"];
	}
	else
	{
		$area = "";
	}
	
	if(array_key_exists("site_type",$site_details_data))
	{
		$site_type = $site_details_data["site_type"];
	}
	else
	{
		$site_type = "";
	}
	
	if(array_key_exists("release_status",$site_details_data))
	{
		$release_status = $site_details_data["release_status"];
	}
	else
	{
		$release_status = "";
	}
	
	// Query
    $site_uquery_base = "update crm_site_master set";
	
	$site_uquery_set = "";
	
	$site_uquery_where = " where crm_site_id=:site_id";
	$site_udata = array(":site_id"=>$site_id);
	
	$filter_count = 0;
	
	if($bank != "")
	{
		$site_uquery_set = $site_uquery_set." crm_site_bank_name=:bank,";
		$site_udata[":bank"] = $bank;
		$filter_count++;
	}
	
	if($mortgage_date != "")
	{
		$site_uquery_set = $site_uquery_set." crm_site_mortgaged_on=:mortgage_date,";
		$site_udata[":mortgage_date"] = $mortgage_date;
		$filter_count++;
	}
	
	if($release_date != "")
	{
		$site_uquery_set = $site_uquery_set." crm_site_released_on=:release_date,";
		$site_udata[":release_date"] = $release_date;
		$filter_count++;
	}
	
	if($site_no != "")
	{
		$site_uquery_set = $site_uquery_set." crm_site_no=:site_no,";
		$site_udata[":site_no"] = $site_no;
		$filter_count++;
	}
	
	if($block != "")
	{
		$site_uquery_set = $site_uquery_set." crm_site_block=:block,";
		$site_udata[":block"] = $block;
		$filter_count++;
	}
	
	if($wing != "")
	{
		$site_uquery_set = $site_uquery_set." crm_site_wing=:wing,";
		$site_udata[":wing"] = $wing;
		$filter_count++;
	}
	
	if($dimension != "")
	{
		$site_uquery_set = $site_uquery_set." crm_site_dimension=:dimension,";
		$site_udata[":dimension"] = $dimension;
		$filter_count++;
	}
	
	if($area != "")
	{
		$site_uquery_set = $site_uquery_set." crm_site_area=:area,";
		$site_udata[":area"] = $area;
		$filter_count++;
	}
	
	if($site_type != "")
	{
		$site_uquery_set = $site_uquery_set." crm_site_type=:site_type,";
		$site_udata[":site_type"] = $site_type;
		$filter_count++;
	}
	
	if($release_status != "")
	{
		$site_uquery_set = $site_uquery_set." crm_site_release_status=:release_status,";
		$site_udata[":release_status"] = $release_status;
		$filter_count++;
	}
	
	$site_uquery = $site_uquery_base.trim($site_uquery_set,',').$site_uquery_where;	

    try
    {
        $dbConnection = get_conn_handle();
        
        $site_ustatement = $dbConnection->prepare($site_uquery);            
        
        $site_ustatement -> execute($site_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $site_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To get site data
INPUT 	: Site Summary Data
OUTPUT 	: Site Data
BY 		: Nitin Kashyap
*/
function db_get_site_summary($site_summary_data)
{
	// Extract Data
	if(array_key_exists("project",$site_summary_data))
	{
		$project_id = $site_summary_data["project"];
	}
	else
	{
		$project_id = "";
	}
	
	if(array_key_exists("status",$site_summary_data))
	{
		$status = $site_summary_data["status"];
	}
	else
	{
		$status = "";
	}
	
	if(array_key_exists("delete_status",$site_summary_data))
	{
		$delete_status = $site_summary_data["delete_status"];
	}
	else
	{
		$delete_status = "";
	}

	$get_site_summary_base = "select count(*) as site_count,sum(crm_site_area) as area from crm_site_master";
	
	$get_site_summary_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_site_summary_data = array();
	
	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_summary_where = $get_site_summary_where." where crm_project_id=:project_id";								
		}
		else
		{
			// Query
			$get_site_summary_where = $get_site_summary_where." and crm_project_id=:project_id";				
		}
		
		// Data
		$get_site_summary_data[':project_id']  = $project_id;
		
		$filter_count++;
	}	
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_summary_where = $get_site_summary_where." where crm_site_status=:status";								
		}
		else
		{
			// Query
			$get_site_summary_where = $get_site_summary_where." and crm_site_status=:status";				
		}
		
		// Data
		$get_site_summary_data[':status']  = $status;
		
		$filter_count++;
	}

	if($delete_status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_summary_where = $get_site_summary_where." where crm_delete_status = :delete_status";								
		}
		else
		{
			// Query
			$get_site_summary_where = $get_site_summary_where." and crm_delete_status = :delete_status";				
		}
		
		// Data
		$get_site_summary_data[':delete_status']  = $delete_status;
		
		$filter_count++;
	}	
	
	$get_site_summary_squery = $get_site_summary_base.$get_site_summary_where;	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_site_summary_sstatement = $dbConnection->prepare($get_site_summary_squery);
		
		$get_site_summary_sstatement -> execute($get_site_summary_data);
		
		$get_site_summary_details = $get_site_summary_sstatement -> fetchAll();
		
		if(FALSE === $get_site_summary_details)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_site_summary_details) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_site_summary_details;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}/*PURPOSE : To add new CRM User Project MappingINPUT 	: User ID, Project ID, Remarks, Added ByOUTPUT 	: Mapping ID, success or failure messageBY 		: Lakshmi*/function db_add_crm_user_project_mapping($user_id,$project_id,$remarks,$added_by){	// Query   $crm_user_project_mapping_iquery = "insert into crm_user_project_mapping    (crm_user_project_mapping_user_id,crm_user_project_mapping_project_id,crm_user_project_mapping_active,crm_user_project_mapping_remarks,crm_user_project_mapping_added_by,   crm_user_project_mapping_added_on) values (:user_id,:project_id,:active,:remarks,:added_by,:added_on)";  	    try    {        $dbConnection = get_conn_handle();        $crm_user_project_mapping_istatement = $dbConnection->prepare($crm_user_project_mapping_iquery);                // Data        $crm_user_project_mapping_idata = array(':user_id'=>$user_id,':project_id'=>$project_id,':active'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,		':added_on'=>date("Y-m-d H:i:s"));				$dbConnection->beginTransaction();        $crm_user_project_mapping_istatement->execute($crm_user_project_mapping_idata);		$crm_user_project_mapping_id = $dbConnection->lastInsertId();		$dbConnection->commit();                $return["status"] = SUCCESS;		$return["data"]   = $crm_user_project_mapping_id;		    }    catch(PDOException $e)    {        // Log the error        $return["status"] = FAILURE;		$return["data"]   = "";    }        return $return;}/*PURPOSE : To get CRM User Project Mapping ListINPUT 	: Mapping ID, User ID, Project ID, Active, Added By, Start Date(for added on), End Date(for added on)OUTPUT 	: List of User Project MappingBY 		: Lakshmi*/function db_get_crm_user_project_mapping($crm_user_project_mapping_search_data){  	if(array_key_exists("mapping_id",$crm_user_project_mapping_search_data))	{		$mapping_id = $crm_user_project_mapping_search_data["mapping_id"];	}	else	{		$mapping_id= "";	}		if(array_key_exists("user_id",$crm_user_project_mapping_search_data))	{		$user_id = $crm_user_project_mapping_search_data["user_id"];	}	else	{		$user_id = "";	}		if(array_key_exists("project_id",$crm_user_project_mapping_search_data))	{		$project_id = $crm_user_project_mapping_search_data["project_id"];	}	else	{		$project_id = "";	}		if(array_key_exists("active",$crm_user_project_mapping_search_data))	{		$active = $crm_user_project_mapping_search_data["active"];	}	else	{		$active = "";	}		if(array_key_exists("project_active",$crm_user_project_mapping_search_data))	{		$project_active = $crm_user_project_mapping_search_data["project_active"];	}	else	{		$project_active = "";	}		if(array_key_exists("added_by",$crm_user_project_mapping_search_data))	{		$added_by = $crm_user_project_mapping_search_data["added_by"];	}	else	{		$added_by = "";	}		if(array_key_exists("start_date",$crm_user_project_mapping_search_data))	{		$start_date= $crm_user_project_mapping_search_data["start_date"];	}	else	{		$start_date= "";	}		if(array_key_exists("end_date",$crm_user_project_mapping_search_data))	{		$end_date= $crm_user_project_mapping_search_data["end_date"];	}	else	{		$end_date= "";	}		$get_crm_user_project_mapping_list_squery_base = "select *,U.user_name as project_user,AU.user_name as added_by from crm_user_project_mapping CUPM inner join users U on U.user_id=CUPM.crm_user_project_mapping_user_id inner join crm_project_master CPM on CPM.project_id= CUPM.crm_user_project_mapping_project_id inner join users AU on AU.user_id= CUPM.crm_user_project_mapping_added_by";		$get_crm_user_project_mapping_list_squery_where = "";	$get_crm_user_project_mapping_list_oder_by = "";	$filter_count = 0;		// Data	$get_crm_user_project_mapping_list_sdata = array();		if($mapping_id != "")	{		if($filter_count == 0)		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." where crm_user_project_mapping_id = :mapping_id";										}		else		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." and crm_user_project_mapping_id = :mapping_id";						}				// Data		$get_crm_user_project_mapping_list_sdata[':mapping_id'] = $mapping_id;				$filter_count++;	}		if($user_id != "")	{		if($filter_count == 0)		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." where crm_user_project_mapping_user_id = :user_id";										}		else		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." and crm_user_project_mapping_user_id = :user_id";						}				// Data		$get_crm_user_project_mapping_list_sdata[':user_id'] = $user_id;				$filter_count++;	}		if($project_id != "")	{		if($filter_count == 0)		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." where crm_user_project_mapping_project_id = :project_id";										}		else		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." and crm_user_project_mapping_project_id = :project_id";						}				// Data		$get_crm_user_project_mapping_list_sdata[':project_id'] = $project_id;				$filter_count++;	}		if($active != "")	{		if($filter_count == 0)		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." where crm_user_project_mapping_active = :active";										}		else		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." and crm_user_project_mapping_active = :active";						}				// Data		$get_crm_user_project_mapping_list_sdata[':active']  = $active;				$filter_count++;	}		if($project_active != "")	{		if($filter_count == 0)		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." where CPM.project_active = :project_active";			}		else		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." and CPM.project_active = :project_active";		}				// Data		$get_crm_user_project_mapping_list_sdata[':project_active']  = $project_active;				$filter_count++;	}		if($added_by!= "")	{		if($filter_count == 0)		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." where crm_user_project_mapping_added_by = :added_by";										}		else		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." and crm_user_project_mapping_added_by = :added_by";		}				//Data		$get_crm_user_project_mapping_list_sdata[':added_by']  = $added_by;				$filter_count++;	}		if($start_date != "")	{		if($filter_count == 0)		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." where crm_user_project_mapping_added_on >= :start_date";										}		else		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." and crm_user_project_mapping_added_on >= :start_date";						}				//Data		$get_crm_user_project_mapping_list_sdata[':start_date']  = $start_date;				$filter_count++;	}	if($end_date != "")	{		if($filter_count == 0)		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." where crm_user_project_mapping_added_on <= :end_date";										}		else		{			// Query			$get_crm_user_project_mapping_list_squery_where = $get_crm_user_project_mapping_list_squery_where." and crm_user_project_mapping_added_on <= :end_date";		}				//Data		$get_crm_user_project_mapping_list_sdata['end_date']  = $end_date;				$filter_count++;	}	$get_crm_user_project_mapping_list_oder_by = " order by project_name ASC";	$get_crm_user_project_mapping_list_squery = $get_crm_user_project_mapping_list_squery_base.$get_crm_user_project_mapping_list_squery_where.$get_crm_user_project_mapping_list_oder_by;		try	{		$dbConnection = get_conn_handle();				$get_crm_user_project_mapping_list_sstatement = $dbConnection->prepare($get_crm_user_project_mapping_list_squery);				$get_crm_user_project_mapping_list_sstatement -> execute($get_crm_user_project_mapping_list_sdata);				$get_crm_user_project_mapping_list_sdetails = $get_crm_user_project_mapping_list_sstatement -> fetchAll();				if(FALSE === $get_crm_user_project_mapping_list_sdetails)		{			$return["status"] = FAILURE;			$return["data"]   = "";		}		else if(count($get_crm_user_project_mapping_list_sdetails) <= 0)		{			$return["status"] = DB_NO_RECORD;			$return["data"]   = "";		}		else		{			$return["status"] = DB_RECORD_ALREADY_EXISTS;			$return["data"]   = $get_crm_user_project_mapping_list_sdetails;		}	}	catch(PDOException $e)	{		// Log the error		$return["status"] = FAILURE;		$return["data"] = "";	}		return $return; } /*PURPOSE : To update CRM User Project MappingINPUT 	: Mapping ID, User Project Mapping Update ArrayOUTPUT 	: Mapping ID; Message of success or failureBY 		: Lakshmi*/function db_update_crm_user_project_mapping($mapping_id,$crm_user_project_mapping_update_data){	if(array_key_exists("user_id",$crm_user_project_mapping_update_data))	{			$user_id = $crm_user_project_mapping_update_data["user_id"];	}	else	{		$user_id = "";	}		if(array_key_exists("project_id",$crm_user_project_mapping_update_data))	{			$project_id = $crm_user_project_mapping_update_data["project_id"];	}	else	{		$project_id = "";	}		if(array_key_exists("active",$crm_user_project_mapping_update_data))	{			$active = $crm_user_project_mapping_update_data["active"];	}	else	{		$active = "";	}		if(array_key_exists("remarks",$crm_user_project_mapping_update_data))	{			$remarks = $crm_user_project_mapping_update_data["remarks"];	}	else	{		$remarks = "";	}		if(array_key_exists("added_by",$crm_user_project_mapping_update_data))	{			$added_by = $crm_user_project_mapping_update_data["added_by"];	}	else	{		$added_by = "";	}		if(array_key_exists("added_on",$crm_user_project_mapping_update_data))	{			$added_on = $crm_user_project_mapping_update_data["added_on"];	}	else	{		$added_on = "";	}		// Query    $crm_user_project_mapping_update_uquery_base = "update crm_user_project_mapping set ";  		$crm_user_project_mapping_update_uquery_set = "";		$crm_user_project_mapping_update_uquery_where = " where crm_user_project_mapping_id = :mapping_id";		$crm_user_project_mapping_update_udata = array(":mapping_id"=>$mapping_id);		$filter_count = 0;		if($user_id != "")	{		$crm_user_project_mapping_update_uquery_set = $crm_user_project_mapping_update_uquery_set." crm_user_project_mapping_user_id = :user_id,";		$crm_user_project_mapping_update_udata[":user_id"] = $user_id;		$filter_count++;	}		if($project_id != "")	{		$crm_user_project_mapping_update_uquery_set = $crm_user_project_mapping_update_uquery_set." crm_user_project_mapping_project_id = :project_id,";		$crm_user_project_mapping_update_udata[":project_id"] = $project_id;		$filter_count++;	}		if($active != "")	{		$crm_user_project_mapping_update_uquery_set = $crm_user_project_mapping_update_uquery_set." crm_user_project_mapping_active = :active,";		$crm_user_project_mapping_update_udata[":active"] = $active;		$filter_count++;	}		if($remarks != "")	{		$crm_user_project_mapping_update_uquery_set = $crm_user_project_mapping_update_uquery_set." crm_user_project_mapping_remarks = :remarks,";		$crm_user_project_mapping_update_udata[":remarks"] = $remarks;		$filter_count++;	}		if($added_by != "")	{		$crm_user_project_mapping_update_uquery_set = $crm_user_project_mapping_update_uquery_set." crm_user_project_mapping_added_by = :added_by,";		$crm_user_project_mapping_update_udata[":added_by"] = $added_by;		$filter_count++;	}		if($added_on != "")	{		$crm_user_project_mapping_update_uquery_set = $crm_user_project_mapping_update_uquery_set." crm_user_project_mapping_added_on = :added_on,";		$crm_user_project_mapping_update_udata[":added_on"] = $added_on;				$filter_count++;	}		if($filter_count > 0)	{		$crm_user_project_mapping_update_uquery_set = trim($crm_user_project_mapping_update_uquery_set,',');	}		$crm_user_project_mapping_update_uquery = $crm_user_project_mapping_update_uquery_base.$crm_user_project_mapping_update_uquery_set.$crm_user_project_mapping_update_uquery_where;    try    {        $dbConnection = get_conn_handle();                $crm_user_project_mapping_update_ustatement = $dbConnection->prepare($crm_user_project_mapping_update_uquery);		                $crm_user_project_mapping_update_ustatement -> execute($crm_user_project_mapping_update_udata);                $return["status"] = SUCCESS;		$return["data"]   = $mapping_id;    }    catch(PDOException $e)    {        // Log the error        $return["status"] = FAILURE;		$return["data"]   = "";    }		return $return;}
?>