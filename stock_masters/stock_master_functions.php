<?php
/**
 * @author Nitin kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_stock_masters.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');

/*
PURPOSE : To add Storage 
INPUT 	: Storage, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_storage($name,$remarks,$added_by)
{
	$storage_search_data = array("name"=>$name,"storage_name_check"=>'1',"active"=>'1');
	$storage_sresult = db_get_storage_condition($storage_search_data);
	
	if($storage_sresult["status"] == DB_NO_RECORD)
	{
		$storage_iresult = db_add_storage_condition($name,$remarks,$added_by);
		
		if($storage_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Storage Condition Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Storage name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Storage list
INPUT 	: Storage Search Data Array
OUTPUT 	: Storage List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_storage_condition_list($storage_search_data)
{
	$storage_sresult = db_get_storage_condition($storage_search_data);
	
	if($storage_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $storage_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Storage Condition added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Storage Condition Master List
INPUT 	: Storage ID, Storage Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_storage_condition($storage_id,$storage_update_data)
{
	$storage_search_data = array("name"=>$storage_update_data['name'],"storage_name_check"=>'1',"active"=>'1');
	$storage_sresult = db_get_storage_condition($storage_search_data);
	
	$allow_update = false;
	if($storage_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($storage_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($storage_sresult['data'][0]['stock_storage_id'] == $storage_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$storage_condition_sresult = db_update_storage_condition($storage_id,$storage_update_data);
		
		if($storage_condition_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Storage Condition Details Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Storage Name Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To delete Storage Condition Master List
INPUT 	: Storage ID, Storage Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_storage_condition($storage_id,$storage_update_data)
{
	$storage_update_data = array('active'=>'0');
	$storage_condition_sresult = db_update_storage_condition($storage_id,$storage_update_data);
	
	if($storage_condition_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Storage Condition Successfully Deleted";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}
/*
PURPOSE : To add Transporation
INPUT 	: Transporation Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_transporation($transportation_name,$transportation_remarks,$transportation_added_by)
{
	$transportation_search_data = array("transportation_name"=>$transportation_name,"active"=>'1',"stock_transportation_name_check"=>'1');
	$transportation_sresult = db_get_stock_transportation_list($transportation_search_data);
	
	if($transportation_sresult["status"] == DB_NO_RECORD)
	{
		$transporation_iresult = db_add_stock_transportation($transportation_name,$transportation_remarks,$transportation_added_by);
		
		if($transporation_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Transporation Details Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Transportation name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Transporation list
INPUT 	: Transportation Search Data: Transporation Name, Active
OUTPUT 	: Transporation List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_transportation_list($transportation_search_data)
{
	$transportation_sresult = db_get_stock_transportation_list($transportation_search_data);
	
	if($transportation_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $transportation_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Transportation Details added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Transporation List
INPUT 	: Transporation ID, Transporation Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_transportation_list($transportation_id,$transportation_update_data)
{
	$transportation_search_data = array("transportation_name"=>$transportation_update_data['transportation_name'],"stock_transportation_name_check"=>'1',"active"=>'1');
	$transportation_sresult = db_get_stock_transportation_list($transportation_search_data);
	
	$allow_update = false;
	if($transportation_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($transportation_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($transportation_sresult['data'][0]['stock_transportation_id'] == $transportation_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$transportation_sresult = db_update_transportation_list($transportation_id,$transportation_update_data);
		
		if($transportation_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Transportation Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Transporation Mode Already Exists";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To update Transporation List
INPUT 	: Transporation ID, Transporation Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_transportation_list($transportation_id,$transportation_update_data)
{
	$transportation_sresult = db_update_transportation_list($transportation_id,$transportation_update_data);
	
	if($transportation_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "transportation Successfully added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}

	return $return;
}

/*
PURPOSE : To add Unit Measure 
INPUT 	: Unit Name, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_unit_measure($name,$remarks,$added_by)
{
	$stock_unit_measure_search_data = array("name"=>$name,"unit_name_check"=>'1',"active"=>'1');
	$unit_sresult = db_get_stock_unit_measure_list($stock_unit_measure_search_data);
	
	if($unit_sresult["status"] == DB_NO_RECORD)
	{
		$unit_iresult = db_add_stock_unit_measure($name,$remarks,$added_by);
		
		if($unit_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Unit Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Unit Name already exists";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Unit Measure list
INPUT 	: Unit Name, Active
OUTPUT 	: Unit List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_stock_unit_measure_list($stock_unit_measure_search_data)
{
	$unit_sresult = db_get_stock_unit_measure_list($stock_unit_measure_search_data);
	
	if($unit_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $unit_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No unit added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Unit Measure Master List
INPUT 	: Unit ID, Unit Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_unit_measure_list($unit_id,$unit_measure_update_data)
{
	$stock_unit_measure_search_data = array("name"=>$unit_measure_update_data['name'],"unit_name_check"=>'1',"active"=>'1');
	$unit_sresult = db_get_stock_unit_measure_list($stock_unit_measure_search_data);
	
	$allow_update = false;
	if($unit_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($unit_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($unit_sresult['data'][0]['stock_unit_id'] == $unit_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$unit_measure_sresult = db_update_unit_measure_list($unit_id,$unit_measure_update_data);
		
		if($unit_measure_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "unit measure Successfully added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Unit Name Already Exists";
		$return["status"] = FAILURE;
	}
		
	return $return;
}
/*
PURPOSE : To delete Unit
INPUT 	: Unit ID, Unit Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_unit_measure_list($unit_id,$unit_measure_update_data)
{
	$unit_measure_sresult = db_update_unit_measure_list($unit_id,$unit_measure_update_data);
	
	if($unit_measure_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "unit measure Successfully added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}

		
	return $return;
}

/*
PURPOSE : To add Indicator
INPUT 	: Indicator Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_indicator($indicator_name,$indicator_remarks,$indicator_added_by)
{
	$stock_indicator_search_data = array("indicator_name"=>$indicator_name,"indicator_name_check"=>'1',"active"=>'1');
	$indicator_sresult = db_get_stock_indicator_list($stock_indicator_search_data);
	
	if($indicator_sresult["status"] == DB_NO_RECORD)
	{
		$indicator_iresult = db_add_stock_indicator($indicator_name,$indicator_remarks,$indicator_added_by);
		
		if($indicator_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Indicator Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "This Indicator is already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Indicator list
INPUT 	: Indicator Name, Active
OUTPUT 	: Indicator List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_indicator_list($stock_indicator_search_data)
{
	$indicator_sresult = db_get_stock_indicator_list($stock_indicator_search_data);
	
	if($indicator_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $indicator_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No indicator added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Indicator
INPUT 	: Indicator ID, Indicator Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_indicator_list($indicator_id,$indicator_update_data)
{
	$stock_indicator_search_data = array("indicator_name"=>$indicator_update_data['indicator_name'],"indicator_name_check"=>'1','active'=>'1');
	$indicator_rresult = db_get_stock_indicator_list($stock_indicator_search_data);
	
	$allow_update = false;
	if($indicator_rresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($indicator_rresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($indicator_rresult['data'][0]['stock_indicator_id'] == $indicator_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$indicator_sresult = db_update_indicator_list($indicator_id,$indicator_update_data);
		
		if($indicator_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Indicator Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
			$return["data"]   = "Indicator Name already Exists";
			$return["status"] = FAILURE;
	}
	return $return;
}

/*
PURPOSE : To delete Indicator
INPUT 	: Indicator ID, Indicator Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_indicator_list($indicator_id,$indicator_update_data)
{
	$indicator_sresult = db_update_indicator_list($indicator_id,$indicator_update_data);

	if($indicator_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Indicator Successfully Deleted";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}

	return $return;
}

/*
PURPOSE : To add Rate 
INPUT 	: Rate ,Apllicabel Rate, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_stock_rate_list($stock_rate,$stock_rate_applicable_date,$stock_rate_remarks,$stock_rate_added_by)
{
	$stock_rate_search_data = array("stock_rate"=>$stock_rate,"stock_rate_applicable_date"=>$stock_rate_applicable_date);
	$rate_sresult = db_get_stock_rate_list($stock_rate_search_data);
	if($rate_sresult["status"] == DB_NO_RECORD)
	{
		$rate_iresult = db_add_stock_rate_history($stock_rate,$stock_rate_applicable_date,$stock_rate_remarks,$stock_rate_added_by);
		
		if($rate_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Rate Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "This Rate is already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Rate list
INPUT 	: Stock Rate Search data
OUTPUT 	: Rate List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_stock_rate_list($stock_rate_search_data)
{
	$rate_sresult = db_get_stock_rate_list($stock_rate_search_data);
	
	if($rate_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $rate_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No indicator added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Stock Rate History List
INPUT 	: Rate ID, Stock Rate Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_stock_rate($rate_id,$stock_rate_update_data)

{
		$rate_sresult = db_update_stock_rate($rate_id,$stock_rate_update_data);
		
		if($rate_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "rate Successfully added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
		
	return $return;
}

/*
PURPOSE : To add Material
INPUT 	: Material Code,Material Name,Material Type,Unit,Storage conditions, Transporation, Manufacturer Part No, Lead Time, Indicator,Price,Price Date, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_material($material_code,$material_name,$material_type,$material_unit,$material_storage,$material_transportation_mode,$material_manufacturer_part_number,$material_lead_time,$material_indicator,$material_price,$material_price_as_on,$material_remarks,$material_added_by)
{
	$stock_material_search_data = array("material_code_check"=>'1',"material_code"=>$material_code,"material_active"=>'1');
	$material_sresult = db_get_stock_material_master_list($stock_material_search_data);	
	
	if($material_sresult["status"] == DB_NO_RECORD)
	{
		$material_iresult = db_add_stock_material_master($material_code,$material_name,$material_type,$material_unit,$material_storage,$material_transportation_mode,$material_manufacturer_part_number,$material_lead_time,$material_indicator,$material_price,$material_price_as_on,$material_remarks,$material_added_by);		
		
		if($material_iresult['status'] == SUCCESS)
		{
			$return["data"]   = $material_iresult['data'];
			$return["status"] = SUCCESS;	
			
			// Add Opening stock as 0
			$opening_stock_result = i_add_material_stock($material_iresult['data'],'0',$material_unit,'0','0','0','0','',$material_added_by);
				
			$rate_iresult = db_add_stock_rate_history($material_price,$material_price_as_on,$material_remarks,$material_added_by);
				 
			if($rate_iresult['status'] == SUCCESS)
			{ 
			     $return["status"] = SUCCESS;		
			}
			else
			{
				$return["data"]   = "Internal Error. Please try again later";
				$return["status"] = FAILURE;
			}
		}
	}		
	else
	{
		$return["data"]   = "This Material already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Material list
INPUT 	: Material Code Material Name, Active
OUTPUT 	: Material List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_stock_material_master_list($stock_material_search_data)
{
	$material_sresult = db_get_stock_material_master_list($stock_material_search_data);
	
	if($material_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $material_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Material added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update To update Material Master List
INPUT 	: Material ID, Material Master update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_material_list($material_id,$material_master_update_data)
{
	$stock_material_search_data = array("material_code"=>$material_master_update_data['material_code'],"material_code_check"=>'1',"active"=>'1');
	$material_sresult = db_get_stock_material_master_list($stock_material_search_data);
	
	$allow_update = false;
	if($material_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($material_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($material_sresult['data'][0]['stock_material_id'] == $material_id)
		{
			$allow_update = true;
		}
	}
		
	if($allow_update == true)
	{
		$material_master_sresult = db_update_material_list($material_id,$material_master_update_data);
		
		if($material_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Material Master Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Material Name already Exists";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To delete Material 
INPUT 	: Material ID, Material Master update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_material_list($material_id,$material_master_update_data)
{
	$material_master_sresult = db_update_material_list($material_id,$material_master_update_data);
	
	if($material_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Material Successfully Deleted";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add Project
INPUT 	: Project Name, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_stock_project($project_name,$remarks,$added_by)
{
	$stock_project_search_data = array("project_name"=>$project_name,'project_name_check'=>'1',"active"=>'1');
	$project_sresult = db_get_stock_project_list($stock_project_search_data);
	
	if($project_sresult["status"] == DB_NO_RECORD)
	{
		$project_iresult = db_add_stock_project($project_name,$remarks,$added_by);
		
		if($project_iresult['status'] == SUCCESS)
		{
			$return["data"]   = $project_iresult['data'];
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "This Project already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Project list
INPUT 	: Project Filter Data
OUTPUT 	: Project List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_project_list($stock_project_search_data)
{
	$project_sresult = db_get_stock_project_list($stock_project_search_data);
	
	if($project_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $project_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No project added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Stock Project List
INPUT 	: Project ID, Stock Project Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_stock_project($project_id,$project_update_data)
{
	$stock_project_search_data = array("project_name"=>$project_update_data['project_name'],'project_name_check'=>'1','active'=>'1');
	$project_sresult = db_get_stock_project_list($stock_project_search_data);
	
	$allow_update = false;
	if($project_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($project_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($project_sresult['data'][0]['stock_project_id'] == $project_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$project_sresult = db_update_stock_project($project_id,$project_update_data);
		
		if($project_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Project Data Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Project Name Already Exists";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To delete Stock Project List
INPUT 	: Project ID, Stock Project Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_stock_project($project_id,$project_update_data)
{
	$project_dresult = db_update_stock_project($project_id,$project_update_data);
	
	if($project_dresult['status'] == SUCCESS)
	{
		$return["data"]   = "Project Successfully deleted";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}

	return $return;
}

/*
PURPOSE : To add Stock Tax Type Master
INPUT 	: Name, Value, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_stock_tax_type_master($name,$value,$jurisdiction,$remarks,$added_by)
{
	$stock_tax_type_master_search_data = array("name"=>$name,"name_check"=>'1',"active"=>'1');
	$tax_type_master_sresult = db_get_stock_tax_type_master_list($stock_tax_type_master_search_data);
	
	if($tax_type_master_sresult["status"] == DB_NO_RECORD)
	{
		$tax_type_master_iresult = db_add_stock_tax_type_master($name,$value,$jurisdiction,$remarks,$added_by);
		
		if($tax_type_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Tax type master Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "This tax type master already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}


/*
PURPOSE : To get Tax Type Master list
INPUT 	: Tax Type Master ID, Active
OUTPUT 	: Tax Type Master List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_stock_tax_type_master_list($stock_tax_type_master_search_data)
{
	$tax_type_master_sresult = db_get_stock_tax_type_master_list($stock_tax_type_master_search_data);
	
	if($tax_type_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $tax_type_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No tax type added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Stock Tax Type Master List
INPUT 	: Tax Type Master ID, Tax Type Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_stock_tax_type_master($tax_type_master_id,$tax_type_master_update_data)
{
	$stock_tax_type_master_search_data = array("name"=>$tax_type_master_update_data['name'],"name_check"=>'1',"active"=>'1');
	$tax_type_master_sresult = db_get_stock_tax_type_master_list($stock_tax_type_master_search_data);
	
	$allow_update = false;
	if($tax_type_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($tax_type_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($tax_type_master_sresult['data'][0]['stock_tax_type_master_id'] == $tax_type_master_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$tax_type_master_sresult = db_update_stock_tax_type_master($tax_type_master_id,$tax_type_master_update_data);
		
		if($tax_type_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Tax Type Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "This Tax Type Already Exists";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To Delete Stock Tax Type Master List
INPUT 	: Tax Type Master ID,Stock Project Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_stock_tax_type_master($tax_type_master_id,$tax_type_master_update_data)
{
	$tax_type_master_sresult = db_update_stock_tax_type_master($tax_type_master_id,$tax_type_master_update_data);
	
	if($tax_type_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "tax type master Successfully added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}

		
	return $return;
}

/*
PURPOSE : To add Stock Location Master
INPUT 	: Location code, Location Name, Address, Remarks,Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_stock_location_master($location_code,$location_name,$address,$remarks,$added_by)
{
	$stock_location_master_search_data = array("location_code"=>$location_code);
	$location_master_sresult = db_get_stock_location_master_list($stock_location_master_search_data);
	
	if($location_master_sresult["status"] == DB_NO_RECORD)
	{
		$location_master_iresult = db_add_stock_location_master($location_code,$location_name,$address,$remarks,$added_by);
		
		if($location_master_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Location Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "This location code already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Location list
INPUT 	: Stock Location Master,Active
OUTPUT 	: Location List or Error Details, success or failure message
BY 		: Lakshmi
*/
function i_get_stock_location_master_list($stock_location_master_search_data)
{
	$location_master_sresult = db_get_stock_location_master_list($stock_location_master_search_data);
	
	if($location_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $location_master_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No location added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Location
INPUT 	: Location ID, Location Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_location_master($location_id,$location_master_update_data)
{
	$stock_location_master_search_data = array("location_code"=>$location_master_update_data['location_code']);
	$location_master_sresult = db_get_stock_location_master_list($stock_location_master_search_data);
	
	$allow_update = false;
	if($location_master_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($location_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($location_master_sresult['data'][0]['stock_location_id'] == $location_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$location_master_sresult = db_update_location_master($location_id,$location_master_update_data);
		
		if($location_master_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Location Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "This Location already exists";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To delete Location
INPUT 	: Location ID, Location Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_delete_location_master($location_id,$location_master_update_data)
{
	$location_master_sresult = db_update_location_master($location_id,$location_master_update_data);
	
	if($location_master_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "location master Successfully added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	return $return;
}

/*
PURPOSE : To add Rate Card
INPUT 	: Materail Id,Start Date,End Date, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_rate_card($material_id,$start_date,$end_date,$uom,$remarks,$added_by)
{
		$rate_iresult = db_add_rate_card($material_id,$start_date,$end_date,$uom,$remarks,$added_by);
		
		if($rate_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Rate Card Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
		
	return $return;
}

/*
PURPOSE : To get Rate Card list
INPUT 	: Rate Card Array
OUTPUT 	: Rate Card List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_rate_card_list($rate_search_data)
{
	$rate_sresult = db_get_rate_card($rate_search_data);
	
	if($rate_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $rate_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Rate added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update To update Rate Card Master
INPUT 	: Rate Card ID, Rate Card Master Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_stock_rate_card($rate_card_id,$rate_card_update_data)

{
		$rate_card_sresult = db_update_stock_rate_card($rate_card_id,$rate_card_update_data);
		
		if($rate_card_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "rate card Successfully added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
		
	return $return;
}

/*
PURPOSE : To add Company 
INPUT 	: Company Name ,Contact Person, Address, TIN No, Remarks Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_add_company_master($company_name,$contact_person,$address,$tin_no,$remarks,$added_by)
{
	$stock_company_master_search_data = array("company_name"=>$company_name,"tin_no"=>$tin_no,"active"=>'1');
	$company_sresult = db_get_stock_company_master_list($stock_company_master_search_data);
	
	if($company_sresult["status"] == DB_NO_RECORD)
	{
		$company_iresult = db_add_stock_company_master($company_name,$contact_person,$address,$tin_no,$remarks,$added_by);
		
		if($company_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Company Master Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "This Company already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get Company Master list
INPUT 	: Company Search Data Array
OUTPUT 	: Company List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_company_list($stock_company_master_search_data)
{
	$company_sresult = db_get_stock_company_master_list($stock_company_master_search_data);
	
	if($company_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $company_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Company Master added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Company Master List
INPUT 	: Company ID, Company Update Array
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D
*/
function i_update_company_master($company_id,$company_update_data)
{
	$stock_company_master_search_data = array("company_name"=>$company_update_data['company_name'],"tin_no"=>$company_update_data['tin_no'],"active"=>'1');
	$company_sresult = db_get_stock_company_master_list($stock_company_master_search_data);
	
	$allow_update = false;
	if($company_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($company_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($company_sresult['data'][0]['stock_storage_id'] == $company_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$company_sresult = db_update_stock_company_master($company_id,$company_update_data);
		
		if($company_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Company Data Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Company Name Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To delete Company Master List
INPUT 	: Company ID, Company Update Array
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi
*/
function i_delete_company($company_id,$company_update_data)
{
	$company_update_data = array('active'=>'0');
	$company_sresult = db_update_stock_company_master($company_id,$company_update_data);
	
	if($company_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Company Successfully Deleted";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}/*PURPOSE : To add new stock Machine MasterINPUT 	: Name, Number, Use, Machine Type, Remarks, Added ByOUTPUT 	: Message, success or failure messageBY 		: Lakshmi*/function i_add_stock_machine_master($name,$machine_no,$vendor,$type,$remarks,$added_by){	$stock_machine_master_search_data = array("name"=>$name,"machine_name_check"=>'1',"number"=>$machine_no,"vendor"=>$vendor,"active"=>'1');	$stock_machine_master_sresult = db_get_stock_machine_master($stock_machine_master_search_data);	if($stock_machine_master_sresult["status"] == DB_NO_RECORD)	{		$stock_machine_master_iresult = db_add_stock_machine_master($name,$machine_no,$vendor,$type,$remarks,$added_by);			if($stock_machine_master_iresult['status'] == SUCCESS)		{			$return["data"]   = "stock Machine Master Successfully Added";			$return["status"] = SUCCESS;			}		else		{			$return["data"]   = "Internal Error. Please try again later";			$return["status"] = FAILURE;		}	}	else	{		$return["data"]   = "This Machine Already Exists";		$return["status"] = FAILURE;	}					return $return;}	/*PURPOSE : To get stock Machine Master listINPUT 	: machine ID, Name, Number, Use, Machine Type, Active, Added By, Start Date(for added on), End Date(for added on)OUTPUT 	: stock Machine Master List or Error Details, success or failure messageBY 		: Lakshmi*/function i_get_stock_machine_master($stock_machine_master_search_data){	$stock_machine_master_sresult = db_get_stock_machine_master($stock_machine_master_search_data);		if($stock_machine_master_sresult['status'] == DB_RECORD_ALREADY_EXISTS)    {		$return["status"] = SUCCESS;        $return["data"]   = $stock_machine_master_sresult["data"];     }    else    {	    $return["status"] = FAILURE;        $return["data"]   = "No Machine Master Added. Please contact the system admin";     }		return $return;}/*PURPOSE : To update stock Machine Master INPUT 	: Machine ID, stock Machine Master Update ArrayOUTPUT 	: Message, success or failure messageBY 		: Lakshmi*/function  i_update_stock_machine_master($machine_id,$vendor,$stock_machine_master_update_data){        $stock_machine_master_search_data = array("number_check"=>$stock_machine_master_update_data['number'],"machine_name_check"=>'1',"vendor"=>$vendor,"active"=>'1');	$stock_machine_master_sresult = db_get_stock_machine_master($stock_machine_master_search_data);		$allow_update = false;	if($stock_machine_master_sresult["status"] == DB_NO_RECORD)	{		$allow_update = true;	}	else if($stock_machine_master_sresult["status"] == DB_RECORD_ALREADY_EXISTS)	{		if($stock_machine_master_sresult['data'][0]['stock_machine_master_id'] == $machine_id)		{			$allow_update = true;		}	}		if($allow_update == true)	{		$stock_machine_master_sresult = db_update_stock_machine_master($machine_id,$stock_machine_master_update_data);				if($stock_machine_master_sresult['status'] == SUCCESS)		{			$return["data"]   = "Machine Master Successfully Updated";			$return["status"] = SUCCESS;									}		else		{			$return["data"]   = "Internal Error. Please try again later";			$return["status"] = FAILURE;		}	}	else	{		$return["data"]   = "Machine Name Already Exists";		$return["status"] = FAILURE;	}		return $return;}/*PURPOSE : To delete stock Machine Master INPUT 	: Machine ID, stock Machine Master Update ArrayOUTPUT 	: Message, success or failure messageBY 		: Lakshmi*/function  i_delete_stock_machine_master($machine_id,$stock_machine_master_update_data){	$stock_machine_master_sresult = db_update_stock_machine_master($machine_id,$stock_machine_master_update_data);		if($stock_machine_master_sresult['status'] == SUCCESS)	{		$return["data"]   = "Machine Master Successfully Added";		$return["status"] = SUCCESS;								}	else	{		$return["data"]   = "Internal Error. Please try again later";		$return["status"] = FAILURE;	}			return $return;}
?>