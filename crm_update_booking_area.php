<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 16th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["booking"]))
	{
		$booking_id = $_GET["booking"];
	}
	else
	{
		$booking_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["update_booking_area_submit"]))
	{
		$booking_id         = $_POST["hd_booking_id"];
		$consideration_area = $_POST["stxt_area"];
		
		// Check for mandatory fields
		if(($booking_id !="") && ($consideration_area !=""))
		{
			$booking_area_usresult = i_update_booking($booking_id,'','','','',$consideration_area);
			
			if($booking_area_usresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert = "Booking Area updated successfully.";
			}
			else
			{
				$alert_type = 0;
				$alert      = $booking_area_usresult["data"];
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get the customer details as captured already
	$cust_details = i_get_cust_profile_list('',$booking_id,'','','','','','','','');
	if($cust_details["status"] == SUCCESS)
	{
		$cust_details_list = $cust_details["data"];
	}
	else
	{
		$alert = $cust_details["data"];
		$alert_type = 0;
	}
	
	// Get booking details
	$booking_list = i_get_site_booking($booking_id,'','','','','','','','','','','');
	if($booking_list["status"] == SUCCESS)
	{
		$booking_list_data = $booking_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$booking_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Update Booking Area</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
						<?php 
						if($cust_details["status"] == SUCCESS)
						{
						?>
						<h3>Project: <?php echo $cust_details_list[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;Site No: <?php echo $cust_details_list[0]["crm_site_no"]; ?>&nbsp;&nbsp;&nbsp;Customer: <?php echo $cust_details_list[0]["crm_customer_name_one"]." (".$cust_details_list[0]["crm_customer_contact_no_one"].")"; ?>&nbsp;&nbsp;&nbsp;</h3>
						<?php
						}
						else
						{
							$alert_type = 0;
							$alert      = "Customer Profile not added!";?>
	      				<h3>Project: <?php echo $booking_list_data[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;Site No: <?php echo $booking_list_data[0]["crm_site_no"]; ?>&nbsp;&nbsp;&nbsp;Customer: <?php echo $booking_list_data[0]["name"]." (".$booking_list_data[0]["cell"].")"; ?>&nbsp;&nbsp;&nbsp;</h3>
						<?php
						}?>
						<span style="float:right; padding-right:15px;"><a href="crm_customer_transactions.php?booking=<?php echo $booking_list_data[0]["crm_booking_id"]; ?>">Back to customer profile</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Update Booking</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?>
										</strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?>&nbsp;&nbsp;&nbsp;
										  <?php
										  if($cust_details["status"] == SUCCESS)
										  {
										  ?>
										  <a href="crm_view_customer_profile.php?profile=<?php echo $cust_details_list[0]["crm_customer_details_id"]; ?>">View Customer Profile</a>&nbsp;&nbsp;&nbsp;
										  <a href="crm_edit_customer_profile.php?profile=<?php echo $cust_details_list[0]["crm_customer_details_id"]; ?>">Edit Customer Profile</a>
										  <?php
										  }
										  else
										  {
										  ?>
										  <a href="crm_add_customer_profile.php?booking=<?php echo $booking_list_data[0]["crm_booking_id"]; ?>">Add Customer Profile</a>
										  <?php
										  }
										  ?></strong>
                                    </div>
								<?php
								}
								?>								
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="update_booking_area_form" class="form-horizontal" method="post" action="crm_update_booking_area.php">
								<input type="hidden" name="hd_booking_id" value="<?php echo $booking_id; ?>" />								
									<fieldset>										
												
										<div class="control-group">											
											<label class="control-label" for="stxt_area">Consideration Area (in sq. ft)*</label>
											<div class="controls">
												<input type="number" name="stxt_area" min="1" step="0.01" class="span6" required value="" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="update_booking_area_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
