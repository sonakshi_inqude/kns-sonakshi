<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_survey_project.php');

/*
PURPOSE : To add new Survey Process
INPUT 	: Survey ID, Process ID, Start Date, End Date, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_survey_process($survey_id,$process_id,$process_start_date,$process_end_date,$remarks,$added_by)
{   
	$survey_process_iresult =  db_add_survey_process($survey_id,$process_id,$process_start_date,$process_end_date,$remarks,$added_by);
	
	if($survey_process_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Survey Process Successfully added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get Survey Process List
INPUT 	: Survey Process ID, Survey ID, Process ID, Start Date, End Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Process, success or failure message
BY 		: Lakshmi
*/
function i_get_survey_process($survey_process_search_data)
{
	$survey_process_sresult = db_get_survey_process($survey_process_search_data);
	
	if($survey_process_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$survey_process_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Survey Process Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Survey Process
INPUT 	: Survey Process ID, Survey Process Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_survey_process($survey_process_id,$survey_process_update_data)
{       
		$survey_process_sresult = db_update_survey_process($survey_process_id,$survey_process_update_data);
		
		if($survey_process_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Process Successfully Added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}


/*
PURPOSE : To add new Survey Single Process
INPUT 	: Survey ID, Process ID, Start Date, End Date, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_survey_single_process($survey_id,$process_id,$process_start_date,$process_end_date,$remarks,$added_by)
{   
	$survey_process_iresult =  db_add_survey_single_process($survey_id,$process_id,$process_start_date,$process_end_date,$remarks,$added_by);
	
	if($survey_process_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Survey Single Process Successfully added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get Survey Single Process List
INPUT 	: Survey Process ID, Survey ID, Process ID, Start Date, End Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Process, success or failure message
BY 		: Lakshmi
*/
function i_get_survey_single_process($survey_single_process_search_data)
{
	$survey_process_sresult = db_get_survey_single_process($survey_single_process_search_data);
	
	if($survey_process_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$survey_process_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Survey Single Process Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Survey Single Process
INPUT 	: Survey Process ID, Survey Process Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_survey_single_process($survey_process_id,$survey_single_process_update_data)
{       
		$survey_process_sresult = db_update_survey_single_process($survey_process_id,$survey_single_process_update_data);
		
		if($survey_process_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Single Process Successfully Added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To add new Survey Process Delay
INPUT 	: Reason ID, Process ID, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_survey_process_delay($reason_id,$process_id,$remarks,$added_by)
{   
	$survey_process_delay_iresult =  db_add_survey_process_delay($reason_id,$process_id,$remarks,$added_by);
	
	if($survey_process_delay_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Survey Process Delay Successfully added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get Survey Process Delay List
INPUT 	: Delay ID, Reason ID, Process ID, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Process, success or failure message
BY 		: Lakshmi
*/
function i_get_survey_process_delay($survey_process_delay_search_data)
{
	$survey_process_delay_sresult = db_get_survey_process_delay($survey_process_delay_search_data);
	
	if($survey_process_delay_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$survey_process_delay_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Survey Process Delay Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Survey Process Delay
INPUT 	: Delay ID, Survey Process Delay Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_survey_process_delay($delay_id,$survey_process_delay_update_data)
{       
		$survey_process_delay_sresult = db_update_survey_process_delay($delay_id,$survey_process_delay_update_data);
		
		if($survey_process_delay_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Process delay Successfully Added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To add new Survey Details
INPUT 	: Project, Village, Planned Start Date, Planned End Date, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_survey_details($project,$village,$planned_start_date,$planned_end_date,$remarks,$added_by)
{   
	$survey_details_search_data = array("project"=>$project,"village"=>$village,"active"=>'1');
	$survey_details_sresult = db_get_survey_details($survey_details_search_data);
	
	if($survey_details_sresult["status"] == DB_NO_RECORD)
	{
		$survey_details_iresult = db_add_survey_details($project,$village,$planned_start_date,$planned_end_date,$remarks,$added_by);
		
		if($survey_details_iresult['status'] == SUCCESS)
		{
			$return["data"]   = $survey_details_iresult["data"];
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Details already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}



/*
PURPOSE : To get Survey Details List
INPUT 	: Survey ID, Project, Village, Planned Start Date, Planned End Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Details, success or failure message
BY 		: Lakshmi
*/
function i_get_survey_details($survey_details_search_data)
{
	$survey_details_sresult = db_get_survey_details($survey_details_search_data);
	
	if($survey_details_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$survey_details_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Survey Details Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Survey Details
INPUT 	: Survey ID, Survey Details Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_survey_details($survey_id,$survey_details_update_data)
{       
	$survey_details_search_data = array("project"=>$survey_details_update_data['project'],"village"=>$survey_details_update_data['village'],"active"=>'1');
	$survey_details_sresult = db_get_survey_details($survey_details_search_data);
	
	$allow_update = false;
	if($survey_details_sresult["status"] == DB_NO_RECORD)
	{
		$allow_update = true;
	}
	else if($survey_details_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($survey_details_sresult['data'][0]['survey_details_id'] == $survey_id)
		{
			$allow_update = true;
		}
	}
	
	if($allow_update == true)
	{
		$survey_details_sresult = db_update_survey_details($survey_id,$survey_details_update_data);
		
		if($survey_details_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Details Successfully Updated";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Survey Details Already Exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To Delete Survey Details
INPUT 	: Survey ID, Survey File Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_delete_survey_file($survey_id,$survey_details_update_data)
{       
		$survey_delete_sresult = db_update_survey_details($survey_id,$survey_details_update_data);
		
		if($survey_delete_sresult['status'] == SUCCESS)
		{
			
		$survey_process_update_data = array("survey_id"=>$survey_id);
		$survey_delete_iresult = i_delete_all_file_process($survey_process_update_data);
		
		if($survey_delete_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey All Details successfully added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}	
	}
	else
	{
		$return["data"]   = "The Survey Details already exists";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new Survey File
INPUT 	: Survey ID, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_survey_file($bd_file_id,$survey_id,$remarks,$added_by)
{   
	$survey_file_iresult =  db_add_survey_file($bd_file_id,$survey_id,$remarks,$added_by);
	
	if($survey_file_iresult['status'] == SUCCESS)
	{
			$return["data"]   = $survey_file_iresult['data'] ;
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get Survey File List
INPUT 	: BD file ID, file ID, Survey ID, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey File, success or failure message
BY 		: Lakshmi
*/
function i_get_survey_file($survey_file_search_data)
{
	$survey_file_sresult = db_get_survey_file($survey_file_search_data);
	
	if($survey_file_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$survey_file_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Survey File Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Survey File
INPUT 	: Survey ID, Survey File Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_survey_file($file_id,$survey_file_update_data)
{       
		$survey_file_sresult = db_update_survey_file($file_id,$survey_file_update_data);
		
		if($survey_file_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey File Successfully Added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To add new Survey File
INPUT 	: Survey ID, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_survey_document($process_id,$file_path_id,$document_type_id,$remarks,$added_by)
{   
	$survey_document_iresult =  db_add_survey_document($process_id,$file_path_id,$document_type_id,$remarks,$added_by);
	
	if($survey_document_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Survey Document Successfully added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get Survey Document List
INPUT 	: Document ID, Process ID, File Path ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Document, success or failure message
BY 		: Lakshmi
*/
function i_get_survey_document($survey_document_search_data)
{
	$survey_document_sresult = db_get_survey_document($survey_document_search_data);
	
	if($survey_document_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$survey_document_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Survey Document Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Survey Document
INPUT 	: Document ID, Survey Document Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_survey_document($document_id,$survey_document_update_data)
{       
	$survey_document_sresult = db_update_survey_document($document_id,$survey_document_update_data);
		
	if($survey_document_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Survey Document Successfully Added";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new Survey Process Task
INPUT 	: Process ID, Requested Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_survey_process_task($process_id,$request_remarks,$added_by)
{   
	$survey_process_task_iresult =  db_add_survey_process_task($process_id,$request_remarks,'','',$added_by);
	
	if($survey_process_task_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Survey Process Task Successfully added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To get Survey Process Task List
INPUT 	: Task ID, Process ID, Status, Active, Requested Remarks, Response Remarks, Updated By, Updated On, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Process Task, success or failure message
BY 		: Lakshmi
*/
function i_get_survey_process_task($survey_process_task_search_data)
{
	$survey_process_task_sresult = db_get_survey_process_task($survey_process_task_search_data);
	
	if($survey_process_task_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $survey_process_task_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Survey Process Task Added"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Survey Process Task
INPUT 	: Task ID, urvey Process Task Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_update_survey_process_task($task_id,$survey_process_task_update_data)
{       
	$survey_process_task_sresult = db_update_survey_process_task($task_id,$survey_process_task_update_data);
		
	if($survey_process_task_sresult['status'] == SUCCESS)
	{
		$return["data"]   = "Survey Process Task Successfully Updated";
		$return["status"] = SUCCESS;							
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new Survey File Process
INPUT 	: File ID, Process ID, File Start Date, File End Date, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_survey_file_process($file_id,$process_id,$file_start_date,$file_end_date,$remarks,$added_by)
{   
	$survey_file_process_iresult =  db_add_survey_file_process($file_id,$process_id,$file_start_date,$file_end_date,$remarks,$added_by);
	
	if($survey_file_process_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Survey File Process Successfully added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get Survey File Process List
INPUT 	: File Process ID, File ID, Process ID, File Start Date,  File End Date, Active, Updated By, Updated On, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey File Process, success or failure message
BY 		: Lakshmi
*/
function i_get_survey_file_process($survey_file_process_search_data)
{
	$survey_file_process_sresult = db_get_survey_file_process($survey_file_process_search_data);
	
	if($survey_file_process_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$survey_file_process_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Survey File Process Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Survey File Process
INPUT 	: File Process ID, Survey File Process Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_survey_file_process($file_process_id,$survey_file_process_update_data)
{       
		$survey_file_process_sresult = db_update_survey_file_process($file_process_id,$survey_file_process_update_data);
		
		if($survey_file_process_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey File Process Successfully Added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To add new Survey File Process User
INPUT 	: File Process ID, User ID, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_survey_file_process_user($file_process_id,$user_id,$remarks,$added_by)
{   
	$survey_file_process_user_iresult =  db_add_survey_file_process_user($file_process_id,$user_id,$remarks,$added_by);
	
	if($survey_file_process_user_iresult['status'] == SUCCESS)
	{
		$return["data"]   = "Survey Process User Successfully added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get Survey File Process User List
INPUT 	: File User ID, File Process ID, User ID, Active, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey File Process User, success or failure message
BY 		: Lakshmi
*/
function i_get_survey_file_process_user($survey_file_process_user_search_data)
{
	$survey_file_process_user_sresult = db_get_survey_file_process_user($survey_file_process_user_search_data);
	
	if($survey_file_process_user_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$survey_file_process_user_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Survey Process User Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Survey File Process User
INPUT 	: File User ID, Survey File Process User Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_survey_file_process_user($file_user_id,$survey_file_process_user_update_data)
{       
		$survey_file_process_user_sresult = db_update_survey_file_process_user($file_user_id,$survey_file_process_user_update_data);
		
		if($survey_file_process_user_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Process User Successfully Added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To add new Survey Query
INPUT 	: Process ID, Query, Query By, Remarks, Added By
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/
function i_add_survey_query($process_id,$query,$query_by,$remarks,$added_by)
{   
	$survey_query_iresult =  db_add_survey_query($process_id,$query,$query_by,$remarks,$added_by);
	
	if($survey_query_iresult['status'] == SUCCESS)
	{
			$return["data"]   = $survey_query_iresult['data'] ;
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get Survey Query list
INPUT 	: Query ID, Process ID, Query, Query By, Query Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey Query
BY 		: Lakshmi
*/
function i_get_survey_query($survey_query_search_data)
{
	$survey_query_sresult = db_get_survey_query($survey_query_search_data);
	
	if($survey_query_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$survey_query_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Survey Query Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To update Survey Query 
INPUT 	: Query ID, Survey Query Update Array
OUTPUT 	: Message, success or failure message
BY 		: Lakshmi
*/

function i_update_survey_query($query_id,$survey_query_update_data)
{       
		$survey_query_response_sresult = db_update_survey_query($query_id,$survey_query_update_data);
		
		if($survey_query_response_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Query Successfully Added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To add new Survey Response
INPUT 	: Query ID, response, Follow up Date, Status, Remarks, Added By
OUTPUT 	: Response ID, success or failure message
BY 		: Lakshmi
*/
function i_add_survey_response($query_id,$response,$follow_up_date,$status,$remarks,$added_by)
{   
	$survey_response_iresult =  db_add_survey_response($query_id,$response,$follow_up_date,$status,$remarks,$added_by);
	
	if($survey_response_iresult['status'] == SUCCESS)
	{
			$return["data"]   = $survey_response_iresult['data'] ;
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To get Survey Response list
INPUT 	: Response ID, Query ID, response, Follow Up Date, Remarks, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Survey response
BY 		: Lakshmi
*/
function i_get_survey_response($survey_response_search_data)
{
	$survey_response_sresult = db_get_survey_response($survey_response_search_data);
	
	if($survey_response_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   =$survey_response_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No  Survey Response Added. Please Contact The System Admin"; 
    }
	
	return $return;
}

  /*
PURPOSE : To update Survey Response 
INPUT 	: Response ID, Survey Response Update Array
OUTPUT 	: Response ID; Message of success or failure
BY 		: Lakshmi
*/

function i_update_survey_response($response_id,$survey_response_update_data)
{       
		$survey_response_response_sresult = db_update_survey_response($response_id,$survey_response_update_data);
		
		if($survey_response_response_sresult['status'] == SUCCESS)
		{
			$return["data"]   = "Survey Response Successfully Added";
			$return["status"] = SUCCESS;							
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	
	return $return;
}

/*
PURPOSE : To Delete All File Process 
INPUT 	: Survey ID, Survey File Process Update Array
OUTPUT 	: Survey ID; Message of success or failure
BY 		: Lakshmi
*/
function i_delete_all_file_process($survey_process_update_data)
{
	$survey_process_update_data = array("survey_id"=>$survey_process_update_data['survey_id']);
	$survey_process_delete_uresult = db_delete_all_process($survey_process_update_data);
	
	$survey_file_update_data = array("survey_id"=>$survey_process_update_data['survey_id']);
	$survey_file_delete_iresult = db_delete_all_survey_file($survey_file_update_data);
	
	if(($survey_file_delete_iresult['status'] == SUCCESS) && ($survey_process_delete_uresult['status'] == SUCCESS))
	{
		$return["data"]   = "Survey All Details successfully added";
		$return["status"] = SUCCESS;	
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}
?>