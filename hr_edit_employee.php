<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 31st March 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_employee_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert = "";
	$alert_type = -1;
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["employee"]))
	{
		$employee_id = $_GET["employee"];
	}
	else
	{
		$employee_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["edit_employee_submit"]))
	{
		$employee_user              = $_POST["ddl_user"];
		$employee_name              = $_POST["stxt_name"];
		$employee_postal_address    = $_POST["txt_postal_address"];
		$employee_permanent_address = $_POST["txt_permanent_address"];
		$employee_father_name       = $_POST["stxt_father_name"];
		$employee_dob               = $_POST["dt_dob"];
		$employee_employment_date   = $_POST["dt_employment_date"];
		$employee_code              = $_POST["stxt_employee_code"];
		$employee_week_off			= $_POST["rd_week_off"];
		$employee_id                = $_POST["hd_employee_id"];
		
		// Check for mandatory fields
		if(($employee_user !="") && ($employee_name !="") && ($employee_code !=""))
		{
			$employee_details = array("user"=>$employee_user,"name"=>$employee_name,"postal_address"=>$employee_postal_address,"perm_address"=>$employee_permanent_address,"father"=>$employee_father_name,"dob"=>$employee_dob,"emp_date"=>$employee_employment_date,"emp_code"=>$employee_code,"emp_week_off"=>$employee_week_off);
			$employee_uresult = i_edit_employee_details($employee_id,$employee_details);
			
			if($employee_uresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert      = "Employee details updated successfully";
				
				header("location:hr_employee_list.php");
			}
			else
			{
				$alert_type = 0;
				$alert      = "There was an internal error. Please try again later!";
			}
		}
		else
		{
			$alert      = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get list of users
	$user_list = i_get_user_list('','','','');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
	}
	
	// Get list of employees
	$employee_filter_data = array("employee_id"=>$employee_id,"user_status"=>'1');
	$employee_list = i_get_employee_list($employee_filter_data);
	if($employee_list["status"] == SUCCESS)
	{
		$employee_list_data = $employee_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$employee_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Edit Employee</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>HR</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Edit Employee</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="edit_emp_form" class="form-horizontal" method="post" action="hr_edit_employee.php">
								<input type="hidden" name="hd_employee_id" value="<?php echo $employee_id; ?>" />
									<fieldset>										
															
										<div class="control-group">											
											<label class="control-label" for="ddl_user">User*</label>
											<div class="controls">
												<select name="ddl_user" required>
												<option value="">- - Select User - -</option>
												<?php
												for($count = 0; $count < count($user_list_data); $count++)
												{
												?>
												<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($user_list_data[$count]["user_id"] == $employee_list_data[0]["hr_employee_user"]){ ?> selected <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>
												<?php
												}
												?>
												</select>
												<p class="help-block">The application user to which this employee needs to be mapped</p>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_name">Name*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_name" value="<?php echo $employee_list_data[0]["hr_employee_name"]; ?>" placeholder="Employee Full name as in records" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="txt_postal_address">Postal Address</label>
											<div class="controls">
												<textarea class="span6" name="txt_postal_address"><?php echo $employee_list_data[0]["hr_employee_postal_address"]; ?></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_permanent_address">Permanent Address</label>
											<div class="controls">
												<textarea class="span6" name="txt_permanent_address"><?php echo $employee_list_data[0]["hr_employee_permanent_address"]; ?></textarea>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="stxt_father_name">Father's Name</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_father_name" value="<?php echo $employee_list_data[0]["hr_employee_father_name"]; ?>">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="dt_dob">Date Of Birth</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_dob" value="<?php echo $employee_list_data[0]["hr_employee_dob"]; ?>">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="dt_employment_date">Employment Date</label>
											<div class="controls">
												<input type="date" class="span6" name="dt_employment_date" value="<?php echo $employee_list_data[0]["hr_employee_employment_date"]; ?>">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->																			
										
										<div class="control-group">											
											<label class="control-label" for="stxt_employee_code">Employee Code*</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_employee_code" required="required" value="<?php echo $employee_list_data[0]["hr_employee_code"]; ?>">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="rd_week_off">Employee Week Off</label>
											<div class="controls">
											<input type="radio" name="rd_week_off" value="0" <?php echo ($employee_list_data[0]["hr_employee_week_off"] == 0 ? 'checked' : '');?> > Sunday<br>
											<input type="radio" name="rd_week_off" value="1" <?php echo ($employee_list_data[0]["hr_employee_week_off"] == 1 ? 'checked' : '');?> > Monday<br>
											<input type="radio" name="rd_week_off" value="2" <?php echo ($employee_list_data[0]["hr_employee_week_off"] == 2 ? 'checked' : '');?> > Tuesday<br>
											<input type="radio" name="rd_week_off" value="3" <?php echo ($employee_list_data[0]["hr_employee_week_off"] == 3 ? 'checked' : '');?> > Wednesday<br>
											<input type="radio" name="rd_week_off" value="4" <?php echo ($employee_list_data[0]["hr_employee_week_off"] == 4 ? 'checked' : '');?> > Thursday<br>
											<input type="radio" name="rd_week_off" value="5" <?php echo ($employee_list_data[0]["hr_employee_week_off"] == 5 ? 'checked' : '');?> >   Friday<br>
											<input type="radio" name="rd_week_off" value="6" <?php echo ($employee_list_data[0]["hr_employee_week_off"] == 6 ? 'checked' : '');?> > Saturday<br>
											
											</div> <!-- /controls -->												
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="edit_employee_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
