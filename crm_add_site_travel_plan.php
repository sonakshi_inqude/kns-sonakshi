<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 7th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

define('CRM_ADD_SITE_TRAVEL_PLAN_FUNC_ID','108');

/* TBD - START */
/* TBD - END */
$_SESSION['module'] = 'CRM Transactions';

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$add_perms_list     = i_get_user_perms($user,'',CRM_ADD_SITE_TRAVEL_PLAN_FUNC_ID,'1','1');
	$view_perms_list    = i_get_user_perms($user,'',CRM_ADD_SITE_TRAVEL_PLAN_FUNC_ID,'2','1');
	$edit_perms_list    = i_get_user_perms($user,'',CRM_ADD_SITE_TRAVEL_PLAN_FUNC_ID,'3','1');
	$delete_perms_list  = i_get_user_perms($user,'',CRM_ADD_SITE_TRAVEL_PLAN_FUNC_ID,'4','1');

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query string data
	// Nothing here
	
	// Initialization
	$project = "";
	$date    = "";
	
	// Capture the form data
	if((!(isset($_POST["add_stp_submit"]))) && (isset($_POST["ddl_project"])))
	{
		$project = $_POST["ddl_project"];
	}
	if(isset($_POST["add_stp_submit"]))
	{
		$date    = $_POST["dt_site_visit"];
		$project = $_POST["ddl_project"];
		$sv_plan = $_POST["cb_sv_plans"];
		$cab     = $_POST["ddl_cab"];		
		
		// Check for mandatory fields
		if(($date !="") && ($cab !=""))
		{
			// Process all the checked site visits
			for($count = 0; $count < count($sv_plan); $count++)
			{
				$stp_result = i_add_site_travel_plan($sv_plan[$count],$cab,$project,$date,$user);
			}
			
			$alert = "Cab Plan entered successfully";
			$alert_type = 1;
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get project list
	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"active"=>'1',"project_active"=>'1');
	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Cab List
	$cab_list = i_get_cab_list('','');
	if($cab_list["status"] == SUCCESS)
	{
		$cab_list_data = $cab_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$cab_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Site Visit Plan List
	$sv_plan_list = i_get_site_visit_plan_list('','',$project,'1','','','','asc','',$user);	
	if($sv_plan_list["status"] == SUCCESS)
	{
		$sv_plan_list_data = $sv_plan_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$sv_plan_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Site Travel Plan</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Site Travel Plan</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_stp" class="form-horizontal" method="post" action="crm_add_site_travel_plan.php">
								<input type="hidden" name="hd_enquiry" value="<?php echo $enquiry_id; ?>" />
									<fieldset>																											
																					
										<div class="control-group">											
											<label class="control-label" for="ddl_project">Project*</label>
											<div class="controls">
												<select name="ddl_project" required onchange="this.form.submit();">
												<option value="">- - Select Project - -</option>
												<?php
												for($count = 0; $count < count($project_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php if($project == $project_list_data[$count]["project_id"])
												{
												?>
												
												selected="selected"
												
												<?php
												}?>><?php echo $project_list_data[$count]["project_name"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->

										<div class="control-group">											
											<label class="control-label" for="dt_site_visit">Site Visit Date*</label>
											<div class="controls">
												<input type="datetime-local" class="span6" name="dt_site_visit" required="required" value="<?php echo $date; ?>">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="dt_site_visit">Site Visit Plans*</label>
											<div class="controls">
											<?php 
											if($sv_plan_list["status"] == SUCCESS)
											{												
											?>
											<table class="table table-bordered" style="table-layout: fixed;">
											<tr>
														<td style="word-wrap:break-word;"><strong>SL No.</strong></td>
														<td style="word-wrap:break-word;"><strong>Select Visit Plan</strong></td>
														<td style="word-wrap:break-word;"><strong>Site Visit Plan Date</strong></td>
														<td style="word-wrap:break-word;"><strong>Pickup Time</strong></td>
														<td style="word-wrap:break-word;"><strong>Enquiry Number</strong></td>
														<td style="word-wrap:break-word;"><strong>Name</strong></td>
														<td style="word-wrap:break-word;"><strong>Project</strong></td>
														<td style="word-wrap:break-word;"><strong>Mobile</strong></td>
														<td style="word-wrap:break-word;"><strong>STM</strong></td>
														<td style="word-wrap:break-word;"><strong>Site Visit Planned By</strong></td>
														<td style="word-wrap:break-word;"><strong>Pickup Location</strong></td>
														<td style="word-wrap:break-word;"><strong>Confirmation Status</strong></td>
														<td style="word-wrap:break-word;"><strong>Drive Type</strong></td>
														<tr>
											<?php
												$sl_no = 0;
												for($count = 0; $count < count($sv_plan_list_data); $count++)
												{
													if(($sv_plan_list_data[$count]["crm_site_visit_plan_status"] == "1") && ($sv_plan_list_data[$count]["crm_site_visit_plan_confirmation"] == "1"))
													{
														$travel_plan = i_get_site_travel_plan_list('',$sv_plan_list_data[$count]["crm_site_visit_plan_id"],'','','','','','','');
														
														if($travel_plan["status"] != SUCCESS )
														{
															$sl_no++;
													?>
														<tr>
														<td><?php echo $sl_no; ?></td>
														<td><input type="checkbox" name="cb_sv_plans[]" value="<?php echo $sv_plan_list_data[$count]["crm_site_visit_plan_id"]; ?>" /></td>
														<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($sv_plan_list_data[$count]["crm_site_visit_plan_date"])); ?></td>
														<td style="word-wrap:break-word;"><?php echo $sv_plan_list_data[$count]["crm_site_visit_plan_time"]; ?></td>
														<td style="word-wrap:break-word;"><?php echo $sv_plan_list_data[$count]["enquiry_number"]; ?></td>
														<td style="word-wrap:break-word;"><?php echo $sv_plan_list_data[$count]["name"]; ?></td>
														<td style="word-wrap:break-word;"><?php echo $sv_plan_list_data[$count]["project_name"]; ?></td>
														<td style="word-wrap:break-word;"><?php echo $sv_plan_list_data[$count]["cell"]; ?></td>
														<td style="word-wrap:break-word;"><?php echo $sv_plan_list_data[$count]["assignee"]; ?></td>
														<td style="word-wrap:break-word;"><?php echo $sv_plan_list_data[$count]["assigner"]; ?></td>
														<td style="word-wrap:break-word;"><?php echo $sv_plan_list_data[$count]["crm_site_visit_pickup_location"]; ?></td>
														<td style="word-wrap:break-word;"><?php if($sv_plan_list_data[$count]["crm_site_visit_plan_confirmation"] == "0")
														{
															echo "Tentative";
														}
														else if($sv_plan_list_data[$count]["crm_site_visit_plan_confirmation"] == "1")
														{
															echo "Confirmed";
														}?></td>
														<td style="word-wrap:break-word;"><?php if($sv_plan_list_data[$count]["crm_site_visit_plan_drive"] == "0")
														{
															echo "KNS";
														}
														else if($sv_plan_list_data[$count]["crm_site_visit_plan_drive"] == "1")
														{
															echo "Self-Drive";
														}?></td>
														<tr>
													<?php
														}
													}
												}
											?>
											</table>
											<?php											
											}
											else
											{
												echo "No site visit planned!";
											}?>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="ddl_cab">Cab*</label>
											<div class="controls">
												<select name="ddl_cab" required>
												<?php
												for($count = 0; $count < count($cab_list_data); $count++)
												{
												?>
												<option value="<?php echo $cab_list_data[$count]["crm_cab_id"]; ?>"><?php echo $cab_list_data[$count]["crm_cab_travels"]; ?></option>								
												<?php
												}
												?>														
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_stp_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

  </body>

</html>

