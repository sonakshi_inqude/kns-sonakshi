<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 24th Feb 2016
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["site"]))
	{
		$site_id = $_GET["site"];
	}
	else
	{
		$site_id = "";
	}
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["update_mortgage_details_submit"]))
	{
		$site_id       = $_POST["hd_site_id"];
		$mortgage_date = $_POST["dt_mortgage_date"];
		$release_date  = $_POST["dt_release_date"];
		
		// Check for mandatory fields
		if($site_id !="")
		{
			if((strtotime($mortgage_date) <= strtotime(date("Y-m-d"))) && (strtotime($release_date) <= strtotime(date("Y-m-d"))))
			{			
				if((strtotime($mortgage_date) <= strtotime($release_date)) || ($release_date == "0000-00-00") || ($release_date == ""))
				{
					$site_data_array = array("site_id"=>$site_id,"mortgage_date"=>$mortgage_date,"release_date"=>$release_date);
					$mortgage_data_usresult = i_update_site_details($site_data_array,$user);
					
					if($mortgage_data_usresult["status"] == SUCCESS)
					{
						$alert_type = 1;
						$alert      = $mortgage_data_usresult["data"];
						
						header("location:crm_site_status_report.php");
					}
					else
					{
						$alert_type = 0;
						$alert      = $mortgage_data_usresult["data"];
					}
				}
				else
				{
					$alert      = "Release date cannot be earlier than mortgage date";
					$alert_type = 0;
				}
			}
			else
			{
				$alert      = "Mortgage Date and Release Date cannot be later than today";
				$alert_type = 0;
			}
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get the customer details as captured already
	$site_details = i_get_site_list('','','','','','','',$site_id);
	if($site_details["status"] == SUCCESS)
	{
		$site_details_list = $site_details["data"];
	}
	else
	{
		$alert = $site_details["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Update Mortgage Details</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
						<?php 
						if($site_details["status"] == SUCCESS)
						{
						?>
						<h3>Project: <?php echo $site_details_list[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;Site No: <?php echo $site_details_list[0]["crm_site_no"]; ?>&nbsp;&nbsp;&nbsp;</h3>
						<?php
						}
						else
						{
							$alert_type = 0;
							$alert      = "Customer Profile not added!";?>
	      				<h3>Project: <?php echo "<i>Invalid Project</i>"; ?>&nbsp;&nbsp;&nbsp;Site No: <?php echo "<i>Invalid Site</i>"; ?>&nbsp;&nbsp;&nbsp;</h3>
						<?php
						}?>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Update Mortgage Details</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="update_mortgage_details_form" class="form-horizontal" method="post" action="crm_update_mortgage_details.php">
								<input type="hidden" name="hd_site_id" value="<?php echo $site_id; ?>" />								
									<fieldset>										
												
										<div class="control-group">											
											<label class="control-label" for="dt_mortgage_date">Mortgage Date*</label>
											<div class="controls">
												<input type="date" name="dt_mortgage_date" class="span6" required value="<?php echo $site_details_list[0]["crm_site_mortgaged_on"]; ?>" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="dt_release_date">Release Date</label>
											<div class="controls">
												<input type="date" name="dt_release_date" class="span6" value="<?php echo $site_details_list[0]["crm_site_released_on"]; ?>" />
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<br />
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="update_mortgage_details_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
