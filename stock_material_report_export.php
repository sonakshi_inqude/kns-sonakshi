<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: Attendance Report

CREATED ON	: 26-Mar-2016

CREATED BY	: Nitin Kashyap

PURPOSE     : Attendance Report

*/



/*

TBD: 

*/



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'PHPExcel-1.8'.DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_history_function.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');


if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];





	// Temp data

	$alert      = "";

	$alert_type = "";		
	
	$is_not_started = "";
	// Query String
	
	if(isset($_GET["project"]))

	{

		$project = $_GET["project"];

	}

	else

	{

		$project = "";

	}

	if(isset($_GET["start_date"]))

	{

		$start_date_filter = $_GET["start_date"];

	}

	else

	{

		$start_date_filter = "";

	}
	
	if(isset($_GET["material_id"]))

	{

		$material_id = $_GET["material_id"];

	}

	else

	{

		$material_id = "";

	}

	

	if(isset($_GET["end_date"]))

	{

		$end_date_filter = $_GET["end_date"];

	}

	else

	{

		$end_date_filter = "";

	}
	

	// Get list of files
	$stock_history_search_data = array('active'=>'1',"material_id"=>$material_id,"project"=>$project,"start_date"=>$start_date_filter,"end_date"=>$end_date_filter);

	$stock_material_history_list = i_get_stock_history($stock_history_search_data);

	

	/* Create excel sheet and write the column headers - START */

	// Instantiate a new PHPExcel object

	$objPHPExcel = new PHPExcel(); 

	// Set the active Excel worksheet to sheet 0

	$objPHPExcel->setActiveSheetIndex(0); 

	// Initialise the Excel row number

	$row_count = 1; 

	// Excel column identifier array

	$column_array = array('A','B','C','D','E','F','G','H','I','J');

	

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "DATE");

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, "TRANSACTION TYPE"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, "TRANSACTION DETAILS"); 
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, "PROJECT"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, "OPENING STOCK"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, "RECEIPTS"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, "ISSUES"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, "CLOSING STOCK"); 

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, "RATE"); 	

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, "VALUE"); 

	

	$style_array = array('font' => array('bold' => true));

	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[9].$row_count)->applyFromArray($style_array);

	$row_count++;

	if($stock_material_history_list["status"] == SUCCESS)

	{
		$stock_material_history_list_data = $stock_material_history_list["data"];
		$sl_no = 0;
		$total_opening_qty = 0;
		$total_issue_qty = 0;
		$total_purchase_qty = 0;
		$total_closing_stock = 0;
		$material_history = [];
		$start = 0;
		
		$end_date = date('Y-m-d',strtotime($start_date_filter.' -1 day'));
		$start_date_cl_st = $end_date.' 23:59:59';
		//Get closing Stock
		$total_closing_stock_value = 	i_get_stock_closing_stock($material_id,$project,"1990-01-01 00:00:00",$start_date_cl_st);
		
		if($start_date_filter != '')
		{
			$total_closing_stock = $total_closing_stock_value;
			$start = 1;

			/* Opening stock for this duration */
			$material_array[0]['added_on'] = "";
			$material_array[0]['transaction_type'] = "";
			$material_array[0]['project_name'] = "";
			$material_array[0]['opening_qty'] = $total_closing_stock_value;
			$material_array[0]['purchase_qty'] = 0;
			$material_array[0]['issue_qty'] = 0;	
			$material_array[0]['total_closing_stock'] = $total_closing_stock_value;
			$material_array[0]['value'] = "";					
			$material_array[0]['transaction_details'] = "Opening Stock";
			$material_array[0]['sl_no'] = "";
			$material_array[0]['uom'] = "";
		}					

		

		for($stock_history_count = 0; $stock_history_count < count($stock_material_history_list_data); $stock_history_count++)

		{

			$opening_qty = 0;
			$purchase_qty = 0;
			$issue_qty = 0;
			$total_value = 0;
			$transaction_type = $stock_material_history_list_data[$stock_history_count]["stock_history_transaction_type"];
			if($transaction_type == "Purchase")
			{
				//Get Grn Inspectioned List
				$stock_grn_engineer_inspection_search_data = array("inspection_id"=>$stock_material_history_list_data[$stock_history_count]["stock_history_reference_id"]);
				$stock_grn_inspection_list = i_get_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data);
				if($stock_grn_inspection_list["status"] == SUCCESS)
				{
					$stock_grn_inspection_list_data = $stock_grn_inspection_list["data"];
					$grn_qty = $stock_grn_inspection_list_data[0]["stock_grn_engineer_inspection_approved_quantity"];
					$grn_number = $stock_grn_inspection_list_data[0]["stock_grn_no"];
					$vendor = $stock_grn_inspection_list_data[0]["stock_vendor_name"];
					$grn_by = $stock_grn_inspection_list_data[0]["user_name"];
					$grn_inspectioned_date = get_formatted_date($stock_grn_inspection_list_data[0]["stock_grn_engineer_inspection_added_on"],"d-M-Y");
				}
				else
				{
					$grn_qty = 0;
					$grn_number = "";
					$grn_inspectioned_date = 0;
					$vendor = "";
				}
				
				
			}
			else if($transaction_type == "Issue")
			{
			
				//Get Indent Issue List
				$stock_issue_item_search_data = array("issue_item_id"=>$stock_material_history_list_data[$stock_history_count]["stock_history_reference_id"]);
				$stock_issued_items_list = i_get_stock_issued_item_without_sum($stock_issue_item_search_data);
				if($stock_issued_items_list["status"] == SUCCESS)
				{
					$stock_issued_items_list_data = $stock_issued_items_list["data"];
					$stock_issued_no = $stock_issued_items_list_data[0]["stock_issue_no"];
					$stock_issued_qty = $stock_issued_items_list_data[0]["stock_issue_item_qty"];
					$stock_issued_date = $stock_issued_items_list_data[0]["stock_issue_item_issued_on"];
					$machine_name = $stock_issued_items_list_data[0]["stock_machine_master_name"];
					$machine_number = $stock_issued_items_list_data[0]["stock_machine_master_id_number"];
					$issued_by  = $stock_issued_items_list_data[0]["added_by"];
					$issued_to  = $stock_issued_items_list_data[0]["user_name"];
				}
				else
				{
					$stock_issued_no = "";
					$stock_issued_qty = "";
					$stock_issued_date = "";
					$machine_name = "";
					$machine_number = "";
					$stock_issued_by = "";
				}
			}
			if(($transaction_type == "Transfer Out") || ($transaction_type == "Transfer In"))
			{
				$stock_transfer_search_data = array("transfer_id"=>$stock_material_history_list_data[$stock_history_count]["stock_history_reference_id"]);
				$stock_transfer_list = i_get_stock_transfer($stock_transfer_search_data);
				if($stock_transfer_list["status"] == SUCCESS)
				{
					$stock_transfer_list_data = $stock_transfer_list["data"];
					$stock_source_project = $stock_transfer_list_data[0]["source"];
					$stock_destination_project = $stock_transfer_list_data[0]["dest"];
				}
				else
				{
					$stock_source_project = "";
					$stock_destination_project = "";
				}
			}
			else
			{
				// Do nothing
			}
			switch($transaction_type)
			{
				case  "Opening" :
				$opening_qty = $stock_material_history_list_data[$stock_history_count]["stock_history_quantity"];
				$transaction_details =  'Added By : '.' '.$stock_material_history_list_data[$stock_history_count]["user_name"];
				
				break;
				
				case  "Purchase" :
				$purchase_qty = $stock_material_history_list_data[$stock_history_count]["stock_history_quantity"];
				$transaction_details =  'Grn no : '.' '.$grn_number.' <br>'.'Vendor  : '.' '.$vendor.' <br>'.'Grn By  : '.' '.$grn_by ; 
			
				break;
				
				case  "Issue" :
				$issue_qty = $stock_material_history_list_data[$stock_history_count]["stock_history_quantity"];
				$transaction_details = 'Issue No : '.' '.$stock_issued_no.' <br>'.'Machine  : '.' '.$machine_name.' '.$machine_number.' <br>'.'Issued By  : '.' '.$issued_by.' <br>'.'Issued to  : '.' '.$issued_to ;
				
				break;
				
				case  "Transfer Out" :
				$issue_qty = $issue_qty + $stock_material_history_list_data[$stock_history_count]["stock_history_quantity"];$transaction_details =  'Source : '.' '.$stock_source_project.' '.'Dest : '.' '.$stock_destination_project ;							
				
				break;
				
				case  "Transfer In" :
				$purchase_qty = $purchase_qty + $stock_material_history_list_data[$stock_history_count]["stock_history_quantity"];
				$transaction_details =  'Source : '.' '.$stock_source_project.' '.'Dest : '.' '.$stock_destination_project ;
				break;
						
			}
			
			/*if($stock_material_history_list_data[$count]["stock_history_transaction_type"] == "Purchase"){ 
				$transaction_details =  'Grn no : '.' '.$grn_number.' <br>'.'Vendor  : '.' '.$vendor.' <br>'.'Grn By  : '.' '.$grn_by ; 
				
			}
			if($stock_material_history_list_data[$count]["stock_history_transaction_type"] == "Issue") 
			{ 
				$transaction_details = 'Issue No : '.' '.$stock_issued_no.' <br>'.'Machine  : '.' '.$machine_name.' '.$machine_number.' <br>'.'Issued By  : '.' '.$issued_by.' <br>'.'Issued to  : '.' '.$issued_to ;
			} 
			if(($stock_material_history_list_data[$count]["stock_history_transaction_type"] == "Transfer Out") ||($stock_material_history_list_data[$count]["stock_history_transaction_type"] == "Transfer In")) 
			{ 
				$transaction_details =  'Source : '.' '.$stock_source_project.' '.'Dest : '.' '.$stock_destination_project ;
			}*/
			
			$total_opening_qty = $total_opening_qty + $opening_qty;
			$total_issue_qty = $total_issue_qty + $issue_qty;
			$total_purchase_qty = $total_purchase_qty + $purchase_qty;
			
			$rate = $stock_material_history_list_data[$stock_history_count]["stock_material_price"];
			$closing_stock = ($opening_qty + $purchase_qty - $issue_qty);
			$total_closing_stock = $total_closing_stock + $closing_stock;
			
			//$value = $rate * $total_closing_stock;
			$value = $rate * $total_closing_stock;
			$material_array[$stock_history_count+$start]['added_on'] = date("d-M-Y",strtotime($stock_material_history_list_data[$stock_history_count]["stock_history_added_on"]));
			$material_array[$stock_history_count+$start]['transaction_type'] = $stock_material_history_list_data[$stock_history_count]["stock_history_transaction_type"];
			$material_array[$stock_history_count+$start]['project_name'] = $stock_material_history_list_data[$stock_history_count]["stock_project_name"];
			$material_array[$stock_history_count+$start]['opening_qty'] = $opening_qty;
			$material_array[$stock_history_count+$start]['purchase_qty'] = $purchase_qty;
			$material_array[$stock_history_count+$start]['issue_qty'] = $issue_qty;
			$material_array[$stock_history_count+$start]['total_closing_stock'] = $total_closing_stock;
			$material_array[$stock_history_count+$start]['value'] = $rate * $total_closing_stock;
			$material_array[$stock_history_count+$start]['transaction_details'] = $transaction_details;
			$material_array[$stock_history_count+$start]['sl_no'] = $sl_no;
			$material_array[$stock_history_count+$start]['uom'] = $stock_material_history_list_data[$stock_history_count]["stock_unit_name"];
			$total_value = $total_value + $value;
		}
			
		for($display_count = (count($material_array) - 1) ; $display_count >= 0 ; $display_count--)
		{						
			$sl_no++;
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, $material_array[$display_count]['added_on']); 

			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $material_array[$display_count]['transaction_type']); 
			
			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, $material_array[$display_count]['transaction_details']); 

			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$material_array[$display_count]['project_name']); 

			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, $material_array[$display_count]['opening_qty']); 

			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, $material_array[$display_count]['purchase_qty']); 

			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, $material_array[$display_count]['issue_qty']); 

			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, $material_array[$display_count]['total_closing_stock']); 

			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, $rate); 			

			$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, $material_array[$display_count]['value']);
			
			$row_count++;
		}
	}

	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'0'.':'.$column_array[9].$row_count)->getAlignment()->setWrapText(true); 	

	

	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[9].$row_count);

	$row_count++;

	

	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'1'.':'.$column_array[9].($row_count - 1))->getAlignment()->setWrapText(true); 	

	/* Create excel sheet and write the column headers - END */

	

	

	header('Content-Type: application/vnd.ms-excel'); 

	header('Content-Disposition: attachment;filename="Material Report - Live.xls"'); 

	header('Cache-Control: max-age=0'); 

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 

	$objWriter->save('php://output');

}		

else

{

	header("location:login.php");

}	

?>