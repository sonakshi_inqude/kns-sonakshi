<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'meetings'.DIRECTORY_SEPARATOR.'meeting_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Initialization
	$alert = "";
	$alert_type = -1; // No alert	

	// Form Data
	$meeting_id = $_POST["hd_navigate_meeting_id"];
	$department = $_POST["hd_navigate_department"];

	// Capture all form data
	$meeting_id  = $_POST["hd_navigate_meeting_id"];
	$department  = $_POST["hd_navigate_department_id"];
	$mom_item    = $_POST["stxt_mom_item"];
	$description = $_POST["txt_details"];
	$target_date = $_POST["dt_target_date"];
	$assignee    = $_POST["ddl_assigned_to"];
	
	if($assignee != "")
	{
		if($target_date == "")
		{
			$target_date = date("Y-m-d");
		}
		$type = 1;
		
		$task_result = i_add_task_plan_general($type,$assignee,$mom_item,$department,$target_date,$user);
		$task_id = $task_result["data"];
	}
	else	
	{
		$task_id = "";
	}
		
	$mom_iresult = i_add_meeting_mom($meeting_id,$mom_item,$description,$target_date,$task_id,$user);
	
	if($mom_iresult["status"] == SUCCESS)
	{
		header("location:meeting_details.php?meeting=".$meeting_id."&msg=MOM item successfully added");
	}
	else
	{
		header("location:meeting_details.php?meeting=".$meeting_id."&msg=".$mom_iresult["data"]);
	}
}
else
{
	header("location:login.php");
}	
?>