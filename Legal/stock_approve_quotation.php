<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

TBD:

*/



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_quotation_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	// Update attendance details

	$quotation_id = $_POST["quotation_id"];

	$status       = $_POST["action"];

	$approved_by  = $user;

	$approved_on  = date("Y-m-d H:i:s");		

	

	$stock_quotation_compare_search_data = array("quotation_id"=>$quotation_id);

	$quotation_list_results =  i_get_stock_quotation_compare_list($stock_quotation_compare_search_data);

	$quotation_list_results_data = $quotation_list_results["data"];

	$item_id  = $quotation_list_results_data[0]["stock_quotation_indent_id"];
	$project = $quotation_list_results_data[0]["stock_quotation_project"];
	

	$price = $quotation_list_results_data[0]["stock_quotation_amount"];

	

	//Update Material Master price

	$material_master_update_data = array("material_price"=>$price);

	$materail_master_list = i_delete_material_list($item_id,$material_master_update_data);

	
	$stock_quotation_compare_search_data = array("indent_id"=>$item_id,'status'=>'Waiting','project'=>$project);
	$quotation_item_list_results =  i_get_stock_quotation_compare_list($stock_quotation_compare_search_data);

	if($quotation_item_list_results["status"] == SUCCESS)

	{

		for($count = 0 ; $count < count($quotation_item_list_results["data"]) ; $count++)

		{

			$quotation_compare_update_data = array("approved_by"=>$approved_by,"approved_on"=>$approved_on,"status"=>'Rejected');

			$approve_quotation_result = i_update_quotation_compare($quotation_item_list_results['data'][$count]['stock_quotation_id'],'',$quotation_compare_update_data);

		}

	}

	else

	{

		

	}	

	

	$quotation_compare_update_data = array("approved_by"=>$approved_by,"approved_on"=>$approved_on,"status"=>$status);

	$approve_quotation_result = i_update_quotation_compare($quotation_id,'',$quotation_compare_update_data);

	

	if($approve_quotation_result["status"] == FAILURE)

	{		

		echo $approve_quotation_result["data"];

	}

	else

	{

		// Reset the quote process for this item

		$update_quote_reset_data = array('reset_date_time'=>date('Y-m-d H:i:s'),'updated_by'=>$user);
		$quote_reset_uresult = i_update_stock_quote_reset($item_id,$project,$update_quote_reset_data);
		

		echo "SUCCESS";

	}

}

else

{

	header("location:login.php");

}

?>