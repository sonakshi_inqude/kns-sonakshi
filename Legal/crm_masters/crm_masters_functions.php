<?php
/**
 * @author Nitin kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_crm_masters.php');

/*
PURPOSE : To add bank
INPUT 	: Bank, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_bank($bank_name,$added_by)
{
	$bank_sresult = db_get_bank_list($bank_name,'','','','');
	
	if($bank_sresult["status"] == DB_NO_RECORD)
	{
		$bank_iresult = db_add_bank($bank_name,$added_by);
		
		if($bank_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Bank Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Bank Name already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get bank list
INPUT 	: Bank Name, Active
OUTPUT 	: Bank List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_bank_list($bank_name,$active)
{
	$bank_sresult = db_get_bank_list($bank_name,$active,'','','');
	
	if($bank_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $bank_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No bank added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add dimension
INPUT 	: Dimension Name, Length, Breadth, Non-Standard,, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_dimension($name,$length,$breadth,$non_standard,$added_by)
{
	$dimension_sresult = db_get_dimension_list($name,$length,$breadth,'','','','','');
	
	if($dimension_sresult["status"] == DB_NO_RECORD)
	{
		$dimension_iresult = db_add_dimension($name,$length,$breadth,$non_standard,$added_by);
		
		if($dimension_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Dimension Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Dimension already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get dimension list
INPUT 	: Dimension Name, Length, Breadth, Non-Standard, Active
OUTPUT 	: Dimension List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_dimension_list($name,$length,$breadth,$non_standard,$active)
{
	$dimension_sresult = db_get_dimension_list($name,$length,$breadth,$non_standard,$active,'','','');
	
	if($dimension_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $dimension_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No dimension added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add interest status
INPUT 	: Interest Status, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_interest_status($int_status,$added_by)
{
	$int_status_sresult = db_get_customer_interest_list($int_status,'','','','');
	
	if($int_status_sresult["status"] == DB_NO_RECORD)
	{
		$int_status_iresult = db_add_interest_status($int_status,$added_by);
		
		if($int_status_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Interest Status Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Interest Status already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get interest status list
INPUT 	: Interest Status, Active
OUTPUT 	: Interest Status List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_interest_status_list($interest_status,$active)
{
	$int_status_sresult = db_get_customer_interest_list($interest_status,$active,'','','');
	
	if($int_status_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $int_status_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No interest status added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add enquiry source
INPUT 	: Enquiry Source, Enquiry Source Type, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_enquiry_source($enq_source,$enquiry_source_type,$added_by)
{
	$enq_source_sresult = db_get_enquiry_source_list($enq_source,'','','','');
	
	if($enq_source_sresult["status"] == DB_NO_RECORD)
	{
		$enq_source_iresult = db_add_enquiry_source($enq_source,$enquiry_source_type,$added_by);
		
		if($enq_source_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Enquiry Source Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Enquiry Source already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get enquiry source list
INPUT 	: Enquiry Source, Active, Source ID, Source Type
OUTPUT 	: Enquiry Source List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_enquiry_source_list($enquiry_source,$active,$enquiry_source_id='',$enquiry_source_type='')
{
	$source_type = $enquiry_source_type;
	
	$enquiry_source_sresult = db_get_enquiry_source_list($enquiry_source,$source_type,$active,'','','',$enquiry_source_id,$enquiry_source_type);
	
	if($enquiry_source_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $enquiry_source_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No enquiry source added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To get enquiry source type list
INPUT 	: Enquiry Source Type, Active
OUTPUT 	: Enquiry Source Type List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_enquiry_source_type_list($enquiry_source_type,$active)
{
	$enquiry_source_type_sresult = db_get_enquiry_source_type_list($enquiry_source_type,$active,'','','');
	
	if($enquiry_source_type_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $enquiry_source_type_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No enquiry source type added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add reason
INPUT 	: Reason, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_reason($reason,$added_by)
{
	$reason_sresult = db_get_reason_list($reason,'','','','');
	
	if($reason_sresult["status"] == DB_NO_RECORD)
	{
		$reason_iresult = db_add_reason($reason,$added_by);
		
		if($reason_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Reason Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Reason already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get reason list
INPUT 	: Reason, Active
OUTPUT 	: Reason List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_reason_list($reason,$active)
{
	$reason_sresult = db_get_reason_list($reason,$active,'','','');
	
	if($reason_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $reason_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No reason added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add release status
INPUT 	: Release Status, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_release_status($release_status,$added_by)
{
	$release_status_sresult = db_get_rel_status_list($release_status,'','','','');
	
	if($release_status_sresult["status"] == DB_NO_RECORD)
	{
		$release_status_iresult = db_add_release_status($release_status,$added_by);
		
		if($release_status_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Release Status Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Release Status already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get release status
INPUT 	: Release Status, Active
OUTPUT 	: Release Status List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_release_status($rel_status,$active)
{
	$rel_status_sresult = db_get_rel_status_list($rel_status,$active,'','','');
	
	if($rel_status_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $rel_status_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No release status added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add site type
INPUT 	: Site Type, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_site_type($site_type,$added_by)
{
	$site_type_sresult = db_get_site_type_list($site_type,'','','','');
	
	if($site_type_sresult["status"] == DB_NO_RECORD)
	{
		$site_type_iresult = db_add_site_type($site_type,$added_by);
		
		if($site_type_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Site Type Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Site Type already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get site type list
INPUT 	: Site Type, Active
OUTPUT 	: Site Type List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_site_type_list($site_type,$active)
{
	$site_type_sresult = db_get_site_type_list($site_type,$active,'','','');
	
	if($site_type_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $site_type_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No site type added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add site status
INPUT 	: Site Status, Max. Days, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_site_status($site_status,$max_days,$added_by)
{
	$site_status_sresult = db_get_site_status_list($site_status,'','','','');
	
	if($site_status_sresult["status"] == DB_NO_RECORD)
	{
		$site_status_iresult = db_add_site_status($site_status,$max_days,$added_by);
		
		if($site_status_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Site Status Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Site Status already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get site status list
INPUT 	: Site Status, Active
OUTPUT 	: Site Status List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_site_status_list($site_status,$active)
{
	$site_status_sresult = db_get_site_status_list($site_status,$active,'','','');
	
	if($site_status_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $site_status_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No site status added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To add cab
INPUT 	: Travels, Cab No, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_cab($travels,$number,$added_by)
{
	$cab_sresult = db_get_cab_list('',$number,'','','');
	
	if($cab_sresult["status"] == DB_NO_RECORD)
	{
		$cab_iresult = db_add_cab($travels,$number,$added_by);
		
		if($cab_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Cab Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Cab already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get cab list
INPUT 	: Cab ID, Cab Number
OUTPUT 	: Cab List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_cab_list($cab_id,$cab_number)
{
	$cab_list_sresult = db_get_cab_list($cab_id,$cab_number,'','','');
	
	if($cab_list_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $cab_list_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No cab added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To get active status name
INPUT 	: Active Status
OUTPUT 	: Active yes/no
BY 		: Nitin Kashyap
*/
function i_get_active_name($active)
{
	if(1 == $active)
	{
		$return = "Enabled";
	}
	else
	{
		$return = "Disabled";
	}
	
	return $return;
}

/*
PURPOSE : To add management block reason
INPUT 	: Reason Name, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_mgmnt_blk_reason($reason_name,$added_by)
{
	$mgmnt_blk_reason_sresult = db_get_mgmt_block_reason_list($reason_name,'','','','');
	
	if($mgmnt_blk_reason_sresult["status"] == DB_NO_RECORD)
	{
		$mgmnt_blk_reason_iresult = db_add_mgmt_block_reason($reason_name,$added_by);
		
		if($mgmnt_blk_reason_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Management Block Reason Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Cab already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get mgmt block reason list
INPUT 	: Reason Name
OUTPUT 	: Reason List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_mgmnt_blk_reason_list($reason_name)
{
	$mgmnt_blk_reason_list_sresult = db_get_mgmt_block_reason_list($reason_name,'1','','','');
	
	if($mgmnt_blk_reason_list_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $mgmnt_blk_reason_list_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No management block reason added. Please contact the system admin"; 
    }
	
	return $return;
}

// Author : Nitin Kashyap
// Purpose: To get payment mode list
// Input  : Payment Mode, Active
// Output : Payment Mode list
function i_get_payment_mode_list($payment_mode,$active)
{
	// get filing mode list
	$payment_mode_sresult = db_get_payment_mode_list($payment_mode,$active,'','','');
	
	if($payment_mode_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$return["status"] = SUCCESS;
		$return["data"]   = $payment_mode_sresult["data"];
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"]   = "";
	}
	
	return $return;
}

/*
PURPOSE : To add cancellation reason
INPUT 	: Reason Name, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_cancellation_reason($cancel_reason,$added_by)
{
	$cancel_reason_sresult = db_get_cancel_reason_list($cancel_reason,'','','','');
	
	if($cancel_reason_sresult["status"] == DB_NO_RECORD)
	{
		$cancel_reason_iresult = db_add_cancellation_reason($cancel_reason,$added_by);
		
		if($cancel_reason_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Cancellation Reason Successfully Added";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Cancellation Reason already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get cancellation reason list
INPUT 	: Cancel Reason Name, Active
OUTPUT 	: Cancel Reason List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_cancel_reason_list($cancel_reason,$active)
{
	$cancel_reason_sresult = db_get_cancel_reason_list($cancel_reason,$active,'','','');
	
	if($cancel_reason_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $cancel_reason_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No cancellation reason added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To get delay reason list
INPUT 	: Delay Reason Name, Active
OUTPUT 	: Delay Reason List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_crm_delay_reason_list($delay_reason,$active)
{
	$delay_reason_sresult = db_get_crm_delay_reason_master_list($delay_reason,$active,'','','');
	
	if($delay_reason_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $delay_reason_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No delay reason added. Please contact the system admin"; 
    }
	
	return $return;
}
?>