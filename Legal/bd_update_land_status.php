<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 5th Sep 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_masters'.DIRECTORY_SEPARATOR.'bd_masters_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET["file"]))
	{
		$file_id = $_GET["file"];
	}
	else
	{
		$file_id = "-1";
	}

	// Capture the form data
	if(isset($_POST["add_land_status_submit"]))
	{
		$land_status = $_POST["ddl_status"];
		$date   	 = $_POST["date"];	
		$file_id	 = $_POST["file_id"];
		
		// Check for mandatory fields
		if($land_status != "")
		{
			$status_iresult = i_add_bd_land_status($file_id,$land_status,$date,$user);
			
			if($status_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert      = 'Land Status was successfully updated';
				$land_status_uresult = i_edit_bd_file($file_id,'','','','','','','',$land_status,'','','','','','','','','');
				if($land_status_uresult['status'] != SUCCESS)
				{
					$alert_type = 0;
					$alert      = 'There was some issue in updating land status of the file. Please contact the admin';
				}
			}
			else
			{
				$alert_type = 0;
				$alert      = $status_iresult['data'];
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get File Details
	$bd_file_details = i_get_bd_files_list($file_id,'','','','','','','');
	if($bd_file_details['status'] == SUCCESS)
	{
		$bd_file_details_data = $bd_file_details['data'];
	}
	else
	{
		header('location:bd_file_list.php');
	}
	
	// Get Land Status List for this file
	$land_status_list = i_get_bd_land_status_list($file_id,'','','');
	if($land_status_list['status'] == SUCCESS)
	{
		$land_status_list_data = $land_status_list['data'];
	}
	
	//Get Owner Status list
	$owner_status_list = i_get_owner_status_list('');
	if($owner_status_list["status"]== SUCCESS)
	{
		$owner_status_list_data = $owner_status_list["data"];
	}
	else
	{
		$owner_status_list_data = "";
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Land Status</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>File ID: <?php echo $bd_file_details_data[0]['bd_project_file_id']; ?>&nbsp;&nbsp;&nbsp;Survey No: <?php echo $bd_file_details_data[0]['bd_file_survey_no']; ?>&nbsp;&nbsp;&nbsp;Project: <?php echo $bd_file_details_data[0]['bd_project_name']; ?>&nbsp;&nbsp;&nbsp;Land Owner: <?php echo $bd_file_details_data[0]['bd_file_owner']; ?>&nbsp;&nbsp;&nbsp;</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Land Status</a>
						  </li>						  
						</ul>
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_land_status_form" class="form-horizontal" method="post" action="bd_update_land_status.php">
								<input type="hidden" name="file_id" value="<?php echo $file_id; ?>" />
									<fieldset>										
																				
										<div class="control-group">											
											<label class="control-label" for="ddl_status">Land Status *</label>
												<div class="controls">
											    <select name="ddl_status" required>
												<option value="">- - Select Status - -</option>
												<?php
												for($count = 0; $count < count($owner_status_list_data); $count++)
												{
												?>
												<option value="<?php echo $owner_status_list_data[$count]["bd_file_owner_status_id"]; ?>"><?php echo $owner_status_list_data[$count]["bd_file_owner_status_name"]; ?></option>					
												<?php
												}
												?>
												</select>												
											</div> <!-- /controls -->									
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="date">Date*</label>
											<div class="controls">
												<input type="date" class="span6" name="date" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                         										 <br />
																					
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_land_status_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>	
								<table class="table table-bordered" style="table-layout: fixed;">
								<thead>
								  <tr>
									<th>Land Status</th>					
									<th>Status As On</th>										
									<th>Updated By</th>
									<th>Updated On</th>									
								  </tr>
								</thead>
								<tbody>							
								<?php
								if($land_status_list["status"] == SUCCESS)
								{									
									for($count = 0; $count < count($land_status_list_data); $count++)
									{																	
									?>
									<tr>
									<td style="word-wrap:break-word;"><?php echo $land_status_list_data[$count]["bd_file_owner_status_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($land_status_list_data[$count]["bd_file_land_status_date"])); ?></td>									
									<td style="word-wrap:break-word;"><?php echo $land_status_list_data[$count]["user_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i:s",strtotime($land_status_list_data[$count]["bd_file_land_status_added_on"])); ?></td>									
									</tr>
									<?php									
									}
								}
								else
								{
									// Do nothing
								}	
								?>	
								<tr>
								<td style="word-wrap:break-word;"><?php echo $bd_file_details_data[0]["bd_file_owner_status_name"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($bd_file_details_data[0]["bd_project_file_mapping_added_on"])); ?></td>									
								<td style="word-wrap:break-word;"><?php echo $bd_file_details_data[0]["user_name"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i:s",strtotime($bd_file_details_data[0]["bd_project_file_mapping_added_on"])); ?></td>	
								</tr>								
								</tbody>
							  </table>
								
							</div>
						  						  
						</div>
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->	      		      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
