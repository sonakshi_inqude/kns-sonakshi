<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: kns_grn_engineer_inspection_list.php

CREATED ON	: 30-Sep-2016

CREATED BY	: Lakshmi

PURPOSE     : List of grn engineer inspection for customer withdrawals

*/



/*

TBD: 

*/
$_SESSION['module'] = 'Stock Transactions';


/* DEFINES - START */

define('GRN_INSPECTION_FUNC_ID','173');

/* DEFINES - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_grn_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_purchase_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	

	// Get permission settings for this user for this page

	$view_perms_list   = i_get_user_perms($user,'',GRN_INSPECTION_FUNC_ID,'2','1');

	$edit_perms_list   = i_get_user_perms($user,'',GRN_INSPECTION_FUNC_ID,'3','1');

	$delete_perms_list = i_get_user_perms($user,'',GRN_INSPECTION_FUNC_ID,'4','1');

	$add_perms_list    = i_get_user_perms($user,'',GRN_INSPECTION_FUNC_ID,'1','1');



	// Query String Data

	if(isset($_REQUEST["grn_item_id"]))

	{

		$grn_item_id = $_REQUEST["grn_item_id"];

	}

	else

	{

		$grn_item_id = "-1";

	}

	

	

	// Temp data

	// Get GRN Engineer Inspection already added

	if($grn_item_id != "-1")

	{

		$stock_grn_engineer_inspection_search_data = array("grn_item_id"=>$grn_item_id,"active"=>'1');

		$grn_engineer_inspection_list = i_get_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data);		

		if($grn_engineer_inspection_list['status'] == SUCCESS)

		{

			$grn_engineer_inspection_list_data = $grn_engineer_inspection_list['data'];

			$item = $grn_engineer_inspection_list_data[0]["stock_grn_engineer_inspection_grn_item_id"];

			$qty = $grn_engineer_inspection_list_data[0]["stock_grn_engineer_inspection_approved_quantity"];

			//Grn Details

		}

		else

		{

			$grn_id = "";

			$item 	= "";

			$qty 	= "";

		}

	}

	else

	{

		$stock_grn_engineer_inspection_search_data = array("active"=>'1');

		$grn_engineer_inspection_list = i_get_stock_grn_engineer_inspection_list($stock_grn_engineer_inspection_search_data);		

		if($grn_engineer_inspection_list['status'] == SUCCESS)

		{

			$grn_engineer_inspection_list_data = $grn_engineer_inspection_list['data'];

		}	

	}

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>GRN Engineer Inspection List</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

   





    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>

    



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">

       

          <div class="span6" style="width:100%;">

          

          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>GRN Engineer Inspection List</h3><?php if($add_perms_list['status'] == SUCCESS){ ?><span style="float:right; padding-right:20px;"><a href="stock_add_grn_engineer_inspection.php">Add GRN Inspection</a></span><?php } ?>

            </div>			

            <!-- /widget-header -->

            <div class="widget-content">

			

              <table class="table table-bordered" style="table-layout: fixed;">

                <thead>

                  <tr>

				  

				    <th style="word-wrap:break-word;">SL No</th>

					<th style="word-wrap:break-word;">PO No</th>

					<th style="word-wrap:break-word;">Item Name</th>

					<th style="word-wrap:break-word;">Item Code</th>
					
					<th style="word-wrap:break-word;">UOM</th>

					<th style="word-wrap:break-word;">Vendor</th>			

					<th style="word-wrap:break-word;">GRN No</th>
					
					<th style="word-wrap:break-word;">Project</th>

					<th style="word-wrap:break-word;">GRN Date</th>

					<th style="word-wrap:break-word;">Inward Qty</th>

					<th style="word-wrap:break-word;">Accepted Qty</th>

					<th style="word-wrap:break-word;">Rejected Qty</th>
					
					<th style="word-wrap:break-word;">Total Value</th>

					<th style="word-wrap:break-word;">Inspected By</th>

					<th style="word-wrap:break-word;">Inspected Date</th>	

					<th style="word-wrap:break-word;">Document</th>

				</tr>

				</thead>

				<tbody>							

				<?php

				if($grn_engineer_inspection_list["status"] == SUCCESS)

				{

					$sl_no = 0;

					for($count = 0; $count < count($grn_engineer_inspection_list_data); $count++)

					{

						$sl_no++;

						

						// Get PO details						

						$stock_purchase_order_items_search_data = array('item'=>$grn_engineer_inspection_list_data[$count]['stock_grn_item'],'order_id'=>$grn_engineer_inspection_list_data[$count]['stock_grn_purchase_order_id']);

						$po_sresult = i_get_stock_purchase_order_items_list($stock_purchase_order_items_search_data);

						

						$item_cost    = $po_sresult['data'][0]["stock_purchase_order_item_cost"];
						
						$project_name = $po_sresult['data'][0]["stock_location_name"];

						$accepted_qty = $grn_engineer_inspection_list_data[$count]["stock_grn_engineer_inspection_approved_quantity"];

						$item_value   = $item_cost*$accepted_qty;

						$tax_rate     = $po_sresult['data'][0]["stock_tax_type_master_value"];

						$tax_value    = ($po_sresult['data'][0]["stock_tax_type_master_value"] * $item_value)/100;

					?>

					<tr>

					<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>

					<td style="word-wrap:break-word;"><?php echo $grn_engineer_inspection_list_data[$count]["stock_purchase_order_number"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $grn_engineer_inspection_list_data[$count]["stock_material_name"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $grn_engineer_inspection_list_data[$count]["stock_material_code"]; ?></td>
					
					<td style="word-wrap:break-word;"><?php echo $grn_engineer_inspection_list_data[$count]["stock_unit_name"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $grn_engineer_inspection_list_data[$count]["stock_vendor_name"]; ?></td>				

					<td style="word-wrap:break-word;"><?php echo $grn_engineer_inspection_list_data[$count]["stock_grn_no"]; ?></td>
					
					<td style="word-wrap:break-word;"><?php echo $project_name; ?></td>

					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($grn_engineer_inspection_list_data[$count]["stock_grn_added_on"])); ?></td>

					<td style="word-wrap:break-word;"><?php echo $grn_engineer_inspection_list_data[$count]["stock_grn_item_inward_quantity"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo $accepted_qty; ?></td>

					<td style="word-wrap:break-word;"><?php echo ($grn_engineer_inspection_list_data[$count]["stock_grn_item_inward_quantity"] - $accepted_qty); ?></td>
					
					<td><?php echo ($item_value + $tax_value); ?></td>
					
					<td style="word-wrap:break-word;"><?php echo $grn_engineer_inspection_list_data[$count]["user_name"]; ?></td>

					<td style="word-wrap:break-word;"><?php echo date('d-M-Y',strtotime($grn_engineer_inspection_list_data[$count]["stock_grn_engineer_inspection_added_on"])); ?></td>

					<td style="word-wrap:break-word;"><?php if($grn_engineer_inspection_list_data[$count]["stock_grn_doc"] != '')

					{?>	

					<a href="documents/<?php echo $grn_engineer_inspection_list_data[$count]["stock_grn_doc"]; ?>" target="_blank">DOWNLOAD</a>

					<?php

					}

					else

					{?>

						NO DOCUMENT

					<?php

					}

					?></td>				

					</tr>

					<?php									

					}

				}

				else

				{

				?>

				<td colspan="14">No grn engineer inspection added yet!</td>

				<?php

				}

				 ?>	



                </tbody>

              </table>

            </div>

            <!-- /widget-content --> 

          </div>

          <!-- /widget --> 

         

          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>



<script>

function delete_grn_engineer_inspection(inspection_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "stock_grn_engineer_inspection_list.php";

					}

				}

			}



			xmlhttp.open("POST", "stock_delete_grn_engineer_inspection.php");   // file name where delete code is written

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("inspection_id=" + inspection_id + "&action=0");

		}

	}	

}

function go_to_edit_grn_engineer_inspection(inspection_id)

{		

	var form = document.createElement("form");

    form.setAttribute("method", "post");

    form.setAttribute("action", "stock_edit_grn_engineer_inspection.php");

	

	var hiddenField1 = document.createElement("input");

	hiddenField1.setAttribute("type","hidden");

	hiddenField1.setAttribute("name","inspection_id");

	hiddenField1.setAttribute("value",inspection_id);

	

	form.appendChild(hiddenField1);

	

	document.body.appendChild(form);

    form.submit();

}

</script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>



  </body>



</html>