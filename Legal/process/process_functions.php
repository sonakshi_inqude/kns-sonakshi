<?php
/**
 * @author Nitin kashyap
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_files.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_mail.php');

/*
PURPOSE : To add legal process plan
INPUT 	: Process Type, File ID, Start Date, Assigned to, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_process_plan_legal($process_type,$file_id,$start_date,$assigned_to,$added_by)
{	
	$process_plan_legal_iresult = db_add_process_plan_legal($process_type,$file_id,$start_date,$assigned_to,$added_by);
		
	if($process_plan_legal_iresult['status'] == SUCCESS)
	{
		$return["data"]   = $process_plan_legal_iresult["data"];
		$return["status"] = SUCCESS;				
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To add legal process type
INPUT 	: Process Name, Module, Is this a bulk process, Added By
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_add_process_type_legal($process_name,$module,$is_bulk,$added_by)
{
	$process_type_legal_sresult = db_get_process_type_list($process_name,$module,'','','');
	
	if($process_type_legal_sresult["status"] == DB_NO_RECORD)
	{
		$process_type_legal_iresult = db_add_process_type_legal($process_name,$module,$is_bulk,$added_by);
		
		if($process_type_legal_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Process Type successfully created";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Process type already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get process type list
INPUT 	: Process Name, Module, Active Status, Is Bulk process
OUTPUT 	: Process Type List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_process_type_list($process_name,$module,$status="1",$is_bulk="")
{
	$process_type_sresult = db_get_process_type_list($process_name,$module,'','','',$status,$is_bulk);
	
	if($process_type_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $process_type_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No process type added. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To get list of legal process plans
INPUT 	: Process Plan ID, File ID, Process Type, Party name, Village, Extent, File Type, Assigned To
OUTPUT 	: File Type List or Error Details, success or failure message
BY 		: Nitin Kashyap
*/
function i_get_legal_process_plan_list($process_plan_id,$file_id,$type,$party_name,$survey_no,$village,$extent,$file_type,$assigned_to,$status,$order)
{
	$legal_process_plan_sresult = db_get_legal_process_plan_list($process_plan_id,$file_id,$type,$party_name,$survey_no,$village,$extent,$file_type,$assigned_to,'','','',$status,$order);
	
	if($legal_process_plan_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $legal_process_plan_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No process plan yet!"; 
    }
	
	return $return;
}

/*
PURPOSE : To get details of given legal process plan
INPUT 	: Process Plan ID
OUTPUT 	: Process Plan Details
BY 		: Nitin Kashyap
*/
function i_get_legal_process_plan_details($process_plan_id)
{
	$legal_process_plan_sresult = db_get_legal_process_plan_list($process_plan_id,'','','','','','','','','','','','','');
	
	if($legal_process_plan_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $legal_process_plan_sresult["data"][0]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No process plan with this ID!"; 
    }
	
	return $return;
}

/*
PURPOSE : To update status of a project plan
INPUT 	: Process Plan ID, Status
OUTPUT 	: success or failure message
BY 		: Nitin Kashyap
*/
function i_update_project_plan($process_plan_id,$status)
{
	$process_plan_uresult = db_update_process_plan($process_plan_id,$status);
	
	if($process_plan_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $process_plan_uresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please try again later!"; 
    }
	
	return $return;
}

/*
PURPOSE : To delete a project plan
INPUT 	: Process Plan ID, Status
OUTPUT 	: success or failure message
BY 		: Nitin Kashyap
*/
function i_delete_project_plan($process_plan_id,$file_id)
{
	$process_plan_uresult = db_enable_disable_process_plan($process_plan_id,$file_id,'0');
	
	if($process_plan_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $process_plan_uresult["data"]; 
		
		$task_dresult = i_delete_task('',$process_plan_id);
		
		if($task_dresult["status"] == SUCCESS)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = "Process Plan Deleted Successfully";
		}
		else
		{
			$return["status"] = SUCCESS;
			$return["data"]   = "Process Plan Deleted but all the tasks under it were not deleted. Please contact the admin";
		}
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please try again later!"; 
    }
	
	return $return;
}

/*
PURPOSE : To enable/disable process type
INPUT 	: Process Type, Action
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_enable_disable_process_type($process_type_id,$action)
{
    $process_type_uresult = db_enable_disable_process_type($process_type_id,$action);
	
	if($process_type_uresult['status'] == SUCCESS)
	{
		$return["data"]   = "Process Type status updated";
		$return["status"] = SUCCESS;
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To add bulk legal process
INPUT 	: Process Type, Action
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_legal_add_bulk_process($legal_bulk_process_type,$legal_process_assigned_to,$legal_process_start_date,$legal_bulk_process_added_by)
{
	$bulk_iresult= db_add_legal_bulk_process($legal_bulk_process_type,$legal_process_start_date,$legal_process_assigned_to,$legal_bulk_process_added_by);

	if($bulk_iresult["status"]==SUCCESS)
	{
		$return["status"] = SUCCESS;
		$return["data"]   = $bulk_iresult['data'];
	}
	else
	{
		$return["status"] =FAILURE;
		$return["data"]   = "There was an Internal Error";
	}
	
	return $return;	
}

/*
PURPOSE : To add files to a bulk legal process
INPUT 	: Process Type, Action
OUTPUT 	: Message, success or failure message
BY 		: Nitin Kashyap
*/
function i_legal_add_bulk_files($legal_bulk_process_files_process_id,$legal_bulk_process_file_id,$legal_bulk_process_files_assigned_to,$legal_bulk_process_files_added_by)
{
	$legal_files_search_data = array('legal_bulk_process_files_process_id' => $legal_bulk_process_files_process_id,'legal_bulk_process_file_id' => $legal_bulk_process_file_id,"active" => '1');
	$bulk_files_sresults = db_get_legal_bulk_files($legal_files_search_data);
	
	if($bulk_files_sresults["status"] == DB_NO_RECORD)
	{
		$bulk_files_iresult = db_add_legal_bulk_files($legal_bulk_process_files_process_id,$legal_bulk_process_file_id,$legal_bulk_process_files_assigned_to,$legal_bulk_process_files_added_by);

		if($bulk_files_iresult["status"] == SUCCESS)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = $bulk_files_iresult;
		}
		else
		{
			$return["status"] = FAILURE;
			$return["data"]   = "There was an Internal Error";
		}
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"]   = "File already added to this process";
	}
	
	return $return;		
}

/*
PURPOSE : To get details of given bulk legal process plan
INPUT 	: Bulk Process Filter Array
OUTPUT 	: Process Plan Details
BY 		: Nitin Kashyap
*/
function i_get_bulk_legal_process_plan_details($legal_bulk_search_data)
{	
	$legal_process_plan_sresult = db_get_legal_bulk_process($legal_bulk_search_data);
	
	if($legal_process_plan_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $legal_process_plan_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No process plan with this ID!"; 
    }
	
	return $return;
}

/*
PURPOSE : To get files of given bulk legal process plan
INPUT 	: Process Plan ID
OUTPUT 	: Process Plan Details
BY 		: Nitin Kashyap
*/
function i_get_bulk_legal_process_plan_files($legal_bulk_files_search_data)
{
	$legal_process_files_sresult = db_get_legal_bulk_files($legal_bulk_files_search_data);
	
	if($legal_process_files_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $legal_process_files_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No files for this ID!"; 
    }
	
	return $return;
}

/*
PURPOSE : To update status of a bulk process plan
INPUT 	: Process Plan ID, Status
OUTPUT 	: success or failure message
BY 		: Nitin Kashyap
*/
function i_update_bulk_project_plan($process_plan_id,$status)
{
	$process_plan_uresult = db_update_bulk_process_plan($process_plan_id,$status);
	
	if($process_plan_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $process_plan_uresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please try again later!"; 
    }
	
	return $return;
}

/*
PURPOSE : To add Legal Bulk related documents
INPUT 	: Bulk Process ID, Document Type, Document Remarks
OUTPUT 	: Success or Failure message,If its SUCCESS it will add new data into table.
BY 		: Sonakshi D
*/
function i_add_legal_bulk_documents($bulk_document_process,$bulk_document_type,$bulk_document_remarks,$bulk_document_added_by)
{
	$legal_bulk_doc_search_data = array('bulk_document_process_id'=>$bulk_document_process,'bulk_document_type'=>$bulk_document_type);
	$legal_bulk_doc_sresults = db_get_legal_bulk_documents($legal_bulk_doc_search_data);
	
	if($legal_bulk_doc_sresults["status"] == DB_NO_RECORD)
	{
		$legal_bulk_doc_iresult = db_add_legal_bulk_documents($bulk_document_process,$bulk_document_type,$bulk_document_remarks,$bulk_document_added_by);

		if($legal_bulk_doc_iresult["status"] == SUCCESS)
		{
			$return["status"] = SUCCESS;
			$return["data"]   = $legal_bulk_doc_iresult['data'];
		}
		else
		{
			$return["status"] = FAILURE;
			$return["data"]   = "There was an Internal Error";
		}
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"]   = "This document is already added for this file";
	}
		
	return $return;
}

/*
PURPOSE : To get Legal Bulk related documents
INPUT 	: Bulk Process Document Search Array
OUTPUT 	: Success or Failure message,If its SUCCESS it will add new data into table.
BY 		: Sonakshi D
*/
function i_get_legal_bulk_documents($legal_bulk_doc_search_data)
{	
	$legal_bulk_doc_sresults = db_get_legal_bulk_documents($legal_bulk_doc_search_data);
	
	if($legal_bulk_doc_sresults["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$return["status"] = SUCCESS;
		$return["data"]   = $legal_bulk_doc_sresults['data'];
	}
	else
	{
		$return["status"] = FAILURE;
		$return["data"]   = "This document is already added for this file";
	}
		
	return $return;
}

/*
PURPOSE : To update status of a bulk document
INPUT 	: Document ID, Status
OUTPUT 	: success or failure message
BY 		: Nitin Kashyap
*/
function i_update_bulk_document_status($document_id,$status,$updated_by)
{
	$bulk_doc_uresult = db_update_bulk_document($document_id,$status,$updated_by);
	
	if($bulk_doc_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $bulk_doc_uresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "There was an internal error. Please try again later!"; 
    }
	
	return $return;
}

/*
PURPOSE : To add Process User Mapping 
INPUT 	: Process ID, User, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi D	
*/
function i_add_process_user($process_id,$user,$added_by)
{
	$process_user_sresult = db_get_process_user_list($process_id,$user,'','','');
	
	if($process_user_sresult["status"] == DB_NO_RECORD)
	{
		$process_user_iresult = db_add_process_user($process_id,$user,$added_by);
		
		if($process_user_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Process and User Mapping is successfully created";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Process type for this user is already mapped!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get process user list
INPUT 	: Process ID, user
OUTPUT 	: Process user List or Error Details, success or failure message
BY 		: Sonakshi D
*/
function i_get_process_user_list($process_id,$user,$added_by)
{
	$process_user_sresult = db_get_process_user_list($process_id,$user,$added_by,'','');
	
	if($process_user_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $process_user_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No User added for this process. Plaese select from user list"; 
    }
	
	return $return;
}

/*
PURPOSE : To add legal completed process plan
INPUT 	: File Id, Added By
OUTPUT 	: Message, success or failure message
BY 		: Sonakshi
*/
function i_add_completed_process_plan_legal($file_id,$added_by)
{
	$process_completed_process_plan_legal_sresult = db_get_completed_process_plan_list($file_id,$added_by,'1','','');
	
	if($process_completed_process_plan_legal_sresult["status"] == DB_NO_RECORD)
	{
		$process_completed_process_plan_legal_iresult = db_add_completed_process_plan($file_id,$added_by);
		
		if($process_completed_process_plan_legal_iresult['status'] == SUCCESS)
		{
			$return["data"]   = "Completed process plan legal added successfully created";
			$return["status"] = SUCCESS;	
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Process plan legal already Completed!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get completed process plan list
INPUT 	: File ID, Added By, Status
OUTPUT 	: Completed Process Plan List or Error Details, success or failure message
BY 		: Sonakshi
*/
function i_get_completed_process_plan_list($file_id,$added_by,$status)
{
	$process_completed_process_plan_legal_sresult = db_get_completed_process_plan_list($file_id,$added_by,$status,'','');
	
	if($process_completed_process_plan_legal_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = $process_completed_process_plan_legal_sresult["data"]; 
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "No Legal process plan Completed. Please contact the system admin"; 
    }
	
	return $return;
}

/*
PURPOSE : To get completed process plan status
INPUT 	: File ID, Status
OUTPUT 	: File ID or Error Details, success or failure message
BY 		: Sonakshi
*/
function i_update_completed_process_plan_list($file_id,$status)
{
	$process_completed_process_plan_legal_uresult = db_update_process_plan_completion($file_id,$status);
	
	if($process_completed_process_plan_legal_uresult['status'] == SUCCESS)
    {
		$return["status"] = SUCCESS;
        $return["data"]   = 'File completion details successfully updated';
    }
    else
    {
	    $return["status"] = FAILURE;
        $return["data"]   = "Internal Error! Please try again later"; 
    }
	
	return $return;
}
?>