

<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: kns_material_master_list.php

CREATED ON	: 05-Oct-2016

CREATED BY	: Lakshmi

PURPOSE     : List of Material for customer withdrawals

*/



/*

TBD: 

*/
$_SESSION['module'] = 'Stock Masters';


/* DEFINES - START */
define('MATERIAL_MASTER_FUNC_ID','152');
/* DEFINES - END */



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');



/* DATA INITIALIZATION - START */

$alert_type = -1;

$alert = "";

/* DATA INITIALIZATION - END */

	

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];

	

	// Get permission settings for this user for this page

	$view_perms_list   = i_get_user_perms($user,'',MATERIAL_MASTER_FUNC_ID,'2','1');

	$edit_perms_list   = i_get_user_perms($user,'',MATERIAL_MASTER_FUNC_ID,'3','1');

	$delete_perms_list = i_get_user_perms($user,'',MATERIAL_MASTER_FUNC_ID,'4','1');

	$add_perms_list    = i_get_user_perms($user,'',MATERIAL_MASTER_FUNC_ID,'1','1');



	// Query String Data

	// Nothing

	

	if(isset($_POST["hd_material_id"]))

	{

		$search_material = $_POST["hd_material_id"];		

	}

	else

	{

		$search_material = "";		

	}

	

	// Temp data

	// Get material modes already added

	$stock_material_search_data = array("material_active"=>'1',"material_id"=>$search_material);

	$material_list = i_get_stock_material_master_list($stock_material_search_data);

	if($material_list['status'] == SUCCESS)

	{

		$material_list_data = $material_list['data'];

	}



	else

	{

		$alert = $alert."Alert: ".$material_list["data"];

	}

}

else

{

	header("location:login.php");

}	

?>



<!DOCTYPE html>

<html lang="en">

  

<head>

    <meta charset="utf-8">

    <title>Material Master List</title>

    

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="apple-mobile-web-app-capable" content="yes">    

    

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">

    <link href="css/font-awesome.css" rel="stylesheet">

    

    <link href="css/style.css" rel="stylesheet">

    <link href="css/style1.css" rel="stylesheet">	



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->



  </head>



<body>



<?php

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');

?>

    



<div class="main">

  <div class="main-inner">

    <div class="container">

      <div class="row">

       

          <div class="span6" style="width:100%;">

          

          <div class="widget widget-table action-table">

            <div class="widget-header"> <i class="icon-th-list"></i>

              <h3>Material Master List</h3>

			  <?php

			  if($add_perms_list['status'] == SUCCESS)

			  {

			  ?>

			  <span style="float:right;padding-right:20px;"><a href="stock_master_add_material.php">Add Material Master</a></span>

			  <?php

			  }

			  ?>

            </div>

			<div class="widget-header" style="height:50px; padding-top:10px;">               

			  <form method="post" id="file_search_form" action="stock_master_material_list.php">

			  <input type="hidden" name="hd_material_id" id="hd_material_id" value="" />			  

			  <span style="padding-right:20px;">										

					<input type="text" name="stxt_material" class="span6" autocomplete="off" id="stxt_material" onkeyup="return get_material_list();" placeholder="Search Material by name or code" />

					<div id="search_results" class="dropdown-content"></div>

			  </span>			

			  

			  <input type="submit" name="material_search_submit" />

			  </form>			  

            </div>

            <!-- /widget-header -->

            <div class="widget-content">

			

              					<table class="table table-bordered" style="table-layout: fixed;">

								<thead>

								  <tr>

									<th style="word-wrap:break-word;">Material Code</th>

                                    <th style="word-wrap:break-word;">Material Name</th>

                                    <th style="word-wrap:break-word;">Material Type</th>

									<th style="word-wrap:break-word;">Unit Of Measure</th>

									<th style="word-wrap:break-word;">HAN/SAC Code</th>

									<th style="word-wrap:break-word;">Indicator</th>
									
									<th style="word-wrap:break-word;">Rate</th>

									<th style="word-wrap:break-word;">Added By</th>

									<th style="word-wrap:break-word;">Added On</th>									

									<th style="word-wrap:break-word;">Last Updated By</th>		

									<th style="word-wrap:break-word;">Last Updated On</th>

                                    <th colspan="2" style="text-align:center;">Actions</th>								

								</tr>

								</thead>

								<tbody>							

								<?php

								if($material_list["status"] == SUCCESS)

								{									

									for($count = 0; $count < count($material_list_data); $count++)

									{																	

									?>

									<tr>

									<td style="word-wrap:break-word;"><?php echo $material_list_data[$count]["stock_material_code"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $material_list_data[$count]["stock_material_name"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $material_list_data[$count]["stock_material_type"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $material_list_data[$count]["stock_unit_name"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $material_list_data[$count]["stock_material_manufacturer_part_number"]; ?></td>

									<td style="word-wrap:break-word;"><?php echo $material_list_data[$count]["stock_indicator_name"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $material_list_data[$count]["stock_material_price"]; ?></td>
									<td style="word-wrap:break-word;"><?php echo $material_list_data[$count]["added_by"]; ?></td>	
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($material_list_data[$count]["stock_material_added_on"])); ?></td>
									<td style="word-wrap:break-word;"><?php echo $material_list_data[$count]["updated_by"]; ?></td>	
									<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($material_list_data[$count]["stock_material_updated_on"])); ?></td>
									<td style="word-wrap:break-word;"><?php

			                        if($edit_perms_list['status'] == SUCCESS)

			                        {

			                          ?><a style="padding-right:10px" href="stock_master_edit_material.php?material_id=<?php echo $material_list_data[$count]["stock_material_id"]; ;?>" >Edit </a>

									<?php } ?></td>

					                <td><?php

									if($delete_perms_list['status'] == SUCCESS)

			                        {

									?>

									<?php if(($material_list_data[$count]["stock_material_active"] == "1")){?><a href="#" onclick="return delete_material

									(<?php echo $material_list_data[$count]["stock_material_id"]; ?>);">Delete</a><?php } ?>

									<?php } ?></td>									

									</tr>

									<?php

                                    }																		

								}

								else

								{

								?>

								<td colspan="10">No material data added yet!</td>

								<?php

								}	

								?>	

								</tbody>

							  </table>

            </div>

            <!-- /widget-content --> 

          </div>

          <!-- /widget --> 

         

          </div>

          <!-- /widget -->

        </div>

        <!-- /span6 --> 

      </div>

      <!-- /row --> 

    </div>

    <!-- /container --> 

  </div>

  <!-- /main-inner --> 

</div>

    

    

    

 

<div class="extra">



	<div class="extra-inner">



		<div class="container">



			<div class="row">

                    

                </div> <!-- /row -->



		</div> <!-- /container -->



	</div> <!-- /extra-inner -->



</div> <!-- /extra -->





    

    

<div class="footer">

	

	<div class="footer-inner">

		

		<div class="container">

			

			<div class="row">

				

    			<div class="span12">

    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.

    			</div> <!-- /span12 -->

    			

    		</div> <!-- /row -->

    		

		</div> <!-- /container -->

		

	</div> <!-- /footer-inner -->

	

</div> <!-- /footer -->

    





<script src="js/jquery-1.7.2.min.js"></script>

	

<script src="js/bootstrap.js"></script>

<script src="js/base.js"></script>

<script>

function delete_material(material_id)

{

	var ok = confirm("Are you sure you want to Delete?")

	{         

		if (ok)

		{



			if (window.XMLHttpRequest)

			{// code for IE7+, Firefox, Chrome, Opera, Safari

				xmlhttp = new XMLHttpRequest();

			}

			else

			{// code for IE6, IE5

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

			}



			xmlhttp.onreadystatechange = function()

			{

				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

				{

					if(xmlhttp.responseText != "SUCCESS")

					{

					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;

					 document.getElementById("span_msg").style.color = "red";

					}

					else					

					{

					 window.location = "stock_master_material_list.php";

					}

				}

			}



			xmlhttp.open("POST", "ajax/stock_master_delete_material.php");  

			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

			xmlhttp.send("material_id=" + material_id + "&action=0");

		}

	}	

}



function get_material_list()

{ 

	var searchstring = document.getElementById('stxt_material').value;

	

	if(searchstring.length >= 3)

	{

		if (window.XMLHttpRequest)

		{// code for IE7+, Firefox, Chrome, Opera, Safari

			xmlhttp = new XMLHttpRequest();

		}

		else

		{// code for IE6, IE5

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

		}



		xmlhttp.onreadystatechange = function()

		{				

			if (xmlhttp.readyState == 4 && xmlhttp.status == 200)

			{		

				if(xmlhttp.responseText != 'FAILURE')

				{

					document.getElementById('search_results').style.display = 'block';

					document.getElementById('search_results').innerHTML     = xmlhttp.responseText;

				}

			}

		}



		xmlhttp.open("POST", "ajax/get_material.php");   // file name where delete code is written

		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

		xmlhttp.send("search=" + searchstring);				

	}

	else	

	{

		document.getElementById('search_results').style.display = 'none';

	}

}



function select_material(material_id,search_material)

{

	document.getElementById('hd_material_id').value 	= material_id;

	document.getElementById('stxt_material').value = search_material;

	

	document.getElementById('search_results').style.display = 'none';

}





</script>

<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>




  </body>



</html>