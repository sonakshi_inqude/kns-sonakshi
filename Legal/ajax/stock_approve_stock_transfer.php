<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

TBD:

*/



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock'.DIRECTORY_SEPARATOR.'stock_functions.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	// Update attendance details

	$transfer_id    = $_POST["transfer_id"];

	$status         = $_POST["action"];

	$approved_on    = date("Y-m-d H:i:s");

	$approved_by    = $user;

	

	$stock_transfer_update_data = array("status"=>'Approved',"approved_on"=>$approved_on,"approved_by"=>$approved_by);

	$approve_files_result = i_update_stock_transfer($transfer_id,$stock_transfer_update_data);

	

	if($approve_files_result["status"] == FAILURE)

	{

		echo $approve_files_result["data"];

	}

	else

	{

		echo "SUCCESS";

	}

}

else

{

	header("location:login.php");

}

?>