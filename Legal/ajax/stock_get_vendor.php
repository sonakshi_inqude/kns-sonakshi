<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'stock_masters'.DIRECTORY_SEPARATOR.'stock_vendor_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$search  = $_POST["search"];
	$stock_vendor_master_search_data = array("vendor_name"=>$search,"vendor_name_check"=>'2');
	$vendor_list_result =  i_get_stock_vendor_master_list($stock_vendor_master_search_data);
	if($vendor_list_result["status"] == SUCCESS)
	{		
		$vendor_list = '';

		for($count = 0; $count < count($vendor_list_result['data']); $count++)
		{
			$vendor_list = $vendor_list.'<a style="display:block; width:100%; border-bottom:1px solid #efefef; text-decoration:none; cursor:pointer; padding:5px;" onclick="return select_vendor(\''.$vendor_list_result['data'][$count]['stock_vendor_id'].'\',\''.$vendor_list_result['data'][$count]['stock_vendor_name'].'\');">'.$vendor_list_result['data'][$count]['stock_vendor_name'].'</a>';
		}
	}
	else
	{
		$vendor_list = 'FAILURE';
	}
	
	echo $vendor_list;
}
else
{
	header("location:login.php");
}
?>