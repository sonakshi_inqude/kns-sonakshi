<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: task_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Task Plans for a particular process ID
*/

/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/$_SESSION['module'] = 'General Task';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Temp data
	$alert = "";
	
	// Initialize
	$search_task_type  = "";
	$search_status     = "";
	$search_user       = "";
	$search_department = "";
	$search_assigner   = "";
	
	// Form Data
	if(isset($_POST["task_search_submit"]))
	{
		$search_task_type  = $_POST["search_task_type"];
		$search_status     = $_POST["search_status"];
		$search_user       = $_POST["search_user"];
		$search_assigner   = $_POST["search_assigner"];
		$search_department = $_POST["search_department"];
	}

	if($search_user != "")
	{
		$task_user = $search_user;
	}
	else	
	{
		$task_user = $user;
	}

	$general_task_plan_list = i_get_gen_task_plan_list('',$search_task_type,$task_user,$search_department,'','','',$search_assigner,$search_status);
	if($general_task_plan_list["status"] == SUCCESS)
	{
		$general_task_plan_list_data = $general_task_plan_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$general_task_plan_list["data"];
	}	
	
	// Get task type list
	$gen_task_type_list = i_get_gen_task_type_list('','1');
	if($gen_task_type_list["status"] == SUCCESS)
	{
		$gen_task_type_list_data = $gen_task_type_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$gen_task_type_list["data"];
		$alert_type = 0; // Failure
	}

	// Get list of users
	$user_list = i_get_user_list('','','','','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get department list
	$department_list = i_get_department_list('','1');
	if($department_list["status"] == SUCCESS)
	{
		$department_list_data = $department_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$department_list["data"];
		$alert_type = 0; // Failure
	}	
	
	// Get user list
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>General Tasks - Consolidated Report</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>General Tasks - Consolidated Report&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Number of Tasks: <?php if($general_task_plan_list["status"] == SUCCESS)
			  {
				echo count($general_task_plan_list_data);
			  }
			  else
			  {
				echo '0';
			  }?></h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
			<div class="widget-header" style="height:84px; padding-top:10px;">               
			  <form method="post" id="task_search_form" action="general_task_consolidated_report.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_task_type">
			  <option value="">- - Select Task Type - -</option>
			  <?php
			  for($count = 0; $count < count($gen_task_type_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $gen_task_type_list_data[$count]["general_task_type_id"]; ?>" <?php if($search_task_type == $gen_task_type_list_data[$count]["general_task_type_id"]) { ?> selected="selected" <?php } ?>><?php echo $gen_task_type_list_data[$count]["general_task_type_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_status">
			  <option value="">- - Select Status - -</option>
			  <option value="0" <?php if($search_status == "0") { ?> selected="selected" <?php } ?>>NOT STARTED</option>			  
			  <option value="1" <?php if($search_status == "1") { ?> selected="selected" <?php } ?>>IN PROGRESS</option>
			  <option value="3" <?php if($search_status == "3") { ?> selected="selected" <?php } ?>>COMPLETED</option>			  			  
			  </select>
			  </span>
			  <?php if($role == 1)
			  {?>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_user">
			  <option value="">- - Select Assigned To - -</option>
			  <?php
			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($task_user == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <?php
			  }
			  else
			  {
			  ?>
			  <input type="hidden" name="search_user" value="<?php echo $user; ?>" />
			  <?php
			  }
			  ?>
			  <?php if($role == 1)
			  {?>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_assigner">
			  <option value="">- - Select Assigned By - -</option>
			  <?php
			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_assigner == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <?php
			  }
			  else
			  {
			  ?>
			  <input type="hidden" name="search_assigner" value="" />
			  <?php
			  }
			  ?>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_department">
			  <option value="">- - Select Department - -</option>
			  <?php
			  for($count = 0; $count < count($department_list_data); $count++)
			  {
			  ?>
			  <option value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>" <?php if($search_department == $department_list_data[$count]["general_task_department_id"]) { ?> selected="selected" <?php } ?>><?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="submit" name="task_search_submit" />
			  </span>
			  </form>			  
            </div>
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>						
					<th>Task Details</th>
					<th>Department</th>
					<th>Planned End Date</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Days taken</th>
					<th>Variance</th>
					<th>Status</th>					
					<th>Assigned To</th>
					<th>Remarks</th>
					<th>Assigned By</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				 <?php
				if($general_task_plan_list["status"] == SUCCESS)
				{
					$sl_count = 0;
					for($count = 0; $count < count($general_task_plan_list_data); $count++)
					{						
						$sl_count++;
						if(($general_task_plan_list_data[$count]["general_task_end_date"] != "0000-00-00") && ($general_task_plan_list_data[$count]["general_task_end_date"] != "") && ($general_task_plan_list_data[$count]["general_task_end_date"] != "1969-12-31") && ($general_task_plan_list_data[$count]["general_task_planned_date"] != "1970-01-01") && ($role == 3))
						{
							// Do nothing
						}					
						else
						{
						if(get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"Y-m-d") == "0000-00-00")
						{
							$end_date = date("Y-m-d");
						}
						else
						{
							$end_date = $general_task_plan_list_data[$count]["general_task_end_date"];
						}
						$start_date = $general_task_plan_list_data[$count]["general_task_planned_date"];
						
						$variance = get_date_diff($start_date,$end_date);
						if($variance["status"] == 1)
						{
							if((get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"Y-m-d") == "1969-12-31"))
							{
								$font_class = "#FF0000";								
							}
							else						
							{
								$font_class = "#FFA500";								
							}
						}
						else
						{
							if((get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"Y-m-d") == "1969-12-31"))
							{
								$font_class = "#0000FF";								
							}
							else
							{	
								$font_class = "#00FF00";								
							}
						}
						
						// Get remarks for this task
						$remarks_list = i_get_remarks($general_task_plan_list_data[$count]["general_task_id"]);
						if($remarks_list["status"] == SUCCESS)
						{
							$latest_remarks = $remarks_list["data"][0]["general_task_remarks"];
						}
						else
						{
							$latest_remarks = 'NO REMARKS';
						}
					?>					
					<tr style="color:<?php echo $font_class; ?>;">
						<td style="word-wrap:break-word;"><?php echo $sl_count; ?></td>						
						<td style="word-wrap:break-word;"><?php echo $general_task_plan_list_data[$count]["general_task_type_name"]; ?><br /><br />
						<?php echo $general_task_plan_list_data[$count]["general_task_details"]; ?></td>	
						<td style="word-wrap:break-word;"><?php echo $general_task_plan_list_data[$count]["general_task_department_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($general_task_plan_list_data[$count]["general_task_planned_date"],"d-M-Y"); ?></td>
						
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($general_task_plan_list_data[$count]["general_task_start_date"],"d-M-Y"); ?></td>
						
						<td style="word-wrap:break-word;"><?php echo get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"d-M-Y"); ?></td>
						
						<td style="word-wrap:break-word;"><?php $day_var = get_date_diff($general_task_plan_list_data[$count]["general_task_start_date"],$end_date); echo $day_var["data"];?></td>
						
						<td style="word-wrap:break-word;"><?php echo $variance["data"];?></td>
						
						<td style="word-wrap:break-word;">
						<?php
						switch($general_task_plan_list_data[$count]["general_task_completion_status"])
						{
							case '0':
							echo 'NOT STARTED';
							break;
							
							case '1':
							echo 'IN PROGRESS';
							break;
							
							case '2':
							echo 'INVALID START DATE';
							break;
							
							case '3':
							echo 'COMPLETED';
							break;						
						}												
						?>
						</td>												
						
						<td style="word-wrap:break-word;"><?php echo $general_task_plan_list_data[$count]["assignee"]; ?></td>
						<td style="word-wrap:break-word;"><a href="#" onClick="alert('<?php echo $latest_remarks; ?>')"><?php echo substr($latest_remarks,0,35); ?></a></td>
						<td style="word-wrap:break-word;"><?php echo $general_task_plan_list_data[$count]["assigner"]; ?>
						<td style="word-wrap:break-word;"><a target="_blank" href="view_gen_task_remarks.php?task=<?php echo $general_task_plan_list_data[$count]["general_task_id"]; ?>">All Remarks</a></td><?php /* $assigned_by = i_get_user_list($general_task_plan_list_data[$count]["general_task_added_by"],'','','');echo $assigned_by["data"][0]["user_name"]; */?></td>
					</tr>
					<?php 
						}
					}
				}
				else
				{
				?>
				<td colspan="9">No tasks added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  <br />
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>