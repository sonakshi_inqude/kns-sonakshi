<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 08-Nov-2016
// LAST UPDATED BY: Lakshmi
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Query String Data
	if(isset($_REQUEST['project_plan_process_id']))
	{
		$process_id = $_REQUEST['project_plan_process_id'];
	}
	else
	{
		$process_id = '';
	}
	
	if(isset($_REQUEST['plan_id']))
	{
		$plan_id = $_REQUEST['plan_id'];
	}
	else
	{
		$plan_id = '';
	}
	
	// Capture the form data
	if(isset($_POST["add_delay_reason_submit"]))
	{
		
		$process_id = $_POST["hd_process_id"];
		$plan_id	= $_POST["hd_plan_id"];
		$start_date = $_POST["start_date"];
		$end_date 	= $_POST["end_date"];
		$name	    = $_POST["ddl_reason_id"];
		$remarks 	= $_POST["txt_remarks"];
		
		// Check for mandatory fields
		if(($name != ""))
		{
			$delay_reason_iresult = i_add_project_process_delay_reason($start_date,$end_date,$process_id,$name,$remarks,$user);
			
			if($delay_reason_iresult["status"] == SUCCESS)				
			{	
				header("location:project_plan_process_list.php?plan_id=$plan_id");
				$alert_type = 1;
			}
			
			$alert = $delay_reason_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}

	// Get Delay Reason Master
	$delay_reason_master_search_data = array();
	$project_reason_master_list = i_get_delay_reason_master($delay_reason_master_search_data);
	if($project_reason_master_list["status"] == SUCCESS)
	{
		$project_reason_master_list_data = $project_reason_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_reason_master_list["data"];
	}
	// Get Process Delay Reason 
	$process_delay_reason_search_data = array("process_id"=>$process_id);
	$project_process_reason_list = i_get_project_process_delay_reason($process_delay_reason_search_data);
	if($project_process_reason_list["status"] == SUCCESS)
	{
		$project_process_reason_list_data = $project_process_reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_process_reason_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Master - Add Delay Reason</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>
    
<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Project Master - Add Delay Reason Master</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Project Master Add Delay Reason Master</a>
						  </li>	
						</ul>
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <?php echo $alert; ?>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="project_master_add_process_form" class="form-horizontal" method="post" action="project_update_process_delay_reason.php" enctype="multipart/form-data">
								<input type="hidden" name="hd_process_id" value="<?php echo $process_id; ?>" />		
								<input type="hidden" name="hd_plan_id" value="<?php echo $plan_id; ?>" />		
									<fieldset>										
																
											<div class="control-group">											
											<label class="control-label" for="ddl_reason_id">Delay Reason*</label>
											<div class="controls">
												<select name="ddl_reason_id" required >
												<option value="">- - Select Delay - -</option>
												<?php
												for($count = 0; $count < count($project_reason_master_list_data); $count++)
												{
												?>
												<option value="<?php echo $project_reason_master_list_data[$count]["delay_reason_master_id"]; ?>"><?php echo $project_reason_master_list_data[$count]["delay_reason_master_name"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="start_date">Start Date</label>
											<div class="controls">
												<input type="date" class="span6" name="start_date">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="end_date">End Date</label>
											<div class="controls">
												<input type="date" class="span6" name="end_date">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
								
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks</label>
											<div class="controls">
												<input type="text" class="span6" name="txt_remarks" placeholder="Remarks">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_delay_reason_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>
								
							</div> 
							<div class="widget-content">
							 <table class="table table-bordered">
						  <thead>
						  <tr>
							<th>SL No</th>
							<th>Delay Reason</th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Remarks</th>
								
						</tr>
						</thead>
						<tbody>							
						<?php
						if($project_process_reason_list["status"] == SUCCESS)
						{
							$sl_no = 0;
							for($count = 0; $count < count($project_process_reason_list_data); $count++)
							{
								$sl_no++;
								
							?>
							<tr>
							<td><?php echo $sl_no; ?></td>
							<td><?php echo $project_process_reason_list_data[$count]["delay_reason_master_name"]; ?></td>
							<td><?php echo $project_process_reason_list_data[$count]["project_process_delay_reason_start_date"]; ?></td>
							<td><?php echo $project_process_reason_list_data[$count]["project_process_delay_reason_end_date"]; ?></td>
							<td><?php echo $project_process_reason_list_data[$count]["project_process_delay_reason_remarks"]; ?></td>
							</tr>
							<?php
							}
							
						}
						else
						{
						?>
						<td colspan="6">No Project Master condition added yet!</td>
						
						<?php
						}
						 ?>	

						</tbody>
					  </table>
					</div>
							
							
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
