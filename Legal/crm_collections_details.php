<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_approved_booking_list.php
CREATED ON	: 04-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : Approved Booking List
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["project"]))
	{
		$project = $_GET["project"];
	}
	else
	{
		$project = "";
	}
	
	if(isset($_GET["reason"]))
	{
		$reason = $_GET["reason"];
	}
	else
	{
		$reason = "";
	}
	
	if(isset($_GET["status"]))
	{
		$status = $_GET["status"];
	}
	else
	{
		$status = "";
	}

	// Temp data
	$alert      = "";
	$alert_type = -1;
	if($reason == '0')
	{
		$delayed_payment_list = i_get_pending_nondelayed_payment_list($project,$status,'');
	}
	else if($reason != '')
	{
		$delayed_payment_list = i_get_delayed_payment_list($project,$status,$reason,'');
	}
	else
	{
		$reason_list = i_get_delayed_payment_list($project,$status,$reason,'');
		
		$collection_list      = i_get_pending_nondelayed_payment_list($project,$status,'');
		if($reason_list['status'] == SUCCESS)
		{
			if($collection_list['status'] == SUCCESS)
			{
				$delayed_payment_list['data'] = array_merge($collection_list['data'],$reason_list['data']);
			}
			else
			{
				$delayed_payment_list['data'] = $reason_list['data'];
			}
			
			$delayed_payment_list['status'] = SUCCESS;
		}
		else
		{
			$delayed_payment_list = $collection_list;
		}
	}	
		
	if($delayed_payment_list["status"] == SUCCESS)
	{
		$delayed_payment_list_data = $delayed_payment_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Overall Report</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:80px;">
              <h3>Sold Site Report (Total Sites Sold = <span id="total_count_section"><i>Calculating</i></span>)&nbsp;&nbsp;&nbsp;(Total Area Sold = <span id="total_area_section"><i>Calculating</i></span> sq. ft)&nbsp;&nbsp;&nbsp;<br />Total Amount = <span id="total_amount"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Received Amount = <span id="total_received_amount"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Pending Amount = <span id="total_pending_amount"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;<br />Others - Total Amount = <span id="total_other_amount"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Others - Received Amount = <span id="total_other_received_amount"><i>Calculating</i></span>&nbsp;&nbsp;&nbsp;Others - Pending Amount = <span id="total_other_pending_amount"><i>Calculating</i></span></h3>
            </div>			
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>
					<th>Enquiry No</th>
					<th>Project</th>
					<th>Site No.</th>
					<th>Release Status</th>					
					<th>Client Name</th>
					<th>Booking Date</th>
					<th>Agmnt Date</th>			
					<th>Regn Date</th>
					<th>Khata Transfer Date</th>					
					<th>Booked By</th>	
					<th>Source</th>	
					<th>Amount</th>
					<th>Received Amount</th>
					<th>Pending Amount</th>
					<th>Others - Amount</th>
					<th>Others - Received Amount</th>
					<th>Others - Pending Amount</th>
					<th>Loan</th>					
				</tr>
				</thead>
				<tbody>							
				<?php			
				$total_area                = 0;
				$sum_amount                = 0;
				$sum_received_amount       = 0;
				$sum_pending_amount        = 0;
				$sum_other_amount          = 0;
				$sum_other_received_amount = 0;
				$sum_other_pending_amount  = 0;
				if($delayed_payment_list["status"] == SUCCESS)
				{										
					$sl_no = 0;					
					$cancelled_count = 0;
					for($count = 0; $count < count($delayed_payment_list_data); $count++)
					{		
						// Get customer profile details, if entered
						$cust_details = i_get_cust_profile_list('',$delayed_payment_list_data[$count]["crm_booking_id"],'','','','','','','','');
						if($cust_details["status"] == SUCCESS)
						{
							if($cust_details["data"][0]["crm_customer_funding_type"] == '2')
							{
								$bank_loan = $cust_details["data"][0]["crm_bank_name"];
							}
							else if($cust_details["data"][0]["crm_customer_funding_type"] == '3')
							{
								$bank_loan = "Waiting for info";
							}
							else
							{
								$bank_loan = "Self Funding";
							}
						}
						else
						{
							$bank_loan = "";
						}
					
						// Current Status Data										
						if($delayed_payment_list_data[$count]["crm_booking_date"] != "0000-00-00")
						{
							$booking_date = date("d-M-Y",strtotime($delayed_payment_list_data[$count]["crm_booking_date"])); 
						}
						else
						{
							$booking_date = "NA. Approved Date: ".date("d-M-Y",strtotime($delayed_payment_list_data[$count]["crm_booking_approved_on"])); 
						}												
												
						$agreement_list = i_get_agreement_list('',$delayed_payment_list_data[$count]["crm_booking_id"],'','','','','','','','','');
						if($agreement_list["status"] == SUCCESS)
						{
							$agreement_date = date("d-M-Y",strtotime($agreement_list["data"][0]["crm_agreement_date"])); 
						}
						else
						{
							$agreement_date = "";
						}											
												
						$registration_list = i_get_registration_list('',$delayed_payment_list_data[$count]["crm_booking_id"],'','','','','','','','','');
						if($registration_list["status"] == SUCCESS)
						{
							$registration_date = date("d-M-Y",strtotime($registration_list["data"][0]["crm_registration_date"])); 
						}
						else
						{
							$registration_date = "";
						}						
												
						$kt_list = i_get_katha_transfer_list('',$delayed_payment_list_data[$count]["crm_booking_id"],'','','','','','','','','','');
						if($kt_list["status"] == SUCCESS)
						{
							$kt_date = date("d-M-Y",strtotime($kt_list["data"][0]["crm_katha_transfer_date"])); 
						}
						else
						{
							$kt_date = "";
						}												
						
						// Payment Data
						$total_payment_done = 0;
						$payment_details = i_get_payment('',$delayed_payment_list_data[$count]["crm_booking_id"],'','','');
						if($payment_details["status"] == SUCCESS)
						{
							for($pay_count = 0; $pay_count < count($payment_details["data"]); $pay_count++)
							{
								$total_payment_done = $total_payment_done + $payment_details["data"][$pay_count]["crm_payment_amount"];
							}
						}
						else						
						{
							$total_payment_done = 0;
						}
						
						// Other Payment - To be paid data
						$other_total_payment = 0;
						$other_payment_details = i_get_payment_terms($delayed_payment_list_data[$count]["crm_booking_id"]);
						if($other_payment_details["status"] == SUCCESS)
						{
							$other_total_payment = $other_payment_details["data"][0]["crm_pt_layout_maintenance_charges"] + $other_payment_details["data"][0]["crm_pt_water_charges"] + $other_payment_details["data"][0]["crm_pt_club_house_charges"] + $other_payment_details["data"][0]["crm_pt_documentation_charges"] + $other_payment_details["data"][0]["crm_pt_other_charges"];
						}
						else						
						{
							$other_total_payment = 0;
						}
						
						// Other Payment Data						
						$other_total_payment_done_data = i_get_other_payment_total($delayed_payment_list_data[$count]["crm_booking_id"],'');						
						if(($other_total_payment_done_data["data"] == NULL) || ($other_total_payment_done_data["data"] == "NULL"))
						{
							$other_total_payment_done = 0;
						}
						else
						{
							$other_total_payment_done = $other_total_payment_done_data["data"];
						}	
					
						if($delayed_payment_list_data[$count]["crm_booking_status"] == "1")
						{							
							$total_area = $total_area + $delayed_payment_list_data[$count]["crm_site_area"];
							
							// Total payment to be done
							if($delayed_payment_list_data[$count]["crm_booking_consideration_area"] != "0")
							{
								$consideration_area = $delayed_payment_list_data[$count]["crm_booking_consideration_area"];
							}
							else
							{
								$consideration_area = $delayed_payment_list_data[$count]["crm_site_area"];
							}
							$total_payment_to_be_done = ($consideration_area * $delayed_payment_list_data[$count]["crm_booking_rate_per_sq_ft"]);
							
							if(($total_payment_to_be_done - $total_payment_done) > 0)
							{
								$sl_no++;
								?>
								<tr>
								<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
								<td style="word-wrap:break-word;"><?php echo $delayed_payment_list_data[$count]["enquiry_number"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo $delayed_payment_list_data[$count]["project_name"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo $delayed_payment_list_data[$count]["crm_site_no"]; ?> (<?php echo $delayed_payment_list_data[$count]["crm_site_area"].' sq. ft'; ?>)</td>
								<td style="word-wrap:break-word;"><?php echo $delayed_payment_list_data[$count]["release_status"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo $delayed_payment_list_data[$count]["name"]; ?><br />
								<?php echo $delayed_payment_list_data[$count]["cell"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo $booking_date; ?></td>
								<td style="word-wrap:break-word;"><?php echo $agreement_date; ?></td>
								<td style="word-wrap:break-word;"><?php echo $registration_date; ?></td>
								<td style="word-wrap:break-word;"><?php echo $kt_date; ?></td>
								<td style="word-wrap:break-word;"><?php echo $delayed_payment_list_data[$count]["user_name"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo $delayed_payment_list_data[$count]["enquiry_source_master_name"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo $total_payment_to_be_done; 
								$sum_amount = $sum_amount + $total_payment_to_be_done; ?></td>
								<td style="word-wrap:break-word;"><?php echo $total_payment_done; 
								$sum_received_amount = $sum_received_amount + $total_payment_done; ?></td>					
								<td style="word-wrap:break-word;"><?php echo ($total_payment_to_be_done - $total_payment_done); 
								$sum_pending_amount = $sum_pending_amount + ($total_payment_to_be_done - $total_payment_done);?></td>							
								<td style="word-wrap:break-word;"><?php echo $other_total_payment; 
								$sum_other_amount = $sum_other_amount + $other_total_payment; ?></td>
								<td style="word-wrap:break-word;"><?php echo $other_total_payment_done; 
								$sum_other_received_amount = $sum_other_received_amount + $other_total_payment_done; ?></td>					
								<td style="word-wrap:break-word;"><?php echo ($other_total_payment - $other_total_payment_done); 
								$sum_other_pending_amount = $sum_other_pending_amount + ($other_total_payment - $other_total_payment_done); ?></td>
								<td style="word-wrap:break-word;"><?php echo $bank_loan; ?></td>
								</tr>
								<?php							
							}
						}
						else
						{
							$cancelled_count++;
						}
					}
				}
				else
				{
				?>
				<td colspan="14">No site booked yet!</td>
				<?php
				}
				?>	
				<script>
				document.getElementById("total_count_section").innerHTML = <?php echo $sl_no; ?>;
				document.getElementById("total_area_section").innerHTML = <?php echo $total_area; ?>;
				document.getElementById("total_amount").innerHTML = <?php echo $sum_amount; ?>;
				document.getElementById("total_received_amount").innerHTML = <?php echo $sum_received_amount; ?>;
				document.getElementById("total_pending_amount").innerHTML = <?php echo $sum_pending_amount; ?>;
				document.getElementById("total_other_amount").innerHTML = <?php echo $sum_other_amount; ?>;
				document.getElementById("total_other_received_amount").innerHTML = <?php echo $sum_other_received_amount; ?>;
				document.getElementById("total_other_pending_amount").innerHTML = <?php echo $sum_other_pending_amount; ?>;
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>