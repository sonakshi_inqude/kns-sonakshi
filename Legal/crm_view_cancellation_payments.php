<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 19th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_config.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["booking"]))
	{
		$booking_id = $_GET["booking"];
	}
	else
	{
		$booking_id = "";
	}		
	/* QUERY STRING - END */
	
	// Get the payment details
	$payment_details = i_get_cancel_payment('',$booking_id,'','','','','','');
	if($payment_details["status"] == SUCCESS)
	{
		$payment_details_list = $payment_details["data"];
	}
	else
	{
		$alert = $payment_details["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>View Cancellation Payments</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
						<?php if($payment_details["status"] == SUCCESS)
						{?>
	      				<h3>Project: <?php echo $payment_details_list[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Site No: <?php echo $payment_details_list[0]["crm_site_no"]; ?></h3>
						<?php
						}
						else
						{
							?>
							<h3><a href="crm_cancellation_payment.php?booking=<?php echo $booking_id; ?>">Add Cancellation Payment</a></h3>
							<?php
						}?>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">						
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">							
								<div class="tab-pane active" id="formcontrols">
								<?php 
								if($payment_details["status"] == SUCCESS)
								{
									for($count = 0; $count < count($payment_details_list); $count++)
									{
										$payment_track_sresult = i_get_cancel_payment_track('',$payment_details_list[0]["crm_cancellation_payment_id"],'','');
										?>										
										<div class="widget-header">
										<i class="icon-user"></i>
										<h3>Amount: <?php echo $payment_details_list[$count]["crm_cancelled_payment_amount"]; ?>&nbsp;&nbsp;&nbsp;Mode: <?php echo $payment_details_list[$count]["payment_mode_name"]; ?>&nbsp;&nbsp;&nbsp;Date: <?php echo date("d-M-y",strtotime($payment_details_list[$count]["crm_cancelled_payment_instrument_date"])); ?>&nbsp;&nbsp;&nbsp;Bank: <?php echo $payment_details_list[$count]["crm_cancelled_payment_bank"]; ?>&nbsp;&nbsp;&nbsp;Branch: <?php echo $payment_details_list[$count]["crm_cancelled_payment_branch"]; ?>&nbsp;&nbsp;&nbsp;<a href="crm_add_cancellation_payment_track.php?payment=<?php echo $payment_details_list[$count]["crm_cancellation_payment_id"]; ?>">Track Payment</a></h3>
										</div> <!-- /widget-header -->
										<br />									
										<fieldset>	
											<table class="table table-bordered" style="table-layout: fixed;">
											  <thead>			  
											  <tr>
											  <th>Status</th>
											  <th>Remarks</th>
											  <th>Date</th>
											  <th>Updated By</th>
											  </tr>
											  </thead>
											  <tbody>
											  <?php 
											  if($payment_track_sresult["status"] == SUCCESS)
											  {
												  for($pt_count = 0; $pt_count < count($payment_track_sresult["data"]); $pt_count++)
												  {?>
												  <tr>
												  <td><?php switch($payment_track_sresult["data"][$pt_count]["crm_cancel_payment_track_status"]) 
												  {
												  case PAYMENT_MADE:
												  $display_status = "Payment Made";
												  break;
												  
												  case PAYMENT_DEPOSITED:
												  $display_status = "Payment Deposited";
												  break;
												  
												  case PAYMENT_REALIZED:
												  $display_status = "Payment Realized";
												  break;
												  
												  case PAYMENT_FAILURE:
												  $display_status = "Payment Failed";
												  break;
												  
												  default:
												  $display_status = "Payment Made";
												  break;
												  }
												  echo $display_status;?></td>
												  <td><?php echo $payment_track_sresult["data"][$pt_count]["crm_cancel_payment_track_remarks"]; ?></td>
												  <td><?php echo date("d-M-Y",strtotime($payment_track_sresult["data"][$pt_count]["crm_cancel_payment_track_added_on"])); ?></td>
												  <td><?php echo $payment_track_sresult["data"][$pt_count]["user_name"]; ?></td>
												  </tr>
												  <?php
												  }
											  }
											  else
											  {
											  ?>
											  <tr>
											  <td colspan="4">&nbsp;</td>											  
											  </tr>
											  <?php
											  }?>
											  </tbody>
											</table>										 																	
										</fieldset>
									<?php
									}
								}
								else
								{
									echo "No payments yet!";
								}?>
								</div>																
								
							</div>
						  						  
						</div>
	
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
