<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: task_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Task Plans for a particular process ID
*/

/*
TBD: 
1. Date display and calculation
2. Session management
3. Linking Tasks
*/$_SESSION['module'] = 'General Task';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET["task_type"]))
	{
		$task_type = $_GET["task_type"];
	}
	else
	{
		$task_type = "";
	}
	
	if(isset($_GET["task_department"]))
	{
		$task_department = $_GET["task_department"];
	}
	else
	{
		$task_department = "";
	}

	// Temp data
	$alert = "";

	if($role == 1)
	{
		$assigned_to = "";
	}
	else
	{
		$assigned_to = $user;
	}

	if(isset($_POST["gen_task_update_button"]))
	{	
		$record_count    = $_POST["count"];
		
		for($count = 0; $count < $record_count; $count++)
		{
			$task                = $_POST["task_data"][$count]['task_id'];			
			$planned_end_date    = $_POST["task_data"][$count]['planned_date'];
			$start_date          = $_POST["task_data"][$count]['start_date'];
			$actual_end_date     = $_POST["task_data"][$count]['end_date'];
			
			$start_status = 0;
			$end_status   = 0;	
			
			if($actual_end_date <= date("Y-m-d"))
			{
				// Determine status
				if(($start_date != '') && ($start_date != '0000-00-00'))
				{
					$start_status = 1;
				}
				if(($actual_end_date != '') && ($actual_end_date != '0000-00-00'))
				{
					$end_status = 1;
				}
				
				if(($start_status == 1) && ($end_status == 1))
				{
					$status = '3'; // 
				}
				if(($start_status == 1) && ($end_status == 0))
				{
					$status = '1'; // 
				}
				if(($start_status == 0) && ($end_status == 1))
				{
					$status = '2'; // 
				}
				if(($start_status == 0) && ($end_status == 0))
				{
					$status = '0'; // 
				}
				
				$gen_task_plan_update_result = i_update_gen_task_plan($task,$planned_end_date,$start_date,$actual_end_date,$status);		
				
				if($gen_task_plan_update_result["status"] == SUCCESS)
				{
					$alert = $gen_task_plan_update_result["data"];
					$disp_class = "green";
				}
				else
				{
					$alert = $gen_task_plan_update_result["data"];
					$disp_class = "red";
				}
			}
			else
			{
				$alert = "Actual Date cannot be later than today!";
				$disp_class = "red";
			}
		}
	}

	// Get task list
	if(isset($_POST["task_search_submit"]))
	{
		$task_user = $_POST["ddl_search_assigned_to"];
	}
	else
	{
		$task_user = $user;
	}
	
	$general_task_plan_list = i_get_gen_task_plan_list('',$task_type,$task_user,$task_department,'','','','','3');
	if($general_task_plan_list["status"] == SUCCESS)
	{
		$general_task_plan_list_data = $general_task_plan_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$general_task_plan_list["data"];
	}

	// User List
	$user_list = i_get_user_list('','','','');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Completed Task List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?> 

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Completed Task List</h3>
            </div>
            <!-- /widget-header -->
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="task_search" action="general_completed_task_list.php">			  
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_assigned_to">
			  <option value="<?php echo $user; ?>"><?php echo $loggedin_name; ?></option>
			  <?php
				if($user_list["status"] == SUCCESS)
				{
					for($count = 0; $count < count($user_list_data); $count++)
					{
						if($user_list_data[$count]["user_manager"] == $user)
						{
						?>
						<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
						if($task_user == $user_list_data[$count]["user_id"])
						{
						?>												
						selected="selected"
						<?php
						}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
						<?php
						}
					}
				}
      		  ?>
			  </select>
			  </span>			
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="task_search_submit" />
			  </span>
			  </form>			  
            </div>
            <div class="widget-content">
			<form action="general_completed_task_list.php" method="post" id="task_update_form">
			<input type="hidden" name="count" value="<?php echo count($general_task_plan_list_data); ?>" />
			<span style="padding-left:50px;">
			Completed Tasks
			</span>
			
              <table class="table table-bordered">
                <thead>
                  <tr>
					<th>SL No</th>						
					<th>Task Type</th>
					<th>Planned End Date</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Days</th>
					<th>Status</th>					
					<th>Assigned To</th>								
					<th>Assigned Date</th>								
				</tr>
				</thead>
				<tbody>
				 <?php
				if($general_task_plan_list["status"] == SUCCESS)
				{
					$sl_count = 0;
					for($count = 0; $count < count($general_task_plan_list_data); $count++)
					{						
						$sl_count++;
						if(($general_task_plan_list_data[$count]["general_task_end_date"] != "0000-00-00") && ($general_task_plan_list_data[$count]["general_task_end_date"] != "") && ($general_task_plan_list_data[$count]["general_task_end_date"] != "1969-12-31") && ($general_task_plan_list_data[$count]["general_task_end_date"] != "1970-01-01"))
						{
							// Do nothing
						}					
						else
						{
						if(get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"Y-m-d") == "0000-00-00")
						{
							$end_date = date("Y-m-d");
						}
						else
						{
							$end_date = $general_task_plan_list_data[$count]["general_task_end_date"];
						}
						$start_date = $general_task_plan_list_data[$count]["general_task_planned_date"];
						
						$variance = get_date_diff($start_date,$end_date);
						if($variance["status"] == 1)
						{
							if((get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"Y-m-d") == "1969-12-31"))
							{
								$css_class = "#FF0000";								
							}
							else						
							{
								$css_class = "#0000FF";								
							}
						}
						else
						{
							if((get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"Y-m-d") == "0000-00-00") || (get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"Y-m-d") == "1969-12-31"))
							{
								$css_class = "#000000";								
							}
							else
							{	
								$css_class = "#32CD32";								
							}
						}
					?>
					<input type="hidden" name="task_data[<?php echo $count; ?>][task_id]" value="<?php echo $general_task_plan_list_data[$count]["general_task_id"]; ?>" />
					<tr style="color:<?php echo $css_class; ?>">
					<td><?php echo $sl_count; ?></td>
					<td colspan="8">TASK: <?php echo $general_task_plan_list_data[$count]["general_task_details"]; ?></td>
					</tr>
					<tr style="color:<?php echo $css_class; ?>">
						<td style="word-wrap:break-word;"><?php echo $sl_count; ?></td>						
						<td style="word-wrap:break-word;"><?php echo $general_task_plan_list_data[$count]["general_task_type_name"]; ?></td>						
						<td style="word-wrap:break-word;"><input type="date" name="task_data[<?php echo $count; ?>][planned_date]" value="<?php echo get_formatted_date($general_task_plan_list_data[$count]["general_task_planned_date"],"Y-m-d"); ?>" <?php if(($general_task_plan_list_data[$count]["general_task_planned_date"] != "0000-00-00") && ($general_task_plan_list_data[$count]["general_task_planned_date"] != "") && ($general_task_plan_list_data[$count]["general_task_planned_date"] != "1969-12-31") && ($general_task_plan_list_data[$count]["general_task_planned_date"] != "1970-01-01") && (($role == 3) || ($role == 2))){?> readOnly="true" <?php } ?> /></td>
						
						<td style="word-wrap:break-word;"><input type="date" name="task_data[<?php echo $count; ?>][start_date]" value="<?php echo get_formatted_date($general_task_plan_list_data[$count]["general_task_start_date"],"Y-m-d"); ?>" <?php if(($general_task_plan_list_data[$count]["general_task_start_date"] != "0000-00-00") && ($general_task_plan_list_data[$count]["general_task_start_date"] != "") && ($general_task_plan_list_data[$count]["general_task_start_date"] != "1969-12-31") && ($general_task_plan_list_data[$count]["general_task_start_date"] != "1970-01-01") && (($role == 3) || ($role == 2))) { ?> readOnly="true" <?php } ?> /></td>
						
						<td style="word-wrap:break-word;"><input type="date" name="task_data[<?php echo $count; ?>][end_date]" value="<?php echo get_formatted_date($general_task_plan_list_data[$count]["general_task_end_date"],"Y-m-d"); ?>" <?php if(($general_task_plan_list_data[$count]["general_task_end_date"] != "0000-00-00") && ($general_task_plan_list_data[$count]["general_task_end_date"] != "") && ($general_task_plan_list_data[$count]["general_task_end_date"] != "1969-12-31") && ($general_task_plan_list_data[$count]["general_task_end_date"] != "1970-01-01") && (($role == 3) || ($role == 2))) { ?> readOnly="true" <?php } ?> /></td>
						
						<td style="word-wrap:break-word;"><?php echo $variance["data"];?></td>
						
						<td style="width:70px;">
						<?php 						
						switch($general_task_plan_list_data[$count]["general_task_completion_status"])
						{
							case '0':
							echo 'NOT STARTED';
							break;
							
							case '1':
							echo 'IN PROGRESS';
							break;
							
							case '2':
							echo 'INVALID START DATE';
							break;
							
							case '3':
							echo 'COMPLETED';
							break;
						}
						?><br /><br />
						<a href="view_gen_task_remarks.php?task=<?php echo $general_task_plan_list_data[$count]["general_task_id"]; ?>"><span style="color:black; text-decoration: underline;">View Remarks</span></a>&nbsp;&nbsp;&nbsp;
						<a href="add_gen_task_remarks.php?task=<?php echo $general_task_plan_list_data[$count]["general_task_id"]; ?>"><span style="color:black; text-decoration: underline;">Add Remarks</span></a>
						</td>												
						
						<td style="word-wrap:break-word;"><?php echo $general_task_plan_list_data[$count]["assignee"]; ?>
						</td><td><b><?php echo date("d-M-Y H:i:s",strtotime($general_task_plan_list_data[$count]["general_task_added_on"])); ?></b></td>																					
					</tr>					
					<?php 
						}
					}
				}
				else
				{
				?>
				<td colspan="9">No tasks added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
			  <br />
			<?php 
			if($role == 1)
			{
			?>
			<input type="submit" class="btn btn-primary" name="gen_task_update_button" value="Save" />
			<?php 
			}
			?>
			</form>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script><script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>


  </body>

</html>