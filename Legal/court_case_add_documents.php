<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 15th June 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
// 1. Module ID should be from config
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'bd_projects'.DIRECTORY_SEPARATOR.'bd_court_case_functions.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	if(isset($_GET['case']))
	{
		$case_id = $_GET["case"];
	}
	else
	{
		$case_id = "";
	}

	// Capture the form data
	if(isset($_POST["add_document_submit"]))
	{
		$file_path = upload("file_path",$user);
		$remarks   = $_POST["stxt_remarks"];
		$case_id   = $_POST['hd_case_id'];
		
		// Check for mandatory fields
		if($file_path !="")
		{
			$court_case_documents_iresult = i_add_court_case_documents($file_path,$case_id,$remarks,$user);
			
			if($court_case_documents_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				header('location:court_case_document_list.php?case='.$case_id);
			}
			else
			{
				$alert_type = 0;
			}
			
			$alert = $court_case_documents_iresult["data"];
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
}	
else
{
	header("location:login.php");
}

// Functions
function upload($file_id,$user_id) 
{
	if($_FILES[$file_id]["name"]!="")
	{
		if ($_FILES[$file_id]["error"] > 0)
		{
			// echo "Return Code: " . $_FILES[$file_id]["error"] . "<br />";
		}
		else
		{
			$_FILES[$file_id]["name"]=$user_id."_".$_FILES[$file_id]["name"];
			move_uploaded_file($_FILES[$file_id]["tmp_name"], "legal_court_case_document/".$_FILES[$file_id]["name"]);							  
		}
	}
	
	return $_FILES[$file_id]["name"];
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Court Case Document</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Add Document</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="add_document_form" class="form-horizontal" method="post" action="court_case_add_documents.php" enctype="multipart/form-data">
								<input type="hidden" id="hd_case_id" name="hd_case_id" value="<?php echo $case_id ;?>" />
									<fieldset>										
																				
										<div class="control-group">											
											<label class="control-label" for="diffident_name">Document</label>
											<div class="controls">
												<input type="file" class="span6" name="file_path" placeholder="Document" required="required">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->	
										
										<div class="control-group">											
											<label class="control-label" for="stxt_remarks">Subject</label>
											<div class="controls">
												<input type="text" class="span6" name="stxt_remarks" placeholder="Subject of the document" >
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->	
										
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_document_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
