<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_plan_process_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD: 
*/

$_SESSION['module'] = 'Projectmgmnt';

/* DEFINES - START */
define('PROJECT_PLAN_PROCESS_LIST_FUNC_ID','269');
define('PROJECT_PLAN_TASK_LIST_FUNC_ID','270');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');


	$alert_type = -1;
	$alert = "";
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	// Get permission settings for this user for this page
	$add_perms_list     = i_get_user_perms($user,'',PROJECT_PLAN_PROCESS_LIST_FUNC_ID,'1','1');
	$view_perms_list    = i_get_user_perms($user,'',PROJECT_PLAN_PROCESS_LIST_FUNC_ID,'2','1');
	$edit_perms_list    = i_get_user_perms($user,'',PROJECT_PLAN_PROCESS_LIST_FUNC_ID,'3','1');
	$delete_perms_list  = i_get_user_perms($user,'',PROJECT_PLAN_PROCESS_LIST_FUNC_ID,'4','1');
	$approve_perms_list = i_get_user_perms($user,'',PROJECT_PLAN_PROCESS_LIST_FUNC_ID,'6','1');
	
	$view_perms_list_task   = i_get_user_perms($user,'',PROJECT_PLAN_TASK_LIST_FUNC_ID,'2','1');
	
	if(isset($_REQUEST["plan_id"]))
	{
		$plan_id = $_REQUEST["plan_id"];
	}
	else
	{
		$plan_id = "";
	}
	
	if(isset($_REQUEST["source"]))
	{
		$source = $_REQUEST["source"];
	}
	else
	{
		$source = "-1";
	}
	
	if(isset($_REQUEST["process_id"]))
	{
		$process_id = $_REQUEST["process_id"];
	}
	else
	{
		$process_id = "";
	}

	// Query String Data
	// Nothing
	
	$search_project   	 = "";
	
	/*if(isset($_POST["file_search_submit"]))
	{
		$search_project   = $_POST["search_project"];
	}
	else
	{
		$search_project   	 = "";
	}*/
	
	$search_user   	 = "";
	
	
	if(isset($_POST["file_search_submit"]))
	{
		$search_user   = $_POST["search_user"];
	}
	
	// Get Project Management Master modes already added
	$project_management_master_search_data = array("active"=>'1');
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list['status'] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list['data'];
	}	

	else
	{
		$alert = $project_management_master_list["data"];
		$alert_type = 0;
	}
	
	 // Get Users modes already added
	$user_list = i_get_user_list('','','','1');
	if($user_list['status'] == SUCCESS)
	{
		$user_list_data = $user_list['data'];
	}
	else
	{
		$alert = $user_list["data"];
		$alert_type = 0;
	}


	// Temp data
	$project_plan_process_search_data = array("active"=>'1',"plan_id"=>$plan_id,"project_id"=>$search_project,"assigned_user"=>$search_user,"process_id"=>$process_id);
	$project_plan_process_list = i_get_project_plan_process($project_plan_process_search_data);
	if($project_plan_process_list["status"] == SUCCESS)
	{
		$project_plan_process_list_data = $project_plan_process_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_plan_process_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Plan Process List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Plan Process List</h3><span style="float:right; padding-right:20px;"><a href="project_add_process.php?plan_id=<?php echo $plan_id ;?>">Add Process</a></span>
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="file_search_form" action="project_plan_process_list.php">
			 <!-- <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($project_management_master_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$process_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$process_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$process_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>-->
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_user">
			  <option value="">- - Select Assigned To - -</option>
			  <?php
			  for($user_count = 0; $user_count < count($user_list_data); $user_count++)
			  {
			  ?>
			  <option value="<?php echo $user_list_data[$user_count]["user_id"]; ?>" <?php if($search_user == $user_list_data[$user_count]["user_id"]) { ?> selected="selected" <?php } ?>><?php echo $user_list_data[$user_count]["user_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			 
			  <input type="submit" name="file_search_submit" />
			  </form>			  
            </div>
			
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th style="word-wrap:break-word;">SL No</th>
					<th style="word-wrap:break-word;">Project Name</th>
					<th style="word-wrap:break-word;">Process Name</th>
					<th style="word-wrap:break-word;">Work Description</th>
					<th style="word-wrap:break-word;">Plan Start Date</th>
					<th style="word-wrap:break-word;">Plan End Date</th>
					<th style="word-wrap:break-word;">Days</th>
                    <th style="word-wrap:break-word;">Assigned User</th>					
					<th style="word-wrap:break-word;">Added On</th>									
					<th style="word-wrap:break-word;">Completion Percentage</th>									
					<th style="word-wrap:break-word;">Pending Percentage</th>	
					<th style="word-wrap:break-word;">View Task</th>	
					<?php if($source != "view")
					{?>
					<th colspan="6" style="text-align:center;">Actions</th><?php
					}
					?>
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($project_plan_process_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_plan_process_list_data); $count++)
					{
						$sl_no++;
						
						$plan_process_start_date = date("d-M-Y",strtotime($project_plan_process_list_data[$count]["project_plan_process_start_date"]));
						$plan_process_end_date = date("d-M-Y",strtotime($project_plan_process_list_data[$count]["project_plan_process_end_date"]));
			
						$no_of_days =  get_date_diff($plan_process_start_date,$plan_process_end_date);
						
						//Get Project Process Task
						$total_completion_percentage = 0;
						$project_process_task_search_data = array("process_id"=>$project_plan_process_list_data[$count]["project_plan_process_id"],"active"=>'1');
						$project_process_task = i_get_project_process_task($project_process_task_search_data);
						if($project_process_task["status"] == SUCCESS)
						{
							$task_id = $project_process_task["data"][0]["project_process_task_id"];
							$project_process_task_list_data = $project_process_task["data"];
							$no_of_task = count($project_process_task_list_data);
							for($task_count = 0 ; $task_count < count($project_process_task_list_data) ; $task_count++)
							{
								$total_completion_percentage = $total_completion_percentage + $project_process_task_list_data[$task_count]["project_process_task_completion"];
							}
							$avg_completion_percentage = $total_completion_percentage/$no_of_task;
							$pending_percentage = 100 - $avg_completion_percentage;
						}
						else
						{
							$task_id = -1;
							$avg_completion_percentage = 0;
							$pending_percentage = 100;
						}
						//Get project plan
						$project_plan_search_data = array("plan_id"=>$project_plan_process_list_data[$count]["project_plan_process_plan_id"]);
						$project_plan_list = i_get_project_plan($project_plan_search_data);
						if($project_plan_list["status"] == SUCCESS)
						{
							$project_plan_list_data = $project_plan_list["data"];
							$project_id = $project_plan_list_data[0]["project_plan_project_id"];
						}
						
						//Get project Master
						$project_management_master_search_data = array("project_id"=>$project_id);
						$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
						if($project_management_master_list["status"] == SUCCESS)
						{
							$project_management_master_list_data = $project_management_master_list["data"];
							$project_name = $project_management_master_list_data[0]["project_master_name"];
						}
					?>
					<tr>
					<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_name; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_plan_process_list_data[$count]["project_process_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_plan_process_list_data[$count]["project_plan_process_method"]; ?></td>
					<td style="word-wrap:break-word;"><?php if($project_plan_process_list_data[$count]["project_plan_process_start_date"] != "0000-00-00")
					{ ?><?php echo date("d-M-Y",strtotime($project_plan_process_list_data[$count][
					"project_plan_process_start_date"])); ?><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($project_plan_process_list_data[$count]["project_plan_process_end_date"] != "0000-00-00")
					{ ?><?php echo date("d-M-Y",strtotime($project_plan_process_list_data[$count][
					"project_plan_process_end_date"])); ?><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($no_of_days["data"] != "") { ?><?php echo $no_of_days["data"] + 1 ; ?> <?php } else { echo $no_of_days["data"] + 1; } ?></td>
					<td style="word-wrap:break-word;"><?php echo $project_plan_process_list_data[$count]["assigned_user"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo get_formatted_date($project_plan_process_list_data[$count][
					"project_plan_process_added_on"],"d-M-Y"); ?></td>
					<td><?php echo round($avg_completion_percentage,2) ;?>%</td>
					<td><?php echo round($pending_percentage,2) ;?>%</td>
					<td style="word-wrap:break-word;"><?php if($view_perms_list_task['status'] == SUCCESS){ ?><a style="padding-right:10px" href="#" onclick="return go_to_project_plan_process_task('<?php echo $project_plan_process_list_data[$count]["project_plan_process_id"]; ?>','<?php echo $project_plan_process_list_data[$count]["project_plan_process_plan_id"]; ?>',
					'<?php echo $project_plan_process_list_data[$count]["project_plan_process_name"]; ?>');">Task</a><?php } ?></td>
					<?php if($source != "view")
					{?>
					<td style="word-wrap:break-word;"><?php if($edit_perms_list['status'] == SUCCESS){ ?><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_project_plan_process('<?php echo $project_plan_process_list_data[$count]["project_plan_process_id"]; ?>','<?php echo $project_plan_process_list_data[$count]["project_plan_process_plan_id"]; ?>');">Edit </a><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if($delete_perms_list['status'] == SUCCESS){ ?><?php if(($project_plan_process_list_data[$count]["project_plan_process_active"] == "1")){?><a href="#" onclick="return project_delete_project_plan_process(<?php echo $project_plan_process_list_data[$count]["project_plan_process_id"]; ?>);">Delete</a><?php } ?><?php } ?></td>
					
					<td style="word-wrap:break-word;"><?php if(($plan_process_start_date != '0000-00-00') && ($plan_process_end_date != '0000-00-00') && ($task_id != '-1')) {?><?php if(($project_plan_process_list_data[$count]["project_plan_process_status"] == "Rejected") || ($project_plan_process_list_data[$count]["project_plan_process_status"] == "Waiting")){?>
					<?php if($approve_perms_list['status'] == SUCCESS){ ?><a href="#" onclick="return project_approve_project_plan_process
					(<?php echo $project_plan_process_list_data[$count]["project_plan_process_id"]; ?>);">Approve</a><?php } ?><?php }  else { echo "Approved" ; } ?>
					<?php } ?></td>
					
					<td style="word-wrap:break-word;"><?php if(($plan_process_start_date != '0000-00-00') && ($plan_process_end_date != '0000-00-00') && ($task_id != '-1')) {?><?php if(($project_plan_process_list_data[$count]["project_plan_process_status"] == "Approved") || ($project_plan_process_list_data[$count]["project_plan_process_status"] == "Waiting")){ ?>
					<?php if($approve_perms_list['status'] == SUCCESS){ ?>
					<a href="#" onclick="return project_reject_project_plan_process
					(<?php echo $project_plan_process_list_data[$count]["project_plan_process_id"]; ?>);">Reject</a><?php } ?><?php }  else { echo "Rejected" ; } ?><?php } ?></td>
					
					
					
					<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_plan_process_update_delay('<?php echo $project_plan_process_list_data[$count]["project_plan_process_id"]; ?>','<?php echo $project_plan_process_list_data[$count]["project_plan_process_plan_id"]; ?>',
					'<?php echo $project_plan_process_list_data[$count]["project_plan_process_name"]; ?>');">Update Delay Reason</a></div></td>
					<?php 
					}
					?>
					
					
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No Project Master condition added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function project_delete_project_plan_process(process_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_plan_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_delete_project_plan_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_id=" + process_id + "&action=0");
		}
	}	
}
function project_approve_project_plan_process(process_id)
{
	var ok = confirm("Are you sure you want to Approve?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_plan_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_approve_project_plan_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_id=" + process_id + "&action=Approved");
		}
	}	
}

function project_reject_project_plan_process(process_id)
{
	var ok = confirm("Are you sure you want to Approve?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_plan_process_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_reject_project_plan_process.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("process_id=" + process_id + "&action=Approved");
		}
	}	
}

function go_to_project_edit_project_plan_process(process_id,plan_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "project_edit_project_plan_process.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","process_id");
	hiddenField1.setAttribute("value",process_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","plan_id");
	hiddenField2.setAttribute("value",plan_id);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}

function go_to_project_plan_process_task(project_plan_process_id,plan_id,process_master_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_plan_process_task_list.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_plan_process_id");
	hiddenField1.setAttribute("value",project_plan_process_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","plan_id");
	hiddenField2.setAttribute("value",plan_id);
	
	var hiddenField3 = document.createElement("input");
	hiddenField3.setAttribute("type","hidden");
	hiddenField3.setAttribute("name","process_master_id");
	hiddenField3.setAttribute("value",process_master_id);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	form.appendChild(hiddenField3);
	
	document.body.appendChild(form);
    form.submit();
}
function go_to_project_plan_process_update_delay(project_plan_process_id,plan_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("target", "_blank");
    form.setAttribute("action", "project_update_process_delay_reason.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_plan_process_id");
	hiddenField1.setAttribute("value",project_plan_process_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","plan_id");
	hiddenField2.setAttribute("value",plan_id);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
function go_to_process_view_delay(project_plan_process_id,plan_id)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", "project_update_process_delay_reason.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","project_plan_process_id");
	hiddenField1.setAttribute("value",project_plan_process_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","plan_id");
	hiddenField2.setAttribute("value",plan_id);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>