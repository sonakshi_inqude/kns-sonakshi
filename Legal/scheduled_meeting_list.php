<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: process_list.php
CREATED ON	: 05-June-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/
$_SESSION['module'] = 'General Task';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'meetings'.DIRECTORY_SEPARATOR.'meeting_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing here

	// Temp data
	$alert = "";		

	// Search parameters
	if(isset($_POST["mtng_search_submit"]))
	{
		$department = $_POST["ddl_department"];		
		if($role == "1")
		{
			$participant = $_POST["ddl_user"];
		}
		else
		{
			$participant = $user;
		}
	}
	else
	{
		$department = "";
		if($role == "1")
		{
			$participant = "";
		}
		else
		{
			$participant = $user;
		}
	}
	
	// Get list of meetings
	$meeting_data = array("user"=>$participant,"department"=>$department);
	$meeting_list = i_get_meetings($meeting_data);
	if($meeting_list["status"] == SUCCESS)
	{
		$meeting_list_data = $meeting_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$meeting_list["data"];
	}	
	
	// Get list of departments
	$department_list = i_get_department_list('','1');
	if($department_list["status"] == SUCCESS)
	{
		$department_list_data = $department_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$department_list["data"];
	}
	
	// Get list of users
	$user_list = i_get_user_list('','','','','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Meeting List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Meeting List</h3>			  
            </div>
            <!-- /widget-header -->
			
			<div class="widget-header" style="height:50px; padding-top:10px;">               
			  <form method="post" id="file_search_form" action="scheduled_meeting_list.php">
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_department">
				<option value="">- - Select Department - -</option>
				<?php
				for($count = 0; $count < count($department_list_data); $count++)
				{
				?>
				<option value="<?php echo $department_list_data[$count]["general_task_department_id"]; ?>" <?php if($department_list_data[$count]["general_task_department_id"] == $department) {?> selected <?php } ?>><?php echo $department_list_data[$count]["general_task_department_name"]; ?></option>								
				<?php
				}
				?>														
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="ddl_user">
				<option value="">- - Select User - -</option>
				<?php
				for($count = 0; $count < count($user_list_data); $count++)
				{
				?>
				<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php if($participant == $user_list_data[$count]["user_id"]) {?> selected <?php } ?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
				<?php
				}
				?>														
			  </select>
			  </span>
			  <span style="padding-left:20px; padding-right:20px;">
			  <input type="submit" name="mtng_search_submit" />
			  </span>
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                <tr>
				    <th>Meeting Number</th>
					<th>Department</th>	
					<th>Agenda</th>
					<th>Document</th>
				    <th>Date</th>
					<th>Time</th>	
					<th>Venue</th>
					<th>Details</th>	
					<th>Added By</th>
					<th>Added On</th>	
					<th>&nbsp;</th>	
				</tr>
				</thead>
				<tbody>
				 <?php
				if($meeting_list["status"] == SUCCESS)
				{				
					for($count = 0; $count < count($meeting_list_data); $count++)
					{						
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $meeting_list_data[$count]["meeting_no"]; ?></td>		
						<td style="word-wrap:break-word;"><?php echo $meeting_list_data[$count]["general_task_department_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $meeting_list_data[$count]["meeting_agenda"]; ?></td>
						<td style="word-wrap:break-word;"><?php if($meeting_list_data[$count]["meeting_document_path"] != ""){?><a href="docs/<?php echo $meeting_list_data[$count]["meeting_document_path"]; ?>">DOWNLOAD</a><?php } ?></td>
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($meeting_list_data[$count]["meeting_date"])); ?></td>
						<td style="word-wrap:break-word;"><?php echo $meeting_list_data[$count]["meeting_time"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $meeting_list_data[$count]["meeting_venue"]; ?></td>									
						<td style="word-wrap:break-word;"><?php echo $meeting_list_data[$count]["meeting_details"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $meeting_list_data[$count]["user_name"]; ?></td>		
						<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($meeting_list_data[$count]["meeting_added_on"])); ?></td>
						<td style="word-wrap:break-word; vertical-align:middle;"><a href="meeting_details.php?meeting=<?php echo $meeting_list_data[$count]["meeting_id"]; ?>">Details</a></td>
					</tr>
					<?php 
					}
				}
				else
				{
				?>
				<td colspan="11">No meetings added yet!</td>
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
/* Open the sidenav */
function openNav() {
    document.getElementById("mySidenav").style.width = "75%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>

  </body>

</html>
