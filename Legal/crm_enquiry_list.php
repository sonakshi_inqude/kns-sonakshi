<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_enquiry_list.php
CREATED ON	: 06-Sep-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Enquiries
*/
define('CRM_ENQUIRY_LIST_FUNC_ID','101');
/*
TBD: 
*/$_SESSION['module'] = 'CRM Transactions';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page	$add_perms_list     = i_get_user_perms($user,'',CRM_ENQUIRY_LIST_FUNC_ID,'1','1');	$view_perms_list    = i_get_user_perms($user,'',CRM_ENQUIRY_LIST_FUNC_ID,'2','1');	$edit_perms_list    = i_get_user_perms($user,'',CRM_ENQUIRY_LIST_FUNC_ID,'3','1');	$delete_perms_list  = i_get_user_perms($user,'',CRM_ENQUIRY_LIST_FUNC_ID,'4','1');

	// Query String / Get form Data
	// Nothing here
		
	if(($role == 1) || ($role == 5) || ($role == 8))
	{
		$assigned_to = "";
	}	
	else
	{
		$assigned_to = '';
	}
	
	if(isset($_POST["enquiry_search_submit"]))
	{
		$int_status = $_POST["ddl_search_int_status"];
		
		if($_POST["dt_start_date"] != "")
		{
			$start_date_form = $_POST["dt_start_date"];
			$start_date      = $start_date_form." 00:00:00";
		}
		else
		{
			$start_date_form = "";
			$start_date      = "";
		}
		
		if($_POST["dt_end_date"] != "")
		{
			$end_date_form = $_POST["dt_end_date"];
			$end_date      = $end_date_form." 23:59:59";
		}
		else
		{
			$end_date_form = "";
			$end_date      = "";
		}
		
		if($_POST["dt_start_date_entry_form"] != "")
		{
			$start_date_entry_form = $_POST["dt_start_date_entry_form"];
			$start_date_entry      = $start_date_entry_form." 00:00:00";
		}
		else
		{
			$start_date_entry_form = "";
			$start_date_entry         = "";
		}
		
		if($_POST["dt_end_date_entry_form"] != "")
		{
			$end_date_entry_form = $_POST["dt_end_date_entry_form"];
			$end_date_entry      = $end_date_entry_form." 23:59:59";
		}
		else
		{
			$end_date_entry_form = "";
			$end_date_entry      = "";
		}

		$source          = $_POST["ddl_search_source"];
		$assigner        = $_POST["ddl_search_assigned_by"];
		$enquiry_number  = $_POST["stxt_enquiry_num"];
		$cell_number     = $_POST["stxt_cust_cell_num"];
		if(($role == 1) || ($role == 5))
		{
			if($_POST["ddl_search_assigned_to"] != "")
			{
				$assigned_to = $_POST["ddl_search_assigned_to"];
			}
		}	
		$project = $_POST["ddl_search_project"];
		
		if(isset($_POST['cb_is_sold']))
		{
			$is_sold = '1';
		}
		else
		{
			$is_sold = '0';
		}
	}
	else
	{
		$int_status       = "-1";
		$start_date       = "-1";
		$end_date         = "-1";
		$source           = "-1";
		$assigner         = "-1";
		$enquiry_number   = "";
		$cell_number      = "";
		$start_date_entry = "-1";
		$end_date_entry   = "-1";
		$project          = "";
		$is_sold          = '0';
	}

	// Temp data
	$alert = "";
	
	if($role == "6")
	{
		$order = "entry_date_desc";
		$enquiry_list = i_get_enquiry_list('',$enquiry_number,$project,'','',$cell_number,'','',$source,'','',$assigned_to,$assigner,$start_date_entry,$end_date_entry,$start_date,$end_date,$order,'','','',$user);
	}
	else if(($role == "7") || ($role == "8"))
	{
		$order = "assign_date_desc";
		$enquiry_list = i_get_enquiry_list('',$enquiry_number,$project,'','',$cell_number,'','',$source,'','',$assigned_to,$assigner,$start_date_entry,$end_date_entry,$start_date,$end_date,$order,'','','',$user);
	}
	else
	{
		$user_details = i_get_user_list($assigned_to,'','','');		
		if($user_details["data"][0]["user_role"] == "6")
		{
			$order = "entry_date_desc";
			$enquiry_list = i_get_enquiry_list('',$enquiry_number,$project,'','',$cell_number,'','',$source,'','',$assigned_to,$assigner,$start_date_entry,$end_date_entry,$start_date,$end_date,$order,'','','',$user);
		}
		else		
		{
			$order = "assign_date_desc";
			$enquiry_list = i_get_enquiry_list('',$enquiry_number,$project,'','',$cell_number,'','',$source,'','',$assigned_to,$assigner,$start_date_entry,$end_date_entry,$start_date,$end_date,$order,'','','',$user);
		}
	}
		
	if($enquiry_list["status"] == SUCCESS)
	{
		$enquiry_list_data = $enquiry_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$enquiry_list["data"];
	}
	
	// Source List
	$source_list = i_get_enquiry_source_list('','1');
	if($source_list["status"] == SUCCESS)
	{
		$source_list_data = $source_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$source_list["data"];
	}
	
	// Interest Status List
	$int_status_list = i_get_interest_status_list('','1');
	if($int_status_list["status"] == SUCCESS)
	{
		$int_status_list_data = $int_status_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$int_status_list["data"];
	}
	
	// CRM Project List	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"project_active"=>'1',"active"=>'1');	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);	
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
	}
	
	// User List
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Enquiry List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Enquiry List (Total Enquiries = <span id="total_count_section">
			  <?php
			  if($enquiry_list["status"] == SUCCESS)
			  {
				$preload_count = count($enquiry_list_data);			
			  }
			  else
			  {
			    $preload_count = 0;
			  }
			  
			  echo $preload_count;
			  ?></span>)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="float:right;"><a href="crm_reports/crm_export_enquiry_report.php?int_status=<?php echo $int_status; ?>&project=<?php echo $project; ?>&start_date=<?php echo $start_date; ?>&end_date=<?php echo $end_date; ?>&start_date_entry=<?php echo $start_date_entry; ?>&end_date_entry=<?php echo $end_date_entry; ?>&source=<?php echo $source; ?>&assigner=<?php echo $assigner; ?>&enquiry_num=<?php echo $enquiry_number; ?>&cell=<?php echo $cell_number; ?>&assignee=<?php echo $assigned_to; ?>" target="_blank">Export to Excel</a></span></h3>
            </div>
			
			<div class="widget-header" style="height:120px; padding-top:10px;">               
			  <form method="post" id="enquiry_search" action="crm_enquiry_list.php">
			  <span style="padding-left:8px; padding-right:8px;">
			  Assigned Date - From
			  <input type="date" name="dt_start_date" value="<?php echo $start_date_form; ?>" />			  
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  Assigned Date - To
			  <input type="date" name="dt_end_date" value="<?php echo $end_date_form; ?>" />		  
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_source">
			  <option value="">- - Select Source - -</option>
			  <?php
				for($count = 0; $count < count($source_list_data); $count++)
				{
					?>
					<option value="<?php echo $source_list_data[$count]["enquiry_source_master_id"]; ?>" <?php 
					if($source == $source_list_data[$count]["enquiry_source_master_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $source_list_data[$count]["enquiry_source_master_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>			  
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_int_status">
			  <option value="">- - Select Interest Status - -</option>
			  <?php
				for($count = 0; $count < count($int_status_list_data); $count++)
				{
					?>
					<option value="<?php echo $int_status_list_data[$count]["crm_cust_interest_status_id"]; ?>" <?php 
					if($int_status == $int_status_list_data[$count]["crm_cust_interest_status_id"])
					{
					?>						
					selected="selected"
					<?php
					}?>><?php echo $int_status_list_data[$count]["crm_cust_interest_status_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  Entered Date - From
			  <input type="date" name="dt_start_date_entry_form" value="<?php echo $start_date_entry_form; ?>" />			  
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  Entered Date - To
			  <input type="date" name="dt_end_date_entry_form" value="<?php echo $end_date_entry_form; ?>" />			  
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_assigned_by">
			  <option value="">- - Select Assigner - -</option>
			  <?php
				for($count = 0; $count < count($user_list_data); $count++)
				{
					
					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
					if($assigner == $user_list_data[$count]["user_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <?php
			  if(($role == 1) || ($role == 5))
			  {
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_assigned_to">
			  <option value="">- - Select Assignee - -</option>
			  <?php
				for($count = 0; $count < count($user_list_data); $count++)
				{
					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
					if($assigned_to == $user_list_data[$count]["user_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <?php
			  }
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
				for($count = 0; $count < count($project_list_data); $count++)
				{
					?>
					<option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php 
					if($project == $project_list_data[$count]["project_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $project_list_data[$count]["project_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="text" name="stxt_enquiry_num" value="<?php echo $enquiry_number; ?>" placeholder="Full Enquiry Number" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="text" name="stxt_cust_cell_num" value="<?php echo $cell_number; ?>" placeholder="Full Mobile Number of Customer" />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  Is Sold&nbsp;&nbsp;&nbsp;<input type="checkbox" name="cb_is_sold" <?php if($is_sold == '1') { ?> checked <?php } ?> />
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="enquiry_search_submit" />
			  </span>
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>					
					<th>Enquiry No</th>					
					<th>Project</th>					
					<th>Name</th>
					<th>Mobile</th>
					<th>Email</th>
					<th>Company</th>
					<th>Source</th>
					<th>Walk In</th>
					<th>Latest Status</th>
					<th>Sold Status</th>
					<th>Enquiry Date</th>
					<th>Enquiry Assigned Date</th>
					<th>Follow Up Count</th>
					<th>Next FollowUp</th>
					<th>Site Visit Count</th>
					<th>Assignee</th>					
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<?php if(($role == 1) || ($role == 5) || ($role == 6) || ($role == 7))
					{?>
					<th>&nbsp;</th>
					<?php
					}?>
				</tr>
				</thead>
				<tbody>							
				<?php
				if($enquiry_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					$unq_count = 0;
					$int_filter_out_count = 0;
					$unsold_count = 0;
					for($count = 0; $count < count($enquiry_list_data); $count++)
					{						
						$latest_follow_up_data = i_get_enquiry_fup_list($enquiry_list_data[$count]["enquiry_id"],'','','desc','0','1');
						$follow_up_data = i_get_enquiry_fup_list($enquiry_list_data[$count]["enquiry_id"],'','','','','');
						if($follow_up_data["status"] == SUCCESS)
						{
							$fup_count = count($follow_up_data["data"]);
						}
						else
						{
							$fup_count = 0;
						}
						
						// Site Visit Data
						$site_visit_data = i_get_site_travel_plan_list('','',$enquiry_list_data[$count]["enquiry_id"],'','','','2','','');
						if($site_visit_data["status"] == SUCCESS)
						{
							$sv_count = count($site_visit_data["data"]);
						}
						else
						{
							$sv_count = 0;
						}
						
						// Is the enquiry unqualified
						$unqualified_data = i_get_unqualified_leads('',$enquiry_list_data[$count]["enquiry_id"],'','','','','');						
						if(($int_status != "") && ($latest_follow_up_data["data"][0]["crm_cust_interest_status_id"] != $int_status))
						{
							$int_filter_out_count++;
						}
						else
						{
						$sl_no++;
						
						// Check if there is an active booking for this enquiry
						$sales_result = i_get_site_booking('','','','','','1','','','','','','',$enquiry_list_data[$count]["enquiry_id"],'','','','');
						if(($is_sold != '1') || (($is_sold == '1') && ($sales_result['status'] == SUCCESS)))
						{
					?>
					<tr>
					<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["enquiry_number"]; ?><?php if($role == 1)
					{?>
					<br /><br /><a href="crm_edit_enquiry.php?enquiry=<?php echo $enquiry_list_data[$count]["enquiry_id"]; ?>">Edit Enquiry</a>
					<?php
					}?></td>					
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["project_name"]; ?></td>					
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["cell"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["email"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["company"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["enquiry_source_master_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php if($enquiry_list_data[$count]["walk_in"] == "1")
					{
						echo "Yes";
					}
					else
					{
						echo "No";
					}?></td>
					<td style="word-wrap:break-word;"><?php 
						if($unqualified_data['status'] == SUCCESS)
						{
							echo 'Unqualified Already';
						}
						else
						{
							echo $latest_follow_up_data["data"][0]["crm_cust_interest_status_name"]; 
						}
						if(($latest_follow_up_data["data"][0]["enquiry_follow_up_int_status"] == "5") && ($unqualified_data['status'] != SUCCESS))
						{
						?>
						<br />
						<br />
						<a href="crm_add_prospective_profile.php?enquiry=<?php echo $enquiry_list_data[$count]["enquiry_id"]; ?>">Add Prospective Client Profile</a>
						<?php
						}?></td>
					<td style="word-wrap:break-word;"><?php if($sales_result["status"] == SUCCESS)
					{
						echo 'SOLD';
					}
					else
					{
						echo 'UNSOLD';
					}?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($enquiry_list_data[$count]["added_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($enquiry_list_data[$count]["assigned_on"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo $fup_count; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y H:i",strtotime($latest_follow_up_data["data"][0]["enquiry_follow_up_date"])); ?></td>
					<td style="word-wrap:break-word;"><?php echo $sv_count; ?></td>
					<td style="word-wrap:break-word;"><?php echo $enquiry_list_data[$count]["user_name"]; ?></td>					
					<td style="word-wrap:break-word;"><?php if((($enquiry_list_data[$count]["added_by"] == $user) || ($enquiry_list_data[$count]["assigned_to"] == $user) || ($role == '1') || ($role == '5')) && ($unqualified_data['status'] != SUCCESS)){ ?><a href="crm_enquiry_fup_list.php?enquiry=<?php echo $enquiry_list_data[$count]["enquiry_id"]; ?>">Follow Up</a><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if((($enquiry_list_data[$count]["added_by"] == $user) || ($enquiry_list_data[$count]["assigned_to"] == $user) || ($role == '1') || ($role == '5')) && ($unqualified_data['status'] != SUCCESS)){ ?><a href="crm_add_site_visit_plan.php?enquiry=<?php echo $enquiry_list_data[$count]["enquiry_id"]; ?>">Site Visit Plan</a><br /><br /><a href="crm_site_visit_plan_list.php?enquiry=<?php echo $enquiry_list_data[$count]["enquiry_id"]; ?>">Site Visit Plan List</a><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if((($enquiry_list_data[$count]["added_by"] == $user) || ($enquiry_list_data[$count]["assigned_to"] == $user) || ($role == '1') || ($role == '5')) && ($unqualified_data['status'] != SUCCESS)){ ?><a href="crm_assign_enquiry.php?enquiry=<?php echo $enquiry_list_data[$count]["enquiry_id"]; ?>">Assign</a><?php } ?></td>
					<td style="word-wrap:break-word;"><?php if((($enquiry_list_data[$count]["added_by"] == $user) || ($enquiry_list_data[$count]["assigned_to"] == $user) || ($role == '1') || ($role == '5')) && ($unqualified_data['status'] != SUCCESS)){ ?><a href="crm_add_site_travel_actual.php?enquiry=<?php echo $enquiry_list_data[$count]["enquiry_id"]; ?>">Site Visit Actual</a><br /><br /><a href="crm_site_travel_actual_list.php?enquiry=<?php echo $enquiry_list_data[$count]["enquiry_id"]; ?>">Actual Site Visit List</a><?php } ?></td>
					<?php if((($role == 1) || ($role == 5) || ($enquiry_list_data[$count]["added_by"] == $user) || ($enquiry_list_data[$count]["assigned_to"] == $user)) && ($unqualified_data['status'] != SUCCESS))
					{?>
					<td style="word-wrap:break-word;"><a href="crm_add_unqualified_enquiry.php?enquiry=<?php echo $enquiry_list_data[$count]["enquiry_id"]; ?>">Mark as Unqualified</a></td>
					<?php 
					}?>
					</tr>
					<?php
						}
						else
						{
							$unsold_count++;
						}
					}										
					}
				}
				else
				{
				?>
				<tr><td colspan="14">No enquiries!</td></tr>
				<?php
				}	
				if($enquiry_list["status"] == SUCCESS)
				{
					$final_count = count($enquiry_list_data) - $unq_count - $int_filter_out_count - $unsold_count;			
				}
				else
				{
					$final_count = 0;
				}
				 ?>	
				<script>
				document.getElementById("total_count_section").innerHTML = <?php echo $final_count; ?>;
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>