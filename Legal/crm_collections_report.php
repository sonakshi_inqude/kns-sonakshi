<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_approved_booking_list.php
CREATED ON	: 04-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : Approved Booking List
*/

/*
TBD: 
*/$_SESSION['module'] = 'CRM Reports';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing

	// Temp data
	$alert = "";
	
	if(isset($_POST["collection_report_submit"]))
	{			
		$status = $_POST["ddl_status"];		
	}
	else
	{
		$status = "";		
	}
	
	// Delay Reason List
	$delay_reason_list = i_get_crm_delay_reason_list('','1');
	if($delay_reason_list["status"] == SUCCESS)
	{
		$delay_reason_list_data = $delay_reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$delay_reason_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get project list	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"active"=>'1',"project_active"=>'1');	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);	if($project_list["status"] == SUCCESS)	{		$project_list_data = $project_list["data"];	}	else	{		$alert = $alert."Alert: ".$project_list["data"];		$alert_type = 0; // Failure	}
	
	// User List
	$user_list = i_get_user_list('','','','');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Pending Collection Report</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:50px;"> <i class="icon-th-list"></i>
              <h3>Pending Collection Report</h3><span style="float:right; padding-right:20px;"><a href="excelexports/crm_collection_report.php?status=<?php echo $status; ?>" target="_blank">Export Receivables Report</a></span>
            </div>
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="collection_search" action="crm_collections_report.php">
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_status">
			  <option value="">- - Select Status - -</option>
			  <option value="1" <?php if($status == "1") { ?> selected <?php } ?>>Booked</option>
			  <option value="2" <?php if($status == "2") { ?> selected <?php } ?>>Agreement</option>
			  </select>
			  </span>	
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="collection_report_submit" />
			  </span>
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>					
					<th rowspan="2">Project</th>
					<?php					
					for($count = 0; $count < count($delay_reason_list_data); $count++)
					{						
					?>
					<th colspan="2"><?php echo $delay_reason_list_data[$count]["crm_delay_reason_master_name"]; ?></th>					
					<?php
					}
					?>
					<th colspan="2">Collection</th>					
					<th rowspan="2">Total Sites</th>
					<th rowspan="2">Total Amount</th>
				</tr>
				<tr>				   
				   <?php
					$reason_total_site_count_array      = array();
					$reason_total_pending_payment_array = array();
					for($count = 0; $count < count($delay_reason_list_data); $count++)
					{
						$reason_total_site_count_array[$count]      = 0;
						$reason_total_pending_payment_array[$count] = 0;
					?>
					<th>No. of Sites</th>					
					<th>Pending Amount</th>					
					<?php
					}
					?>
					<th>No. of Sites</th>					
					<th>Pending Amount</th>					
				</tr>
				</thead>
				<tbody>							
				<?php			
				if($project_list["status"] == SUCCESS)
				{										
					$sl_no = 0;					
					$cancelled_count = 0;					
					$total_collection_sites   = 0;
					$total_collection_pending = 0;
					$total_pending_amount_all = 0;
					$total_sites_all          = 0;						
					
					for($count = 0; $count < count($project_list_data); $count++)
					{		
						$project_total_site_count      = 0;
						$project_total_pending_payment = 0;
						$sl_no++;
						?>
						<tr>						
						<td style="word-wrap:break-word;"><?php echo $project_list_data[$count]["project_name"]; ?></td>
						<?php
						// Get reasons and append 2 columns for each reason						
						for($reason_count = 0; $reason_count < count($delay_reason_list_data); $reason_count++)
						{
							$delayed_payment_list = i_get_delayed_payment_list('','','','');
							
							$no_of_sites     = 0;
							$payment_pending = 0;
							if($delayed_payment_list["status"] == SUCCESS)
							{															
								for($pay_count = 0; $pay_count < count($delayed_payment_list["data"]); $pay_count++)
								{
									if(($project_list_data[$count]["project_id"] == $delayed_payment_list["data"][$pay_count]["crm_project_id"]) && ($delay_reason_list_data[$reason_count]["crm_delay_reason_master_id"] == $delayed_payment_list["data"][$pay_count]["crm_delay_reason"]))
									{
										if(($status == '') || (($status == '1') && (($delayed_payment_list["data"][$pay_count]["crm_agreement_date"] == '') || ($delayed_payment_list["data"][$pay_count]["crm_agreement_date"] == NULL) || ($delayed_payment_list["data"][$pay_count]["crm_agreement_date"] == 'NULL'))) || (($status == '2') && (($delayed_payment_list["data"][$pay_count]["crm_agreement_date"] != '') && ($delayed_payment_list["data"][$pay_count]["crm_agreement_date"] != NULL) && ($delayed_payment_list["data"][$pay_count]["crm_agreement_date"] != 'NULL'))))
										{
											if($delayed_payment_list['data'][$pay_count]["crm_booking_consideration_area"] != "0")
											{
												$consideration_area = $delayed_payment_list['data'][$pay_count]["crm_booking_consideration_area"];
											}
											else
											{
												$consideration_area = $delayed_payment_list['data'][$pay_count]["crm_site_area"];
											}
											
											// Get payable amount
											$total_payable = round($delayed_payment_list["data"][$pay_count]["crm_booking_rate_per_sq_ft"] * $consideration_area);
											
											// Get payments done
											$payment_done = 0;
											$payments_done_sresult = i_get_payment('',$delayed_payment_list["data"][$pay_count]["crm_delay_reason_booking"],'','','');											
											if($payments_done_sresult["status"] == SUCCESS)
											{
												for($payment_count = 0; $payment_count < count($payments_done_sresult["data"]); $payment_count++)
												{
													$payment_done = $payment_done + $payments_done_sresult["data"][$payment_count]["crm_payment_amount"];
												}
											}
											else
											{
												$payment_done = 0;
											}		
											$payment_done = round($payment_done);
										
											$payment_pending = $payment_pending + ($total_payable - $payment_done);																						if(($total_payable - $payment_done) > 0)											{
												$no_of_sites++;											}
										}
									}	
								}
							}
							else
							{
								$no_of_sites     = 0;
								$payment_pending = 0;
							}
						?>						
						<td style="word-wrap:break-word;"><a href="crm_schedule_report.php?project=<?php echo $project_list_data[$count]["project_id"]; ?>&status=<?php echo $status; ?>&reason=<?php echo $delay_reason_list_data[$reason_count]["crm_delay_reason_master_id"]; ?>" target="_blank"><?php echo $no_of_sites; ?></a></td>
						<td style="word-wrap:break-word;"><?php echo $payment_pending; ?></td>						
						<?php
							$reason_total_site_count_array[$reason_count]      = $reason_total_site_count_array[$reason_count] + $no_of_sites;
							$reason_total_pending_payment_array[$reason_count] = $reason_total_pending_payment_array[$reason_count] + $payment_pending;
							$project_total_site_count                          = $project_total_site_count + $no_of_sites;
							$project_total_pending_payment                     = $project_total_pending_payment + $payment_pending;
						}						
						?>
						<?php
						$collection_sites   = 0;
						$collection_pending = 0;
						$collection_list = i_get_pending_nondelayed_payment_list($project_list_data[$count]["project_id"],$status,'');						
						if($collection_list["status"] == SUCCESS)
						{							
							for($col_count = 0; $col_count < count($collection_list["data"]); $col_count++)
							{
								if($collection_list['data'][$col_count]["crm_booking_consideration_area"] != "0")
								{
									$consideration_area = $collection_list['data'][$col_count]["crm_booking_consideration_area"];
								}
								else
								{
									$consideration_area = $collection_list['data'][$col_count]["crm_site_area"];
								}
											
								// Get payable amount
								$total_payable = round($collection_list["data"][$col_count]["crm_booking_rate_per_sq_ft"] * $consideration_area);
								
								// Get payments done
								$payment_done = 0;
								$payments_done_sresult = i_get_payment_total('',$collection_list["data"][$col_count]["crm_booking_id"],'','','');
								if($payments_done_sresult["status"] == SUCCESS)
								{
									$payment_done = round($payments_done_sresult["data"][0]["paid"]);
								}
								else
								{
									$payment_done = 0;
								}
																if(($total_payable - $payment_done) > 0)								{
									$collection_sites++;								}
								$collection_pending = $collection_pending + ($total_payable - $payment_done);
							}	
						}
						else
						{
							$collection_sites   = 0;
							$collection_pending = 0;
						}
						
						$total_collection_sites   = $total_collection_sites + $collection_sites;
						$total_collection_pending = $total_collection_pending + $collection_pending;
						?>
						<td style="word-wrap:break-word;"><a href="crm_schedule_report.php?project=<?php echo $project_list_data[$count]["project_id"]; ?>&status=<?php echo $status; ?>&reason=<?php echo '0'; ?>" target="_blank"><?php echo $collection_sites; ?></a></td>
						<td style="word-wrap:break-word;"><?php echo $collection_pending; ?></td>
						<td style="word-wrap:break-word;"><a href="crm_schedule_report.php?project=<?php echo $project_list_data[$count]["project_id"]; ?>&status=<?php echo $status; ?>" target="_blank"><?php echo ($project_total_site_count + $collection_sites); ?></a></td>
						<td style="word-wrap:break-word;"><?php echo ($project_total_pending_payment + $collection_pending); ?></td>
						</tr>
						<?php
						$total_pending_amount_all = $total_pending_amount_all + $project_total_pending_payment + $collection_pending;
						$total_sites_all = $total_sites_all + $project_total_site_count + $collection_sites;
					}
				}
				?>
				<tr>				
				<td style="word-wrap:break-word;"><b>GRAND TOTAL</b></td>
				<?php
				for($count = 0; $count < count($delay_reason_list_data); $count++)
				{
				?>
				<td style="word-wrap:break-word;"><a href="crm_schedule_report.php?status=<?php echo $status; ?>&reason=<?php echo $delay_reason_list_data[$count]["crm_delay_reason_master_id"]; ?>" target="_blank"><b><?php echo $reason_total_site_count_array[$count]; ?></b></a></td>					
				<td style="word-wrap:break-word;"><b><?php echo $reason_total_pending_payment_array[$count]; ?></b></td>					
				<?php
				}
				?>
				<td style="word-wrap:break-word;"><a href="crm_schedule_report.php?status=<?php echo $status; ?>&reason=<?php echo '0'; ?>" target="_blank"><b><?php echo $total_collection_sites; ?></b></a></td>
				<td style="word-wrap:break-word;"><b><?php echo $total_collection_pending; ?></b></td>
				<td style="word-wrap:break-word;"><a href="crm_schedule_report.php?status=<?php echo $status; ?>" target="_blank"><b><?php echo $total_sites_all; ?></b></a></td>
				<td style="word-wrap:break-word;"><b><?php echo $total_pending_amount_all; ?></b></td>				
				</tr>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script><script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>


  </body>

</html>