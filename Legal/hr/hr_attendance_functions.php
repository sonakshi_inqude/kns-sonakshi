<?php
//ini_set('memory_limit',$ '-1');
/**
 * @author Perumal
 * @copyright 2015
 */

$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_hr_attendance.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'da_hr_leaves.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_config.php');

/* INTERFACE FUNCTIONS - START */

/*
PURPOSE : To add new attendance
INPUT 	: Date, Employee, Shift, In Time, Out Time, Work Duration, OT Duration, Total Duration, Type, Status, Added By
OUTPUT 	: Attendance ID, success or failure message
BY 		: Nitin
*/
function i_add_attendance($attendance_date,$attendance_employee,$shift,$in_time,$out_time,$work_duration,$ot_duration,$total_duration,$type,$attendance_entered_by)
{
	// Check if this employee is already added
	$attendance_filter_data = array("employee_id"=>$attendance_employee,"attendance_date"=>$attendance_date,"attendance_status"=>'1');
	$attendance_sresult = db_get_attendance_list($attendance_filter_data);
	
	if($attendance_sresult["status"] != DB_RECORD_ALREADY_EXISTS)
	{
		$attendance_iresult = db_add_attendance($attendance_date,$attendance_employee,$shift,$in_time,$out_time,$work_duration,$ot_duration,$total_duration,$type,$attendance_entered_by);
		
		if($attendance_iresult['status'] == SUCCESS)
		{
			$return["data"]   = $attendance_iresult["data"];
			$return["status"] = SUCCESS;
			
			// Check if it was a public holiday and add Comp off if applicable
			$holiday_filter_data = array("date_start"=>$attendance_date,"date_end"=>$attendance_date,"status"=>'1');
			$holiday_list = i_get_holiday($holiday_filter_data);
			if((($type == "1") || ($type == "2")) && ($holiday_list["status"] == SUCCESS))
			{
				// Add comp off
				$comp_off_result = p_add_comp_off($attendance_employee,$attendance_iresult["data"],$type,$attendance_date);
				
				if($comp_off_result["status"] == SUCCESS)
				{
					$return["data"]   = $attendance_iresult["data"];
					$return["status"] = SUCCESS;
				}
				else
				{
					$return["data"]   = $attendance_iresult["data"];
					$return["status"] = FAILURE;
				}
			}
			// Check if it was a Sunday and add Comp off if applicable
			else if((($type == "1") || ($type == "2")) && (date("D",strtotime($attendance_date)) == "Sun"))
			{
				// Add comp off
				$comp_off_result = p_add_comp_off($attendance_employee,$attendance_iresult["data"],$type,$attendance_date);
				
				if($comp_off_result["status"] == SUCCESS)
				{
					$return["data"]   = $attendance_iresult["data"];
					$return["status"] = SUCCESS;
				}
				else
				{
					$return["data"]   = $attendance_iresult["data"];
					$return["status"] = FAILURE;
				}
			}
			
			// Check if last day of month. If yes, initiate earned leave calculations
			$date_array = explode('-',$attendance_date);
			$number_of_days = cal_days_in_month(CAL_GREGORIAN,$date_array[1],$date_array[0]);
			if($attendance_date == $date_array[0].'-'.$date_array[1].'-'.$number_of_days)
			{
				// Initiate Earned Leave Calculations
				p_calculate_earned_leaves($attendance_employee,$date_array[0],$date_array[1]);
				
				// Initiate Sick Leave Calculations
				p_calculate_sick_leaves($attendance_employee,$date_array[0],$date_array[1]);
			}
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Attendance for this employee for this day already exists!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get attendance list
INPUT 	: Attendance Filter Data
OUTPUT 	: Attendance List, success or failure message
BY 		: Nitin
*/
function i_get_attendance_list($attendance_filter_data)
{
	$attendance_sresult = db_get_attendance_list($attendance_filter_data);
	
	if($attendance_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
        $return["data"]   = $attendance_sresult["data"]; 
		$return["status"] = SUCCESS;
    }
    else
    {
		$return["data"]   = "No attendance with these details";
	    $return["status"] = FAILURE;
    }
	
	return $return;
}

/*
PURPOSE : To get attendance type
INPUT 	: Attendance Type Filter Data
OUTPUT 	: Attendance Type List, success or failure message
BY 		: Nitin
*/
function i_get_attendance_type($attendance_type_filter_data)
{
	$attendance_type_sresult = db_get_attendance_type_list($attendance_type_filter_data);
	
	if($attendance_type_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
        $return["data"]   = $attendance_type_sresult["data"]; 
		$return["status"] = SUCCESS;
    }
    else
    {
		$return["data"]   = "No attendance type with these details";
	    $return["status"] = FAILURE;
    }
	
	return $return;
}

/*
PURPOSE : To get attendance type
INPUT 	: Attendance ID, Attendance Data
OUTPUT 	: success or failure message
BY 		: Nitin
*/
function i_update_attendance($attendance_id,$attendance_data)
{	$allow_edit = false;		// Check attendance date	$attendance_check_filter_data = array("attendance_id"=>$attendance_id);	$attendance_check_sresult = db_get_attendance_list($attendance_check_filter_data);			if($attendance_check_sresult['status'] == DB_RECORD_ALREADY_EXISTS)	{		$attendance_check_month = date('m',strtotime($attendance_check_sresult['data'][0]['hr_attendance_date']));				$attendance_check_year = date('Y',strtotime($attendance_check_sresult['data'][0]['hr_attendance_date']));				$no_of_days_in_month = cal_days_in_month(CAL_GREGORIAN,$attendance_check_month,$attendance_check_year);		$month_last_day = $attendance_check_year.'-'.$attendance_check_month.'-'.$no_of_days_in_month;		$max_allowed_date = date('Y-m-d',strtotime($month_last_day.' +5 days'));				if((date('Y-m-d') <= $max_allowed_date) || (date('m') == $attendance_check_month))		{			$allow_edit = true;		}	}		if($allow_edit)	{
		$attendance_uresult = db_update_attendance_details($attendance_id,$attendance_data);
		if($attendance_uresult['status'] == SUCCESS)
		{
			$return["data"]   = "Attendance Details updated successfully";
			$return["status"] = SUCCESS;
			
			// Get attendance details
			$attendance_filter_data = array("attendance_id"=>$attendance_id);
			$attendance_sresult = db_get_attendance_list($attendance_filter_data);		
			if($attendance_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
			{
				$type                = $attendance_sresult['data'][0]['hr_attendance_type'];
				$attendance_date     = $attendance_sresult['data'][0]['hr_attendance_date'];
				$attendance_employee = $attendance_sresult['data'][0]['hr_attendance_employee'];
			
				// Check if it was a public holiday and add Comp off if applicable
				$holiday_filter_data = array("date_start"=>$attendance_date,"date_end"=>$attendance_date,"status"=>'1');
				$holiday_list = i_get_holiday($holiday_filter_data);						
				if((($type == "1") || ($type == "2")) && ($holiday_list["status"] == SUCCESS))
				{
					// Check whether to update or add comp off
					$co_filter_data = array("attendance_id"=>$attendance_id);
					$co_sresult = db_get_comp_off_list($co_filter_data);
					
					if($co_sresult['status'] == DB_NO_RECORD)
					{
						// Add comp off
						$comp_off_result = p_add_comp_off($attendance_employee,$attendance_id,$type,$attendance_date);
						
						if($comp_off_result["status"] == SUCCESS)
						{
							$return["data"]   = $attendance_id;
							$return["status"] = SUCCESS;
						}
						else
						{
							$return["data"]   = $attendance_id;
							$return["status"] = FAILURE;
						}
					}
					else
					{
						// edit comp off
						$co_details = array('type'=>$type);
						$co_uresult = db_update_co_details($attendance_id,$co_details);
						
						if($co_uresult['status'] == SUCCESS)
						{
							$return["data"]   = $attendance_id;
							$return["status"] = SUCCESS;
						}
						else
						{
							$return["data"]   = 'Attendance updated successfully. However, comp off details were not updated';
							$return["status"] = FAILURE;
						}
					}
				}			
				// Check if it was a Sunday and add Comp off if applicable
				else if((($type == "1") || ($type == "2")) && (date("D",strtotime($attendance_date)) == "Sun"))
				{				
					// Check whether to update or add comp off
					$co_filter_data = array("attendance_id"=>$attendance_id);
					$co_sresult = db_get_comp_off_list($co_filter_data);
					
					if($co_sresult['status'] == DB_NO_RECORD)
					{
						// Add comp off
						$comp_off_result = p_add_comp_off($attendance_employee,$attendance_id,$type,$attendance_date);					
						if($comp_off_result["status"] == SUCCESS)
						{
							$return["data"]   = $attendance_id;
							$return["status"] = SUCCESS;
						}
						else
						{
							$return["data"]   = $attendance_id;
							$return["status"] = FAILURE;
						}
					}
					else
					{
						// edit comp off
						$co_details = array('type'=>$type);
						$co_uresult = db_update_co_details($attendance_id,$co_details);					
						if($co_uresult['status'] == SUCCESS)
						{
							$return["data"]   = $attendance_id;
							$return["status"] = SUCCESS;
						}
						else
						{
							$return["data"]   = 'Attendance updated successfully. However, comp off details were not updated';
							$return["status"] = FAILURE;
						}
					}
				}
				
				// Check if last day of month. If yes, initiate earned leave calculations
				$date_array = explode('-',$attendance_date);
				$number_of_days = cal_days_in_month(CAL_GREGORIAN,$date_array[1],$date_array[0]);
				if($attendance_date == $date_array[0].'-'.$date_array[1].'-'.$number_of_days)
				{
					// Initiate Earned Leave Calculations
					p_calculate_earned_leaves($attendance_employee,$date_array[0],$date_array[1],'2');
					
					// Initiate Sick Leave Calculations
					p_calculate_sick_leaves($attendance_employee,$date_array[0],$date_array[1]);
				}
			}
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later!";
			$return["status"] = FAILURE;
		}	}	else	{		$return["data"]   = "You cannot edit attendance data now.";		$return["status"] = FAILURE;	}
	return $return;
}

/*
PURPOSE : To add new absence details
INPUT 	: Employee, Absence Date, Absence Type, Absence Duration(1 if Full Day, 2 if Half Day), Remarks, Added By
OUTPUT 	: Absence ID, success or failure message
BY 		: Nitin
*/
function i_add_absence_details($employee,$absence_date,$absence_type,$absence_duration,$remarks,$added_by)
{
	$allow_leave = 1;
	$half_count  = 0;

	// Check if absence for this this employee is already added for this day
	$absence_filter_data = array("employee_id"=>$employee,"date"=>$absence_date,"status"=>'1');
	$absence_sresult = db_get_absence_list($absence_filter_data);
	
	if($absence_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($absence_duration == '1')
		{
			$allow_leave = $allow_leave & 0;
		}
		else if($absence_duration == '2')
		{
			if(count($absence_sresult["data"]) < 2)
			{
				$half_count = 1;
				$allow_leave = $allow_leave & 1;
			}
			else
			{
				$allow_leave = $allow_leave & 0;
			}
		}
	}
	else
	{
		$allow_leave = $allow_leave & 1;
	}
	
	// Check if absence for this this employee is already added for this day but pending approval
	$absence_pending_filter_data = array("employee_id"=>$employee,"date"=>$absence_date,"status"=>'0');
	$absence_pending_sresult = db_get_absence_list($absence_pending_filter_data);
	
	if($absence_pending_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		if($absence_duration == '1')
		{
			$allow_leave = $allow_leave & 0;
		}
		else if($absence_duration == '2')
		{
			if(count($absence_pending_sresult["data"]) < 2)
			{
				if($half_count == 0)
				{
					$allow_leave = $allow_leave & 1;
				}
				else				
				{
					$allow_leave = $allow_leave & 0;
				}
			}
			else
			{
				$allow_leave = $allow_leave & 0;
			}
		}
	}
	else
	{
		$allow_leave = $allow_leave & 1;
	}
	
	if($allow_leave == 1)
	{
		// Check for whether user has enough leaves
		$leave_available_count = p_get_pending_leaves($employee,$absence_type,$absence_date);
		
		if($leave_available_count > 0)
		{
			$absence_iresult = db_add_absence($employee,$absence_date,$absence_type,$absence_duration,$remarks,$added_by);
			
			if($absence_iresult['status'] == SUCCESS)
			{
				$return["data"]   = $absence_iresult["data"];
				$return["status"] = SUCCESS;			
			}
			else
			{
				$return["data"]   = "Internal Error. Please try again later";
				$return["status"] = FAILURE;
			}
		}
		else
		{
			$return["data"]   = "Not enough leaves for you to apply";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Leave already applied for this day by this employee!";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get list of leave applications
INPUT 	: Leave Filter Data
OUTPUT 	: Leave List, success or failure message
BY 		: Nitin
*/
function i_get_leave_list($leave_filter_data)
{	
	$leave_list_sresult = db_get_absence_list($leave_filter_data);
	
	if($leave_list_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
        $return["data"]   = $leave_list_sresult["data"]; 
		$return["status"] = SUCCESS;
    }
    else
    {
		$return["data"]   = "No leaves have been applied for";
	    $return["status"] = FAILURE;
    }
	
	return $return;
}

/*
PURPOSE : To get name of a leave status
INPUT 	: Status Value
OUTPUT 	: Status Name
BY 		: Nitin
*/
function i_get_leave_status_name($leave_status)
{	
	switch($leave_status)
	{
	case "0":
	$return = "Pending";
	break;
	
	case "1":
	$return = "Approved";
	break;
	
	case "2":
	$return = "Rejected";
	break;
	
	case "3":
	$return = "Cancelled";
	break;
	
	default:
	$return = "NA";
	break;
	}
	
	return $return;
}

/*
PURPOSE : To get name of a leave duration
INPUT 	: Duration Value
OUTPUT 	: Duration Name
BY 		: Nitin
*/
function i_get_leave_duration_name($leave_duration)
{	
	switch($leave_duration)
	{
	case "1":
	$return = "Full Day";
	break;
	
	case "2":
	$return = "Half Day";
	break;
	
	default:
	$return = "NA";
	break;
	}
	
	return $return;
}

/*
PURPOSE : To update leave status
INPUT 	: Leave ID, Leave Data
OUTPUT 	: success or failure message
BY 		: Nitin
*/
function i_update_leave($leave_id,$leave_data)
{
	$is_updatable = true;
	
	// Check for available leaves before approving
	if((array_key_exists("status",$leave_data)) && ($leave_data["status"] == "1"))
	{
		// Get attendance details
		$absence_filter_data = array("leave_id"=>$leave_id);
		$leave_sdetails = db_get_absence_list($absence_filter_data);
		
		if($leave_sdetails["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$is_leave_available = p_get_pending_leaves($leave_sdetails["data"][0]["hr_absence_employee"],$leave_sdetails["data"][0]["hr_absence_type"],$leave_sdetails["data"][0]["hr_absence_date"]);
			
			if($leave_sdetails["data"][0]["hr_absence_duration"] == ATTENDANCE_TYPE_PRESENT)
			{
				if($is_leave_available < 1)
				{
					$is_updatable = false;
				}
				else
				{
					$is_updatable = true;
				}
			}
			else if($leave_sdetails["data"][0]["hr_absence_duration"] == ATTENDANCE_TYPE_HALFDAY)
			{
				if($is_leave_available < 0.5)
				{
					$is_updatable = false;
				}
				else
				{
					$is_updatable = true;
				}
			}
		}
		else
		{
			$is_updatable = false;
		}
	}
	
	if($is_updatable == true)
	{
		$leave_uresult = db_update_leave_details($leave_id,$leave_data);
		
		if($leave_uresult['status'] == SUCCESS)
		{
			$return["data"]   = "Leave Details updated successfully";
			$return["status"] = SUCCESS;
		}
		else
		{
			$return["data"]   = "Internal Error. Please try again later!";
			$return["status"] = FAILURE;
		}
	}
	else
	{
		$return["data"]   = "Not enough leaves for you to apply";
		$return["status"] = FAILURE;
	}
	
	return $return;
}

/*
PURPOSE : To add new Out Pass request
INPUT 	: Employee, Date, Out TIme, Finish Time, Remarks, Type, Added By
OUTPUT 	: Out Pass ID, success or failure message
BY 		: Nitin
*/
function i_add_out_pass_request($employee_id,$date,$out_time,$finish_time,$remarks,$type,$added_by)
{
	$out_pass_iresult = db_add_out_pass_request($employee_id,$date,$out_time,$finish_time,$remarks,$type,$added_by);
			
	if($out_pass_iresult['status'] == SUCCESS)
	{
		$return["data"]   = $out_pass_iresult["data"];
		$return["status"] = SUCCESS;			
	}
	else
	{
		$return["data"]   = "Internal Error. Please try again later";
		$return["status"] = FAILURE;
	}
		
	return $return;
}

/*
PURPOSE : To get list of out pass applications
INPUT 	: Out Pass Filter Data
OUTPUT 	: Out Pass List, success or failure message
BY 		: Nitin
*/
function i_get_out_pass_list($out_pass_filter_data)
{	
	$out_pass_list_sresult = db_get_out_pass_list($out_pass_filter_data);
	
	if($out_pass_list_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
    {
        $return["data"]   = $out_pass_list_sresult["data"]; 
		$return["status"] = SUCCESS;
    }
    else
    {
		$return["data"]   = "No out pass requests have been applied for";
	    $return["status"] = FAILURE;
    }
	
	return $return;
}

/*
PURPOSE : To update out pass application request
INPUT 	: Out Pass ID, Out Pass Data Array
OUTPUT 	: Out Pass ID, success or failure message
BY 		: Nitin
*/
function i_update_out_pass($out_pass_id,$out_pass_details)
{	
	$out_pass_uresult = db_update_out_pass_details($out_pass_id,$out_pass_details);
	
	if($out_pass_uresult['status'] == SUCCESS)
    {
        $return["data"]   = $out_pass_uresult["data"]; 
		$return["status"] = SUCCESS;
		
		if($out_pass_details["status"] == '1')
		{
			/* Add to total working hours - Start */
			// Get out pass details
			$out_pass_filter_data = array("out_pass_id"=>$out_pass_id);
			$out_pass_sresult = db_get_out_pass_list($out_pass_filter_data);
			
			// Get attendance details of that day
			$attendance_filter_data = array("employee_id"=>$out_pass_sresult["data"][0]["hr_out_pass_employee"],"attendance_date"=>$out_pass_sresult["data"][0]["hr_out_pass_date"]);
			$attendance_sresult = db_get_attendance_list($attendance_filter_data);
			
			if($attendance_sresult['status'] == DB_RECORD_ALREADY_EXISTS)
			{
				// Calculate out time
				$out_time_array = explode(':',$out_pass_sresult["data"][0]["hr_out_pass_out_time"]);
				$out_time = ($out_time_array[0]*60) + $out_time_array[1];
				
				// Calculate finish time
				$finish_time_array = explode(':',$out_pass_sresult["data"][0]["hr_out_pass_finish_time"]);
				$finish_time = ($finish_time_array[0]*60) + $finish_time_array[1];
				
				// Compute total working hours according to attendance
				$already_loaded_time_array = explode(':',$attendance_sresult["data"][0]["hr_attendance_total_duration"]);
				$already_loaded_time = ($already_loaded_time_array[0]*60) + $already_loaded_time_array[1];
				
				// Calculate total duration
				$total_duration = ($finish_time - $out_time) + $already_loaded_time;
				$tot_dur_array[0] = str_pad(intval($total_duration/60),2,"0",STR_PAD_LEFT);
				$tot_dur_array[1] = str_pad(intval($total_duration%60),2,"0",STR_PAD_LEFT);			
				$tot_duration  = implode(':',$tot_dur_array);
				
				// Check if swipe is clean
				$is_swipe_clean = i_get_employee_swipe($attendance_sresult["data"][0]["hr_attendance_in_time"],$attendance_sresult["data"][0]["hr_attendance_out_time"]);
				
				if($is_swipe_clean == 0)
				{
					// Compute attendance type
					if($total_duration >= 450)
					{
						$type = "1";
					}
					else if($total_duration >= 240)
					{
						$type = "2";
					}
					else
					{
						$type = "3";
					}
				}
				else
				{
					$type = "2";
				}
				
				// Update total duration for the given day
				$attendance_details = array("total_duration"=>$tot_duration,"type"=>$type);				
				$attendance_uresult = i_update_attendance($attendance_sresult["data"][0]["hr_attendance_id"],$attendance_details);
				if($attendance_uresult["status"] == SUCCESS)
				{
					// No change required. Output success data
				}
				else
				{
					$return["status"] = FAILURE;
					$return["data"]   = "Out Pass was approved, but error in changing attendance status for the day";
				}
			}
			/* Add to total working hours - End */
		}
    }
    else
    {
		$return["data"]   = "Internal Error. Please try later!";
	    $return["status"] = FAILURE;
    }
	
	return $return;
}

/*
PURPOSE : To get name of an Out Pass status
INPUT 	: Status Value
OUTPUT 	: Status Name
BY 		: Nitin
*/
function i_get_out_pass_status_name($out_pass_status)
{	
	switch($out_pass_status)
	{
	case "0":
	$return = "Pending";
	break;
	
	case "1":
	$return = "Approved";
	break;
	
	case "2":
	$return = "Rejected";
	break;
	
	case "3":
	$return = "Cancelled";
	break;
	
	default:
	$return = "NA";
	break;
	}
	
	return $return;
}

/*
PURPOSE : To get name of an Out Pass type
INPUT 	: Status Value
OUTPUT 	: Status Name
BY 		: Nitin
*/
function i_get_out_pass_type_name($out_pass_type)
{	
	switch($out_pass_type)
	{
	case "0":
	$return = "Personal";
	break;
	
	case "1":
	$return = "On Duty";
	break;
	
	default:
	$return = "NA";
	break;
	}
	
	return $return;
}

/*
PURPOSE : To check if user has missed swipe in or swipe out
INPUT 	: In Time, Out Time
OUTPUT 	: 0 if none of them are missed, 1 if in time missed, 2 if out time missed, 3 if both are missed
BY 		: Nitin
*/
function i_get_employee_swipe($in_time,$out_time)
{	
	if(($in_time != "") && ($in_time != NULL))
	{
		$in_time_array  = explode(':',$in_time);
	}
	else
	{
		$in_time_array = array("00","00");
	}
	
	if(($out_time != "") && ($out_time != NULL))
	{
		$out_time_array = explode(':',$out_time);
	}
	else	
	{
		$out_time_array = array("00","00");
	}
	
	$return = 0;
	if(($in_time_array[0] + $in_time_array[1]) == 0)
	{
		$return = 1;
	}
	if(($out_time_array[0] + $out_time_array[1]) == 0)
	{
		if($return == 1)
		{
			$return = 0;
		}
		else if($return == 0)
		{
			$return = 2;
		}	
	}
	
	return $return;
}

function i_get_total_working_hours($in_time,$out_time,$op_start_time,$op_end_time)
{
	$in_time_array       = explode(':',$in_time);
	$out_time_array      = explode(':',$out_time);
	$op_start_time_array = explode(':',$op_start_time);
	$op_end_time_array   = explode(':',$op_end_time);
	
	$in_time_mins       = ($in_time_array[0] * 60) + $in_time_array[1];
	$out_time_mins      = ($out_time_array[0] * 60) + $out_time_array[1];
	$op_start_time_mins = ($op_start_time_array[0] * 60) + $op_start_time_array[1];
	$op_end_time_mins   = ($op_end_time_array[0] * 60) + $op_end_time_array[1];
	
	$op_start_process_type = 0;
	$op_end_process_type   = 0;
	// Check if there is overlap
	if(($op_start_time_mins >= $in_time_mins) && ($op_start_time_mins <= $out_time_mins))
	{
		$op_start_process_type = 1; // Calculate from out time to op end time
	}
	if(($op_end_time_mins >= $in_time_mins) && ($op_end_time_mins <= $out_time_mins))
	{
		$op_end_process_type = 1; // Calculate from out time to op end time
	}
	
	if(($op_start_process_type == 1) && ($op_end_process_type == 1))
	{
		// Total working hours = outtime - in time
		$total_working_hours = $out_time_mins - $in_time_mins;
	}
	if(($op_start_process_type == 0) && ($op_end_process_type == 1))
	{
		// Total working hours = outtime - op in time
		$total_working_hours = $out_time_mins - $op_start_time_mins;
	}
	if(($op_start_process_type == 1) && ($op_end_process_type == 0))
	{
		// Total working hours = outtime - op in time
		$total_working_hours = $op_end_time_mins - $in_time_mins;
	}
	if(($op_start_process_type == 0) && ($op_end_process_type == 0))
	{
		if(($op_start_time_mins <= $in_time_mins) && ($op_end_time_mins >= $out_time_mins))
		{
			// Total working hours = outtime - op in time
			$total_working_hours = $op_end_time_mins - $op_start_time_mins;
		}
		else
		{
			// Total working hours = outtime - op in time
			$total_working_hours = $out_time_mins - $in_time_mins + $op_end_time_mins - $op_start_time_mins;
		}
	}		
	
	$total_working_duration = str_pad(intval($total_working_hours/60),2,"0",STR_PAD_LEFT).':'.str_pad(intval($total_working_hours%60),2,"0",STR_PAD_LEFT);
	if($total_working_hours > 510)
	{
		$working_duration = '08:30';
		$ot_duration      = str_pad(intval(($total_working_hours - 510)/60),2,"0",STR_PAD_LEFT).':'.str_pad(intval(($total_working_hours - 510)%60),2,"0",STR_PAD_LEFT);
	}
	else
	{
		$working_duration = $total_working_duration;
		$ot_duration      = '00:00';
	}
	
	$return = array('mins'=>$total_working_hours,'duration'=>$total_working_duration,'work_duration'=>$working_duration,'ot_duration'=>$ot_duration);
	
	return $return;
}

function i_get_attendance_type_from_value($total_time)
{							
	if($total_time >= 450)
	{																
		$type = "1";															
	}
	else if($total_time >= 240)
	{
		$type = "2";																								
	}
	else
	{								
		$type = "3";									
	}
	
	return $type;
}
/* INTERFACE FUNCTIONS - END */

/* PRIVATE FUNCTIONS - START */
function p_calculate_earned_leaves($employee,$year,$month,$mode=1)
{	
	// Check for number of days in the month worked by the employee
	$fd_attendance_filter_data = array("employee_id"=>$employee,"attendance_start_date"=>$year.'-'.$month.'-01',"attendance_end_date"=>$year.'-'.$month.'-31',"attendance_type"=>ATTENDANCE_TYPE_PRESENT,"attendance_status"=>'1');
	$fd_attendance_sresult = i_get_attendance_list($fd_attendance_filter_data);
	
	if($fd_attendance_sresult["status"] == SUCCESS)
	{
		$fd_count = count($fd_attendance_sresult["data"]);
	}
	else
	{
		$fd_count = 0;
	}
	
	$hd_attendance_filter_data = array("employee_id"=>$employee,"attendance_start_date"=>$year.'-'.$month.'-01',"attendance_end_date"=>$year.'-'.$month.'-31',"attendance_type"=>ATTENDANCE_TYPE_HALFDAY,"attendance_status"=>'1');
	$hd_attendance_sresult = i_get_attendance_list($hd_attendance_filter_data);
	
	if($hd_attendance_sresult["status"] == SUCCESS)
	{
		$hd_count = count($hd_attendance_sresult["data"]);
	}
	else
	{
		$hd_count = 0;
	}
	
	// Get public holiday list
	$number_of_days = cal_days_in_month(CAL_GREGORIAN,$month,$year);
	$holiday_filter_data = array("date_start"=>$year.'-'.$month.'-01',"date_end"=>$year.'-'.$month.'-'.$number_of_days,"status"=>'1');
	$holiday_list = i_get_holiday($holiday_filter_data);
	if($holiday_list['status'] == SUCCESS)
	{
		$pholidays = count($holiday_list['data']);
	}
	else
	{
		$pholidays = 0;
	}
	
	// If no. of working days >= 21, add appropriate no of leaves
	if(($fd_count + $hd_count + $pholidays) >= EL_MIN_WORKING_DAYS)
	{
		$leaves_to_be_added = EL_TO_BE_ADDED;
	}
	else
	{
		$leaves_to_be_added = 0;
	}
	
	// Update leaves for the same year. Check for year end. If year end, add leaves for new year.
	$employee_el_filter_data = array("employee_id"=>$employee);
	$employee_el_sresult = db_get_el_list($employee_el_filter_data);
	
	/* Taken leaves - Start */
	$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'1',"type"=>LEAVE_TYPE_EARNED,"duration"=>ATTENDANCE_TYPE_PRESENT);
	$leave_sresult = db_get_absence_list($leave_filter_data);
	if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$taken_leaves_full = count($leave_sresult["data"]);
	}
	else
	{
		$taken_leaves_full = 0;
	}
	
	$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'1',"type"=>LEAVE_TYPE_EARNED,"duration"=>ATTENDANCE_TYPE_HALFDAY);
	$leave_sresult = db_get_absence_list($leave_filter_data);
	if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$taken_leaves_half = count($leave_sresult["data"]);
	}
	else
	{
		$taken_leaves_half = 0;
	}
	$taken_leaves = $taken_leaves_full + ($taken_leaves_half*0.5);
	/* Taken leaves - End */
	
	if($employee_el_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$el_details = array("count"=>$leaves_to_be_added);
		$el_uresult = db_update_el_details($employee,$year,$el_details);
		$carry_over = $employee_el_sresult["data"][0]["hr_earned_leave_count"] - $taken_leaves + $leaves_to_be_added;
	}
	else	
	{
		$el_iresult = db_add_earned_leaves($employee,$leaves_to_be_added,$year);
		$carry_over = $leaves_to_be_added;
	}
	
	if(($month == "12") && ($mode == 1)) // Mode: 1 for add, 2 for edit
	{
		// Add new year with carry over
		$el_iresult = db_add_earned_leaves($employee,$carry_over,($year + 1));		
	}	
}

function p_calculate_sick_leaves($employee,$year,$month)
{	
	/* Calculation for number of leaves to be added - Start */
	// Check for number of days in the month worked by the employee
	$fd_attendance_filter_data = array("employee_id"=>$employee,"attendance_start_date"=>$year.'-'.$month.'-01',"attendance_end_date"=>$year.'-'.$month.'-31',"attendance_type"=>ATTENDANCE_TYPE_PRESENT,"attendance_status"=>'1');
	$fd_attendance_sresult = i_get_attendance_list($fd_attendance_filter_data);
	
	if($fd_attendance_sresult["status"] == SUCCESS)
	{
		$fd_count = count($fd_attendance_sresult["data"]);
	}
	else
	{
		$fd_count = 0;
	}
	
	$hd_attendance_filter_data = array("employee_id"=>$employee,"attendance_start_date"=>$year.'-'.$month.'-01',"attendance_end_date"=>$year.'-'.$month.'-31',"attendance_type"=>ATTENDANCE_TYPE_HALFDAY,"attendance_status"=>'1');
	$hd_attendance_sresult = i_get_attendance_list($hd_attendance_filter_data);
	
	if($hd_attendance_sresult["status"] == SUCCESS)
	{
		$hd_count = count($hd_attendance_sresult["data"]);
	}
	else
	{
		$hd_count = 0;
	}
	
	// Get public holiday list
	$number_of_days = cal_days_in_month(CAL_GREGORIAN,$month,$year);
	$holiday_filter_data = array("date_start"=>$year.'-'.$month.'-01',"date_end"=>$year.'-'.$month.'-'.$number_of_days,"status"=>'1');
	$holiday_list = i_get_holiday($holiday_filter_data);
	if($holiday_list['status'] == SUCCESS)
	{
		$pholidays = count($holiday_list['data']);
	}
	else
	{
		$pholidays = 0;
	}
	
	// If no. of working days >= 15, add appropriate no of leaves
	if(($fd_count + $hd_count + $pholidays) >= SL_MIN_WORKING_DAYS)
	{
		// No of sick leaves per month
		$leaves_to_be_added = SICK_LEAVE_PER_MONTH;
	}
	else
	{
		$leaves_to_be_added = 0;
	}	
	/* Calculation for number of leaves to be added - End */
	
	// Update leaves for the same year. Check for year end. If year end, add leaves for new year.
	$sl_filter_data = array("employee_id"=>$employee);
	$employee_sl_sresult = db_get_sl_list($sl_filter_data);
	
	if($employee_sl_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$sl_details = array("count"=>$leaves_to_be_added);
		$sl_uresult = db_update_sl_details($employee,$year,$sl_details);		
	}
	else	
	{
		$sl_iresult = db_add_sick_leaves($employee,$leaves_to_be_added,$year);
	}
	
	if($month == "12")
	{
		// Add new year with carry over
		$sl_iresult = db_add_sick_leaves($employee,'0.5',($year + 1));		
	}	
}

function p_add_comp_off($employee,$attendance_id,$type,$attendance_date)
{
	$valid_date = date("Y")."-12-31";
	$comp_off_iresult = db_add_comp_off($employee,$valid_date,$attendance_id,$type);
	
	return $comp_off_iresult;
}

function p_get_pending_leaves($employee,$absence_type,$leave_date)
{
	switch($absence_type)
	{
		case LEAVE_TYPE_EARNED:
		$el_filter_data = array("employee_id"=>$employee,"year"=>date("Y"));
		$el_sresult = db_get_el_list($el_filter_data);
		if($el_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$total_leaves = $el_sresult["data"][0]["hr_earned_leave_count"];
		}
		else
		{
			$total_leaves = 0;
		}		
		
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'1',"type"=>LEAVE_TYPE_EARNED,"duration"=>ATTENDANCE_TYPE_PRESENT);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_full = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_full = 0;
		}
		
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'1',"type"=>LEAVE_TYPE_EARNED,"duration"=>ATTENDANCE_TYPE_HALFDAY);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_half = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_half = 0;
		}
		$taken_leaves = $taken_leaves_full + ($taken_leaves_half*0.5);
		break;
		
		case LEAVE_TYPE_SICK:
		$sl_filter_data = array("employee_id"=>$employee,"year"=>date("Y"));
		$sl_sresult = db_get_sl_list($sl_filter_data);
		if($sl_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$total_leaves = $sl_sresult["data"][0]["hr_sick_leave_count"];
		}
		else
		{
			$total_leaves = 0;
		}
		
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'1',"type"=>LEAVE_TYPE_SICK,"duration"=>ATTENDANCE_TYPE_PRESENT);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_full = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_full = 0;
		}
		
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'1',"type"=>LEAVE_TYPE_SICK,"duration"=>ATTENDANCE_TYPE_HALFDAY);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_half = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_half = 0;
		}
		
		$taken_leaves = $taken_leaves_full + ($taken_leaves_half*0.5);
		break;
		
		case LEAVE_TYPE_CASUAL:
		$total_leaves = CASUAL_LEAVE_PER_YEAR;
		
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'1',"type"=>LEAVE_TYPE_CASUAL,"duration"=>ATTENDANCE_TYPE_PRESENT);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_full = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_full = 0;
		}
		
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'1',"type"=>LEAVE_TYPE_CASUAL,"duration"=>ATTENDANCE_TYPE_HALFDAY);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_half = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_half = 0;
		}
		
		$taken_leaves = $taken_leaves_full + ($taken_leaves_half*0.5);
		break;
		
		case LEAVE_TYPE_COMP_OFF:
		$co_filter_data = array("employee_id"=>$employee,"valid_date"=>$leave_date,"type"=>'1');
		$co_sresult = db_get_comp_off_list($co_filter_data);
		if($co_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$total_leaves = count($co_sresult["data"]);
		}
		else
		{
			$total_leaves = 0;
		}
		
		$coh_filter_data = array("employee_id"=>$employee,"valid_date"=>$leave_date,"type"=>'2');
		$coh_sresult = db_get_comp_off_list($coh_filter_data);
		if($coh_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$total_leaves = $total_leaves + (count($coh_sresult["data"]) * 0.5);
		}
		else
		{
			$total_leaves = $total_leaves + 0;
		}
		
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'1',"type"=>LEAVE_TYPE_COMP_OFF,"duration"=>ATTENDANCE_TYPE_PRESENT);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_full = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_full = 0;
		}
		
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'1',"type"=>LEAVE_TYPE_COMP_OFF,"duration"=>ATTENDANCE_TYPE_HALFDAY);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_half = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_half = 0;
		}
		
		$taken_leaves = $taken_leaves_full + ($taken_leaves_half*0.5);
		break;
	}

	$return = $total_leaves - $taken_leaves;
	
	return $return;
}
/* PRIVATE FUNCTIONS - END */

/* TEMPORARY FUNCTIONS - START */
function t_add_comp_off($employee,$attendance_id,$type,$valid_date)
{	
	$comp_off_iresult = db_add_comp_off($employee,$valid_date,$attendance_id,$type);
	
	return $comp_off_iresult;
}

function t_add_cl($employee,$leaves_to_be_added,$year)
{
	$el_iresult = db_add_earned_leaves($employee,$leaves_to_be_added,$year);
	
	return $el_iresult;
}

function t_add_sl($employee,$leaves_to_be_added,$year)
{
	$sl_iresult = db_add_sick_leaves($employee,$leaves_to_be_added,$year);;
	
	return $sl_iresult;
}

function t_update_cl($employee,$leaves_to_be_updated,$year)
{
	$el_details = array("count"=>$leaves_to_be_updated);
	$el_uresult = db_update_el_details_opening($employee,$year,$el_details);
	
	return $el_uresult;
}

function t_update_sl($employee,$leaves_to_be_updated,$year)
{
	$sl_details = array("count"=>$leaves_to_be_updated);
	$sl_iresult = db_update_sl_details_opening($employee,$year,$sl_details);
	
	return $sl_iresult;
}

function t_get_cl($employee)
{
	$el_filter_data = array("employee_id"=>$employee,"year"=>date("Y"));
	$el_sresult = db_get_el_list($el_filter_data);
	if($el_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$total_leaves = $el_sresult["data"][0]["hr_earned_leave_count"];
	}
	else
	{
		$total_leaves = 0;
	}
	
	return $total_leaves;
}

function t_get_sl($employee)
{
	$sl_filter_data = array("employee_id"=>$employee,"year"=>date("Y"));
	$sl_sresult = db_get_sl_list($sl_filter_data);
	if($sl_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
	{
		$total_leaves = $sl_sresult["data"][0]["hr_sick_leave_count"];
	}
	else
	{
		$total_leaves = 0;
	}
	
	return $total_leaves;
}

function t_get_approval_pending_leaves($employee,$absence_type,$leave_date)
{
	switch($absence_type)
	{
		case LEAVE_TYPE_EARNED:		
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'0',"type"=>LEAVE_TYPE_EARNED,"duration"=>ATTENDANCE_TYPE_PRESENT);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_full = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_full = 0;
		}
		
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'0',"type"=>LEAVE_TYPE_EARNED,"duration"=>ATTENDANCE_TYPE_HALFDAY);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_half = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_half = 0;
		}
		$taken_leaves = $taken_leaves_full + ($taken_leaves_half*0.5);
		break;
		
		case LEAVE_TYPE_SICK:		
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'0',"type"=>LEAVE_TYPE_SICK,"duration"=>ATTENDANCE_TYPE_PRESENT);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_full = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_full = 0;
		}
		
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'0',"type"=>LEAVE_TYPE_SICK,"duration"=>ATTENDANCE_TYPE_HALFDAY);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_half = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_half = 0;
		}
		
		$taken_leaves = $taken_leaves_full + ($taken_leaves_half*0.5);
		break;
		
		case LEAVE_TYPE_CASUAL:				
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'0',"type"=>LEAVE_TYPE_CASUAL,"duration"=>ATTENDANCE_TYPE_PRESENT);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_full = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_full = 0;
		}
		
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'0',"type"=>LEAVE_TYPE_CASUAL,"duration"=>ATTENDANCE_TYPE_HALFDAY);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_half = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_half = 0;
		}
		
		$taken_leaves = $taken_leaves_full + ($taken_leaves_half*0.5);
		break;
		
		case LEAVE_TYPE_COMP_OFF:		
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'0',"type"=>LEAVE_TYPE_COMP_OFF,"duration"=>ATTENDANCE_TYPE_PRESENT);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_full = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_full = 0;
		}
		
		$leave_filter_data = array("employee_id"=>$employee,"start_date"=>date("Y")."-01-01","end_date"=>date("Y")."-12-31","status"=>'0',"type"=>LEAVE_TYPE_COMP_OFF,"duration"=>ATTENDANCE_TYPE_HALFDAY);
		$leave_sresult = db_get_absence_list($leave_filter_data);
		if($leave_sresult["status"] == DB_RECORD_ALREADY_EXISTS)
		{
			$taken_leaves_half = count($leave_sresult["data"]);
		}
		else
		{
			$taken_leaves_half = 0;
		}
		
		$taken_leaves = $taken_leaves_full + ($taken_leaves_half*0.5);
		break;
	}

	$return = $taken_leaves;
	
	return $return;
}

function t_check_in_time_proper($in_time,$out_time)
{
	// Check whether there was a clean swipe by the employee
	$is_swipe_clean = i_get_employee_swipe($in_time,$out_time);
	
	if($is_swipe_clean != 0)
	{
		$return = false;
	}
	else
	{
		if(($in_time != "") && ($in_time != NULL))
		{
			$in_time_array  = explode(':',$in_time);
		}
		else
		{
			$in_time_array = array("0","00");
		}				
		
		if($in_time_array[0] > 9)
		{
			$return = false;
		}
		else if(($in_time_array[0] == 9) && ($in_time_array[1] > 10))
		{
			$return = false;
		}
		else
		{
			$return = true;
		}
	}
	
	return $return;
}

//To Get Comp off
function t_get_comp_off($hr_comp_off_employee)
{
	$comp_off_filter = array('employee_id'=>$hr_comp_off_employee);
	$comp_sresults   = db_get_comp_off_list($comp_off_filter);
	
	if($comp_sresults["status"] == DB_NO_RECORD)
	{
		$return["status"] = FAILURE;
		$return["data"]   = "No Records";
	}
	else
	{
		$return["status"] = SUCCESS;
		$return["data"]   = $comp_sresults['data'];
	}
	
	return $return;
}
/* TEMPORARY FUNCTIONS - END */
?>