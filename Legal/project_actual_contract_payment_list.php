<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_actual_contract_payment_list.php
CREATED ON	: 09-May-2017
CREATED BY	: Ashwini
PURPOSE     : List of project for actual contract payment
*/

/*
TBD: 
*/

/* DEFINES - START */
define('PROJECT_CONTRACT_PROJECT_ACTUAL_PAYMENT_FUNC_ID','267');
/* DEFINES - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	// Get permission settings for this user for this page
	$view_perms_list   = i_get_user_perms($user,'',PROJECT_CONTRACT_PROJECT_ACTUAL_PAYMENT_FUNC_ID,'2','1');
	$edit_perms_list   = i_get_user_perms($user,'',PROJECT_CONTRACT_PROJECT_ACTUAL_PAYMENT_FUNC_ID,'3','1');
	
	$alert_type = -1;
	$alert = "";

	// Query String Data
	// Nothing

	// Get Project Actual Contract Payment modes already added
	$project_actual_contract_payment_search_data = array("active"=>'1',"status"=>"Pending");
	$project_actual_contract_payment_list = i_get_project_actual_contract_payment($project_actual_contract_payment_search_data);
	if($project_actual_contract_payment_list['status'] == SUCCESS)
	{
		$project_actual_contract_payment_list_data = $project_actual_contract_payment_list['data'];
	}
	else
	{
		$alert = $alert."Alert: ".$project_actual_contract_payment_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Weekly Contract Payment List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Weekly Contract Payment List</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			<?php if($view_perms_list['status'] == SUCCESS)
			{
			?>
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
				    <th>Project</th>
					<th>Bill No</th>
					<th>From Date</th>
					<th>To Date</th>
					<th>Vendor</th>
					<th>Total Measurement</th>
					<th>UOM</th>
					<th>Rate</th>					
					<th>Total</th>
					<th>Security Deposit</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<th colspan="2">Action</th>									
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($project_actual_contract_payment_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_actual_contract_payment_list_data); $count++)
					{
						$sl_no++;
						$measurement = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_total_measurement"];
						$rate = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_rate"];
						$number = $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_number"];
						if($number != 0)
						{
							$total = $measurement * $rate * $number ;
						}
						else
						{
							$total = $measurement * $rate;
						}
						$project_payment_contract_mapping_search_data = array("payment_id"=>$project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_id"]);
						$payment_contract_mapping_list= i_get_project_payment_contract_mapping($project_payment_contract_mapping_search_data);
						if($payment_contract_mapping_list["status"] == SUCCESS)
						{
							$payment_contract_mapping_list_data = $payment_contract_mapping_list["data"];
							$contract_id = $payment_contract_mapping_list_data[0]["project_payment_contract_mapping_contract_id"];
							$project_task_actual_boq_search_data = array("boq_id"=>$contract_id);
							$actual_payment_contract_list = i_get_project_task_boq_actual($project_task_actual_boq_search_data);
							if($actual_payment_contract_list["status"] == SUCCESS)
							{
								$project = $actual_payment_contract_list["data"][0]["project_master_name"];
								$project_loaction = $actual_payment_contract_list["data"][0]["project_master_remarks"];
							}
						}
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project; ?></td>
					<td><?php echo $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_bill_no"]; ?></td>
					<td><?php echo date("d-M-Y",strtotime($project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_from_date"])); ?></td>
					<td><?php echo date("d-M-Y",strtotime($project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_to_date"])); ?></td>
					<td><?php echo $project_actual_contract_payment_list_data[$count]["project_manpower_agency_name"]; ?></td>
					<td><?php echo $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_total_measurement"]; ?></td>
					<td><?php echo $project_actual_contract_payment_list_data[$count]["stock_unit_name"]; ?></td>
					<td><?php echo $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_rate"]; ?></td>
					<td><?php echo round($project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_amount"]); ?></td>
					<td><?php echo $project_actual_contract_payment_list_data[$count]["project_actual_contract_deposit_amount"]; ?></td>
					<td><?php echo $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_remarks"]; ?></td>
					<td><?php echo $project_actual_contract_payment_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_actual_contract_payment_list_data[$count][
					"project_actual_contract_payment_added_on"])); ?></td>
					
					<td><?php if(($project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_status"] == "Pending")){?><a href="#" onclick="return project_approve_task_actual_contract_payment(<?php echo $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_id"]; ?>);">Approve</a><?php } ?></td>
					
					<td><?php if(($view_perms_list['status'] == SUCCESS)){?><a href="#" target="_blank" onclick="return go_to_contract_print(<?php echo $project_actual_contract_payment_list_data[$count]["project_actual_contract_payment_id"]; ?>);">Print</a><?php } ?></td>
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="15">No unaccepted contract payment yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
	  <?php } ?>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>

function project_approve_task_actual_contract_payment(contract_payment_id)
{
	var ok = confirm("Are you sure you want to Approve?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					alert(xmlhttp.responseText);
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					  window.location = "project_actual_contract_payment_list.php";
					}
				}
			}

			xmlhttp.open("POST", "project_approve_contract_payment.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("contract_payment_id=" + contract_payment_id + "&action=Approved");
		}
	}	
}
function go_to_contract_print(contract_payment_id)
{			
	var form = document.createElement("form");
    form.setAttribute("method", "get");
    form.setAttribute("action", "project_contract_weekly_print.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","contract_payment_id");
	hiddenField1.setAttribute("value",contract_payment_id);
	
	form.appendChild(hiddenField1);
	
	document.body.appendChild(form);
    form.submit();
}
</script>
</body>

</html>