<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: file_process_list.php
CREATED ON	: 06-Nov-2016
CREATED BY	: Nitin Kashyap
PURPOSE     : List of Process Plans for a file
*/

/*
TBD: 
1. Date display and calculation
2. Permission management
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'file_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'process'.DIRECTORY_SEPARATOR.'process_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET['bprocess']))
	{
		$bprocess_id  = $_GET['bprocess'];		
	}
	else
	{
		header("location:pending_project_list.php");
	}
	
	// Initialization
	$alert_type = -1;
	$alert      = '';

	$type   = '';
	$status = '';
	$order  = '';	
	
	$legal_task_plan_list = i_get_task_plan_list('','','','','','1','','slno',$bprocess_id);
	if($legal_task_plan_list["status"] == SUCCESS)
	{
		$legal_task_plan_list_data = $legal_task_plan_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$legal_task_plan_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Task List - Bulk Process</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Task List</h3>
            </div>
            <!-- /widget-header -->
						
            <div class="widget-content">
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
				    <th>SL No</th>	
					<th>Task</th>					
				    <th>Start Date</th>
					<th>End Date</th>	
					<th>Variance</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>								
				<?php
				if($legal_task_plan_list["status"] == SUCCESS)
				{		
					$sl_no = 0;
					for($count = 0; $count < count($legal_task_plan_list_data); $count++)
					{
						$sl_no++;
						
						$start_date = $legal_task_plan_list_data[$count]['task_plan_actual_start_date'];
						$end_date   = $legal_task_plan_list_data[$count]['task_plan_actual_end_date'];
						
						if(($start_date == '') || ($start_date == '0000-00-00'))
						{
							$start_date_display = '';
							$start_date         = date('Y-m-d');
						}
						else
						{
							$start_date_display = date('d-M-Y',strtotime($start_date));
						}
						
						if(($end_date == '') || ($end_date == '0000-00-00'))
						{
							$end_date_display = '';
							$end_date         = date('Y-m-d');
						}
						else
						{
							$end_date_display = date('d-M-Y',strtotime($end_date));
						}
						
						$variance = get_date_diff($start_date,$end_date);
					?>
					<tr>
						<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
						<td style="word-wrap:break-word;"><?php echo $legal_task_plan_list_data[$count]["task_type_name"]; ?></td>
						<td style="word-wrap:break-word;"><?php echo $start_date_display; ?></td>
						<td style="word-wrap:break-word;"><?php echo $end_date_display; ?></td>
						<td style="word-wrap:break-word;"><?php echo $variance['data']; ?></td>
						<td style="word-wrap:break-word;"><a href="reason_list.php?task=<?php echo $legal_task_plan_list_data[$count]["task_plan_legal_id"]; ?>" target="_blank">Remarks</a></td>
					</tr>
					<?php 												
					}														 				 
				}								
				?>								
                </tbody>
				</table>										 				
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function confirm_deletion(file_id)
{
	var ok = confirm("Are you sure you want to delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					 window.location = "pending_file_list.php";
				}
			}

			xmlhttp.open("GET", "legal_file_delete.php?file=" + file_id);   // file name where delete code is written
			xmlhttp.send();
		}
	}	
}

</script>

  </body>

</html>
