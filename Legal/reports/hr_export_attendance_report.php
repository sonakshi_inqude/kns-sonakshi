<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: Attendance Report
CREATED ON	: 26-Mar-2016
CREATED BY	: Nitin Kashyap
PURPOSE     : Attendance Report
*/

/*
TBD: 
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_employee_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_attendance_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'hr'.DIRECTORY_SEPARATOR.'hr_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'PHPExcel-1.8'.DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	if(($_GET["month"] != "") && ($_GET["year"] != ""))
	{
		$date_month = $_GET["month"];
		$date_year  = $_GET["year"];		
	}
	else
	{
		$date_month = "";
		$date_year  = "";		
	}

	// Temp data
	$alert      = "";
	$alert_type = "";		
	
	// Get list of employees
	$employee_filter_data = array("user_status"=>'1');
	$employee_list = i_get_employee_list($employee_filter_data);

	if($employee_list["status"] == SUCCESS)
	{
		$employee_list_data = $employee_list["data"];
	}
	else
	{
		$alert      = $alert."Alert: ".$employee_list["data"];
		$alert_type = 1;
	}
	
	// Check the number of days based on month
	$number_of_days = cal_days_in_month(CAL_GREGORIAN,$date_month,$date_year);
	
	// Month start date
	$month_start = "01-".$date_month."-".$date_year;
	$month_end	 = $number_of_days."-".$date_month."-".$date_year;
	
	/* Create excel sheet and write the column headers - START */
	// Instantiate a new PHPExcel object
	$objPHPExcel = new PHPExcel(); 
	// Set the active Excel worksheet to sheet 0
	$objPHPExcel->setActiveSheetIndex(0); 
	// Initialise the Excel row number
	$row_count = 1; 
	// Excel column identifier array
	$column_array = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
	// Set the first column
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "SL NO"); 
	// Set the second column
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, "EMPLOYEE"); 
	// Set the third column
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, "EMPLOYEE CODE"); 
	for($col_count = 3; $col_count < ($number_of_days + 3); $col_count++)
	{
		// Set the column for day of the month
		$date_month = date("d-M",strtotime($month_start." + ".($col_count - 3)." days"));
		$day_month  = date("D",strtotime($month_start." + ".($col_count - 3)." days"));
		$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$col_count].$row_count, $date_month."\r".$day_month); 
		$objPHPExcel->getActiveSheet()->getStyle($column_array[$col_count].$row_count)->getAlignment()->setWrapText(true);
	}
	// Set the present count column
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$col_count].$row_count, "TOTAL PRESENT"); 
	// Set the absent count column
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$col_count+1].$row_count, "TOTAL ABSENT"); 
	// Set the week off count column
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$col_count+2].$row_count, "WEEK OFF"); 
	// Set the public holidays count column
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$col_count+3].$row_count, "PUBLIC HOLIDAYS"); 
	// Set the CL (EL according to our app) count column
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$col_count+4].$row_count, "CL"); 
	// Set the SL count column
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$col_count+5].$row_count, "SL"); 
	// Set the Comp Off count column
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$col_count+6].$row_count, "CO");
	// Set the total payable count column
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$col_count+7].$row_count, "TOTAL PAYABLE");

	// Increment the Excel row counter
	$row_count++; 	 
	/* Create excel sheet and write the column headers - END */
	
	$sl_no = 1;
	$record_count = 0;
	if($employee_list["status"] == SUCCESS)
	{
		while($record_count < (count($employee_list_data)))
		{ 
			if((strtotime($employee_list_data[$record_count]["hr_employee_employment_date"])) <= strtotime($month_end))
			{
				// Initialize
				$present_count = 0;
				$el_count      = 0;
				$sl_count      = 0;
				$cl_count      = 0;
				$col_count     = 0;
				$week_off      = 0;
				$ph            = 0;
			
				// Set the first column
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, $sl_no); 
				// Set the second column
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $employee_list_data[$record_count]["hr_employee_name"]); 
				// Set the third column
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, $employee_list_data[$record_count]["hr_employee_code"]); 
				for($day_count = 3; $day_count < ($number_of_days + 3); $day_count++)
				{
					// Get this employee's attendance
					$date = date("Y-m-d",strtotime($month_start." + ".($day_count - 3)." days"));
					
					$attendance_filter_data = array();

					$attendance_filter_data["employee_id"]     = $employee_list_data[$record_count]["hr_employee_id"];
					$attendance_filter_data["attendance_date"] = $date;				

					$attendance_list = i_get_attendance_list($attendance_filter_data);
					
					// Get leave list
					$holiday_filter_data = array("date_start"=>$date,"date_end"=>$date,"status"=>'1');
					$holiday_list = i_get_holiday($holiday_filter_data);
						
					if($attendance_list["status"] == SUCCESS)
					{												
						$out_pass_filter_data = array('employee_id'=>$attendance_list['data'][0]["hr_attendance_employee"],"date"=>$attendance_list['data'][0]["hr_attendance_date"],"status"=>'1');
						$out_pass_sresult = i_get_out_pass_list($out_pass_filter_data);
						
						$op_in_time = false;
						if(($out_pass_sresult['status'] == SUCCESS) && ($attendance_list['data'][0]["hr_attendance_in_time"] != '00:00') && ($attendance_list['data'][0]["hr_attendance_in_time"] != '0:0') && ($attendance_list['data'][0]["hr_attendance_in_time"] != '0:00') && ($attendance_list['data'][0]["hr_attendance_out_time"] != '00:00') && ($attendance_list['data'][0]["hr_attendance_out_time"] != '0:0') && ($attendance_list['data'][0]["hr_attendance_out_time"] != '0:00'))
						{
							$op_in_time = t_check_in_time_proper($out_pass_sresult['data'][0]["hr_out_pass_out_time"],$out_pass_sresult['data'][0]["hr_out_pass_finish_time"]);						
							
							$total_working_hours = i_get_total_working_hours($attendance_list['data'][0]["hr_attendance_in_time"],$attendance_list['data'][0]["hr_attendance_out_time"],$out_pass_sresult['data'][0]["hr_out_pass_out_time"],$out_pass_sresult['data'][0]["hr_out_pass_finish_time"]);
						}
						else
						{						
							$total_working_hours = i_get_total_working_hours($attendance_list['data'][0]["hr_attendance_in_time"],$attendance_list['data'][0]["hr_attendance_out_time"],'00:00','00:00');
						}						

						// Check for clean swipe by employee
						$is_swipe_clean = i_get_employee_swipe($attendance_list['data'][0]["hr_attendance_in_time"],$attendance_list['data'][0]["hr_attendance_out_time"]);
						
						// Get attendance type
						if($is_swipe_clean != 0)
						{
							$attendance_type = '2';							
						}
						else
						{
							$attendance_type = i_get_attendance_type_from_value($total_working_hours['mins']);
						}
						
						$attendance_type_filter_data = array('type_id'=>$attendance_type);
						$attendance_type_name = i_get_attendance_type($attendance_type_filter_data);
						
						$attendance_disp = $attendance_type_name["data"][0]["hr_attendance_type_short_code"];
					
						switch($attendance_type)
						{
							case ATTENDANCE_TYPE_PRESENT:										
							if(date("l",strtotime($attendance_list["data"][0]["hr_attendance_date"])) == get_day($employee_list_data[$record_count]["hr_employee_week_off"]))
							{
								$attendance_disp = "WWO";	
								$week_off++;
							}
							else if($holiday_list["status"] == SUCCESS)
							{
								$attendance_disp = "WPH";
								$ph++;
							}
							else						
							{
								// Retain the attendance display value
								$present_count = $present_count + 1;
							}
							break;
							
							case ATTENDANCE_TYPE_HALFDAY:										
							if(date("l",strtotime($attendance_list["data"][0]["hr_attendance_date"])) == get_day($employee_list_data[$record_count]["hr_employee_week_off"]))
							{
								$attendance_disp = "HWWO";		
								$week_off++;
							}
							else if($holiday_list["status"] == SUCCESS)
							{
								$attendance_disp = "HWPH";
								$ph++;
							}
							else
							{
								$leave_filter_data = array("employee_id"=>$attendance_list["data"][0]["hr_employee_id"],"date"=>$attendance_list["data"][0]["hr_attendance_date"],"status"=>LEAVE_STATUS_APPROVED);
								$leave_sresult = i_get_leave_list($leave_filter_data);
								if($leave_sresult["status"] == SUCCESS)
								{
									for($lt_count = 0; $lt_count < count($leave_sresult['data']); $lt_count++)
									{
										if($lt_count == 0)
										{
											$attendance_disp = "H".$leave_sresult["data"][$lt_count]["hr_attendance_type_short_code"];
										}
										else								
										{
											$attendance_disp = $attendance_disp.' '."H".$leave_sresult["data"][$lt_count]["hr_attendance_type_short_code"];
										}
										
										switch($leave_sresult["data"][$lt_count]["hr_absence_type"])
										{
										case LEAVE_TYPE_EARNED:
										$el_count = $el_count + 0.5;
										break;
										
										case LEAVE_TYPE_SICK:
										$sl_count = $sl_count + 0.5;
										break;
										
										case LEAVE_TYPE_CASUAL:
										$cl_count = $cl_count + 0.5;
										break;
										
										case LEAVE_TYPE_COMP_OFF:
										$col_count = $col_count + 0.5;
										break;
									}
								}
							}
							else
							{						
								// Retain the attendance display value							
							}
							$present_count = $present_count + 0.5;
						}
						break;
					
						case ATTENDANCE_TYPE_ABSENT:					
						if(date("l",strtotime($attendance_list["data"][0]["hr_attendance_date"])) == get_day($employee_list_data[$record_count]["hr_employee_week_off"]))
						{
							$attendance_disp = "WO";
							// Set grey color to indicate week off
							$objPHPExcel->getActiveSheet()->getStyle($column_array[$day_count].$row_count)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C0C0C0');	
							$objPHPExcel->getActiveSheet()->getStyle($column_array[$day_count].'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C0C0C0');
							$week_off++;
						}
						else if($holiday_list["status"] == SUCCESS)
						{
							$attendance_disp = "HO";
							// Set green color to indicate week off
							$objPHPExcel->getActiveSheet()->getStyle($column_array[$day_count].$row_count)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00FFFF');	
							$objPHPExcel->getActiveSheet()->getStyle($column_array[$day_count].'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00FFFF');
							$ph++;
						}
						else
						{
							$leave_filter_data = array("employee_id"=>$attendance_list["data"][0]["hr_employee_id"],"date"=>$attendance_list["data"][0]["hr_attendance_date"],"status"=>LEAVE_STATUS_APPROVED);
							$leave_sresult = i_get_leave_list($leave_filter_data);
							if($leave_sresult["status"] == SUCCESS)
							{	
								$attendance_disp = '';
								for($lt_count = 0; $lt_count < count($leave_sresult['data']); $lt_count++)
								{															
									switch($leave_sresult["data"][$lt_count]["hr_absence_type"])
									{
									case LEAVE_TYPE_EARNED:
									if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_PRESENT)
									{
										$el_count++;
										$attendance_disp = $attendance_disp.$leave_sresult["data"][$lt_count]["hr_attendance_type_short_code"].',';
									}
									else if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_HALFDAY)
									{
										$el_count = $el_count + 0.5;			
										$attendance_disp = $attendance_disp."H".$leave_sresult["data"][$lt_count]["hr_attendance_type_short_code"].',';
									}							
									break;
									
									case LEAVE_TYPE_SICK:
									if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_PRESENT)
									{
										$sl_count++;
										$attendance_disp = $attendance_disp.$leave_sresult["data"][$lt_count]["hr_attendance_type_short_code"].',';
									}
									else if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_HALFDAY)
									{
										$sl_count = $sl_count + 0.5;			
										$attendance_disp = $attendance_disp."H".$leave_sresult["data"][$lt_count]["hr_attendance_type_short_code"].',';
									}							
									break;
									
									case LEAVE_TYPE_CASUAL:
									if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_PRESENT)
									{
										$cl_count++;
										$attendance_disp = $attendance_disp.$leave_sresult["data"][$lt_count]["hr_attendance_type_short_code"].',';
									}
									else if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_HALFDAY)
									{
										$cl_count = $cl_count + 0.5;			
										$attendance_disp = $attendance_disp."H".$leave_sresult["data"][$lt_count]["hr_attendance_type_short_code"].',';
									}							
									break;
									
									case LEAVE_TYPE_COMP_OFF:
									if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_PRESENT)
									{
										$col_count++;
										$attendance_disp = $attendance_disp.$leave_sresult["data"][$lt_count]["hr_attendance_type_short_code"].',';
									}
									else if($leave_sresult["data"][$lt_count]["hr_absence_duration"] == ATTENDANCE_TYPE_HALFDAY)
									{
										$col_count = $col_count + 0.5;			
										$attendance_disp = $attendance_disp."H".$leave_sresult["data"][$lt_count]["hr_attendance_type_short_code"].',';
									}							
									break;
									}
								}
								$attendance_disp = trim($attendance_disp,',');
								}
								else
								{						
									// Nothing here
								}
							}
							break;
							}
						}
						else
						{
							if(date("l",strtotime($date)) == get_day($employee_list_data[$record_count]["hr_employee_week_off"]))
							{
								$attendance_disp = "WO";
								$objPHPExcel->getActiveSheet()->getStyle($column_array[$day_count].$row_count)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C0C0C0');
								$objPHPExcel->getActiveSheet()->getStyle($column_array[$day_count].'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C0C0C0');
								$week_off++;
							}
							else if($holiday_list["status"] == SUCCESS)
							{
								$attendance_disp = "HO";
								// Set green color to indicate week off
								$objPHPExcel->getActiveSheet()->getStyle($column_array[$day_count].$row_count)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00FFFF');	
								$objPHPExcel->getActiveSheet()->getStyle($column_array[$day_count].'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00FFFF');
								$ph++;
							}
							else
							{
								$attendance_disp = "A";
							}					
						}								
					
						$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$day_count].$row_count, $attendance_disp); 
					}
			
					// Set the present count column
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$day_count].$row_count, $present_count);
					// Set the absent count column
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$day_count+1].$row_count, $number_of_days - ($present_count + $week_off + $ph + $el_count + $sl_count + $col_count));
					// Set the week off count column
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$day_count+2].$row_count, $week_off);
					// Set the public holiday count column
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$day_count+3].$row_count, $ph);
					// Set the earned leave column
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$day_count+4].$row_count, $el_count);
					// Set the sick leave column
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$day_count+5].$row_count, $sl_count);
					// Set the comp off column
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$day_count+6].$row_count, $col_count);
					// Set the total payable column
					$objPHPExcel->getActiveSheet()->SetCellValue($column_array[$day_count+7].$row_count, ($present_count + $week_off + $ph + $el_count + $sl_count + $col_count));
					// Increment the Excel row counter
					$row_count++; 
					// Increment the serial number
					$sl_no++;
			}
			$record_count++;
		}
			header('Content-Type: application/vnd.ms-excel'); 
			header('Content-Disposition: attachment;filename="attendance_report_'.date("d-M-Y").'.xls"'); 
			header('Cache-Control: max-age=0'); 
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
			$objWriter->save('php://output');
		}
		else
		{
			echo "No results for this search!";
		}
	}
	else
	{
		header("location:login.php");
	}	
?>