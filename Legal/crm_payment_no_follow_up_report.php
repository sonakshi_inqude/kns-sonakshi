<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_payment_follow_up_report.php
CREATED ON	: 21-Sep-2016
CREATED BY	: Nitin Kashyap
PURPOSE     : Payment follow up report
*/

/*
TBD: 
*/$_SESSION['module'] = 'CRM Reports';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing here

	// Temp data
	$alert      = "";
	$alert_type = -1;
	
	if(isset($_POST['search_follow_up']))
	{
		$project  = $_POST['ddl_project'];
		$site_no  = $_POST['stxt_site_no'];
		$status   = $_POST['ddl_status'];
	}
	else
	{
		$project = '';
		$site_no = '';
		$status  = '';
	}
	
	// Get booking details
	$booking_sresult = i_get_site_booking('',$project,'','','','1','','','','','','','','','','',$site_no,'','','','','',$user);
	if($booking_sresult['status'] == SUCCESS)
	{
		$booking_list = $booking_sresult['data'];		
	}
	else
	{
		// No data
	}
	
	// User List
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
	
	// Get project list	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"active"=>'1',"project_active"=>'1');	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);	if($project_list["status"] == SUCCESS)	{		$project_list_data = $project_list["data"];	}	else	{		$alert = $alert."Alert: ".$project_list["data"];		$alert_type = 0; // Failure	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>No Payment Follow Up Added - Report</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header" style="height:50px;">
              <h3>No Payment Follow Up Report&nbsp;&nbsp;&nbsp;Pending Amount: <span id="total_pending_amount"><i>Calculating</i></span></h3>
            </div>			
            <!-- /widget-header -->
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="payment_fup_search" action="crm_payment_no_follow_up_report.php">			  
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_project">
			  <option value="">- - Select Project - -</option>
			  <?php
				for($count = 0; $count < count($project_list_data); $count++)
				{
					?>					
					<option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php 
					if($project == $project_list_data[$count]["project_id"])
					{
					?>					
					selected="selected"
					<?php
					}?>><?php echo $project_list_data[$count]["project_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="text" name="stxt_site_no" value="<?php echo $site_no; ?>" placeholder="Search by site no" />
			  </span>			  
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_status">
			  <option value="">- - Select Status - -</option>
			  <option value="<?php echo BOOKED_STATUS; ?>" <?php if($status == BOOKED_STATUS) { ?> selected <?php } ?>>Booked</option>
			  <option value="<?php echo AGREEMENT_STATUS; ?>" <?php if($status == AGREEMENT_STATUS) { ?> selected <?php } ?>>Agreement</option>
			  <option value="<?php echo REGISTERED_STATUS; ?>" <?php if($status == REGISTERED_STATUS) { ?> selected <?php } ?>>Registration</option>
			  <option value="<?php echo KATHA_TRANSFER_STATUS; ?>" <?php if($status == KATHA_TRANSFER_STATUS) { ?> selected <?php } ?>>Katha Transfer</option>
			  </select>
			  </span>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="search_follow_up" />
			  </span>
			  </form>
			</div>
			
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>										
					<th>Project</th>
					<th>Site No.</th>										
					<th>Client Name</th>
					<th>Client No</th>
					<th>Booking Date</th>
					<th>Lead Time</th>			
					<th>Latest Status</th>
					<th>Pending Amount</th>																									
					<th>Delay Reason</th>
					<th>Follow Up By</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>							
				<?php			
				$total_pending_amount      = 0;
				if($booking_sresult["status"] == SUCCESS)
				{										
					$sl_no = 0;											
					for($count = 0; $count < count($booking_list); $count++)
					{		
						$dont_display = false;
						$color        = 'black';
						
						$kt_sresult = i_get_katha_transfer_list('',$booking_list[$count]["crm_booking_id"],'','','1','','','','','','','');						
						if($dont_display == false)
						{
							// Booking data
							$current_status = 'Booking';
							if($booking_list[$count]["crm_booking_date"] != "0000-00-00")
							{
								$booking_date = date("d-M-Y",strtotime($booking_list[$count]["crm_booking_date"])); 
								$booking_date_l = $booking_list[$count]["crm_booking_date"];
								$current_status = 'Booked';							
								
								$lead_data = get_date_diff($booking_list[$count]["crm_booking_date"],date('Y-m-d'));
							}
							else
							{
								$booking_date = "NA. Approved Date: ".date("d-M-Y",strtotime($booking_list[$count]["crm_booking_approved_on"])); 
								$booking_date_l = $booking_list[$count]["crm_booking_approved_on"];
								$lead_data = get_date_diff($booking_list[$count]["crm_booking_approved_on"],date('Y-m-d'));
							}
							
							// Customer Follow Up Data
							$fup_remarks   = 'NO FOLLOW UP';
							$fup_notes     = '';
							$fup_date      = 'NO FOLLOW UP';
							$color         = 'red';
							$fup_user      = 'NOT ASSIGNED';
							$fup_lead_data = get_date_diff($booking_date_l,date('Y-m-d'));
							$fup_user_name = 'NOT ASSIGNED';
							$customer_fup = i_get_payment_fup_list('',$booking_list[$count]["crm_booking_id"],'','','','','','','','','','added_date_asc');
							if($customer_fup['status'] == SUCCESS)
							{																
								if($customer_fup['data'][count($customer_fup['data']) - 1]['crm_payment_follow_up_cust_remarks'] == '')
								{
									$dont_display = true;
								}																
							}
							
							if($fup_lead_data['data'] <= 0)
							{
								$color = 'black';
							}
							
							// Get Payment Data
							$total_payment_done = 0;
							$payment_details = i_get_payment('',$booking_list[$count]["crm_booking_id"],'','','');
							if($payment_details["status"] == SUCCESS)
							{
								for($pay_count = 0; $pay_count < count($payment_details["data"]); $pay_count++)
								{
									$total_payment_done = $total_payment_done + $payment_details["data"][$pay_count]["crm_payment_amount"];
								}
							}
							else						
							{
								$total_payment_done = 0;
							}
							
							if($booking_list[$count]["crm_booking_consideration_area"] != "0")
							{
								$consideration_area = $booking_list[$count]["crm_booking_consideration_area"];						
							}
							else
							{
								$consideration_area = $booking_list[$count]["crm_site_area"];
							}
						
							$pending_payment = ($consideration_area * $booking_list[$count]["crm_booking_rate_per_sq_ft"]) - $total_payment_done;
							$pending_payment = round($pending_payment);
															
							if(($pending_payment <= 0) && ($kt_sresult['status'] == SUCCESS))
							{
								$dont_display = true;								
							}							
							
							if(($status != '') && ($booking_list[$count]["crm_site_status"] != $status))
							{
								$dont_display = true;								
							}																					
							
							if($dont_display == false)
							{																																
								$agreement_list = i_get_agreement_list('',$booking_list[$count]["crm_booking_id"],'','','','','','','','','');
								if($agreement_list["status"] == SUCCESS)
								{							
									$current_status = 'Agreement';							
								}
								else
								{							
									// Do nothing
								}											
														
								$registration_list = i_get_registration_list('',$booking_list[$count]["crm_booking_id"],'','','','','','','','','');
								if($registration_list["status"] == SUCCESS)
								{							
									$current_status = 'Registration';							
								}
								else
								{														
									// Do nothing
								}	

								// Get delay reason data
								$delay_reason_list = i_get_delay_reason_list($booking_list[$count]["crm_booking_id"],'','1');
								if($delay_reason_list["status"] == SUCCESS)
								{
									$latest_delay_reason = strtoupper($delay_reason_list["data"][0]["crm_delay_reason_master_name"]);
								}
								else
								{
									$latest_delay_reason = 'NO REASON';									
								}
																													
								?>
								<tr style="color:<?php echo $color; ?>;">														
								<td style="word-wrap:break-word;"><?php echo $booking_list[$count]["project_name"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo $booking_list[$count]["crm_site_no"]; ?> (<?php echo $booking_list[$count]["crm_site_area"].' sq. ft'; ?>)</td>
								<td style="word-wrap:break-word;"><?php echo $booking_list[$count]["name"]; ?></td>
								<td style="word-wrap:break-word;"><?php echo $booking_list[$count]["cell"]; ?></td>							
								<td style="word-wrap:break-word;"><?php echo $booking_date; ?></td>
								<td style="word-wrap:break-word;"><?php echo $lead_data['data']; ?></td>
								<td style="word-wrap:break-word;"><?php echo $current_status; ?></td>
								<td style="word-wrap:break-word;"><?php 
								if($booking_list[$count]["crm_booking_consideration_area"] != "0")
								{
									$consideration_area = $booking_list[$count]["crm_booking_consideration_area"];
								}
								else
								{
									$consideration_area = $booking_list[$count]["crm_site_area"];
								}
								$total_payment_to_be_done = ($consideration_area * $booking_list[$count]["crm_booking_rate_per_sq_ft"]);
								if(($total_payment_to_be_done - $total_payment_done) > 5)
								{
									$this_pending_amount = round($total_payment_to_be_done - $total_payment_done); 									
								}
								else
								{
									$this_pending_amount = 0;
								}
								echo $this_pending_amount;
								$total_pending_amount = $total_pending_amount + $this_pending_amount;
								?></td>
								<td style="word-wrap:break-word;"><?php echo $latest_delay_reason; ?></td>
								<td style="word-wrap:break-word;"><?php echo $fup_user_name; ?></td>
								<td><a href="crm_add_payment_fup.php?booking=<?php echo $booking_list[$count]["crm_booking_id"]; ?>" target="_blank">Follow Up Details</a></td>							
								</tr>
								<?php															
							}
						}
					}
				}
				else
				{
				?>
				<td colspan="13">No site for follow up!</td>
				<?php
				}
				?>	
				<script>				
				document.getElementById("total_pending_amount").innerHTML = '<?php echo "Rs. ".$total_pending_amount; ?>';				
				</script>
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script><script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>


  </body>

</html>