<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. In db_get_process_list, name search should be wildcard search
*/

/*
PURPOSE : To add new site project
INPUT 	: File ID, Project ID, Added By, Added On, Updated by, Updated On
OUTPUT 	: Site Project Mapping ID, success or failure message
BY 		: Punith
*/
function db_add_site_project($file_id,$project_id,$added_by)
{
	// Query
    $site_project_iquery = "insert into site_project_mapping (site_project_file_id,site_project_id,site_project_status,site_project_added_by,site_project_added_on,site_project_updated_by,site_project_updated_on) values (:file_id,:project_id,:status,:added_by,:added_on,:updated_by,:updated_on)";    
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $site_project_istatement = $dbConnection->prepare($site_project_iquery);
        
        // Data
        $site_project_idata = array(':file_id'=>$file_id,':project_id'=>$project_id,':status'=>'1',':added_by'=>$added_by,':added_on'=>date('Y-m-d H:i:s'),':updated_by'=>$added_by,':updated_on'=>date('Y-m-d H:i:s'));
    	$dbConnection->beginTransaction();
        $site_project_istatement->execute($site_project_idata);
		$site_project_mapping_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $site_project_mapping_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;

}

/*
PURPOSE : To get site project list
INPUT 	: File ID, Project ID, Status, Added By, Start Date, End Date, Updated By, Updated On
OUTPUT 	: List of site project mapping
BY 		: Punith
*/
function db_get_site_project($file_id,$project_id,$survey_no,$status,$added_by,$start_date,$end_date,$updated_by,$updated_on)
{
	$get_site_project_squery_base = "select * from site_project_mapping SPM inner join crm_project_master CPM on CPM.project_id = SPM.site_project_id inner join files F on F.file_id = SPM.site_project_file_id left outer join bd_project_files BPF on BPF.bd_project_file_id = F.file_bd_file_id left outer join bd_own_account_master BOAM on BOAM.bd_own_accunt_master_id = BPF.bd_file_own_account";
	
	$get_site_project_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_site_project_sdata = array();
	
	if($file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." where site_project_file_id = :file_id";								
		}
		else
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." and site_project_file_id = :file_id";				
		}
		
		// Data
		$get_site_project_sdata[':file_id']  = $file_id;
		
		$filter_count++;
	}
	
	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." where site_project_id = :project_id";								
		}
		else
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." and site_project_id = :project_id";				
		}
		
		// Data
		$get_site_project_sdata[':project_id']  = $project_id;
		
		$filter_count++;
	}
	
	if($survey_no != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." where BPF.bd_file_survey_no = :survey_no";								
		}
		else
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." and BPF.bd_file_survey_no = :survey_no";				
		}
		
		// Data
		$get_site_project_sdata[':survey_no']  = $survey_no;
		
		$filter_count++;
	}
	
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." where site_project_status = :status";								
		}
		else
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." and site_project_status = :status";				
		}
		
		// Data
		$get_site_project_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." where site_project_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." and site_project_added_by = :added_by";				
		}
		
		// Data
		$get_site_project_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." where site_project_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." and site_project_added_on >= :start_date";				
		}
		
		//Data
		$get_site_project_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." where site_project_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." and site_project_added_on <= :end_date";				
		}
		
		//Data
		$get_site_project_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	if($updated_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." where site_project_updated_by = :updated_by";								
		}
		else
		{
			// Query
			$get_site_project_squery_where = $get_site_project_squery_where." and site_project_updated_by = :updated_by";				
		}
		
		// Data
		$get_site_project_sdata[':updated_by']  = $updated_by;
		
		$filter_count++;
	}
	
	$get_site_project_squery = $get_site_project_squery_base.$get_site_project_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_site_project_sstatement = $dbConnection->prepare($get_site_project_squery);
		
		$get_site_project_sstatement -> execute($get_site_project_sdata);
		
		$get_site_project_sdetails = $get_site_project_sstatement -> fetchAll();
		
		if(FALSE === $get_site_project_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_site_project_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_site_project_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update site project details
INPUT 	: Mapping ID, File ID, Project ID, Status, Updated By
OUTPUT 	: File Project Mapping ID; Message of success or failure
BY 		: Nitin Kashyap
*/
function db_update_site_project($site_project_mapping_id,$file_id,$project_id,$status,$updated_by)
{
	// Query
    $site_project_uquery_base = "update site_project_mapping set";  
	
	$site_project_uquery_set = "";
	
	$site_project_uquery_where = " where site_project_mapping_id = :mapping_id";
	
	$site_project_udata = array(":mapping_id"=>$site_project_mapping_id);
	
	$filter_count = 0;
	
	if($file_id != "")
	{
		$site_project_uquery_set = $site_project_uquery_set." site_project_file_id = :file_id,";
		$site_project_udata[":file_id"] = $file_id;
		$filter_count++;
	}
	
	if($project_id != "")
	{
		$site_project_uquery_set = $site_project_uquery_set." site_project_id = :project_id,";
		$site_project_udata[":project_id"] = $project_id;
		$filter_count++;
	}
	
	if($status != "")
	{
		$site_project_uquery_set = $site_project_uquery_set." site_project_status = :status,";
		$site_project_udata[":status"] = $status;
		$filter_count++;
	}
	
	if($updated_by != "")
	{
		$site_project_uquery_set = $site_project_uquery_set." site_project_updated_by = :updated_by,";
		$site_project_udata[":updated_by"] = $updated_by;
		$filter_count++;
	}

	if($updated_on != "")
	{
		$site_project_uquery_set = $site_project_uquery_set." site_project_updated_on = :updated_on,";
		$site_project_udata[":updated_on"] = date('Y-m-d H:i:s');
		$filter_count++;
	}
		
	if($filter_count > 0)
	{
		$site_project_uquery_set = trim($site_project_uquery_set,',');
	}
	
	$site_project_uquery = $site_project_uquery_base.$site_project_uquery_set.$site_project_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $site_project_ustatement = $dbConnection->prepare($site_project_uquery);		
        
        $site_project_ustatement -> execute($site_project_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $file_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new site approval request
INPUT 	: File ID, Approving Authority, Added by
OUTPUT 	: Site Approval ID, success or failure message
BY 		: Punith
*/
function db_add_site_approval($file_id,$approving_authority,$added_by)
{
	// Query
    $site_approval_iquery = "insert into site_approval (site_approval_file_id,site_approval_status,site_approval_remarks,site_approval_approving_authority,site_approval_added_by,site_approval_added_on) values (:file_id,:status,:remarks,:approving_authority,:added_by,:now)";  

    try
    {
        $dbConnection = get_conn_handle();
        
        $site_approval_istatement = $dbConnection->prepare($site_approval_iquery);
        
        // Data        
		$site_approval_idata = array(':file_id'=>$file_id,':status'=>'0',':remarks'=>'',':approving_authority'=>$approving_authority,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		
		$dbConnection->beginTransaction();
        $site_approval_istatement->execute($site_approval_idata);
		$site_approval_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $site_approval_id;		
    }
    catch(PDOException $e)
    {
		// Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get site approval list
INPUT 	: File ID, Status, Approving Authority, Added By, Start Date, End Date, Project ID, Site Approval ID
OUTPUT 	: List of site approvals
BY 		: punith
*/
function db_get_site_approval($file_id,$status,$approving_authority,$added_by,$start_date,$end_date,$project_id='',$approval_id='')
{
	$get_site_approval_squery_base = "select * from site_approval SA left outer join files F on SA.site_approval_file_id = F.file_id left outer join bd_project_files BPF on BPF.bd_project_file_id = F.file_bd_file_id inner join village_master VM on VM.village_id = BPF.bd_file_village left outer join site_project_mapping SPM on SPM.site_project_file_id = SA.site_approval_file_id";
	
	$get_site_approval_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_site_approval_sdata = array();
	
	if($file_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." where site_approval_file_id = :file_id";								
		}
		else
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." and site_approval_file_id = :file_id";				
		}
		
		// Data
		$get_site_approval_sdata[':file_id']  = $file_id;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." where site_approval_status = :status";								
		}
		else
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." and site_approval_status = :status";				
		}
		
		// Data
		$get_site_approval_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($approving_authority != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." where site_approval_approving_authority = :approving_authority";								
		}
		else
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." and site_approval_approving_authority = :approving_authority";				
		}
		
		// Data
		$get_site_approval_sdata[':approving_authority']  = $approving_authority;
		
		$filter_count++;
	}
	
	if($project_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." where SPM.site_project_id = :project";
		}
		else
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." and SPM.site_project_id = :project";				
		}
		
		// Data
		$get_site_approval_sdata[':project']  = $project_id;
		
		$filter_count++;
	}
	
	if($approval_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." where site_approval_id = :approval_id";
		}
		else
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." and site_approval_id = :approval_id";				
		}
		
		// Data
		$get_site_approval_sdata[':approval_id']  = $approval_id;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." where site_approval_added_by = :added_by";								
		}
		else
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." and site_approval_added_by = :added_by";				
		}
		
		// Data
		$get_site_approval_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." where site_approval_added_on >= :start_date";								
		}
		else
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." and site_approval_added_on >= :start_date";				
		}
		
		//Data
		$get_site_approval_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." where site_approval_added_on <= :end_date";								
		}
		else
		{
			// Query
			$get_site_approval_squery_where = $get_site_approval_squery_where." and site_approval_added_on <= :end_date";				
		}
		
		//Data
		$get_site_approval_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_site_approval_squery = $get_site_approval_squery_base.$get_site_approval_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_site_approval_sstatement = $dbConnection->prepare($get_site_approval_squery);
		
		$get_site_approval_sstatement -> execute($get_site_approval_sdata);
		
		$get_site_approval_sdetails = $get_site_approval_sstatement -> fetchAll();
		
		if(FALSE === $get_site_approval_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_site_approval_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_site_approval_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update site approval details
INPUT 	: Approval ID, Status, Remarks, Approving Authority, Added_by
OUTPUT 	: Approval ID; Message of success or failure
BY 		: Nitin Kashyap
*/
function db_update_site_approval($approval_id,$status,$remarks,$approving_authority,$added_by)
{
	// Query
    $site_approval_uquery_base = "update site_approval set";  
	
	$site_approval_uquery_set = "";
	
	$site_approval_uquery_where = " where site_approval_id = :approval_id";
	
	$site_approval_udata = array(":approval_id"=>$approval_id);
	
	$filter_count = 0;
	
	if($status != "")
	{
		$site_approval_uquery_set       = $site_approval_uquery_set." site_approval_status = :status,";
		$site_approval_udata[":status"] = $status;
		$filter_count++;
	}
	
	if($remarks != "")
	{
		$site_approval_uquery_set        = $site_approval_uquery_set." site_approval_remarks = :remarks,";
		$site_approval_udata[":remarks"] = $remarks;
		$filter_count++;
	}
	
	if($approving_authority != "")
	{
		$site_approval_uquery_set                    = $site_approval_uquery_set." site_approval_approving_authority = :approving_authority,";
		$site_approval_udata[":approving_authority"] = $approving_authority;
		$filter_count++;
	}
	
	if($added_by != "")
	{
		$site_approval_uquery_set        = $site_approval_uquery_set." site_approval_added_by = :added_by,";
		$site_approval_udata["added_by"] = $added_by;
		$filter_count++;
	}	
	
	if($filter_count > 0)
	{
		$site_approval_uquery_set = trim($site_approval_uquery_set,',');
	}
	
	$site_approval_uquery = $site_approval_uquery_base.$site_approval_uquery_set.$site_approval_uquery_where;

    try
    {
        $dbConnection = get_conn_handle();
        
        $site_approval_ustatement = $dbConnection->prepare($site_approval_uquery);		
        
        $site_approval_ustatement -> execute($site_approval_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $approval_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
?>