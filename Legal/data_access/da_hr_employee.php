<?php
/**
 * @author Perumal
 * @copyright 2015
 */
 
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

/*
PURPOSE : To add new Employee
INPUT   : User ID of the employee, Employee Name, Employee Postal Address, Employee Permanent Address, Father's Name, DOB, Employment Date, Employee Code, Remarks, Added By
OUTPUT 	: Employee ID id, success or failure message
BY 		: Nitin
*/
function db_add_employee($employee_user,$employee_name,$employee_postal_address,$employee_permanent_address,$employee_father_name,$employee_dob,$employee_employment_date,$employee_code,$employee_week_off,$remarks,$added_by)
{
	// Query
    $employee_iquery = "insert into hr_employee (hr_employee_id,hr_employee_user,hr_employee_name,hr_employee_postal_address,hr_employee_permanent_address,hr_employee_father_name,hr_employee_dob,hr_employee_employment_date,
	hr_employee_code,hr_employee_week_off,hr_employee_status,hr_employee_remarks,hr_employee_added_by,hr_employee_added_date_time) values (:employee_id,:employee_user,:employee_name,:employee_postal_address,:employee_permanent_address,:employee_father_name,:employee_dob,:employee_employment_date,:employee_code,:employee_week_off,:employee_status,:remarks,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $user_istatement = $dbConnection->prepare($employee_iquery);
        
        // Data
		$employee_id = generate_unique_id();
        $employee_idata = array(':employee_id'=>$employee_id,':employee_user'=>$employee_user,':employee_name'=>$employee_name,':employee_postal_address'=>$employee_postal_address,':employee_permanent_address'=>$employee_permanent_address,':employee_father_name'=>$employee_father_name,':employee_dob'=>$employee_dob,':employee_employment_date'=>$employee_employment_date,':employee_code'=>$employee_code,':employee_week_off'=>$employee_week_off,':employee_status'=>'1',':remarks'=>$remarks,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));
		
        $dbConnection->beginTransaction();
        $user_istatement->execute($employee_idata);
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $employee_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get employee list
INPUT 	: Employee Filter Data: Employee ID, Name, Emp Code, Status
OUTPUT 	: Employee List
BY 		: perumal
*/
function db_get_employee_list($employee_filter_data)
{
	$get_employee_list_squery_base = "select *,AU.user_name as manager,AU.user_email_id as manager_email,U.user_email_id as email from hr_employee E inner join users U on U.user_id = E.hr_employee_user inner join users AU on AU.user_id = U.user_manager";
	
	$get_employee_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_employee_list_sdata = array();
	
	if(array_key_exists("employee_id",$employee_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." where hr_employee_id=:employee_id";								
		}
		else
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." and hr_employee_id=:employee_id";				
		}
		
		// Data
		$get_employee_list_sdata[':employee_id']  = $employee_filter_data["employee_id"];
		
		$filter_count++;
	}
	
	if(array_key_exists("employee_user",$employee_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." where hr_employee_user=:employee_user";								
		}
		else
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." and hr_employee_user=:employee_user";				
		}
		
		// Data
		$get_employee_list_sdata[':employee_user']  = $employee_filter_data["employee_user"];
		
		$filter_count++;
	}
	
	if(array_key_exists("employee_name",$employee_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." where hr_employee_name like :employee_name";								
		}
		else
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." and hr_employee_name like :employee_name";				
		}
		
		// Data
		$get_employee_list_sdata[':employee_name']  = '%'.$employee_filter_data["employee_name"].'%';
		
		$filter_count++;
	}

	if(array_key_exists("employee_code",$employee_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." where hr_employee_code = :employee_code";								
		}
		else
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." and hr_employee_code = :employee_code";				
		}
		
		// Data
		$get_employee_list_sdata[':employee_code']  = $employee_filter_data["employee_code"];
		
		$filter_count++;
	}if(array_key_exists("employee_week_off",$employee_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." where hr_employee_week_off = :employee_week_off";								
		}
		else
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." and hr_employee_week_off = :employee_week_off";				
		}
		
		// Data
		$get_employee_list_sdata[':employee_week_off']  = $employee_filter_data["employee_week_off"];
		
		$filter_count++;
	}
	
	if(array_key_exists("employee_status",$employee_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." where hr_employee_status = :employee_status";								
		}
		else
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." and hr_employee_status = :employee_status";				
		}
		
		// Data
		$get_employee_list_sdata[':employee_status']  = $employee_filter_data["employee_status"];
		
		$filter_count++;
	}
	
	if(array_key_exists("user_status",$employee_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." where U.user_active = :user_status";								
		}
		else
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." and U.user_active = :user_status";				
		}
		
		// Data
		$get_employee_list_sdata[':user_status']  = $employee_filter_data["user_status"];
		
		$filter_count++;
	}
	
	if(array_key_exists("employee_manager",$employee_filter_data))
	{
		if($filter_count == 0)
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." where U.user_manager = :manager";								
		}
		else
		{
			// Query
			$get_employee_list_squery_where = $get_employee_list_squery_where." and U.user_manager = :manager";				
		}
		
		// Data
		$get_employee_list_sdata[':manager']  = $employee_filter_data["employee_manager"];
		
		$filter_count++;
	}
	
	$get_employee_list_squery_order = " order by hr_employee_code asc";
	
	$get_employee_list_squery = $get_employee_list_squery_base.$get_employee_list_squery_where.$get_employee_list_squery_order;	
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_employee_list_sstatement = $dbConnection->prepare($get_employee_list_squery);
		
		$get_employee_list_sstatement -> execute($get_employee_list_sdata);
		
		$get_employee_list_sdetails = $get_employee_list_sstatement -> fetchAll();
		
		if(FALSE === $get_employee_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_employee_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_employee_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To add Employee Termination
INPUT   : Employee ID, Termination Date, Reason, Added By
OUTPUT 	: Termination id, success or failure message
BY 		: Nitin
*/
function db_add_employee_termination($employee_id,$termination_date,$reason,$added_by)
{
	// Query
    $employee_termination_iquery = "insert into employee_termination (employee_termination_employee_id,employee_termination_date,employee_termination_reason,employee_termination_added_by,employee_termination_added_on) values (:employee_id,:termination_date,:reason,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $employee_termination_istatement = $dbConnection->prepare($employee_termination_iquery);
        
        // Data		
        $employee_termination_idata = array(':employee_id'=>$employee_id,':termination_date'=>$termination_date,':reason'=>$reason,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));
		
        $dbConnection->beginTransaction();
        $employee_termination_istatement->execute($employee_termination_idata);
		$employee_termination_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = $employee_termination_id;		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get employee termination list
INPUT 	: Employee, Bonus Type, Added By, Start Date, End Date
OUTPUT 	: bonus details
BY 		: perumal
*/
function db_get_termination_list($employee_id,$termination_date,$added_by,$start_date,$end_date)
{
	$get_termination_list_squery_base = "select * from employee_termination";
	
	$get_termination_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_termination_list_sdata = array();
	
	if($employee_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_termination_list_squery_where = $get_termination_list_squery_where." where employee_termination_employee_id=:employee_id";					
		}
		else
		{
			// Query
			$get_termination_list_squery_where = $get_termination_list_squery_where." and employee_termination_employee_id=:employee_id";				
		}
		
		// Data
		$get_termination_list_sdata[':employee_id']  = $employee_id;
		
		$filter_count++;
	}
	
	if($termination_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_termination_list_squery_where = $get_termination_list_squery_where." where employee_termination_date=:termination_date";							
		}
		else
		{
			// Query
			$get_termination_list_squery_where = $get_termination_list_squery_where." and employee_termination_date=:termination_date";				
		}
		
		// Data
		$get_termination_list_sdata[':termination_date']  = $termination_date;
		
		$filter_count++;
	}
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_termination_list_squery_where = $get_termination_list_squery_where." where employee_termination_added_by = :added_by";						
		}
		else
		{
			// Query
			$get_termination_list_squery_where = $get_termination_list_squery_where." and employee_termination_added_by = :added_by";				
		}
		
		// Data
		$get_termination_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_termination_list_squery_where = $get_termination_list_squery_where." where employee_termination_added_on >= :start_date";					
		}
		else
		{
			// Query
			$get_termination_list_squery_where = $get_termination_list_squery_where." and employee_termination_added_on >= :start_date";				
		}
		
		//Data
		$get_termination_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_termination_list_squery_where = $get_termination_list_squery_where." where employee_termination_added_on <= :end_date";					
		}
		else
		{
			// Query
			$get_termination_list_squery_where = $get_termination_list_squery_where." and employee_termination_added_on <= :end_date";				
		}
		
		// Data
		$get_termination_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_termination_list_squery = $get_termination_list_squery_base.$get_termination_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_termination_list_sstatement = $dbConnection->prepare($get_termination_list_squery);
		
		$get_termination_list_sstatement -> execute($get_termination_list_sdata);
		
		$get_termination_list_sdetails = $get_termination_list_sstatement -> fetchAll();
		
		if(FALSE === $get_termination_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_termination_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_termination_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To update employee status  
INPUT 	: Employee ID, Status
OUTPUT 	: Employee id, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_employee_status($employee_id,$status)
{
	// Query
    $employee_status_uquery = "update employee set employee_status=:status where employee_id=:employee_id";  

    try
    {
        $dbConnection = get_conn_handle();
        
        $employee_status_ustatement = $dbConnection->prepare($employee_status_uquery);
        
        // Data
        $employee_status_udata = array(':status'=>$status,':employee_id'=>$employee_id);		
        
        $employee_status_ustatement -> execute($employee_status_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $employee_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To update employee details  
INPUT 	: Employee ID, Employee Data Array
OUTPUT 	: Employee ID, success or failure message
BY 		: Nitin Kashyap
*/
function db_update_employee_details($employee_id,$employee_details)
{
	// Query
    $employee_uquery_base = "update hr_employee set";  
	
	$employee_uquery_set = "";
	
	$employee_uquery_where = " where hr_employee_id=:employee_id";
	
	$employee_udata = array(":employee_id"=>$employee_id);
	
	$filter_count = 0;
	
	if(array_key_exists("user",$employee_details))
	{
		$employee_uquery_set = $employee_uquery_set." hr_employee_user=:user,";
		$employee_udata[":user"] = $employee_details["user"];
		$filter_count++;
	}
	
	if(array_key_exists("name",$employee_details))
	{
		$employee_uquery_set = $employee_uquery_set." hr_employee_name=:name,";
		$employee_udata[":name"] = $employee_details["name"];
		$filter_count++;
	}
	
	if(array_key_exists("postal_address",$employee_details))
	{
		$employee_uquery_set = $employee_uquery_set." hr_employee_postal_address=:postal_address,";
		$employee_udata[":postal_address"] = $employee_details["postal_address"];
		$filter_count++;
	}
	
	if(array_key_exists("perm_address",$employee_details))
	{
		$employee_uquery_set = $employee_uquery_set." hr_employee_permanent_address=:perm_address,";
		$employee_udata[":perm_address"] = $employee_details["perm_address"];
		$filter_count++;
	}
	
	if(array_key_exists("father",$employee_details))
	{
		$employee_uquery_set = $employee_uquery_set." hr_employee_father_name=:father,";
		$employee_udata[":father"] = $employee_details["father"];
		$filter_count++;
	}
	
	if(array_key_exists("dob",$employee_details))
	{
		$employee_uquery_set = $employee_uquery_set." hr_employee_dob=:dob,";
		$employee_udata[":dob"] = $employee_details["dob"];
		$filter_count++;
	}
	
	if(array_key_exists("emp_date",$employee_details))
	{
		$employee_uquery_set = $employee_uquery_set." hr_employee_employment_date=:emp_date,";
		$employee_udata[":emp_date"] = $employee_details["emp_date"];
		$filter_count++;
	}
	
	if(array_key_exists("emp_code",$employee_details))
	{
		$employee_uquery_set = $employee_uquery_set." hr_employee_code=:emp_code,";
		$employee_udata[":emp_code"] = $employee_details["emp_code"];
		$filter_count++;
	}
	
	if(array_key_exists("emp_week_off",$employee_details))
	{
		$employee_uquery_set = $employee_uquery_set." hr_employee_week_off=:emp_week_off,";
		$employee_udata[":emp_week_off"] = $employee_details["emp_week_off"];
		$filter_count++;
	}
	
	if(array_key_exists("emp_status",$employee_details))
	{
		$employee_uquery_set = $employee_uquery_set." hr_employee_status=:emp_status,";
		$employee_udata[":emp_status"] = $employee_details["emp_status"];
		$filter_count++;
	}
	
	if($filter_count > 0)
	{
		$employee_uquery_set = trim($employee_uquery_set,',');
	}
	
	$employee_uquery = $employee_uquery_base.$employee_uquery_set.$employee_uquery_where;	

    try
    {
        $dbConnection = get_conn_handle();
        
        $employee_ustatement = $dbConnection->prepare($employee_uquery);		
        
        $employee_ustatement -> execute($employee_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $employee_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}
?>