<?php
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'status_codes.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'data_access'.DIRECTORY_SEPARATOR.'connection.php');

/*
LIST OF TBDs
1. Wherever there is name search, there should be a wildcard
*/

/*
PURPOSE : To add new Marketing Expenses
INPUT 	: Marketing Source Name,Date,Amount,Leads,Contact Person,Contact No,Email,Remarks,Status,Added By
OUTPUT 	: Marketing Source id, success or failure message
BY 		: Sonakshi D
*/
function db_add_marketing_expenses($marketing_source,$applicable_start_date,$applicable_end_date,$amount,$leads,$contact_person,$contact_no,$contact_email,$remarks,$added_by)
{
	// Query
    $marketing_expenses_iquery = "insert into crm_marketing_expenses (crm_marketing_expenses_source,crm_marketing_expenses_applicable_start_date,crm_marketing_expenses_applicable_end_date,crm_marketing_expenses_amount,crm_marketing_expenses_predicted_leads,crm_marketing_expenses_contact_person,crm_marketing_expenses_contact_no,crm_marketing_expenses_email,crm_marketing_expenses_remarks,crm_marketing_expenses_approval_status,crm_marketing_expenses_added_by,crm_marketing_expenses_added_on,crm_marketing_expenses_updated_by,crm_marketing_expenses_updated_on) values (:marketing_source,:applicable_start_date,:applicable_end_date,:amount,:leads,:contact_person,:contact_no,:email,:remarks,:status,:added_by,:now,:updated_by,:updated_on)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $marketing_expenses_istatement = $dbConnection->prepare($marketing_expenses_iquery);
        
        // Data
        $marketing_expenses_idata = array(':marketing_source'=>$marketing_source,':applicable_start_date'=>$applicable_start_date,':applicable_end_date'=>$applicable_end_date,':amount'=>$amount,':leads'=>$leads,':contact_person'=>$contact_person,':contact_no'=>$contact_no,':email'=>$contact_email,':remarks'=>$remarks,':status'=>'0',':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"),':updated_by'=>'',':updated_on'=>'');	
		
		$dbConnection->beginTransaction();
        $marketing_expenses_istatement->execute($marketing_expenses_idata);
		$crm_marketing_expenses_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = "Marketing Expenses Successfully Added";		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Marketing Expenses list
INPUT 	: Expense ID, Source, Status, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Marketing Expenses
BY 		: Sonakshi D
*/
function db_get_marketing_expenses_list($expense_id,$marketing_source,$applicable_start_date,$applicable_end_date,$amount,$leads,$status,$added_by,$start_date,$end_date)
{	
	$get_marketing_expenses_list_squery_base = "select * from crm_marketing_expenses CME inner join crm_enquiry_source_master CSM on CSM.enquiry_source_master_id=CME.crm_marketing_expenses_source inner join users U on U.user_id = CME.crm_marketing_expenses_added_by";
	
	$get_marketing_expenses_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_marketing_expenses_list_sdata = array();
	
	if($expense_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." where crm_marketing_expenses_id = :expense_id";								
		}
		else
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." and crm_marketing_expenses_id = :expense_id";				
		}
		
		// Data
		$get_marketing_expenses_list_sdata[':expense_id']  = $expense_id;
		
		$filter_count++;
	}
	
	if($marketing_source != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." where crm_marketing_expenses_source=:marketing_source";								
		}
		else
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." and crm_marketing_expenses_source=:marketing_source";				
		}
		
		// Data
		$get_marketing_expenses_list_sdata[':marketing_source']  = $marketing_source;
		
		$filter_count++;
	}
	
	if($applicable_start_date != "")
	{
		if($applicable_end_date != "")
		{
			if($filter_count == 0)
			{
				// Query
				$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." where ((crm_marketing_expenses_applicable_start_date >= :applicable_start_date and crm_marketing_expenses_applicable_start_date <= :applicable_end_date) or (crm_marketing_expenses_applicable_end_date >= :applicable_start_date and crm_marketing_expenses_applicable_end_date <= :applicable_end_date))";								
			}
			else
			{
				// Query
				$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." and ((crm_marketing_expenses_applicable_start_date >= :applicable_start_date and crm_marketing_expenses_applicable_start_date <= :applicable_end_date) or (crm_marketing_expenses_applicable_end_date >= :applicable_start_date and crm_marketing_expenses_applicable_end_date <= :applicable_end_date))";				
			}

			$get_marketing_expenses_list_sdata[':applicable_end_date']  = $applicable_end_date;
		}
		else
		{
			if($filter_count == 0)
			{
				// Query
				$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." where crm_marketing_expenses_applicable_start_date >= :applicable_start_date";								
			}
			else
			{
				// Query
				$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." and crm_marketing_expenses_applicable_start_date >= :applicable_start_date";				
			}
		}
		
		// Data
		$get_marketing_expenses_list_sdata[':applicable_start_date']  = $applicable_start_date;
		
		$filter_count++;
	}
	
	if($amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." where crm_marketing_expenses_amount=:amount";								
		}
		else
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." and crm_marketing_expenses_amount=:amount";				
		}
		
		// Data
		$get_marketing_expenses_list_sdata[':amount']  = $amount;
		
		$filter_count++;
	}
	
	if($leads != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." where crm_marketing_expenses_predicted_leads=:leads";								
		}
		else
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." and crm_marketing_expenses_predicted_leads=:leads";				
		}
		
		// Data
		$get_marketing_expenses_list_sdata[':leads']  = $leads;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." where crm_marketing_expenses_approval_status=:status";								
		}
		else
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." and crm_marketing_expenses_approval_status=:status";				
		}
		
		// Data
		$get_marketing_expenses_list_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." where crm_marketing_expenses_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." and crm_marketing_expenses_added_by=:added_by";				
		}
		
		// Data
		$get_marketing_expenses_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." where crm_marketing_expenses_added_on >= :start_date";						
		}
		else
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." and crm_marketing_expenses_added_on >= :start_date";				
		}
		
		//Data
		$get_marketing_expenses_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." where crm_marketing_expenses_added_on <= :end_date";						
		}
		else
		{
			// Query
			$get_marketing_expenses_list_squery_where = $get_marketing_expenses_list_squery_where." and crm_marketing_expenses_added_on <= :end_date";				
		}
		
		//Data
		$get_marketing_expenses_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_marketing_expenses_list_squery = $get_marketing_expenses_list_squery_base.$get_marketing_expenses_list_squery_where;	
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_marketing_expenses_list_sstatement = $dbConnection->prepare($get_marketing_expenses_list_squery);
		
		$get_marketing_expenses_list_sstatement -> execute($get_marketing_expenses_list_sdata);
		
		$get_marketing_expenses_list_sdetails = $get_marketing_expenses_list_sstatement -> fetchAll();
		
		if(FALSE === $get_marketing_expenses_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_marketing_expenses_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_marketing_expenses_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}

/*
PURPOSE : To change status
INPUT 	: Marketing Source ID, Status
OUTPUT 	: Marketing Source id, success or failure message
BY 		: Sonakshi D
*/
function db_change_marketing_expenses($marketing_expenses_id,$marketing_expenses_details)
{
	// Query
    $marketing_expenses_uquery_base = "update crm_marketing_expenses set";  
	
	$marketing_expenses_uquery_set = "";
	
	$marketing_expenses_uquery_where = " where crm_marketing_expenses_id = :marketing_expenses_id";
	
	$marketing_expenses_udata = array(":marketing_expenses_id"=>$marketing_expenses_id);
	
	$filter_count = 0;
	
	if(array_key_exists("status",$marketing_expenses_details))
	{
		$marketing_expenses_uquery_set = $marketing_expenses_uquery_set." crm_marketing_expenses_approval_status = :status,";
		$marketing_expenses_udata[":status"] = $marketing_expenses_details["status"];
		$filter_count++;
	}
	
	if(array_key_exists("start_date",$marketing_expenses_details))
	{
		$marketing_expenses_uquery_set = $marketing_expenses_uquery_set." crm_marketing_expenses_applicable_start_date = :start_date,";
		$marketing_expenses_udata[":start_date"] = $marketing_expenses_details["start_date"];
		$filter_count++;
	}
	
	if(array_key_exists("end_date",$marketing_expenses_details))
	{
		$marketing_expenses_uquery_set = $marketing_expenses_uquery_set." crm_marketing_expenses_applicable_end_date = :end_date,";
		$marketing_expenses_udata[":end_date"] = $marketing_expenses_details["end_date"];
		$filter_count++;
	}
	
	if(array_key_exists("amount",$marketing_expenses_details))
	{
		$marketing_expenses_uquery_set = $marketing_expenses_uquery_set." crm_marketing_expenses_amount = :amount,";
		$marketing_expenses_udata[":amount"] = $marketing_expenses_details["amount"];
		$filter_count++;
	}
	
	if(array_key_exists("leads",$marketing_expenses_details))
	{
		$marketing_expenses_uquery_set = $marketing_expenses_uquery_set." crm_marketing_expenses_predicted_leads = :leads,";
		$marketing_expenses_udata[":leads"] = $marketing_expenses_details["leads"];
		$filter_count++;
	}
	
	if(array_key_exists("contact_person",$marketing_expenses_details))
	{
		$marketing_expenses_uquery_set = $marketing_expenses_uquery_set." crm_marketing_expenses_contact_person = :contact_person,";
		$marketing_expenses_udata[":contact_person"] = $marketing_expenses_details["contact_person"];
		$filter_count++;
	}
	
	if(array_key_exists("contact_no",$marketing_expenses_details))
	{
		$marketing_expenses_uquery_set = $marketing_expenses_uquery_set." crm_marketing_expenses_contact_no = :contact_no,";
		$marketing_expenses_udata[":contact_no"] = $marketing_expenses_details["contact_no"];
		$filter_count++;
	}
	
	if(array_key_exists("email",$marketing_expenses_details))
	{
		$marketing_expenses_uquery_set = $marketing_expenses_uquery_set." crm_marketing_expenses_email = :email,";
		$marketing_expenses_udata[":email"] = $marketing_expenses_details["email"];
		$filter_count++;
	}
	
	if(array_key_exists("updated_by",$marketing_expenses_details))
	{
		$marketing_expenses_uquery_set = $marketing_expenses_uquery_set." crm_marketing_expenses_updated_by = :updated_by,";
		$marketing_expenses_udata[":updated_by"] = $marketing_expenses_details["updated_by"];
		$filter_count++;
	}
	
	$marketing_expenses_uquery_set = $marketing_expenses_uquery_set." crm_marketing_expenses_updated_on = :updated_on,";
	$marketing_expenses_udata[":updated_on"] = date("Y-m-d H:i:s");
	$filter_count++;
	
	if($filter_count > 0)
	{
		$marketing_expenses_uquery_set = trim($marketing_expenses_uquery_set,',');
	}
	
	$marketing_expenses_uquery = $marketing_expenses_uquery_base.$marketing_expenses_uquery_set.$marketing_expenses_uquery_where;
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $marketing_expenses_ustatement = $dbConnection->prepare($marketing_expenses_uquery);		
        
        $marketing_expenses_ustatement -> execute($marketing_expenses_udata);
        
        $return["status"] = SUCCESS;
		$return["data"]   = $marketing_expenses_id;
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
	
	return $return;
}

/*
PURPOSE : To add new Marketing Expenses Payment Details
INPUT 	: Mode, Instrument Detils, Date,Paid to, Amount, Status, Added By
OUTPUT 	: Marketing Payment ID, success or failure message
BY 		: Sonakshi D
*/
function db_add_marketing_expenses_payment($mode,$instrument_details,$date,$paid_to,$amount,$expenses_id,$added_by)
{
	// Query
    $marketing_payments_iquery = "insert into crm_marketing_expenses_payment_details(crm_marketing_expenses_payment_details_mode,crm_marketing_expenses_payment_details_instrument_details,crm_marketing_expenses_payment_details_date,crm_marketing_expenses_payment_details_paid_to,crm_marketing_expenses_payment_details_amount,crm_marketing_expenses_payment_details_status,crm_marketing_expenses_payment_details_expen_id,crm_marketing_expenses_payment_details_added_by,crm_marketing_expenses_payment_details_added_on) values (:mode,:instrument_details,:date,:paid_to,:amount,:status,:expenses_id,:added_by,:now)";  
	
    try
    {
        $dbConnection = get_conn_handle();
        
        $marketing_payments_istatement = $dbConnection->prepare($marketing_payments_iquery);
        
        // Data
        $marketing_payments_idata = array(':mode'=>$mode,':instrument_details'=>$instrument_details,':date'=>$date,':paid_to'=>$paid_to,':amount'=>$amount,':status'=>'1',':expenses_id'=>$expenses_id,':added_by'=>$added_by,':now'=>date("Y-m-d H:i:s"));	
		$dbConnection->beginTransaction();
        $marketing_payments_istatement->execute($marketing_payments_idata);
		$crm_marketing_expenses_payment_details_id = $dbConnection->lastInsertId();
		$dbConnection->commit(); 
        
        $return["status"] = SUCCESS;
		$return["data"]   = "Marketing Expenses Payment Successfully Added";		
    }
    catch(PDOException $e)
    {
        // Log the error
        $return["status"] = FAILURE;
		$return["data"]   = "";
    }
    
    return $return;
}

/*
PURPOSE : To get Marketing Expenses Payment Details
INPUT 	: Mode, Status, Added By, Start Date(for added on), End Date(for added on)
OUTPUT 	: List of Marketing Expenses Details
BY 		: Sonakshi D
*/
function db_get_marketing_payment_list($mode,$instrument_details,$date,$paid_to,$amount,$status,$expenses_id,$added_by,$start_date,$end_date)
{
	$get_marketing_payment_list_squery_base = "select * from crm_marketing_expenses_payment_details CMEPD inner join users U on U.user_id = CMEPD.crm_marketing_expenses_payment_details_added_by left outer join payment_mode_master PMM on PMM.payment_mode_id = CMEPD.crm_marketing_expenses_payment_details_mode";
	
	$get_marketing_payment_list_squery_where = "";
	
	$filter_count = 0;
	
	// Data
	$get_marketing_payment_list_sdata = array();
	
	if($mode != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." where crm_marketing_expenses_payment_details_mode = :mode";
		}
		else
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." and crm_marketing_expenses_payment_details_mode = :mode";
		}
		
		// Data
		$get_marketing_payment_list_sdata[':mode'] = $mode;
		
		$filter_count++;
	}
	
	if($instrument_details != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." where crm_marketing_expenses_payment_details_instrument_details = :instrument_details";								
		}
		else
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." and crm_marketing_expenses_payment_details_instrument_details = :instrument_details";				
		}
		
		// Data
		$get_marketing_payment_list_sdata[':instrument_details'] = $instrument_details;
		
		$filter_count++;
	}
	 
	if($date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." where crm_marketing_expenses_payment_details_date=:date";
		}
		else
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." and crm_marketing_expenses_payment_details_date=:date";
		}
		
		// Data
		$get_marketing_payment_list_sdata[':date']  = $date;
		
		$filter_count++;
	}
	
	if($paid_to != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." where crm_marketing_expenses_payment_details_paid_to = :paid_to";								
		}
		else
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." and crm_marketing_expenses_payment_details_paid_to = :paid_to";				
		}
		
		// Data
		$get_marketing_payment_list_sdata[':paid_to'] = $paid_to;
		
		$filter_count++;
	}
	
	if($amount != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." where crm_marketing_expenses_payment_details_amount=:amount";								
		}
		else
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." and crm_marketing_expenses_payment_details_amount=:amount";				
		}
		
		// Data
		$get_marketing_payment_list_sdata[':amount'] = $amount;
		
		$filter_count++;
	}
	
	if($status != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." where crm_marketing_expenses_payment_details_status=:status";								
		}
		else
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." and crm_marketing_expenses_payment_details_status=:status";				
		}
		
		// Data
		$get_marketing_payment_list_sdata[':status']  = $status;
		
		$filter_count++;
	}
	
	if($expenses_id != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." where crm_marketing_expenses_payment_details_expen_id=:expenses_id";								
		}
		else
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." and crm_marketing_expenses_payment_details_expen_id=:expenses_id";				
		}
		
		// Data
		$get_marketing_payment_list_sdata[':expenses_id']  = $expenses_id;
		
		$filter_count++;
	}
	
	
	if($added_by != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." where crm_marketing_expenses_payment_details_added_by=:added_by";								
		}
		else
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." and crm_marketing_expenses_payment_details_added_by=:added_by";				
		}
		
		// Data
		$get_marketing_payment_list_sdata[':added_by']  = $added_by;
		
		$filter_count++;
	}
	
	if($start_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." where crm_marketing_expenses_payment_details_added_on >= :start_date";						
		}
		else
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." and crm_marketing_expenses_payment_details_added_on >= :start_date";				
		}
		
		//Data
		$get_marketing_payment_list_sdata[':start_date']  = $start_date;
		
		$filter_count++;
	}

	if($end_date != "")
	{
		if($filter_count == 0)
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." where crm_marketing_expenses_payment_details_added_on <= :end_date";						
		}
		else
		{
			// Query
			$get_marketing_payment_list_squery_where = $get_marketing_payment_list_squery_where." and crm_marketing_expenses_payment_details_added_on <= :end_date";				
		}
		
		//Data
		$get_marketing_payment_list_sdata[':end_date']  = $end_date;
		
		$filter_count++;
	}
	
	$get_marketing_payment_list_squery = $get_marketing_payment_list_squery_base.$get_marketing_payment_list_squery_where;
	
	try
	{
		$dbConnection = get_conn_handle();
		
		$get_marketing_payment_list_sstatement = $dbConnection->prepare($get_marketing_payment_list_squery);
		
		$get_marketing_payment_list_sstatement -> execute($get_marketing_payment_list_sdata);
		
		$get_marketing_payment_list_sdetails = $get_marketing_payment_list_sstatement -> fetchAll();
		
		if(FALSE === $get_marketing_payment_list_sdetails)
		{
			$return["status"] = FAILURE;
			$return["data"]   = "";
		}
		else if(count($get_marketing_payment_list_sdetails) <= 0)
		{
			$return["status"] = DB_NO_RECORD;
			$return["data"]   = "";
		}
		else
		{
			$return["status"] = DB_RECORD_ALREADY_EXISTS;
			$return["data"]   = $get_marketing_payment_list_sdetails;
		}
	}
	catch(PDOException $e)
	{
		// Log the error
		$return["status"] = FAILURE;
		$return["data"] = "";
	}
	
	return $return;
}
?>