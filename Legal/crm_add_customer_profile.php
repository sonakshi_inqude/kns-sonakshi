<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/* FILE HEADER - START */
// LAST UPDATED ON: 14th Nov 2015
// LAST UPDATED BY: Nitin Kashyap
/* FILE HEADER - END */

/* TBD - START */
/* TBD - END */

/* INCLUDES - START */
$base = $_SERVER['DOCUMENT_ROOT'];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_config.php');
/* INCLUDES - END */

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	/* DATA INITIALIZATION - START */
	$alert_type = -1;
	$alert = "";
	/* DATA INITIALIZATION - END */
	
	/* QUERY STRING - START */
	if(isset($_GET["booking"]))
	{
		$booking_id = $_GET["booking"];
	}
	else
	{
		$booking_id = "";
	}	
	/* QUERY STRING - END */

	// Capture the form data
	if(isset($_POST["add_customer_profile_submit"]))
	{
		$booking_id          = $_POST["hd_booking_id"];
		$name_one            = $_POST["stxt_name_one"];
		$relationship_one    = $_POST["stxt_relationship_one"];
		$pan_no_one          = $_POST["stxt_pan_one"];		
		$name_two            = $_POST["stxt_name_two"];
		$relationship_two    = $_POST["stxt_relationship_two"];
		$pan_no_two          = $_POST["stxt_pan_two"];
		$name_three          = $_POST["stxt_name_three"];
		$relationship_three  = $_POST["stxt_relationship_three"];
		$pan_no_three        = $_POST["stxt_pan_three"];
		$name_four           = $_POST["stxt_name_four"];
		$relationship_four   = $_POST["stxt_relationship_four"];
		$pan_no_four         = $_POST["stxt_pan_four"];
		$contact_no_one      = $_POST["stxt_cont_no_one"];
		$contact_no_two      = $_POST["stxt_cont_no_two"];
		$contact_no_three    = $_POST["stxt_cont_no_three"];
		$contact_no_four     = $_POST["stxt_cont_no_four"];
		$email_one           = $_POST["stxt_email_one"];
		$email_two           = $_POST["stxt_email_two"];
		$email_three         = $_POST["stxt_email_three"];
		$email_four          = $_POST["stxt_email_four"];
		$dob_one             = $_POST["dt_dob_one"];
		$dob_two             = $_POST["dt_dob_two"];
		$dob_three           = $_POST["dt_dob_three"];
		$dob_four            = $_POST["dt_dob_four"];
		$anniversary_one     = $_POST["dt_anni_one"];
		$anniversary_two     = $_POST["dt_anni_two"];
		$company             = $_POST["stxt_company"];
		$designation         = $_POST["stxt_designation"];
		$funding_type        = $_POST["rd_funding_type"];
		if($funding_type != "2")
		{
			$bank = "";
		}
		else
		{
			$bank = $_POST["ddl_bank"];
		}	
		$agreement_address   = $_POST["txt_agreement_address"];
		$residential_address = $_POST["txt_residential_address"];
		$office_address      = $_POST["txt_office_address"];
		$gpa_holder          = $_POST["stxt_gpa_holder"];
		$gpa_contact_no      = $_POST["stxt_gpa_contact"];
		$gpa_address         = $_POST["txt_gpa_address"];
		$gpa_email           = $_POST["stxt_gpa_email"];
		$gpa_remarks         = $_POST["txt_gpa_remarks"];
		
		// Check for mandatory fields
		if(($booking_id !="") && ($name_one !="") && ($relationship_one !="") && ($contact_no_one !="") && ($dob_one !="") && ($pan_no_one !=""))
		{
			$customer_profile_iresult = i_add_customer_profile($booking_id,$name_one,$relationship_one,$name_two,$relationship_two,$name_three,$relationship_three,$name_four,$relationship_four,$contact_no_one,$contact_no_two,$contact_no_three,$contact_no_four,$email_one,$email_two,$email_three,$email_four,$dob_one,$dob_two,$dob_three,$dob_four,$anniversary_one,$anniversary_two,$company,$designation,$funding_type,$bank,$agreement_address,$residential_address,$office_address,$pan_no_one,$pan_no_two,$pan_no_three,$pan_no_four,$gpa_holder,$gpa_contact_no,$gpa_address,$gpa_email,$gpa_remarks,$user);
			
			if($customer_profile_iresult["status"] == SUCCESS)
			{
				$alert_type = 1;
				$alert = "Customer Profile Successfully Added!";
			}
			else
			{
				$alert_type = 0;
				$alert      = $customer_profile_iresult["data"];
			}						
		}
		else
		{
			$alert = "Please fill all the mandatory fields";
			$alert_type = 0;
		}
	}
	
	// Get the customer details as captured already
	$booking_details = i_get_site_booking($booking_id,'','','','','','','','','','','');
	if($booking_details["status"] == SUCCESS)
	{
		$booking_details_list = $booking_details["data"];
		if($booking_details_list[0]["crm_booking_status"] != "1")
		{
			header("location:crm_pending_booking_list.php?msg=Booking needs to be approved before entering customer profile");
		}
	}
	else
	{
		$alert = $booking_details["data"];
		$alert_type = 0;
	}
	
	// Get bank master
	$bank_details = i_get_bank_list('','1');
	if($bank_details["status"] == SUCCESS)
	{
		$bank_details_list = $bank_details["data"];		
	}
	else
	{
		$alert = $bank_details["data"];
		$alert_type = 0;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Add Customer Profile</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>     

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Add Customer Profile</h3><span style="float:right; padding-right:10px;"><a href="crm_customer_transactions.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>">Back to customer profile</a></span>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						<strong>Project: <?php echo $booking_details_list[0]["project_name"]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Site No: <?php echo $booking_details_list[0]["crm_site_no"]; ?></strong>
						
						<div class="tabbable">						
						
						<br>
							<div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?>&nbsp;&nbsp;&nbsp;<a href="crm_view_customer_profile.php?profile=<?php echo $customer_profile_iresult["data"]; ?>" target="_blank">View Customer Profile</a>&nbsp;&nbsp;&nbsp;<a href="crm_add_payment_schedule.php?booking=<?php echo $booking_details_list[0]["crm_booking_id"]; ?>">Add Payment Schedule</a></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
							<form id="add_customer_profile" class="form-horizontal" method="post" action="crm_add_customer_profile.php">
							<input type="hidden" name="hd_booking_id" value="<?php echo $booking_id; ?>" />
								<div class="tab-pane active" id="formcontrols">
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>Customer Name</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>																											
										<div class="control-group">											
											<label class="control-label" for="stxt_name_one">Name 1*</label>
											<div class="controls">
												<input type="text" name="stxt_name_one" class="span6" value="<?php echo $booking_details_list[0]["name"]; ?>" required />
												<p class="help-text">Generally the name of buyer. Mandatory field</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_relationship_one">Relationship 1*</label>
											<div class="controls">
												<input type="text" name="stxt_relationship_one" class="span6" required placeholder="Ex: S/O Anjanappa" />
												<p class="help-text">Generally the relationship details of buyer. Mandatory field</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_pan_one">PAN No 1*</label>
											<div class="controls">
												<input type="text" name="stxt_pan_one" class="span6" required />
												<p class="help-text">PAN Number of buyer. Mandatory field</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_name_two">Name 2</label>
											<div class="controls">
												<input type="text" name="stxt_name_two" class="span6" />
												<p class="help-text">Could be a family member or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_relationship_two">Relationship 2</label>
											<div class="controls">
												<input type="text" name="stxt_relationship_two" class="span6" />
												<p class="help-text">Could be the relationship details of a family member or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_pan_two">PAN No 2</label>
											<div class="controls">
												<input type="text" name="stxt_pan_two" class="span6" />
												<p class="help-text">PAN Number of of a family number or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_name_three">Name 3</label>
											<div class="controls">
												<input type="text" name="stxt_name_three" class="span6" />
												<p class="help-text">Could be a family member or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_relationship_three">Relationship 3</label>
											<div class="controls">
												<input type="text" name="stxt_relationship_three" class="span6" />
												<p class="help-text">Could be the relationship details of a family member or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_pan_three">PAN No 3</label>
											<div class="controls">
												<input type="text" name="stxt_pan_three" class="span6" />
												<p class="help-text">PAN Number of of a family number or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_name_four">Name 4</label>
											<div class="controls">
												<input type="text" name="stxt_name_four" class="span6" />
												<p class="help-text">Could be a family member or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_relationship_four">Relationship 4</label>
											<div class="controls">
												<input type="text" name="stxt_relationship_four" class="span6" />
												<p class="help-text">Could be the relationship details of a family member or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_pan_four">PAN No 4</label>
											<div class="controls">
												<input type="text" name="stxt_pan_four" class="span6" />
												<p class="help-text">PAN Number of of a family number or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
                                         																	
									</fieldset>
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>Customer Contact Details</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>																											
										<div class="control-group">											
											<label class="control-label" for="stxt_cont_no_one">Contact Number 1*</label>
											<div class="controls">
												<input type="text" name="stxt_cont_no_one" class="span6" value="<?php echo $booking_details_list[0]["cell"]; ?>" required />
												<p class="help-text">Generally the contact no. of buyer. Mandatory field</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_cont_no_two">Contact Number 2</label>
											<div class="controls">
												<input type="text" name="stxt_cont_no_two" class="span6" />
												<p class="help-text">Could be of a family member or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_cont_no_three">Contact Number 3</label>
											<div class="controls">
												<input type="text" name="stxt_cont_no_three" class="span6" />
												<p class="help-text">Could be of a family member or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_cont_no_four">Contact Number 4</label>
											<div class="controls">
												<input type="text" name="stxt_cont_no_four" class="span6" />
												<p class="help-text">Could be of a family member or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_email_one">Email ID 1</label>
											<div class="controls">
												<input type="text" name="stxt_email_one" value="<?php echo $booking_details_list[0]["email"]; ?>" class="span6" />
												<p class="help-text">Generally the email ID of buyer. NOT MANDATORY</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_email_two">Email ID 2</label>
											<div class="controls">
												<input type="text" name="stxt_email_two" class="span6" />
												<p class="help-text">Could be of a family member or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_email_three">Email ID 3</label>
											<div class="controls">
												<input type="text" name="stxt_email_three" class="span6" />
												<p class="help-text">Could be of a family member or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_email_four">Email ID 4</label>
											<div class="controls">
												<input type="text" name="stxt_email_four" class="span6" />
												<p class="help-text">Could be of a family member or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
									</fieldset>
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>Customer Birthdays and Anniversaries</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>																											
										<div class="control-group">											
											<label class="control-label" for="dt_dob_one">DOB 1*</label>
											<div class="controls">
												<input type="date" name="dt_dob_one" class="span6" required />
												<p class="help-text">Generally the date of birth of buyer. Mandatory field</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="dt_dob_two">DOB 2</label>
											<div class="controls">
												<input type="date" name="dt_dob_two" class="span6" />
												<p class="help-text">Could be of a family member or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="dt_dob_three">DOB 3</label>
											<div class="controls">
												<input type="date" name="dt_dob_three" class="span6" />
												<p class="help-text">Could be of a family member or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="dt_dob_four">DOB 4</label>
											<div class="controls">
												<input type="date" name="dt_dob_four" class="span6" />
												<p class="help-text">Could be of a family member or any other associate</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="dt_anni_one">Anniversary 1</label>
											<div class="controls">
												<input type="date" name="dt_anni_one" class="span6" />
												<p class="help-text">Generally a marriage anniversary. NOT MANDATORY</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="dt_anni_two">Anniversary 2</label>
											<div class="controls">
												<input type="date" name="dt_anni_two" class="span6" />
												<p class="help-text">Generally a marriage anniversary. NOT MANDATORY</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->										
									</fieldset>
									<div class="widget-header">
									<i class="icon-user"></i>
									<h3>Buyer Info</h3>
									</div> <!-- /widget-header -->
									<br />									
									<fieldset>																											
										<div class="control-group">											
											<label class="control-label" for="stxt_company">Company</label>
											<div class="controls">
												<input type="text" name="stxt_company" value="<?php echo $booking_details_list[0]["company"]; ?>" class="span6" />
												<p class="help-text">Employer of the principal buyer</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_designation">Designation</label>
											<div class="controls">
												<input type="text" name="stxt_designation" class="span6" />
												<p class="help-text">Designation of the principal buyer</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_cont_no_three">Funding Type</label>
											<div class="controls">
												<input type="radio" name="rd_funding_type" value="1" checked onclick="return disable_bank();" />&nbsp;&nbsp;&nbsp;Self&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="radio" name="rd_funding_type" value="2" onclick="return enable_bank();" />&nbsp;&nbsp;&nbsp;Loan
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input type="radio" name="rd_funding_type" value="3" onclick="return disable_bank();" />&nbsp;&nbsp;&nbsp;Waiting for Confirmation
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="ddl_bank">Bank</label>
											<div class="controls">
												<select name="ddl_bank" id="ddl_bank" class="span6" disabled>
												<option value="">- - Select Bank - -</option>
												<?php
												for($count = 0; $count < count($bank_details_list); $count++)
												{
												?>
												<option value="<?php echo $bank_details_list[$count]["crm_bank_master_id"]; ?>"><?php echo $bank_details_list[$count]["crm_bank_name"]; ?></option>
												<?php
												}
												?>
												</select>
												<p class="help-text">Mandatory if funding type is loan</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="txt_agreement_address">Agreement Address</label>
											<div class="controls">
												<textarea name="txt_agreement_address" class="span6"></textarea>
												<p class="help-text">Address as needed to be printed on Agreement</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="txt_residential_address">Residential Address</label>
											<div class="controls">
												<textarea name="txt_residential_address" class="span6"></textarea>
												<p class="help-text">Current Residential Address of Buyer</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="txt_office_address">Office Address</label>
											<div class="controls">
												<textarea name="txt_office_address" class="span6"></textarea>
												<p class="help-text">Current Office Address of Buyer</p>
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->										
										<div class="control-group">											
											<label class="control-label" for="stxt_gpa_holder">GPA Holder name</label>
											<div class="controls">
												<input type="text" name="stxt_gpa_holder" class="span6" />												
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_gpa_contact">GPA Contact No</label>
											<div class="controls">
												<input type="text" name="stxt_gpa_contact" class="span6" />												
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="txt_gpa_address">GPA Address</label>
											<div class="controls">
												<textarea name="txt_gpa_address" class="span6"></textarea>												
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="stxt_gpa_email">GPA Email ID</label>
											<div class="controls">
												<input type="text" name="stxt_gpa_email" class="span6" />												
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<div class="control-group">											
											<label class="control-label" for="txt_gpa_remarks">Remarks</label>
											<div class="controls">
												<textarea name="txt_gpa_remarks" class="span6"></textarea>												
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										
										<div class="form-actions">
											<input type="submit" class="btn btn-primary" name="add_customer_profile_submit" value="Submit" />
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  						  
						</div>
	
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgrou.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>

  </body>
<script>
function enable_bank()
{
	document.getElementById("ddl_bank").disabled = false;
	document.getElementById("ddl_bank").required = true;
}

function disable_bank()
{
	document.getElementById("ddl_bank").disabled = true;
	document.getElementById("ddl_bank").required = false;
}
</script>
</html>
