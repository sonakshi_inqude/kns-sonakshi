<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_plan_cancelation_list.php
CREATED ON	: 31-July-2017
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/


// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
//include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
	
if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];
	
	$alert_type = -1;
	$alert 		= "";

	// Query String Data
	// Nothing
	
	$search_project = "";
	
	if(isset($_POST["plan_search_submit"]))
	{
		$search_project   = $_POST["search_project"];
	}
	
	$search_process = "";
	
	if(isset($_POST["plan_search_submit"]))
	{
		$search_process   = $_POST["search_process"];
	}
	
	$search_task = "";
	
	if(isset($_POST["plan_search_submit"]))
	{
		$search_task   = $_POST["search_task"];
	}

	// Temp Project Plan Cancelation List
	$project_plan_cancelation_search_data = array("active"=>'1',"project"=>$search_project,"process"=>$search_process,"task"=>$search_task);
	$project_plan_cancelation_list = i_get_project_plan_cancelation($project_plan_cancelation_search_data);
	if($project_plan_cancelation_list["status"] == SUCCESS)
	{
		$project_plan_cancelation_list_data = $project_plan_cancelation_list["data"];
	}
	
	// Project data
	$project_management_master_search_data = array("active"=>'1');
	$project_management_master_list = i_get_project_management_master_list($project_management_master_search_data);
	if($project_management_master_list["status"] == SUCCESS)
	{
		$project_management_master_list_data = $project_management_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_management_master_list["data"];
	}
	
	// Process Master
	$project_process_master_search_data = array("active"=>'1');
	$project_process_master_list = i_get_project_process_master($project_process_master_search_data);
	if($project_process_master_list["status"] == SUCCESS)
	{
		$project_process_master_list_data = $project_process_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_process_master_list["data"];
	}
	
	// Task Master
	$project_task_master_search_data = array("active"=>'1',"process"=>$search_process);
	$project_task_master_list = i_get_project_task_master($project_task_master_search_data);
	if($project_task_master_list["status"] == SUCCESS)
	{
		$project_task_master_list_data = $project_task_master_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_task_master_list["data"];
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Project Plan Cancelation List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Project Plan Cancelation List</h3><span style="float:right; padding-right:20px;"><a href="project_plan_add_cancelation.php">Add Project Plan Cancelation</a></span>
            </div>
            <!-- /widget-header -->
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="file_search_form" action="project_plan_cancelation_list.php">
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_project">
			  <option value="">- - Select Project - -</option>
			  <?php
			  for($project_count = 0; $project_count < count($project_management_master_list_data); $project_count++)
			  {
			  ?>
			  <option value="<?php echo $project_management_master_list_data[$project_count]["project_management_master_id"]; ?>" <?php if($search_project == $project_management_master_list_data[$project_count]["project_management_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_management_master_list_data[$project_count]["project_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_process">
			  <option value="">- - Select Process - -</option>
			  <?php
			  for($process_count = 0; $process_count < count($project_process_master_list_data); $process_count++)
			  {
			  ?>
			  <option value="<?php echo $project_process_master_list_data[$process_count]["project_process_master_id"]; ?>" <?php if($search_process == $project_process_master_list_data[$process_count]["project_process_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_process_master_list_data[$process_count]["project_process_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			  <span style="padding-left:20px; padding-right:20px;">
			  <select name="search_task">
			  <option value="">- - Select Task - -</option>
			  <?php
			  for($task_count = 0; $task_count < count($project_task_master_list_data); $task_count++)
			  {
			  ?>
			  <option value="<?php echo $project_task_master_list_data[$task_count]["project_task_master_id"]; ?>" <?php if($search_task == $project_task_master_list_data[$task_count]["project_task_master_id"]) { ?> selected="selected" <?php } ?>><?php echo $project_task_master_list_data[$task_count]["project_task_master_name"]; ?></option>
			  <?php
			  }
			  ?>
			  </select>
			  </span>
			  
			  <input type="submit" name="plan_search_submit" />
			  </form>	
							  
            </div>
            <div class="widget-content">
              <table class="table table-bordered">
                <thead>
                  <tr>
				    <th>SL No</th>
				    <th>Project</th>
				    <th>Process</th>
				    <th>Task</th>
					<th>Plan Type</th>
					<th>Remarks</th>
					<th>Added By</th>					
					<th>Added On</th>									
					<!--<th colspan="2" style="text-align:center;">Actions</th>-->
    					
				</tr>
				</thead>
				<tbody>							
				<?php
				if($project_plan_cancelation_list["status"] == SUCCESS)
				{
					$sl_no = 0;
					for($count = 0; $count < count($project_plan_cancelation_list_data); $count++)
					{
						$sl_no++;
						$plan_type = $project_plan_cancelation_list_data[$count]["project_plan_cancelation_plan_type"];
						if($plan_type == "manpower")
						{
							$plan_type = "Manpower";
						}
						elseif($plan_type == "machine")
						{
							$plan_type = "Machine";
						}
						else
						{
							$plan_type = "Contract";
						}
					?>
					<tr>
					<td><?php echo $sl_no; ?></td>
					<td><?php echo $project_plan_cancelation_list_data[$count]["project_master_name"]; ?></td>
					<td><?php echo $project_plan_cancelation_list_data[$count]["project_process_master_name"]; ?></td>
					<td><?php echo $project_plan_cancelation_list_data[$count]["project_task_master_name"]; ?></td>
					<td><?php echo $plan_type; ?></td>
					<td><?php echo $project_plan_cancelation_list_data[$count]["project_plan_cancelation_remarks"]; ?></td>
					<td><?php echo $project_plan_cancelation_list_data[$count]["user_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo date("d-M-Y",strtotime($project_plan_cancelation_list_data[$count][
					"project_plan_cancelation_added_on"])); ?></td>
					<!--<td style="word-wrap:break-word;"><a style="padding-right:10px" href="#" onclick="return go_to_project_edit_cancelation('<?php echo $project_plan_cancelation_list_data[$count]["project_plan_cancelation_id"]; ?>','<?php echo $plan_type ; ?>');">Edit </a></td>
					<td><?php if(($project_plan_cancelation_list_data[$count]["project_plan_cancelation_active"] == "1")){ ?><a href="#" onclick="return delete_project_cancelation(<?php echo $project_plan_cancelation_list_data[$count]["project_plan_cancelation_id"]; ?>);">Delete</a><?php } ?></td>-->
					</tr>
					<?php
					}
					
				}
				else
				{
				?>
				<td colspan="6">No project Plan Cancelation added yet!</td>
				
				<?php
				}
				 ?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function delete_project_cancelation(cancelation_id)
{
	var ok = confirm("Are you sure you want to Delete?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "project_plan_cancelation_list.php";
					}
				}
			}

			xmlhttp.open("POST", "ajax/project_plan_delete_cancelation.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("cancelation_id=" + cancelation_id + "&action=0");
		}
	}	
}
function go_to_project_edit_cancelation(cancelation_id,source)
{		
	var form = document.createElement("form");
    form.setAttribute("method", "GET");
    form.setAttribute("action", "project_plan_edit_cancelation.php");
	
	var hiddenField1 = document.createElement("input");
	hiddenField1.setAttribute("type","hidden");
	hiddenField1.setAttribute("name","cancelation_id");
	hiddenField1.setAttribute("value",cancelation_id);
	
	var hiddenField2 = document.createElement("input");
	hiddenField2.setAttribute("type","hidden");
	hiddenField2.setAttribute("name","source");
	hiddenField2.setAttribute("value",source);
	
	form.appendChild(hiddenField1);
	form.appendChild(hiddenField2);
	
	document.body.appendChild(form);
    form.submit();
}
</script>

  </body>

</html>