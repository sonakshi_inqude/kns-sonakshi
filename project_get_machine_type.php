<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: project_process_master_list.php
CREATED ON	: 0*-Nov-2016
CREATED BY	: Lakshmi
PURPOSE     : List of project for customer withdrawals
*/

/*
TBD: 
*/
$_SESSION['module'] = 'PM Masters';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	// Nothing
	
	
	// Machine Type List
	$project_machine_type_master_search_data = array("active"=>'1');
	$project_machine_type_master_list = i_get_project_machine_type_master($project_machine_type_master_search_data);
	if($project_machine_type_master_list["status"] == SUCCESS)
	{
		$project_machine_type_master_list_data = $project_machine_type_master_list["data"];
		for($machine_count = 0 ; $machine_count < count($project_machine_type_master_list_data); $machine_count++)
		{
			$result[$machine_count] = $project_machine_type_master_list_data[$machine_count]["project_machine_type_master_name"];
		}
	}
	else
	{
		//
	}

	
	echo json_encode($result);
}
else
{
	header("location:login.php");
}
?>