<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
FILE		: crm_approved_booking_list.php
CREATED ON	: 04-Nov-2015
CREATED BY	: Nitin Kashyap
PURPOSE     : Approved Booking List
*/
/* DEFINES - START */define('CRM_APPROVED_BOOKING_LIST_FUNC_ID','96');define('CRM_APPROVED_BOOKING_LIST_DETAILS_FUNC_ID','299');define('CRM_PAYMENT_DELAY_REASON_LIST','300');/* DEFINES - END */
/*
TBD: 
*/$_SESSION['module'] = 'CRM Projects';

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_sales_process.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];		// Get permission settings for this user for this page	$add_perms_list    = i_get_user_perms($user,'',CRM_APPROVED_BOOKING_LIST_FUNC_ID,'1','1');		$view_perms_list   = i_get_user_perms($user,'',CRM_APPROVED_BOOKING_LIST_FUNC_ID,'2','1');	$edit_perms_list   = i_get_user_perms($user,'',CRM_APPROVED_BOOKING_LIST_FUNC_ID,'3','1');	$delete_perms_list   = i_get_user_perms($user,'',CRM_APPROVED_BOOKING_LIST_FUNC_ID,'4','1');		$add_perms_details_list    = i_get_user_perms($user,'',CRM_APPROVED_BOOKING_LIST_DETAILS_FUNC_ID,'1','1');		$view_perms_details_list   = i_get_user_perms($user,'',CRM_APPROVED_BOOKING_LIST_DETAILS_FUNC_ID,'2','1');	$edit_perms_details_list   = i_get_user_perms($user,'',CRM_APPROVED_BOOKING_LIST_DETAILS_FUNC_ID,'3','1');	$delete_perms_details_list   = i_get_user_perms($user,'',CRM_APPROVED_BOOKING_LIST_DETAILS_FUNC_ID,'4','1');
	$add_perms_delay_reason_list    = i_get_user_perms($user,'',CRM_PAYMENT_DELAY_REASON_LIST,'1','1');		$view_perms_delay_reason_list   = i_get_user_perms($user,'',CRM_PAYMENT_DELAY_REASON_LIST,'2','1');	$edit_perms_delay_reason_list   = i_get_user_perms($user,'',CRM_PAYMENT_DELAY_REASON_LIST,'3','1');	$delete_perms_delay_reason_list   = i_get_user_perms($user,'',CRM_PAYMENT_DELAY_REASON_LIST,'4','1');

	// Query String Data
	// Nothing

	// Temp data
	$alert = "";
	
	if(isset($_POST["search_blocking_submit"]))
	{
		$project_id  = $_POST["ddl_project"];		
		if(($role == 1) || ($role == 5) || ($role == 10))
		{
			$added_by = $_POST["ddl_user"];		
		}
		else
		{
			$added_by = '';
		}
		$site_number  = $_POST["stxt_site_num"];
	}
	else
	{
		$project_id  = "-1";		
		$added_by    = "";
		$site_number = "";
	}

	$booking_list = i_get_site_booking('',$project_id,'','','','1',$added_by,'','','','','','','1','','',$site_number,'','','','site_no','',$user);
	if($booking_list["status"] == SUCCESS)
	{
		$booking_list_data = $booking_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$booking_list["data"];
	}
	
	// Get project list	$crm_user_project_mapping_search_data =  array("user_id"=>$user,"active"=>'1',"project_active"=>'1');	$project_list =  i_get_crm_user_project_mapping($crm_user_project_mapping_search_data);	if($project_list["status"] == SUCCESS)	{		$project_list_data = $project_list["data"];	}	else	{		$alert = $alert."Alert: ".$project_list["data"];		$alert_type = 0; // Failure	}
	
	// User List
	$user_list = i_get_user_list('','','','','1','1');
	if($user_list["status"] == SUCCESS)
	{
		$user_list_data = $user_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$user_list["data"];
		$alert_type = 0; // Failure
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Booking Approved Site List</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>
    

<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
       
          <div class="span6" style="width:100%;">
          
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Booking - Approved List</h3>
            </div>
			<div class="widget-header" style="height:80px; padding-top:10px;">               
			  <form method="post" id="pending_booked_list" action="crm_approved_booking_list.php">			  		  
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_project">
			  <option value="">- - Select Project - -</option>
			  <?php
				for($count = 0; $count < count($project_list_data); $count++)
				{
					?>
					<option value="<?php echo $project_list_data[$count]["project_id"]; ?>" <?php 
					if($project_id == $project_list_data[$count]["project_id"])
					{
					?>					
					selected="selected"
					<?php
					}?>><?php echo $project_list_data[$count]["project_name"]; ?></option>								
					<?php					
				}
      		  ?>
			  </select>
			  </span>		
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="text" name="stxt_site_num" value="<?php echo $site_number; ?>" placeholder="Site Number" />
			  </span>			  					
			  <?php if(($role == 1) || ($role == 5) || ($role == 10))
			  {
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <select name="ddl_user">
			  <option value="">- - Select STM - -</option>
			  <?php
				for($count = 0; $count < count($user_list_data); $count++)
				{
					?>
					<option value="<?php echo $user_list_data[$count]["user_id"]; ?>" <?php 
					if($added_by == $user_list_data[$count]["user_id"])
					{
					?>												
					selected="selected"
					<?php
					}?>><?php echo $user_list_data[$count]["user_name"]; ?></option>								
					<?php
				}
      		  ?>
			  </select>
			  </span>
			  <?php
			  }
			  ?>
			  <span style="padding-left:8px; padding-right:8px;">
			  <input type="submit" name="search_blocking_submit" />
			  </span>
			  </form>			  
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
			
              <table class="table table-bordered" style="table-layout: fixed;">
                <thead>
                  <tr>
					<th>SL No</th>
					<th>Enquiry No</th>
					<th>Project</th>
					<th>Site No.</th>
					<th>Area (sq. ft)</th>					
					<th>Booked By</th>
					<th>Booking Added On</th>
					<th>Client Name</th>
					<th>Amount</th>
					<th>Rate per sq. ft</th>
					<th>Remarks</th>
					<?php 
					if(($role == "1") || ($role == "10"))
					{?>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>						
					<?php
					}
					?>					
				</tr>
				</thead>
				<tbody>							
				<?php						
				if($booking_list["status"] == SUCCESS)
				{										
					$sl_no = 0;
					for($count = 0; $count < count($booking_list_data); $count++)
					{	
						$agreement_list = i_get_agreement_list('',$booking_list_data[$count]["crm_booking_id"],'','','1','','','','','','','');		
						if($agreement_list["status"] == SUCCESS)
						{
							$agreement_list_data = $agreement_list["data"];
						}
						else
						{
						$sl_no++;	
							
						$delay_reason_list = i_get_delay_reason_list($booking_list_data[$count]["crm_booking_id"],'','1');
						if($delay_reason_list['status'] == SUCCESS)
						{
							$delay_reason = $delay_reason_list['data'][0]['crm_delay_reason_master_name'];
						}
						else
						{
							$delay_reason = 'NO DELAY';
						}
					?>
					<tr>
					<td style="word-wrap:break-word;"><?php echo $sl_no; ?></td>
					<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["enquiry_number"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["project_name"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["crm_site_no"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["crm_booking_consideration_area"]; ?> <br /><span style="color:blue;">(Total Site Area: <?php echo $booking_list_data[$count]["crm_site_area"]; ?>)</span></td>					
					<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["user_name"]; ?></td>										
					<td style="word-wrap:break-word;"><?php if($booking_list_data[$count]["crm_booking_date"] != "0000-00-00")
					{
						echo date("d-M-Y",strtotime($booking_list_data[$count]["crm_booking_date"]));
					}
					else
					{
						echo "Booking Date Not Entered <br />Ap Date: ".date("d-M-Y",strtotime($booking_list_data[$count]["crm_booking_approved_on"]));
					}
					?></td>	
					<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["name"]; ?></td>		
					<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["crm_booking_initial_amount"]; ?></td>
					<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["crm_booking_rate_per_sq_ft"]; ?></td>					
					<td style="word-wrap:break-word;"><?php echo $booking_list_data[$count]["crm_booking_remarks"]; ?></td>
					
						<td style="word-wrap:break-word;"><?php if($view_perms_details_list['status'] == SUCCESS)			{			?><a href="crm_customer_transactions.php?booking=<?php echo $booking_list_data[$count]["crm_booking_id"]; ?>">Details</a><?php } ?></td>						
						<td style="word-wrap:break-word;"><?php echo $delay_reason; ?><br /><?php if($view_perms_delay_reason_list['status'] == SUCCESS)			{			?><a href="crm_add_payment_delay_reason.php?booking=<?php echo $booking_list_data[$count]["crm_booking_id"]; ?>&status=<?php echo BOOKED_STATUS; ?>">Update Delay Reason</a><?php } ?></td>
						<td style="word-wrap:break-word;"><?php if($edit_perms_delay_reason_list['status'] == SUCCESS)			{			?><a href="#" onclick="return untag_delay(<?php echo $booking_list_data[$count]["crm_booking_id"]; ?>);">Release from delay</a><?php } ?></td>
						
					</tr>
					<?php
						}
					}
				}
				else
				{
					if($role == "1")
					{
						$empty_colspan = 14;
					}
					else if($role == "10")
					{
						$empty_colspan = 13;
					}
					else if($role == "11")
					{
						$empty_colspan = 10;
					}
					?>
					<td colspan="<?php echo $empty_colspan; ?>">No site booked yet!</td>
					<?php
				}
				?>	

                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget --> 
         
          </div>
          <!-- /widget -->
        </div>
        <!-- /span6 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in/">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>
<script>
function untag_delay(booking_id)
{
	var ok = confirm("Are you sure you want to remove delay reason?")
	{         
		if (ok)
		{

			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}

			xmlhttp.onreadystatechange = function()
			{				
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
				{					
					if(xmlhttp.responseText != "SUCCESS")
					{
					 document.getElementById("span_msg").innerHTML = xmlhttp.responseText;
					 document.getElementById("span_msg").style.color = "red";
					}
					else					
					{
					 window.location = "crm_approved_booking_list.php";
					}
				}
			}

			xmlhttp.open("POST", "crm_untag_delay_reason.php");   // file name where delete code is written
			xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlhttp.send("booking=" + booking_id);
		}
	}	
}
</script><script>/* Open the sidenav */function openNav() {    document.getElementById("mySidenav").style.width = "75%";}/* Close/hide the sidenav */function closeNav() {    document.getElementById("mySidenav").style.width = "0";}</script>

  </body>

</html>