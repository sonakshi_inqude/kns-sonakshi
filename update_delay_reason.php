<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'task_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Query String Data
	if(isset($_GET['task']))
	{
		$task = $_GET['task'];
	}	
	else
	{
		$task = "";
	}
	
	if(isset($_GET['process']))
	{
		$process = $_GET['process'];
	}

	if(isset($_GET['bprocess']))
	{
		$bprocess = $_GET['bprocess'];
	}	

	// Temp data
	$alert = "";

	if(isset($_POST["upload_delay_reason_submit"]))
	{
		if(isset($_POST["cb_sub_task"]))
		{
			$sub_task = '1';
		}
		else
		{
			$sub_task = '0';
		}
		// Capture all form data	
		$planned_end_date      = date("Y-m-d",strtotime($_POST["planned_end_date"]));
		$start_date            = date("Y-m-d",strtotime($_POST["start_date"]));
		$actual_end_date       = $_POST["actual_end_date"];
		$document              = $_POST["document"];	// Hidden field
		$task                  = $_POST["task_plan"];	// Hidden field
		$applicable            = $_POST["applicable"];	// Hidden field
		$approved_on           = $_POST["approved_on"];	// Hidden field
		
		$reason                = $_POST["delay_reason"];
		$reason_remarks        = $_POST["txt_remarks"];
		$process               = $_POST["process"];
		$bprocess              = $_POST["bprocess"];
		
		if($reason != "")
		{	
			$task_plan_update_result = i_update_task_plan($task,$planned_end_date,$start_date,$actual_end_date,$reason,$reason_remarks,$document,$approved_on);
			
			if($task_plan_update_result["status"] == SUCCESS)
			{
				$disp_msg = $task_plan_update_result["data"];
				$disp_class = "";
				
				$delay_reason = i_add_delay_reason($task,$reason,$reason_remarks,$user,$sub_task);
				
				if($delay_reason["status"] != SUCCESS)
				{
					$disp_msg = $delay_reason["data"];
				}	
				else
				{
					if($process != '')
					{
						header("location:pending_task_list.php?process=$process");
					}
					else if($bprocess != '')
					{
						header("location:pending_task_list.php?bprocess=$bprocess");
					}
					else
					{
						header("location:pending_task_list.php?process=$process");
					}
				}
			}
			else
			{
				$disp_msg = $task_plan_update_result["data"];
				$disp_class = "";
			}
		}
		else	
		{
			$disp_msg = "Please fill all the mandatory fields. Mandatory fields are marked with *";
			$disp_class = "";
		}
	}

	// Get task details
	$task_data = i_get_task_plan_list($task,'','','','','','','','',''); // Get task plan for this task plan ID
	if($task_data["status"] == SUCCESS)
	{
		$task_data_details = $task_data["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$task_data["data"];
	}

	// Get list of reasons
	$reason_list = i_get_reason_list('','1','','','');
	if($reason_list["status"] == SUCCESS)
	{
		$reason_list_data = $reason_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$reason_list["data"];
	}
}
else
{
	header("location:login.php");
}

// Functions
function upload($file_id,$user_id) 
{
	if($_FILES[$file_id]["name"]!="")
	{
		if ($_FILES[$file_id]["error"] > 0)
		{
			// echo "Return Code: " . $_FILES[$file_id]["error"] . "<br />";
		}
		else
		{
			$_FILES[$file_id]["name"]=$user_id."_".$_FILES[$file_id]["name"];
			move_uploaded_file($_FILES[$file_id]["tmp_name"], "documents/".$_FILES[$file_id]["name"]);							  
		}
	}
	
	return $_FILES[$file_id]["name"];
}
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Update Delay Reason</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>    

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Your Account</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-content">
						
						
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Update Task Delay Reason</a>
						  </li>						  
						</ul>
						
						<br>
						
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="update_reason_form" class="form-horizontal" method="post" action="update_delay_reason.php" enctype="multipart/form-data">
									<fieldset>
										<input type="hidden" name="process" value="<?php echo $process; ?>" />
										<input type="hidden" name="bprocess" value="<?php echo $bprocess; ?>" />
										<input type="hidden" name="task_plan" value="<?php echo $task_data_details[0]["task_plan_legal_id"]; ?>" />
										<input type="hidden" name="document" value="<?php echo $task_data_details[0]["task_plan_document_path"]; ?>" />
										<input type="hidden" name="applicable" value="<?php echo $task_data_details[0]["task_plan_applicable"]; ?>" />
										<input type="hidden" name="approved_on" value="<?php echo $task_data_details[0]["task_plan_approved_on"]; ?>" />
										<input type="hidden" name="planned_end_date" value="<?php echo $task_data_details[0]["task_plan_planned_end_date"]; ?>" />
										<input type="hidden" name="start_date" value="<?php echo $task_data_details[0]["task_plan_actual_start_date"]; ?>" />
										<input type="hidden" name="actual_end_date" value="<?php echo $task_data_details[0]["task_plan_actual_end_date"]; ?>" />
										
										<div class="control-group">											
											<label class="control-label" for="delay_reason">Reason for delay*</label>
											<div class="controls">
												<select name="delay_reason" required="required">
												<?php 
												for($reason_count = 0; $reason_count < count($reason_list_data); $reason_count++)
												{
												?>
												<option value="<?php echo $reason_list_data[$reason_count]["reason_master_id"]; ?>" <?php if($task_data_details[0]["task_plan_delay_reason"] == $reason_list_data[$reason_count]["reason_master_id"])
												{?>
												selected="selected"
												<?php
												}
												?>><?php echo $reason_list_data[$reason_count]["reason"]; ?></option>
												<?php
												}
												?>
												</select>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="txt_remarks">Remarks*</label>
											<div class="controls">
												<textarea name="txt_remarks" required></textarea>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
										
										<div class="control-group">											
											<label class="control-label" for="cb_sub_task">Is Task</label>
											<div class="controls">
												<input type="checkbox" name="cb_sub_task">
											</div> <!-- /controls -->					
										</div> <!-- /control-group -->
										<br />		
										
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<button type="submit" class="btn btn-primary" name="upload_delay_reason_submit">Submit</button> 
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    <div class="span3">
                        <h4>
                            About Free Admin Template</h4>
                        <ul>
                            <li><a href="javascript:;">EGrappler.com</a></li>
                            <li><a href="javascript:;">Web Development Resources</a></li>
                            <li><a href="javascript:;">Responsive HTML5 Portfolio Templates</a></li>
                            <li><a href="javascript:;">Free Resources and Scripts</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            Support</h4>
                        <ul>
                            <li><a href="javascript:;">Frequently Asked Questions</a></li>
                            <li><a href="javascript:;">Ask a Question</a></li>
                            <li><a href="javascript:;">Video Tutorial</a></li>
                            <li><a href="javascript:;">Feedback</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            Something Legal</h4>
                        <ul>
                            <li><a href="javascript:;">Read License</a></li>
                            <li><a href="javascript:;">Terms of Use</a></li>
                            <li><a href="javascript:;">Privacy Policy</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            Open Source jQuery Plugins</h4>
                        <ul>
                            <li><a href="http://www.egrappler.com">Open Source jQuery Plugins</a></li>
                            <li><a href="http://www.egrappler.com;">HTML5 Responsive Tempaltes</a></li>
                            <li><a href="http://www.egrappler.com;">Free Contact Form Plugin</a></li>
                            <li><a href="http://www.egrappler.com;">Flat UI PSD</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2013 <a href="http://www.egrappler.com/">Bootstrap Responsive Admin Template</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
