<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD:
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Update attendance details
	$remarks_id = $_GET["remarks"];
	$status     = $_GET["status"];
		
	$task_remarks_uresult = i_update_gen_task_remarks($remarks_id,$status);
	
	if($task_remarks_uresult["status"] == FAILURE)
	{
		echo $task_remarks_uresult["data"];
	}
	else
	{
		echo "SUCCESS";
	}
}
else
{
	header("location:login.php");
}
?>