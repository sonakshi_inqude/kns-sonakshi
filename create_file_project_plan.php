<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

/*
TBD: 
1. User drop down from user table
2. Session management
3. Edit form-actions css to have no background
*/

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'post_legal_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_projects'.DIRECTORY_SEPARATOR.'crm_project_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'post_legal_functions.php');

if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))
{
	// Session Data
	$user 		   = $_SESSION["loggedin_user"];
	$role 		   = $_SESSION["loggedin_role"];
	$loggedin_name = $_SESSION["loggedin_user_name"];

	// Initialization
	$alert = "";
	$alert_type = -1; // No alert

	// Query String
	if(isset($_GET["file"]))
	{
		$file_id = $_GET["file"];
	}	
	
	if(isset($_POST["bd_file_search_submit"]))
	{		
		$project_id = $_POST["ddl_project"];
	}
	else
	{
		$project_id = '';
	}
	
	if(isset($_POST["create_file_project_plan_submit"]))
	{
		// Capture all form data	
		$authority  = $_POST["authority"];
		$files      = $_POST["cb_files"];

		if($authority != "")
		{		
			$files      = $_POST['cb_files'];
			$file_approved_status = 1;
			for($count = 0; $count < count($files); $count++)
			{
				$site_approval_result = i_add_site_approval_list($files[$count],$authority,$user);
				if($site_approval_result["status"] != SUCCESS)
				{
					$alert      = $site_approval_result["data"];
					$alert_type = 0; // Failure
					
					$file_approved_status = $file_approved_status & 0;
				}					
			}		

			if($file_approved_status == true)
			{
				header('location:assigned_files_list.php');
			}
			else
			{
				$alert_type = 0;
				$alert      = 'All files were not successfully sent for approval';
			}
		}
		else	
		{
			$alert = "Please fill all the mandatory fields. Mandatory fields are marked with *";
			$alert_type = 0; // Failure
		}
	}
	
	// Get the list of project file mapping
	$project_file_list = i_get_site_project_list('',$project_id,'','1','','','','','');
	if($project_file_list["status"] == SUCCESS)
	{
		$project_file_list_data = $project_file_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_file_list["data"];
	}
	
	// Get project list
	$project_list = i_get_project_list('','');
	if($project_list["status"] == SUCCESS)
	{
		$project_list_data = $project_list["data"];
	}
	else
	{
		$alert = $alert."Alert: ".$project_list["data"];
		$alert_type = -1;
	}
}
else
{
	header("location:login.php");
}	
?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <title>Create File Project Plan</title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    
    <link href="css/style.css" rel="stylesheet">
   


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

<body>

<?php
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'menu_functions.php');
?>

<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	
	      <div class="row">
	      	
	      	<div class="span12">      		
	      		
	      		<div class="widget ">
	      			
	      			<div class="widget-header">
	      				<i class="icon-user"></i>
	      				<h3>Send for Approval</h3>
	  				</div> <!-- /widget-header -->
					
					<div class="widget-header" style="height:50px; padding-top:10px;">               
					  <form method="post" id="file_search_form" action="create_file_project_plan.php">
					  <span style="padding-left:20px; padding-right:20px;">
					  <select name="ddl_project">
					  <option value="">- - Select Project - -</option>
					  <?php for($count = 0; $count < count($project_list_data); $count++)
					  {?>
					  <option value="<?php echo $project_list_data[$count]["project_id"]; ?>"<?php if($project_list_data[$count]["project_id"] == $project_id){ ?> selected="selected" <?php } ?>><?php echo $project_list_data[$count]["project_name"]; ?></option>
					  <?php
					  }?>
					  </select>
					  </span>
					  <input type="submit" name="bd_file_search_submit" />
					  </form>			  
					</div>
					
					<div class="widget-content">
						
						<div class="tabbable">
						<ul class="nav nav-tabs">
						  <li>
						    <a href="#formcontrols" data-toggle="tab">Create File Project Plan</a>
						  </li>						  
						</ul>
						
						<br>
						    <div class="control-group">												
								<div class="controls">
								<?php 
								if($alert_type == 0) // Failure
								{
								?>
									<div class="alert">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>  
								<?php
								}
								?>
                                
								<?php 
								if($alert_type == 1) // Success
								{
								?>								
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong><?php echo $alert; ?></strong>
                                    </div>
								<?php
								}
								?>
								</div> <!-- /controls -->	                                                
							</div> <!-- /control-group -->
							<div class="tab-content">
								<div class="tab-pane active" id="formcontrols">
								<form id="create_file_project_plan" class="form-horizontal" method="post" action="create_file_project_plan.php">
								<fieldset>
										<div class="control-group">											
											<label class="control-label" for="start_date">Authority</label>
											<div class="controls">
												<input type="text" name="authority" placeholder="Assign authority">
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
								
										<label class="control-label" for="cb_files">Choose Files</label>
										<div class="control-group">											
											<div class="controls">
												<table class="table table-bordered" style="table-layout: auto;">
												<tr>
												<td>SL No</td>												
												<td>Project Name</td>
												<td>Survey Number</td>												
												<td>&nbsp;</td>
												</tr>
												<?php 
												$sl_no = 0;
												if($project_file_list["status"] == SUCCESS)
												{
													for($count = 0; $count < count($project_file_list_data); $count++)
													{
														$file_duplicate = true;
														// Check if the files is already sent for approval
														$approval_sresult = i_get_site_approval_list($project_file_list_data[$count]['site_project_file_id'],'','','','','');
														if($approval_sresult['status'] == SUCCESS)
														{
															if($approval_sresult['data'][0]['site_approval_status'] == '2')
															{
																$file_duplicate = false;
															}
														}
														else
														{
															$file_duplicate = false;
														}
														
														if($file_duplicate == false)
														{
															$sl_no = $sl_no + 1;
															?>			
															<tr>
															<td><?php echo $sl_no; ?></td>															
															<td style="word-wrap:break-word;"><?php echo $project_file_list_data[$count]["project_name"]; ?></td><td style="word-wrap:break-word;"><?php echo $project_file_list_data[$count]["file_survey_number"]; ?></td>															
															<td><input type="checkbox" name="cb_files[]" value="<?php echo $project_file_list_data[$count]['site_project_file_id']; ?>" /></td>
															</tr>														
															<?php
														}
													}
												}
												else
												{?>
													<tr>
													<td colspan="7">No files Added</td>
													</tr>
												<?php
												}
												?>	
												</table>
											</div> <!-- /controls -->				
										</div> <!-- /control-group -->
                                                                                                                                                               										 <br />
										
											
										<div class="form-actions">
											<button type="submit" class="btn btn-primary" name="create_file_project_plan_submit">Submit</button> 
											<button type="reset" class="btn">Cancel</button>
										</div> <!-- /form-actions -->
									</fieldset>
								</form>
								</div>																
								
							</div>
						  
						  
						</div>
						
						
						
						
						
					</div> <!-- /widget-content -->
						
				</div> <!-- /widget -->
	      		
		    </div> <!-- /span8 -->
	      	
	      	
	      	
	      	
	      </div> <!-- /row -->
	
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->
    
    
    
 
<div class="extra">

	<div class="extra-inner">

		<div class="container">

			<div class="row">
                    
                </div> <!-- /row -->

		</div> <!-- /container -->

	</div> <!-- /extra-inner -->

</div> <!-- /extra -->


    
    
<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2015 <a href="http://www.knsgroup.in">KNS</a>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->
    


<script src="js/jquery-1.7.2.min.js"></script>
	
<script src="js/bootstrap.js"></script>
<script src="js/base.js"></script>


  </body>

</html>
