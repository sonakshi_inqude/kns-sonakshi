<?php

/* SESSION INITIATE - START */

session_start();

/* SESSION INITIATE - END */



/*

FILE		: Enquiry List Report

CREATED ON	: 03-May-2016

CREATED BY	: Nitin Kashyap

PURPOSE     : Enquiry Report

*/



/*

TBD: 

*/



// Includes

$base = $_SERVER["DOCUMENT_ROOT"];

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'general_config.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'general_task_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_masters'.DIRECTORY_SEPARATOR.'crm_masters_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_transaction_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'crm_transactions'.DIRECTORY_SEPARATOR.'crm_post_sales_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');

include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'PHPExcel-1.8'.DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php');



if((isset($_SESSION["loggedin_user"])) && ($_SESSION["loggedin_user"] != ""))

{

	// Session Data

	$user 		   = $_SESSION["loggedin_user"];

	$role 		   = $_SESSION["loggedin_role"];

	$loggedin_name = $_SESSION["loggedin_user_name"];



	/* Query String / Get form Data	*/
	
	if(isset($_REQUEST['search_department']))
	{
		$search_department = $_REQUEST['search_department'];

		
	}
	else
	{
		$search_department = "";
		
	}
	
	if(isset($_REQUEST['search_project']))
	{
		$search_project = $_REQUEST['search_project'];

		
	}
	else
	{
		$search_project = "";
		
	}
	
	if(isset($_REQUEST['search_task_type']))
	{
		$search_task_type = $_REQUEST['search_task_type'];

		
	}
	else
	{
		$search_task_type = "";
		
	}

	if(isset($_REQUEST['search_status']))
	{
		$search_status = $_REQUEST['search_status'];

		
	}
	else
	{
		$search_status = "";
		
	}

	if(isset($_REQUEST['task_user']))
	{
		$task_user = $_REQUEST['task_user'];

		
	}
	else
	{
		$task_user = "";
		
	}																		

	$general_task_plan_list = i_get_gen_task_plan_list('',$search_task_type,'',$search_department,'','','','',$search_status,$task_user,'',$search_project);
	

	/* Create excel sheet and write the column headers - START */

	// Instantiate a new PHPExcel object

	$objPHPExcel = new PHPExcel(); 

	// Set the active Excel worksheet to sheet 0

	$objPHPExcel->setActiveSheetIndex(0); 

	// Initialise the Excel row number

	$row_count = 1; 

	// Excel column identifier array

	$column_array = array('A','B','C','D','E','F','G','H','I','J','K','L');

	// Set the serial no

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, "SL NO"); 

	// Set the enquiry no

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, "TASK DETAILS"); 

	// Set the project

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, "DEPARTMENT"); 

	// Set the customer name

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, "PROJECT"); 

	// Set the customer mobile

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, "START DATE"); 

	// Set the customer email

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, "END DATE"); 

	// Set the company

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, "ASSIGNED BY"); 

	// Set the enquiry source

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, "ASSIGNED TO"); 

	// Set the enquiry walk in

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, "ASSIGNED DATE"); 

	// Set the latest status

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, "PLAN END DATE");

	// Set the sold status

	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, "AGING");
	
	$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, "REMARKS");

	
	$style_array = array('font' => array('bold' => true));

	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].$row_count.':'.$column_array[11].$row_count)->applyFromArray($style_array);

	// Increment the Excel row counter

	$row_count++; 	 

	/* Create excel sheet and write the column headers - END */

	

	if($general_task_plan_list["status"] == SUCCESS)

	{

		$general_task_plan_list_data = $general_task_plan_list["data"];

		$sl_no = 0;

		for($g_task_count = 0; $g_task_count < count($general_task_plan_list_data); $g_task_count++)

		{	
			if(($general_task_plan_list_data[$g_task_count]["general_task_end_date"] != "0000-00-00") && ($general_task_plan_list_data[$g_task_count]["general_task_end_date"] != "") && ($general_task_plan_list_data[$g_task_count]["general_task_end_date"] != "1969-12-31") && ($general_task_plan_list_data[$g_task_count]["general_task_planned_date"] != "1970-01-01") && ($search_status != '3'))

			{

				// Do nothing

			}					

			else

			{

				if(get_formatted_date($general_task_plan_list_data[$g_task_count]["general_task_end_date"],"Y-m-d") == "0000-00-00")

				{

					$end_date = date("Y-m-d");

				}

				else

				{

					$end_date = $general_task_plan_list_data[$g_task_count]["general_task_end_date"];

				}

				$start_date = $general_task_plan_list_data[$g_task_count]["general_task_planned_date"];

				

				if($general_task_plan_list_data[$g_task_count]["general_task_end_date"] == "0000-00-00")

				{

					$diff_end_date = date("Y-m-d");

				}

				else

				{

					$diff_end_date = $general_task_plan_list_data[$g_task_count]["general_task_end_date"];

				}

				

				$diff_data = get_date_diff($general_task_plan_list_data[$g_task_count]["general_task_added_on"],$diff_end_date);

				if($diff_data["status"] != "2")
				{
					$ageing = $diff_data["data"] + 1 ;
				}
				else
				{
					$ageing = $diff_data["data"];
				}
				
				$all_remarks = "";
				$remarks_details = i_get_remarks($general_task_plan_list_data[$g_task_count]["general_task_id"]);
				if($remarks_details["status"] == SUCCESS)
				{
					for($remarks_count = 0; $remarks_count < count($remarks_details["data"]); $remarks_count++)
					{
						$all_remarks = $remarks_details["data"][$remarks_count]["general_task_remarks"].'-'.date("d-M-Y H:i",strtotime($remarks_details["data"][$remarks_count]["general_task_remarks_added_on"])) .'/'.  $all_remarks ; 
					}
				}
				else
				{
					$all_remarks = "";
				}

				$css_class = "#000000";

				$sl_no++;

				$row_count++;

				// Set the first column

				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[0].$row_count, $sl_no); 

				// Set the second column - Enquiry No

				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[1].$row_count, $general_task_plan_list_data[$g_task_count]["general_task_details"]); 

				// Set the third column - Project Name

				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[2].$row_count, $general_task_plan_list_data[$g_task_count]["general_task_department_name"]); 

				// Set the fourth column - Name

				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[3].$row_count, $general_task_plan_list_data[$g_task_count]["project_master_name"]); 

				// Set the fifth column - Mobile

				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[4].$row_count, $general_task_plan_list_data[$g_task_count]["general_task_start_date"]); 

				// Set the sixth column - Email

				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[5].$row_count, $general_task_plan_list_data[$g_task_count]["general_task_end_date"]); 

				// Set the seventh column - Company

				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[6].$row_count, $general_task_plan_list_data[$g_task_count]["assigner"]); 

				// Set the eight column - Source

				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[7].$row_count, $general_task_plan_list_data[$g_task_count]["assignee"]); 

				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[8].$row_count, $general_task_plan_list_data[$g_task_count]["general_task_added_on"]); 

				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[9].$row_count, $general_task_plan_list_data[$g_task_count]["general_task_planned_date"]); 

				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[10].$row_count, $ageing); 
				
				$objPHPExcel->getActiveSheet()->SetCellValue($column_array[11].$row_count, $all_remarks); 

			}

		}
	}
	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'0'.':'.$column_array[11].$row_count)->getAlignment()->setWrapText(false); 	

	

	$objPHPExcel->getActiveSheet()->mergeCells($column_array[0].$row_count.':'.$column_array[11].$row_count);

	$row_count++;

	

	$objPHPExcel->getActiveSheet()->getStyle($column_array[0].'1'.':'.$column_array[11].($row_count - 1))->getAlignment()->setWrapText(false);
	header('Content-Type: application/vnd.ms-excel'); 

	header('Content-Disposition: attachment;filename="General_task_summary_report_'.date("d-M-Y").'.xls"'); 

	header('Cache-Control: max-age=0'); 

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 

	$objWriter->save('php://output');

}
else
{

	header("location:login.php");

}	

?>