<?php
/* SESSION INITIATE - START */
session_start();
/* SESSION INITIATE - END */

// Includes
$base = $_SERVER["DOCUMENT_ROOT"];	
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_master_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'projectmgmnt'.DIRECTORY_SEPARATOR.'project_management_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'users'.DIRECTORY_SEPARATOR.'user_functions.php');
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'pdf_operations.php');	
include_once($base.DIRECTORY_SEPARATOR.'kns'.DIRECTORY_SEPARATOR.'Legal'.DIRECTORY_SEPARATOR.'utilities'.DIRECTORY_SEPARATOR.'utilities_functions.php');	

// Session Data
$user 		   = $_SESSION["loggedin_user"];
$role 		   = $_SESSION["loggedin_role"];
$loggedin_name = $_SESSION["loggedin_user_name"];

// Temp Data
$alert_type = -1;
$alert = "";

$boq_id = $_REQUEST["boq_id"];
// Temp data
$project_task_actual_boq_search_data = array("boq_id"=>$boq_id);
$contract_work_list = i_get_project_task_boq_actual($project_task_actual_boq_search_data);

if($contract_work_list["status"] == SUCCESS)
{
	$contract_work_list_data = $contract_work_list["data"];
	$date = $contract_work_list_data[0]["project_task_actual_boq_date"];
	$number = $contract_work_list_data[0]["project_task_boq_actual_number"];
	$uom = $contract_work_list_data[0]["stock_unit_name"];
	$length = $contract_work_list_data[0]["project_task_actual_boq_length"];
	$breadth = $contract_work_list_data[0]["project_task_actual_boq_breadth"];
	$depth = $contract_work_list_data[0]["project_task_actual_boq_depth"];
	$measurement = $contract_work_list_data[0]["project_task_actual_boq_total_measurement"];
	$project = $contract_work_list_data[0]["project_master_name"];
	$process = $contract_work_list_data[0]["project_process_master_name"];
	$task = $contract_work_list_data[0]["project_task_master_name"];
	$vendor = $contract_work_list_data[0]["project_manpower_agency_name"];
	$engineer = $contract_work_list_data[0]["checked_by"];
	$remarks = $contract_work_list_data[0]["project_task_actual_boq_remarks"];
	$contract_process = $contract_work_list_data[0]["project_contract_process_name"];
	$contract_work = $contract_work_list_data[0]["project_contract_rate_master_work_task"];
}
else
{
	$alert = $alert."Alert: ".$man_power_list["data"];
}

$po_print_format = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>
<table width="100%" border="1" cellpadding="5" style="border-collapse:collapse; border:1px solid #333;">
  <tr>
    <td colspan="4" rowspan="4" align="center">Logo</td>
    <td colspan="5" rowspan="3" align="center"><h2></h2></td>
    <td colspan="3" align="center">Project Name</td>
    <td width="1%">:</td>
    <td width="27%">'.$project.'</td>
  </tr>
  <tr>
    <td colspan="3" align="center">Date</td>
    <td>:</td>
    <td>'.$date.'</td>
  </tr>
  <tr>
    <td colspan="3" align="center">Contractor</td>
    <td>:</td>
    <td>'.$vendor.'</td>
  </tr>
   <tr>
    <td colspan="8" align="center">Measurement Sheet</td>
    <td>:</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="4%" align="center">sl.no</td>
    <td colspan="4" align="center">Description</td>
    <td width="5%" align="center">Units</td>
    <td width="7%" align="center">Nos</td>
    <td width="7%" align="center">Length</td>
    <td width="16%" align="center">Breadth</td>
    <td width="5%" align="center">Depth</td>
    <td width="7%" align="center">Quantity</td>
    <td colspan="3" align="center">Remarks</td>
  </tr>
  <tr>
    <td align="center">1</td>
    <td colspan="4" align="center">Process : '.$process .'<br> Task : '.$task.'<br> Contract Process : '.$contract_process.' <br> Contract Task '.$contract_work.'</td>
    <td align="center">'.$uom.'</td>
    <td align="center">'.$number.'</td>
    <td align="center">'.$length.'</td>
    <td align="center">'.$breadth.'</td>
    <td align="center">'.$depth.'</td>
    <td align="center">'.$measurement.'</td>
    <td colspan="3" align="center">'.$remarks.'</td>
  </tr>
  <tr style="border:none">
  <td>&nbsp;</td>
  </tr>
  <tr>
  <tr style="border:none">
  <td>&nbsp;</td>
  </tr>
  <tr>
  <td colspan="7" align="center">(prepared by)</td>
  <td colspan="7" align="center">(checked by)</td>
  </tr>
</table>
</body>
</html>';
$mpdf = new mPDF();
$mpdf->WriteHTML($po_print_format);
$mpdf->Output('invoice_'.$invoice_no.'.pdf','I');
